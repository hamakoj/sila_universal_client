image: adoptopenjdk/openjdk8:latest

variables:
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  MAVEN_CLI_OPTS: "--batch-mode --errors --show-version -DinstallAtEnd=true -DdeployAtEnd=false"
  GIT_SUBMODULE_STRATEGY: recursive

# Cache downloaded dependencies and plugins between builds.
cache:
  paths:
    - .m2/repository

stages:
  - manual
  - build_and_test
  - integration_test
  - deploy
  - docker-build
  - publish-npm
  - publish-maven

# Manual Job to setup manual execution of the CI
start:
  stage: manual
  when: manual
  allow_failure: false
  script: echo "Starting to validate and verify"
  except:
    - master

# Validate merge requests using JDK 8
# todo add possibility to optionally build lib & electron after build_and_test when not on master
build_and_test:
  image: maven:3.6.3-adoptopenjdk-8
  stage: build_and_test
  script:
    - |-
      if [[ $CI_COMMIT_BRANCH = "master" ]]; then
          dpkg --add-architecture i386
          apt-get update
          apt-get install --no-install-recommends --assume-yes wine wine32
          mvn -T 1C $MAVEN_CLI_OPTS install -f pom.xml "-Dskip.sila_java.tests=true" "-Dskip.front.electron.all.build=false"
      else
          mvn -T 1C $MAVEN_CLI_OPTS install -f pom.xml "-Dskip.sila_java.tests=true" "-Dskip.front.lib.build=true"
      fi
  artifacts:
    exclude:
      - front/node_modules/**/*
      - front/target/**/*
    paths:
      - back/target/back-*.jar
      - back/target/usc.exe
      - sila-test/target/sila-test-*-SNAPSHOT.jar
      - front/lib/
      - front/src/generated/
      - front/dist/*.AppImage
      - front/dist/*.dmg
      - front/dist/*.exe
    expire_in: 1 week

# Integration test
integration_test:
  image: cimg/openjdk:11.0.16-browsers
  stage: integration_test
  dependencies:
    - build_and_test
  script:
    - sudo apt-get update && sudo apt-get install -y libnss3
    - java -jar sila-test/target/sila-test-0.7.0-SNAPSHOT.jar > server.log &
    - sleep 20 # wait for server to start and to enable discovery
    - java -jar back/target/back-0.7.0-SNAPSHOT-app.jar > usc.log &
    - sleep 20 # wait usc to start
    - cd front
    - npm install
    - npm run test
  artifacts:
    when: on_failure
    paths:
      - ./*.log
      - cert.pem
      - private.pem
      - ./front/*.jpg
deploy:
  stage: deploy
  #when: manual
  only:
    - master
  before_script:
    ##
    ## Install ssh-agent if not already installed, it is required by Docker.
    ## (change apt-get to yum if you use an RPM-based image)
    ##
    - 'command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )'

    ##
    ## Run ssh-agent (inside the build environment)
    ##
    - eval $(ssh-agent -s)

    ##
    ## Add the base64 SSH key stored in SSH_PRIVATE_KEY variable to the agent store
    ## We're using tr to fix line endings which makes ed25519 keys work
    ## without extra base64 encoding.
    ## https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
    ##
    - echo $SSH_PRIVATE_KEY | base64 -d | tr -d '\r' | ssh-add -

    ##
    ## Create the SSH directory and give it the right permissions
    ##
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh

    ##
    ## Optionally, if you will be using any Git commands, set the user name and
    ## and email.
    ##
    # - git config --global user.email "user@example.com"
    # - git config --global user.name "User name"
    #- echo "SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    #- chmod 644 ~/.ssh/known_hosts
    - ssh-keyscan $HOSTNAME >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - scp back/target/back-0.7.0-SNAPSHOT-app.jar debian@$HOSTNAME:/home/debian
    - scp sila-test/target/sila-test-0.7.0-SNAPSHOT.jar debian@$HOSTNAME:/home/debian
    - ssh debian@$HOSTNAME "sudo mv ~/sila-test-0.7.0-SNAPSHOT.jar ~/sila-test/sila-server.jar && sudo rm ~/data/db.h2.mv.db && sudo rm -rf ~/plugins/ && ./restartServer.sh && ./restartTestServer.sh && ./restartUSC.sh && exit"

docker-build:
  stage: deploy
  only:
    - master
  image: docker:latest
  variables:
    SPRING_PROFILES_ACTIVE: gitlab-ci
    DOCKER_DRIVER: overlay
  services:
    - docker:dind
  script:
    - docker build -t registry.gitlab.com/sila2/universal-sila-client/sila_universal_client .
    - echo "$CI_BUILD_TOKEN" | docker login -u gitlab-ci-token --password-stdin registry.gitlab.com
    - docker push registry.gitlab.com/sila2/universal-sila-client/sila_universal_client

# Manual Job to setup manual execution of the CI
npm-deploy:
  stage: publish-npm
  only:
    - master
  when: manual
  image: node:17-alpine3.12
  allow_failure: false
  dependencies:
    - build_and_test
  script:
    - cd front/lib
    - echo "@sila2:registry=https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/" >> .npmrc
    - echo "//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}" >> .npmrc
    - npm publish

maven-deploy:
  stage: publish-maven
  only:
    - master
  when: manual
  image: maven:3.6.3-adoptopenjdk-8
  allow_failure: false
  dependencies:
    - build_and_test
  script:
    - mvn deploy:deploy-file --settings ./.ci/settings.xml -DgroupId="com.sila-standard.universal-client" -DartifactId="back" -Dversion="0.7.0-SNAPSHOT" -Dpackaging=jar -Dfile="back/target/back-0.7.0-SNAPSHOT-lib.jar" -DrepositoryId=gitlab-maven -Durl="https://gitlab.com/api/v4/projects/25605033/packages/maven"
