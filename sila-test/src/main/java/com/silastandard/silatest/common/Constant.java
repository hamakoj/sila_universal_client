package com.silastandard.silatest.common;

import io.grpc.Context;
import io.grpc.Metadata;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceOuterClass;
import sila2.org.silastandard.core.commands.loopbackservice.v1.LoopbackServiceOuterClass;
import sila2.org.silastandard.core.lockcontroller.v1.LockControllerOuterClass;

import static io.grpc.Metadata.BINARY_BYTE_MARSHALLER;

public class Constant {
    public static final Context.Key<LockControllerOuterClass.Metadata_LockIdentifier> LOCK_IDENTIFIER_CTX_KEY =
            Context.key("sila-org.silastandard-core-lockcontroller-v1-metadata-lockidentifier-bin");
    public static final Context.Key<AuthorizationServiceOuterClass.Metadata_AccessToken> ACCESS_TOKEN_CTX_KEY =
            Context.key("sila-org.silastandard-core-authorizationservice-v1-metadata-accesstoken-bin");
    public static final Context.Key<LoopbackServiceOuterClass.Metadata_LoopbackIdentifier> LOOPBACK_IDENTIFIER_CTX_KEY =
            Context.key("sila-org.silastandard-core.commands-loopbackservice-v1-metadata-loopbackidentifier-bin");
    public static final Metadata.Key<byte[]> LOCK_IDENTIFIER_METADATA_KEY =
            Metadata.Key.of("sila-org.silastandard-core-lockcontroller-v1-metadata-lockidentifier-bin", BINARY_BYTE_MARSHALLER);
    public static final Metadata.Key<byte[]> ACCESS_TOKEN_METADATA_KEY =
            Metadata.Key.of("sila-org.silastandard-core-authorizationservice-v1-metadata-accesstoken-bin", BINARY_BYTE_MARSHALLER);
    public static final Metadata.Key<byte[]> LOOPBACK_IDENTIFIER_METADATA_KEY =
            Metadata.Key.of("sila-org.silastandard-core.commands-loopbackservice-v1-metadata-loopbackidentifier-bin", BINARY_BYTE_MARSHALLER);

}