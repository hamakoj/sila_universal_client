package com.silastandard.silatest.server.features.core;

import com.google.common.collect.Lists;
import io.grpc.stub.StreamObserver;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.errorrecoveryservice.v1.ErrorRecoveryServiceGrpc;
import sila2.org.silastandard.core.errorrecoveryservice.v1.ErrorRecoveryServiceOuterClass;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAString;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Slf4j
public class ErrorRecoveryService extends ErrorRecoveryServiceGrpc.ErrorRecoveryServiceImplBase {
    private final Set<Runnable> recoverableErrorsListeners = new HashSet<>();
    private final List<ErrorRecoveryServiceOuterClass.DataType_ContinuationOption> options = Lists.newArrayList(
            ErrorRecoveryServiceOuterClass.DataType_ContinuationOption
                    .newBuilder()
                    .setContinuationOption(
                            ErrorRecoveryServiceOuterClass.DataType_ContinuationOption.ContinuationOption_Struct
                                    .newBuilder()
                                    .setDescription(SiLAString.from("Option 1 takes an integer"))
                                    .setIdentifier(SiLAString.from("Option 1"))
                                    .setRequiredInputData(SiLAString.from("<DataType><Basic>Integer</Basic></DataType>"))
                                    .build()
                    ).build(),
            ErrorRecoveryServiceOuterClass.DataType_ContinuationOption
                    .newBuilder()
                    .setContinuationOption(
                            ErrorRecoveryServiceOuterClass.DataType_ContinuationOption.ContinuationOption_Struct
                                    .newBuilder()
                                    .setDescription(SiLAString.from("Option 2 takes a string"))
                                    .setIdentifier(SiLAString.from("Option 2"))
                                    .setRequiredInputData(SiLAString.from("<DataType><Basic>String</Basic></DataType>"))
                                    .build()
                    ).build(),
            ErrorRecoveryServiceOuterClass.DataType_ContinuationOption
                    .newBuilder()
                    .setContinuationOption(
                            ErrorRecoveryServiceOuterClass.DataType_ContinuationOption.ContinuationOption_Struct
                                    .newBuilder()
                                    .setDescription(SiLAString.from("Option 3 takes a list of string"))
                                    .setIdentifier(SiLAString.from("Option 3"))
                                    .setRequiredInputData(SiLAString.from("<DataType><List><DataType><Basic>String</Basic></DataType></List></DataType>"))
                                    .build()
                    ).build()
    );
    private final  List<ErrorRecoveryServiceOuterClass.DataType_RecoverableError> errors = Lists.newArrayList(
            ErrorRecoveryServiceOuterClass.DataType_RecoverableError
                    .newBuilder()
                    .setRecoverableError(
                            ErrorRecoveryServiceOuterClass.DataType_RecoverableError.RecoverableError_Struct
                                    .newBuilder()
                                    .setAutomaticExecutionTimeout(SiLAInteger.from(10))
                                    .setCommandExecutionUUID(SiLAString.from(UUID.randomUUID().toString()))
                                    .setCommandIdentifier(SiLAString.from("org.silastandard/core.commands/ObservableService/v1/Command/CountTo"))
                                    .setErrorMessage(SiLAString.from("Error 1"))
                                    .setDefaultOption(SiLAString.from("Option 1"))
                                    .addContinuationOptions(this.options.get(0))
                                    .build()
                    ).build(),
            ErrorRecoveryServiceOuterClass.DataType_RecoverableError
                    .newBuilder()
                    .setRecoverableError(
                            ErrorRecoveryServiceOuterClass.DataType_RecoverableError.RecoverableError_Struct
                                    .newBuilder()
                                    .setAutomaticExecutionTimeout(SiLAInteger.from(10))
                                    .setCommandExecutionUUID(SiLAString.from(UUID.randomUUID().toString()))
                                    .setCommandIdentifier(SiLAString.from("org.silastandard/core.commands/ObservableService/v1/Command/CountTo"))
                                    .setErrorMessage(SiLAString.from("Error 2"))
                                    .setDefaultOption(SiLAString.from("Option 2"))
                                    .addContinuationOptions(this.options.get(1))
                                    .build()
                    ).build(),
            ErrorRecoveryServiceOuterClass.DataType_RecoverableError
                    .newBuilder()
                    .setRecoverableError(
                            ErrorRecoveryServiceOuterClass.DataType_RecoverableError.RecoverableError_Struct
                                    .newBuilder()
                                    .setAutomaticExecutionTimeout(SiLAInteger.from(10))
                                    .setCommandExecutionUUID(SiLAString.from(UUID.randomUUID().toString()))
                                    .setCommandIdentifier(SiLAString.from("org.silastandard/core.commands/ObservableService/v1/Command/CountTo"))
                                    .setErrorMessage(SiLAString.from("Error 3"))
                                    .setDefaultOption(SiLAString.from("Option 2"))
                                    .addContinuationOptions(this.options.get(0))
                                    .addContinuationOptions(this.options.get(1))
                                    .build()
                    ).build(),
            ErrorRecoveryServiceOuterClass.DataType_RecoverableError
                    .newBuilder()
                    .setRecoverableError(
                            ErrorRecoveryServiceOuterClass.DataType_RecoverableError.RecoverableError_Struct
                                    .newBuilder()
                                    .setAutomaticExecutionTimeout(SiLAInteger.from(10))
                                    .setCommandExecutionUUID(SiLAString.from(UUID.randomUUID().toString()))
                                    .setCommandIdentifier(SiLAString.from("org.silastandard/core.commands/ObservableService/v1/Command/CountTo"))
                                    .setErrorMessage(SiLAString.from("Error 4"))
                                    .setDefaultOption(SiLAString.from("Option 3"))
                                    .addContinuationOptions(this.options.get(2))
                                    .build()
                    ).build()
    );
    private long errorHandlingTimeout = 0;

    private void notifyListeners() {
        final Set<Runnable> toRemove = new HashSet<>();
        this.recoverableErrorsListeners.forEach(listener -> {
            try {
                listener.run();
            } catch (Exception e) {
                log.debug("Remvoe recoverable errors listener because of exception", e);
                toRemove.add(listener);
            }
        });
        this.recoverableErrorsListeners.removeAll(toRemove);
        log.info("Recoverable errors listeners count {}", this.recoverableErrorsListeners.size());
        log.info("Recoverable errors count {}", this.errors.size());
    }

    @SneakyThrows
    @Override
    public void executeContinuationOption(ErrorRecoveryServiceOuterClass.ExecuteContinuationOption_Parameters request, StreamObserver<ErrorRecoveryServiceOuterClass.ExecuteContinuationOption_Responses> responseObserver) {
        final String continuationOption = request.getContinuationOption().getValue();
        final String commandExecutionUUID = request.getCommandExecutionUUID().getValue();
        final SiLAFramework.Any inputData = request.getInputData();
        log.info("Continuation option for option {} and command execution UUID {} with input data {}", continuationOption, commandExecutionUUID, inputData);
        if (continuationOption.equals(this.options.get(0).getContinuationOption().getIdentifier().getValue())) {
            SiLAFramework.Integer integer = SiLAFramework.Integer.parseFrom(inputData.getPayload());
            log.info("Received integer input data for option 2: {}", integer.getValue());
        } else if (continuationOption.equals(this.options.get(1).getContinuationOption().getIdentifier().getValue())) {
            SiLAFramework.String string = SiLAFramework.String.parseFrom(inputData.getPayload());
            log.info("Received string input data for option 1: {}", string.getValue());
        } else if (continuationOption.equals(this.options.get(2).getContinuationOption().getIdentifier().getValue())) {
            // ignore type name, only used for parsing because it is a list of string
            final SiLAServiceOuterClass.Get_ImplementedFeatures_Responses listString = SiLAServiceOuterClass.Get_ImplementedFeatures_Responses.parseFrom(inputData.getPayload());
            log.info("Received list string input data for option 3: {}", listString.getImplementedFeaturesList());
        } else {
            log.info("Received unknown continuation option {}", continuationOption);
        }
        responseObserver.onNext(ErrorRecoveryServiceOuterClass.ExecuteContinuationOption_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void abortErrorHandling(ErrorRecoveryServiceOuterClass.AbortErrorHandling_Parameters request, StreamObserver<ErrorRecoveryServiceOuterClass.AbortErrorHandling_Responses> responseObserver) {
        final String commandExecutionUUID = request.getCommandExecutionUUID().getValue();
        log.info("Aborting error handling for command execution UUID {}", commandExecutionUUID);
        responseObserver.onNext(ErrorRecoveryServiceOuterClass.AbortErrorHandling_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void setErrorHandlingTimeout(ErrorRecoveryServiceOuterClass.SetErrorHandlingTimeout_Parameters request, StreamObserver<ErrorRecoveryServiceOuterClass.SetErrorHandlingTimeout_Responses> responseObserver) {
        final long timeout = request.getErrorHandlingTimeout().getValue();
        log.info("Setting error handling timeout to {} seconds", timeout);
        this.errorHandlingTimeout = timeout;
        responseObserver.onNext(ErrorRecoveryServiceOuterClass.SetErrorHandlingTimeout_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void subscribeRecoverableErrors(ErrorRecoveryServiceOuterClass.Subscribe_RecoverableErrors_Parameters request, StreamObserver<ErrorRecoveryServiceOuterClass.Subscribe_RecoverableErrors_Responses> responseObserver) {
        final Runnable callback = () -> {
            try {
                responseObserver.onNext(
                        ErrorRecoveryServiceOuterClass.Subscribe_RecoverableErrors_Responses
                                .newBuilder()
                                .addAllRecoverableErrors(this.errors)
                                .build()
                );
            } catch (Exception e) {
                responseObserver.onCompleted();
                throw e;
            }
        };
        callback.run(); // send initial state
        this.recoverableErrorsListeners.add(callback);
    }
}
