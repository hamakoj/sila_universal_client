package com.silastandard.silatest.server.features.core;


import com.silastandard.silatest.server.CoreFeatureValidators;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.lockcontroller.v1.LockControllerGrpc;
import sila2.org.silastandard.core.lockcontroller.v1.LockControllerOuterClass;
import sila_java.library.core.sila.types.SiLABoolean;
import sila_java.library.core.sila.types.SiLAString;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Slf4j
public class LockController extends LockControllerGrpc.LockControllerImplBase {
    public static Boolean locked = false;
    public static Optional<SiLAFramework.String> currentLock = Optional.empty();
    private final Set<Runnable> serverLockListeners = new HashSet<>();

    private void notifyListeners() {
        final Set<Runnable> toRemove = new HashSet<>();
        this.serverLockListeners.forEach(listener -> {
            try {
                listener.run();
            } catch (Exception e) {
                log.debug("Remove server lock listener because of exception", e);
                toRemove.add(listener);
            }
        });
        this.serverLockListeners.removeAll(toRemove);
        log.info("Server lock listeners count {}", this.serverLockListeners.size());
    }

    @Override
    public void lockServer(
            LockControllerOuterClass.LockServer_Parameters request,
            StreamObserver<LockControllerOuterClass.LockServer_Responses> responseObserver
    ) {
        final long timeoutSeconds = request.getTimeout().getValue(); // todo expire lock after timeout
        //final AuthorizationServiceOuterClass.Metadata_AccessToken accessToken = Constant.ACCESS_TOKEN_CTX_KEY.get();
        //log.info("lockServer: Received Access Token [{}]", accessToken);
        final String lockIdentifier = request.getLockIdentifier().getValue();
        log.info("lockServer: Received Lock Identifier [{}]", lockIdentifier);
        //CoreFeatureValidators.AccessTokenValidator.checkAccessToken(accessToken.getAccessToken().getValue());
        //CoreFeatureValidators.LockValidator.checkServerUnlocked();
        LockController.currentLock = Optional.of(SiLAString.from(lockIdentifier));
        responseObserver.onNext(LockControllerOuterClass.LockServer_Responses.newBuilder().build());
        LockController.locked = true;
        notifyListeners();
        responseObserver.onCompleted();
    }

    @Override
    public void unlockServer(
            LockControllerOuterClass.UnlockServer_Parameters request,
            StreamObserver<LockControllerOuterClass.UnlockServer_Responses> responseObserver
    ) {
        //final AuthorizationServiceOuterClass.Metadata_AccessToken accessToken = Constant.ACCESS_TOKEN_CTX_KEY.get();
        //log.info("unlockServer: Received Access Token from Metadata [{}]", accessToken);
        final SiLAFramework.String lockIdentifier = request.getLockIdentifier();
        log.info("unlockServer: Received Lock Identifier from Parameters [{}]", lockIdentifier);
        //CoreFeatureValidators.AccessTokenValidator.checkAccessToken(accessToken.getAccessToken().getValue());
        CoreFeatureValidators.LockValidator.checkServerLocked();
        CoreFeatureValidators.LockValidator.checkLockIdentifier(lockIdentifier);
        responseObserver.onNext(LockControllerOuterClass.UnlockServer_Responses.newBuilder().build());
        LockController.locked = false;
        LockController.currentLock = Optional.empty();
        notifyListeners();
        responseObserver.onCompleted();
    }

    @Override
    public void subscribeIsLocked(LockControllerOuterClass.Subscribe_IsLocked_Parameters request, StreamObserver<LockControllerOuterClass.Subscribe_IsLocked_Responses> responseObserver) {
        //final AuthorizationServiceOuterClass.Metadata_AccessToken accessToken = Constant.ACCESS_TOKEN_CTX_KEY.get();
        //log.info("isLocked: Received Access Token from Metadata [{}]", accessToken);
        //CoreFeatureValidators.AccessTokenValidator.checkAccessToken(accessToken.getAccessToken().getValue());
        final Runnable callback = () -> {
            try {
                responseObserver.onNext(LockControllerOuterClass.Subscribe_IsLocked_Responses.newBuilder()
                        .setIsLocked(SiLABoolean.from(LockController.locked))
                        .build()
                );
            } catch (Exception e) {
                responseObserver.onCompleted();
                throw e;
            }
        };
        callback.run(); // send initial state
        this.serverLockListeners.add(callback);
    }

    @Override
    public void getFCPAffectedByMetadataLockIdentifier(
            LockControllerOuterClass.Get_FCPAffectedByMetadata_LockIdentifier_Parameters request,
            StreamObserver<LockControllerOuterClass.Get_FCPAffectedByMetadata_LockIdentifier_Responses> responseObserver
    ) {
        responseObserver.onNext(
                LockControllerOuterClass.Get_FCPAffectedByMetadata_LockIdentifier_Responses
                        .newBuilder()
                        .addAffectedCalls(SiLAString.from("org.silastandard/core.commands/ImageService/v1/Command/GetImage"))
                        .addAffectedCalls(SiLAString.from("org.silastandard/core/AuthenticationService/v1/Command/Logout"))
                        .addAffectedCalls(SiLAString.from("org.silastandard/core.commands/ObservableService/v1/Command/CountTo"))
                        .build()
        );
        responseObserver.onCompleted();
    }
}
