package com.silastandard.silatest.server;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.commands.constraintservice.v1.ConstraintServiceGrpc;
import sila2.org.silastandard.core.commands.constraintservice.v1.ConstraintServiceOuterClass;

@Slf4j
public class ConstraintService extends ConstraintServiceGrpc.ConstraintServiceImplBase {
    @Override
    public void fQCPIC(ConstraintServiceOuterClass.FQCPIC_Parameters request, StreamObserver<ConstraintServiceOuterClass.FQCPIC_Responses> responseObserver) {
        responseObserver.onNext(ConstraintServiceOuterClass.FQCPIC_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void anyConstraint(ConstraintServiceOuterClass.AnyConstraint_Parameters request, StreamObserver<ConstraintServiceOuterClass.AnyConstraint_Responses> responseObserver) {
        responseObserver.onNext(ConstraintServiceOuterClass.AnyConstraint_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void jsonSchemaCommand(ConstraintServiceOuterClass.JsonSchemaCommand_Parameters request, StreamObserver<ConstraintServiceOuterClass.JsonSchemaCommand_Responses> responseObserver) {
        responseObserver.onNext(ConstraintServiceOuterClass.JsonSchemaCommand_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void xmlSchemaCommand(ConstraintServiceOuterClass.XmlSchemaCommand_Parameters request, StreamObserver<ConstraintServiceOuterClass.XmlSchemaCommand_Responses> responseObserver) {
        responseObserver.onNext(ConstraintServiceOuterClass.XmlSchemaCommand_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void timeConstraintCommand(ConstraintServiceOuterClass.TimeConstraintCommand_Parameters request, StreamObserver<ConstraintServiceOuterClass.TimeConstraintCommand_Responses> responseObserver) {
        responseObserver.onNext(ConstraintServiceOuterClass.TimeConstraintCommand_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void dateConstraintCommand(ConstraintServiceOuterClass.DateConstraintCommand_Parameters request, StreamObserver<ConstraintServiceOuterClass.DateConstraintCommand_Responses> responseObserver) {
        responseObserver.onNext(ConstraintServiceOuterClass.DateConstraintCommand_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void timestampConstraintCommand(ConstraintServiceOuterClass.TimestampConstraintCommand_Parameters request, StreamObserver<ConstraintServiceOuterClass.TimestampConstraintCommand_Responses> responseObserver) {
        responseObserver.onNext(ConstraintServiceOuterClass.TimestampConstraintCommand_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void contentTypeCommand(ConstraintServiceOuterClass.ContentTypeCommand_Parameters request, StreamObserver<ConstraintServiceOuterClass.ContentTypeCommand_Responses> responseObserver) {
        responseObserver.onNext(ConstraintServiceOuterClass.ContentTypeCommand_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void unitLabelCommand(ConstraintServiceOuterClass.UnitLabelCommand_Parameters request, StreamObserver<ConstraintServiceOuterClass.UnitLabelCommand_Responses> responseObserver) {
        responseObserver.onNext(ConstraintServiceOuterClass.UnitLabelCommand_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }
}
