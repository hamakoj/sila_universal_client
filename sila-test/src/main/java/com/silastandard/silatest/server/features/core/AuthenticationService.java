package com.silastandard.silatest.server.features.core;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.authenticationservice.v1.AuthenticationServiceGrpc;
import sila2.org.silastandard.core.authenticationservice.v1.AuthenticationServiceOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAString;

import java.util.AbstractMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class AuthenticationService extends AuthenticationServiceGrpc.AuthenticationServiceImplBase {
    private static final String FULLY_QUALIFIED_FEATURE_IDENTIFIER = "org.silastandard/core/AuthenticationService/v1";

    public static String uniqueAccessToken = UUID.randomUUID().toString();
    public static final Map<String, String> CREDENTIALS_STORE = Stream.of(
                    new AbstractMap.SimpleEntry<>("admin", "admin"))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    @Override
    public void login(
            AuthenticationServiceOuterClass.Login_Parameters request,
            StreamObserver<AuthenticationServiceOuterClass.Login_Responses> responseObserver
    ) {
        try {
            if (request.hasPassword() && request.hasUserIdentification() && request.hasRequestedServer()) {
                if (CREDENTIALS_STORE.containsKey(request.getUserIdentification().getValue())
                        && CREDENTIALS_STORE.get(request.getUserIdentification().getValue())
                                .equals(request.getPassword().getValue())
                ) {
                    responseObserver.onNext(
                            AuthenticationServiceOuterClass.Login_Responses
                                    .newBuilder()
                                    .setAccessToken(SiLAString.from(uniqueAccessToken))
                                    .mergeTokenLifetime(SiLAInteger.from(1000))
                                    .build()
                    );
                    responseObserver.onCompleted();
                } else {
                    throw SiLAErrors.generateDefinedExecutionError(
                            FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + "AuthenticationFailed",
                            "The provided credentials are not valid."
                    );
                }

            }
        } catch (final Throwable t) {
            responseObserver.onError(t);
        }
    }

    @Override
    public void logout(
            AuthenticationServiceOuterClass.Logout_Parameters request,
            StreamObserver<AuthenticationServiceOuterClass.Logout_Responses> responseObserver
    ) {
        try {
            final SiLAFramework.String accessToken = request.getAccessToken();
            log.info("Received Access Token from Parameter [{}]", accessToken);
            //final LockControllerOuterClass.Metadata_LockIdentifier lockIdentifier = Constant.LOCK_IDENTIFIER_CTX_KEY.get();
            //log.info("Received Lock Identifier from Metadata [{}]", lockIdentifier);
            //CoreFeatureValidators.AccessTokenValidator.checkAccessToken(accessToken.getValue());
            //CoreFeatureValidators.LockValidator.checkUnlockTemporary(lockIdentifier.getLockIdentifier());
            this.refreshToken();
            responseObserver.onNext(AuthenticationServiceOuterClass.Logout_Responses.newBuilder().build());
            responseObserver.onCompleted();
        } catch (final Throwable t) {
            responseObserver.onError(t);
        }
    }

    private void refreshToken() {
        AuthenticationService.uniqueAccessToken = UUID.randomUUID().toString();
    }
}
