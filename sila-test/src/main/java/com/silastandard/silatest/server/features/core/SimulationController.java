package com.silastandard.silatest.server.features.core;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.simulationcontroller.v1.SimulationControllerGrpc;
import sila2.org.silastandard.core.simulationcontroller.v1.SimulationControllerOuterClass;
import sila_java.library.core.sila.types.SiLABoolean;

@Slf4j
public class SimulationController extends SimulationControllerGrpc.SimulationControllerImplBase {
    private boolean isSimulation = false;

    @Override
    public void startSimulationMode(
            SimulationControllerOuterClass.StartSimulationMode_Parameters request,
            StreamObserver<SimulationControllerOuterClass.StartSimulationMode_Responses> responseObserver
   ) {
        this.isSimulation = true;
        log.info("Simulation mode enabled");
        responseObserver.onNext(SimulationControllerOuterClass.StartSimulationMode_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void startRealMode(
            SimulationControllerOuterClass.StartRealMode_Parameters request,
            StreamObserver<SimulationControllerOuterClass.StartRealMode_Responses> responseObserver
    ) {
        this.isSimulation = false;
        log.info("Simulation mode disabled");
        responseObserver.onNext(SimulationControllerOuterClass.StartRealMode_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void getSimulationMode(
            SimulationControllerOuterClass.Get_SimulationMode_Parameters request,
            StreamObserver<SimulationControllerOuterClass.Get_SimulationMode_Responses> responseObserver
    ) {
        responseObserver.onNext(
                SimulationControllerOuterClass.Get_SimulationMode_Responses
                        .newBuilder()
                        .setSimulationMode(SiLABoolean.from(this.isSimulation))
                        .build()
        );
        responseObserver.onCompleted();
    }
}
