package com.silastandard.silatest.server.features.core.commands;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.commands.cancelcontroller.v1.CancelControllerGrpc;
import sila2.org.silastandard.core.commands.cancelcontroller.v1.CancelControllerOuterClass;

@Slf4j
public class CancelController extends CancelControllerGrpc.CancelControllerImplBase {
    @Override
    public void cancelCommand(
            CancelControllerOuterClass.CancelCommand_Parameters request,
            StreamObserver<CancelControllerOuterClass.CancelCommand_Responses> responseObserver
    ) {
        log.info("Received command to cancel command {}", request.getCommandExecutionUUID().getUUID());
        responseObserver.onNext(CancelControllerOuterClass.CancelCommand_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void cancelAll(
            CancelControllerOuterClass.CancelAll_Parameters request,
            StreamObserver<CancelControllerOuterClass.CancelAll_Responses> responseObserver
    ) {
        log.info("Received command to cancel all commands");
        responseObserver.onNext(CancelControllerOuterClass.CancelAll_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }
}
