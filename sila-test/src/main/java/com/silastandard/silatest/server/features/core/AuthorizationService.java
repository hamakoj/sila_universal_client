package com.silastandard.silatest.server.features.core;

import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceGrpc;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceOuterClass;
import sila_java.library.core.sila.types.SiLAString;

public class AuthorizationService extends AuthorizationServiceGrpc.AuthorizationServiceImplBase {
    @Override
    public void getFCPAffectedByMetadataAccessToken(
            AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Parameters request,
            StreamObserver<AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Responses> responseObserver
    ) {
        responseObserver.onNext(
                AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Responses
                        .newBuilder()
                        .addAffectedCalls(SiLAString.from("org.silastandard/core/LockController/v1/Command/LockServer"))
                        .addAffectedCalls(SiLAString.from("org.silastandard/core/LockController/v1/Command/UnlockServer"))
                        .addAffectedCalls(SiLAString.from("org.silastandard/core.commands/ImageService/v1/Command/GetImage"))
                        .addAffectedCalls(SiLAString.from("org.silastandard/core/LockController/v1/Property/IsLocked"))
                        .addAffectedCalls(SiLAString.from("org.silastandard/core.commands/ObservableService/v1"))
                        .build()
        );
        responseObserver.onCompleted();
    }
}
