package com.silastandard.silatest.server;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.commands.observableservice.v1.ObservableServiceGrpc;
import sila2.org.silastandard.core.commands.observableservice.v1.ObservableServiceOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLATime;
import sila_java.library.server_base.command.observable.ObservableCommandManager;
import sila_java.library.server_base.command.observable.ObservableCommandTaskRunner;
import sila_java.library.server_base.command.observable.ObservableCommandWrapper;
import sila_java.library.server_base.command.observable.RunnableCommandTask;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

@Slf4j
@NoArgsConstructor
public class ObservableService extends ObservableServiceGrpc.ObservableServiceImplBase implements AutoCloseable {
    private final LocalDateTime startTime = LocalDateTime.now();
    private boolean run = true;
    private final ObservableCommandManager<ObservableServiceOuterClass.CountTo_Parameters, ObservableServiceOuterClass.CountTo_Responses> countToManager = new ObservableCommandManager<>(
            new ObservableCommandTaskRunner(1, 10),
            new RunnableCommandTask<ObservableServiceOuterClass.CountTo_Parameters, ObservableServiceOuterClass.CountTo_Responses>() {
                @Override
                public ObservableServiceOuterClass.CountTo_Responses run(ObservableCommandWrapper<ObservableServiceOuterClass.CountTo_Parameters, ObservableServiceOuterClass.CountTo_Responses> command) throws StatusRuntimeException {
                    long countTo = command.getParameter().getNumber().getValue();
                    LocalDateTime cmdStart = LocalDateTime.now();
                    try {
                        for (int i = 0; i < countTo; ++i) {
                            command.setExecutionInfoAndNotify(i / (double) countTo, Duration.ofSeconds(countTo - i));
                            command.notifyIntermediateResponse(ObservableServiceOuterClass.CountTo_IntermediateResponses.newBuilder().setCurrentCount(SiLAInteger.from(i)).build());
                            Thread.sleep(1000);
                        }
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                    LocalDateTime cmdEnd = LocalDateTime.now();
                    final long elapsedSeconds = ChronoUnit.MINUTES.between(cmdStart, cmdEnd);
                    int minutes = (int)elapsedSeconds / 60;
                    int hours = minutes / 60;
                    int seconds = (int)elapsedSeconds % 60;
                    SiLAFramework.Time time = SiLATime.from(OffsetTime.of(hours, minutes, seconds, 0, ZoneOffset.UTC));
                    return ObservableServiceOuterClass.CountTo_Responses.newBuilder().setTimeElapsed(time).build();
                }

                @Override
                public void validate(ObservableServiceOuterClass.CountTo_Parameters parameters) throws StatusRuntimeException {
                    if (parameters.getNumber().getValue() < 0 || parameters.getNumber().getValue() > 100) {
                        throw SiLAErrors.generateValidationError(
                                "org.silastandard/core.commands/ObservableService/v1/Command/CountTo/Parameter/Number",
                                "Number must be 0 or positive and lower than 100."
                        );
                    }
                }
            }, Duration.ofMinutes(10));

    @Override
    public void countTo(ObservableServiceOuterClass.CountTo_Parameters request, StreamObserver<SiLAFramework.CommandConfirmation> responseObserver) {
        this.countToManager.addCommand(request, responseObserver);
    }

    @Override
    public void countToInfo(SiLAFramework.CommandExecutionUUID request, StreamObserver<SiLAFramework.ExecutionInfo> responseObserver) {
        this.countToManager.get(request).addStateObserver(responseObserver);
    }

    @Override
    public void countToIntermediate(SiLAFramework.CommandExecutionUUID request, StreamObserver<ObservableServiceOuterClass.CountTo_IntermediateResponses> responseObserver) {
        this.countToManager.get(request).addIntermediateResponseObserver(responseObserver);
    }

    @Override
    public void countToResult(SiLAFramework.CommandExecutionUUID request, StreamObserver<ObservableServiceOuterClass.CountTo_Responses> responseObserver) {
        this.countToManager.get(request).sendResult(responseObserver);
    }

    @Override
    public void subscribeUptime(ObservableServiceOuterClass.Subscribe_Uptime_Parameters request, StreamObserver<ObservableServiceOuterClass.Subscribe_Uptime_Responses> responseObserver) {
        while (this.run) {
            try {
                final long uptimeMinutes = ChronoUnit.MINUTES.between(this.startTime, LocalDateTime.now());
                responseObserver.onNext(
                        ObservableServiceOuterClass.Subscribe_Uptime_Responses
                                .newBuilder()
                                .setUptime(SiLAInteger.from(uptimeMinutes))
                                .build()
                );
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                responseObserver.onCompleted();
                Thread.currentThread().interrupt();
                return;
            }
        }
        responseObserver.onCompleted();
    }

    @Override
    public void close() {
        this.run = false;
        this.countToManager.close();
    }
}
