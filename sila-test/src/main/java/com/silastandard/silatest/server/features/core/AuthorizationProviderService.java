package com.silastandard.silatest.server.features.core;

import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.core.authorizationproviderservice.v1.AuthorizationProviderServiceGrpc;
import sila2.org.silastandard.core.authorizationproviderservice.v1.AuthorizationProviderServiceOuterClass;
import sila_java.library.core.sila.types.SiLAInteger;

public class AuthorizationProviderService extends AuthorizationProviderServiceGrpc.AuthorizationProviderServiceImplBase {
    @Override
    public void verify(
            AuthorizationProviderServiceOuterClass.Verify_Parameters request,
            StreamObserver<AuthorizationProviderServiceOuterClass.Verify_Responses> responseObserver
    ) {
        final String accesToken = request.getAccessToken().getValue();
        final String requestedServer = request.getRequestedServer().getValue();
        final String requestedFeature = request.getRequestedFeature().getValue();
        responseObserver.onNext(
                AuthorizationProviderServiceOuterClass.Verify_Responses
                        .newBuilder()
                        .setTokenLifetime(SiLAInteger.from(100))
                        .build()
        );
        responseObserver.onCompleted();
    }
}
