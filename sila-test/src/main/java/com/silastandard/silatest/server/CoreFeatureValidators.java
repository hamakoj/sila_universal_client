package com.silastandard.silatest.server;

import com.silastandard.silatest.server.features.core.AuthenticationService;
import com.silastandard.silatest.server.features.core.LockController;
import io.grpc.StatusRuntimeException;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.util.Objects;

public class CoreFeatureValidators {
    public static class AccessTokenValidator {
        private static final String FULLY_QUALIFIED_FEATURE_IDENTIFIER = "org.silastandard/core/AuthenticationService/v1";

        public static void checkAccessToken(final String accessToken) throws StatusRuntimeException {
            if (Objects.isNull(accessToken) || !AuthenticationService.uniqueAccessToken.equals(accessToken)) {
                throw SiLAErrors.generateDefinedExecutionError(
                        AccessTokenValidator.FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + "InvalidAccessToken",
                        "The sent access token is not valid."
                );
            }
        }

    }

    public static class LockValidator {
        private static final String FULLY_QUALIFIED_FEATURE_IDENTIFIER = "org.silastandard/core/LockController/v1";


        public static void checkUnlockTemporary(final SiLAFramework.String lockIdentifier) throws StatusRuntimeException {
            // should pass if server is unlocked
            if (!LockController.locked) {
                return;
            }

            // should pass if lockIdentifier is matching current lock
            if (!LockController.currentLock.orElseThrow(
                            () -> SiLAErrors.generateUndefinedExecutionError("ServerLockNotFound: " +
                                    "The server is locked but no Lock identifier can be found."))
                    .equals(lockIdentifier)
            ) {
                throw SiLAErrors.generateDefinedExecutionError(
                        LockValidator.FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + "ServerLocked",
                        "Server is locked and the sent lock identifier is not valid."
                );
            }
        }

        public static void checkLockIdentifier(final SiLAFramework.String lockIdentifier) throws StatusRuntimeException {
            if (Objects.isNull(lockIdentifier) || !LockController.currentLock
                    .orElseThrow(() -> SiLAErrors.generateUndefinedExecutionError("ServerLockNotFound: " +
                            "The server is locked but no Lock identifier can be found."))
                    .equals(lockIdentifier)
            ) {
                throw SiLAErrors.generateDefinedExecutionError(
                        LockValidator.FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + "InvalidLockIdentifier",

                        "The sent lock identifier is not valid."
                );
            }
        }

        public static void checkServerLocked() throws StatusRuntimeException {
            if (!LockController.locked) {
                throw SiLAErrors.generateDefinedExecutionError(
                        LockValidator.FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + "ServerNotLocked",
                        "The SiLA Server can not be unlocked because it is not locked."
                );
            }
        }

        public static void checkServerNotAlreadyLocked() throws StatusRuntimeException {
            if (LockController.locked) {
                throw SiLAErrors.generateDefinedExecutionError(
                        LockValidator.FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + "ServerAlreadyLocked",
                        "The SiLA Server can not be locked because it is already locked."
                );
            }
        }

        public static void checkServerUnlocked() throws StatusRuntimeException {
            if (LockController.locked) {
                throw SiLAErrors.generateUndefinedExecutionError(
                        "ServerLocker: The server is locked for now. Retry later."
                );
            }
        }

    }
}
