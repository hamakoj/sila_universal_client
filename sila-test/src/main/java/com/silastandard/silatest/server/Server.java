package com.silastandard.silatest.server;

import com.silastandard.silatest.server.features.core.*;
import com.silastandard.silatest.server.features.core.commands.CancelController;
import com.silastandard.silatest.server.features.core.commands.ParameterConstraintsProvider;
import com.silastandard.silatest.server.features.core.commands.PauseController;
import io.grpc.ServerInterceptors;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.binary_transfer.database.impl.H2BinaryDatabase;
import sila_java.library.server_base.config.IServerConfigWrapper;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.utils.ArgumentHelper;
import sila_java.library.sila_base.EmptyClass;

import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Objects;

import static sila_java.library.core.utils.FileUtils.getFileContent;

@Slf4j
public class Server implements AutoCloseable {
    public static final String SERVER_TYPE = "USCTestServer";
    public static final String SERVER_DESC = "";
    public static final String SERVER_VENDOR = "https://gitlab.com/Timort/universal-sila-client/";
    public static final String SERVER_VERSION = "0.1";
    private final ObservableService observableService = new ObservableService();
    private final SiLAServer server;
    private final H2BinaryDatabase binaryDatabase;

    public static void main(final String[] args) {
        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);
        final Server server = new Server(argumentHelper);
        server.server.blockUntilShutdown();
    }

    @Override
    public void close() {
        this.observableService.close();
        this.server.close();
        this.binaryDatabase.close();
    }

    @SneakyThrows
    private static String getCoreFeatureContent(@NonNull final String coreFeaturePath) {
        return getFileContent(Objects.requireNonNull(EmptyClass.class.getResourceAsStream(
                "/sila_base/feature_definitions/org/silastandard/core/" + coreFeaturePath
        )));
    }

    private Server(@NonNull final ArgumentHelper argumentHelper) {
        final ServerInformation serverInfo = new ServerInformation(
                SERVER_TYPE, SERVER_DESC, SERVER_VENDOR, SERVER_VERSION
        );

        try {
            final SiLAServer.Builder builder = SiLAServer.Builder
                    .newBuilder(serverInfo)
                    .withPersistentConfig(argumentHelper.getConfigFile().isPresent())
                    .withPersistentConfigFile(argumentHelper.getConfigFile().orElseGet(() -> Paths.get("./server.cfg")))
                    .withPersistentTLS(
                            argumentHelper.getPrivateKeyFile(),
                            argumentHelper.getCertificateFile(),
                            argumentHelper.getCertificatePassword()
                    );
            argumentHelper.getPort().ifPresent(builder::withPort);
            argumentHelper.getHost().ifPresent(builder::withHost);
            builder.withDiscovery(argumentHelper.getInterface().isPresent());
            argumentHelper.getInterface().ifPresent(builder::withNetworkInterface);
            builder.withUnsafeCommunication(argumentHelper.useUnsafeCommunication());

            final IServerConfigWrapper configuration = builder.getNewServerConfigurationWrapper();
            builder.withBinaryTransferSupport(true);
            builder.withCustomConfigWrapperProvider((configPath, serverConfiguration) -> configuration);
            this.binaryDatabase = new H2BinaryDatabase(configuration.getCacheConfig().getUuid());
            builder.withCustomBinaryDatabaseProvider((uuid) -> this.binaryDatabase);

            builder.addFeature(getCoreFeatureContent("commands/CancelController.sila.xml"), new CancelController());
            builder.addFeature(getCoreFeatureContent("commands/ParameterConstraintsProvider.sila.xml"), new ParameterConstraintsProvider());
            builder.addFeature(getCoreFeatureContent("commands/PauseController.sila.xml"), new PauseController());
            builder.addFeature(getCoreFeatureContent("AuthenticationService.sila.xml"), ServerInterceptors.intercept(new AuthenticationService(), new ServerMetadataInterceptor()));
            builder.addFeature(getCoreFeatureContent("AuthorizationConfigurationService.sila.xml"), new AuthorizationConfigurationService(configuration.getCacheConfig().getUuid()));
            builder.addFeature(getCoreFeatureContent("AuthorizationProviderService.sila.xml"), new AuthorizationProviderService());
            builder.addFeature(getCoreFeatureContent("AuthorizationService.sila.xml"), new AuthorizationService());
            builder.addFeature(getCoreFeatureContent("ConnectionConfigurationService.sila.xml"), new ConnectionConfigurationService());
            builder.addFeature(getCoreFeatureContent("SimulationController.sila.xml"), new SimulationController());
            builder.addFeature(getCoreFeatureContent("ErrorRecoveryService.sila.xml"), new ErrorRecoveryService());
            //builder.addFeature(getCoreFeatureContent("LockController.sila.xml"), new LockController());
            // just to showcase the metadata interceptor
            builder.addFeature(getCoreFeatureContent("LockController.sila.xml"),
                    ServerInterceptors.intercept(new LockController(), new ServerMetadataInterceptor())
            );
            builder.addFeature(
                    getFileContent(Objects.requireNonNull(
                        Server.class.getResourceAsStream("/com/silastandard/silatest/ImageService.sila.xml"))
                    ),
                    ServerInterceptors.intercept(new ImageService(binaryDatabase), new ServerMetadataInterceptor())
            );
            builder.addFeature(
                    getFileContent(Objects.requireNonNull(
                            Server.class.getResourceAsStream("/com/silastandard/silatest/ObservableService.sila.xml"))
                    ),
                    ServerInterceptors.intercept(this.observableService, new ServerMetadataInterceptor())
            );
            builder.addFeature(
                    getFileContent(Objects.requireNonNull(
                            Server.class.getResourceAsStream("/com/silastandard/silatest/LoopbackService.sila.xml"))
                    ),
                    ServerInterceptors.intercept(new LoopbackService(), new ServerMetadataInterceptor())
            );
            builder.addFeature(
                    getFileContent(Objects.requireNonNull(
                            Server.class.getResourceAsStream("/com/silastandard/silatest/ConstraintService.sila.xml"))
                    ),
                    new ConstraintService()
            );
            builder.addFeature(
                    getFileContent(Objects.requireNonNull(
                            Server.class.getResourceAsStream("/com/silastandard/silatest/ErrorService.sila.xml"))
                    ),
                    new ErrorService()
            );
            builder.addFeature(
                    getFileContent(Objects.requireNonNull(
                            Server.class.getResourceAsStream("/com/silastandard/silatest/GraphService.sila.xml"))
                    ),
                    new GraphService()
            );

            this.server = builder.start();
        } catch (IOException | SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
