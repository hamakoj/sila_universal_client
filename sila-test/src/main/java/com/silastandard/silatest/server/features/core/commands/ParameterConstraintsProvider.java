package com.silastandard.silatest.server.features.core.commands;

import com.google.common.collect.Lists;
import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderGrpc;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderOuterClass;
import sila_java.library.core.sila.types.SiLAString;

import java.util.ArrayList;

public class ParameterConstraintsProvider extends ParameterConstraintsProviderGrpc.ParameterConstraintsProviderImplBase {
    private static final String NumberMinMax100 = "<Constraints>\n" +
            "                    <MinimalInclusive>0</MinimalInclusive>\n" +
            "                    <MaximalInclusive>100</MaximalInclusive>\n" +
            "                </Constraints>";
    private static final String NumberMinMax50 = "<Constraints>\n" +
            "                    <MinimalInclusive>0</MinimalInclusive>\n" +
            "                    <MaximalInclusive>50</MaximalInclusive>\n" +
            "                </Constraints>";
    private static final String NumberMinMax10 = "<Constraints>\n" +
            "                    <MinimalInclusive>10</MinimalInclusive>\n" +
            "                    <MaximalInclusive>20</MaximalInclusive>\n" +
            "                </Constraints>";
    private static final String NumberMinMax1 = "<Constraints>\n" +
            "                    <MinimalInclusive>15</MinimalInclusive>\n" +
            "                    <MaximalInclusive>15</MaximalInclusive>\n" +
            "                </Constraints>";

    @Override
    public void subscribeParametersConstraints(
            ParameterConstraintsProviderOuterClass.Subscribe_ParametersConstraints_Parameters request,
            StreamObserver<ParameterConstraintsProviderOuterClass.Subscribe_ParametersConstraints_Responses> responseObserver
    ) {
        String fqi = "org.silastandard/core.commands/ObservableService/v1/Command/CountTo/Parameter/Number";
        ArrayList<String> strings = Lists.newArrayList(NumberMinMax100, NumberMinMax50, NumberMinMax10, NumberMinMax1);
        try {
            while (true) {
                for (String string : strings) {
                    responseObserver.onNext(
                            ParameterConstraintsProviderOuterClass.Subscribe_ParametersConstraints_Responses
                                    .newBuilder()
                                    .addParametersConstraints(
                                            ParameterConstraintsProviderOuterClass.Subscribe_ParametersConstraints_Responses.ParametersConstraints_Struct
                                                    .newBuilder()
                                                    .setCommandParameterIdentifier(SiLAString.from(fqi))
                                                    .setConstraints(SiLAString.from(string))
                                                    .build()
                                    )
                                    .build()
                    );
                    Thread.sleep(30000);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            responseObserver.onCompleted();
        }
    }
}
