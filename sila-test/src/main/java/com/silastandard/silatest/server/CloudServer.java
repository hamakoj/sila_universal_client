package com.silastandard.silatest.server;

import com.google.common.collect.ImmutableMap;
import io.grpc.ManagedChannel;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila2.org.silastandard.SiLAFramework;
import sila2.com.timothy.diguiet.cloud.cloudservice.v1.CloudServiceOuterClass;
import sila_java.library.cloudier.server.CloudierConnectionConfigurationService;
import sila_java.library.cloudier.server.CloudierServerEndpoint;
import sila_java.library.cloudier.server.CloudierSiLAService;
import sila_java.library.cloudier.server.MessageCaseHandler;
import sila_java.library.core.sila.clients.ChannelBuilder;
import sila_java.library.sila_base.EmptyClass;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

import static sila_java.library.core.utils.FileUtils.getFileContent;
import static sila_java.library.core.utils.Utils.blockUntilShutdown;

@Slf4j
public class CloudServer implements AutoCloseable {
    private final CloudierSiLAService cloudierSiLAService;
    private final CloudierConnectionConfigurationService cloudierConnectionConfigurationService;
    private final CloudService cloudService;
    private CloudClientEndpointGrpc.CloudClientEndpointStub clientEndpoint;
    private CloudierServerEndpoint cloudServerEndpointService;
    private ManagedChannel channel;


    public static void main(String[] args) throws IOException {
        final CloudServer cloudServer = new CloudServer(UUID.randomUUID(), new CloudService());
        log.info("To stop the server press CTRL + C.");
        blockUntilShutdown(cloudServer::close);
    }

    public CloudServer(UUID serverUUID, CloudService cloudService) throws IOException {
        this.cloudService = cloudService;
        this.cloudierSiLAService = new CloudierSiLAService(
                "cloud",
                Server.SERVER_TYPE,
                serverUUID.toString(),
                Server.SERVER_VERSION,
                Server.SERVER_DESC,
                Server.SERVER_VENDOR,
                ImmutableMap.of(
                        "SiLAService", getFileContent(
                                Objects.requireNonNull(
                                        EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/core/SiLAService.sila.xml")
                                )
                        ),
                        "ConnectionConfigurationService", getFileContent(
                                Objects.requireNonNull(
                                        EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/core/ConnectionConfigurationService.sila.xml")
                                )
                        ),
                        "CloudService", getFileContent(
                                Objects.requireNonNull(
                                        Server.class.getResourceAsStream("/com/silastandard/silatest/CloudService.sila.xml")
                                )
                        )
                )
        );
        this.cloudierConnectionConfigurationService = new CloudierConnectionConfigurationService(
                false, (connectionMode) -> {}
        );
        this.startServerInitiatedConnection();
    }

    private void startServerInitiatedConnection() {
        this.channel = ChannelBuilder.withTLSEncryption("127.0.0.1", 50051).build();
        this.clientEndpoint = CloudClientEndpointGrpc.newStub(this.channel);
        HashMap<String, MessageCaseHandler> callForwarderMap = new HashMap<String, MessageCaseHandler>() {{
            put("com.timothy.diguiet/cloud/CloudService/v1/Property/Time", new MessageCaseHandler().withUnobservableProperty(
                    CloudServiceOuterClass.Get_Time_Parameters.parser(), cloudService::getTime
            ));
            put("com.timothy.diguiet/cloud/CloudService/v1/Property/Stock", new MessageCaseHandler().withObservableProperty(
                    CloudServiceOuterClass.Subscribe_Stock_Parameters.parser(), cloudService::subscribeStock
            ));
            put("com.timothy.diguiet/cloud/CloudService/v1/Command/Sum", new MessageCaseHandler().withUnobservableCommand(
                    CloudServiceOuterClass.Sum_Parameters.parser(), cloudService::sum
            ));
            put("com.timothy.diguiet/cloud/CloudService/v1/Command/AddStock", new MessageCaseHandler()
                    .withObservableCommand(CloudServiceOuterClass.AddStock_Parameters.parser(), cloudService::addStock)
                    .withExecInfo(SiLAFramework.CommandExecutionUUID.parser(), cloudService::addStockInfo)
                    .withIntermediate(SiLAFramework.CommandExecutionUUID.parser(), cloudService::addStockIntermediate)
                    .withResult(SiLAFramework.CommandExecutionUUID.parser(), cloudService::addStockResult)
            );
            put("com.timothy.diguiet/cloud/CloudService/v1/Metadata/Metadata", new MessageCaseHandler().withMetadata(
                    CloudServiceOuterClass.Get_FCPAffectedByMetadata_Metadata_Parameters.parser(), cloudService::getFCPAffectedByMetadataMetadata
            ));
        }};
        this.cloudServerEndpointService = new CloudierServerEndpoint(
                this.cloudierSiLAService,
                this.cloudierConnectionConfigurationService,
                this.clientEndpoint,
                callForwarderMap,
                null,
                null
        );
    }

    @Override
    public void close() {
        this.channel.shutdown();
    }
}
