package com.silastandard.silatest.server.features.core;

import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.authorizationconfigurationservice.v1.AuthorizationConfigurationServiceGrpc;
import sila2.org.silastandard.core.authorizationconfigurationservice.v1.AuthorizationConfigurationServiceOuterClass;
import sila_java.library.core.sila.types.SiLAString;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Slf4j
@AllArgsConstructor
public class AuthorizationConfigurationService extends AuthorizationConfigurationServiceGrpc.AuthorizationConfigurationServiceImplBase {
    private UUID authorizationProviderServer;
    private final Set<Runnable> authorizationProviderListeners = new HashSet<>();

    @Override
    public void setAuthorizationProvider(
            AuthorizationConfigurationServiceOuterClass.SetAuthorizationProvider_Parameters request,
            StreamObserver<AuthorizationConfigurationServiceOuterClass.SetAuthorizationProvider_Responses> responseObserver
    ) {
        this.authorizationProviderServer = UUID.fromString(request.getAuthorizationProvider().getValue());
        this.notifyListeners();
        responseObserver.onNext(
                AuthorizationConfigurationServiceOuterClass.SetAuthorizationProvider_Responses.newBuilder().build()
        );
        responseObserver.onCompleted();
    }

    private void notifyListeners() {
        final Set<Runnable> toRemove = new HashSet<>();
        this.authorizationProviderListeners.forEach(listener -> {
            try {
                listener.run();
            } catch (Exception e) {
                log.debug("Remove paused commands listener because of exception", e);
                toRemove.add(listener);
            }
        });
        this.authorizationProviderListeners.removeAll(toRemove);
        log.info("Authorization provider listeners count {}", this.authorizationProviderListeners.size());
        log.info("Authorization provider server to use {}", this.authorizationProviderServer);
    }

    @Override
    public void subscribeAuthorizationProvider(AuthorizationConfigurationServiceOuterClass.Subscribe_AuthorizationProvider_Parameters request, StreamObserver<AuthorizationConfigurationServiceOuterClass.Subscribe_AuthorizationProvider_Responses> responseObserver) {
        final Runnable callback = () -> {
            try {
                responseObserver.onNext(
                        AuthorizationConfigurationServiceOuterClass.Subscribe_AuthorizationProvider_Responses
                                .newBuilder()
                                .setAuthorizationProvider(SiLAString.from(this.authorizationProviderServer.toString()))
                                .build()
                );
            } catch (Exception e) {
                responseObserver.onCompleted();
                throw e;
            }
        };
        callback.run(); // send initial state
        this.authorizationProviderListeners.add(callback);
    }
}
