package com.silastandard.silatest.server.features.core.commands;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.commands.pausecontroller.v1.PauseControllerGrpc;
import sila2.org.silastandard.core.commands.pausecontroller.v1.PauseControllerOuterClass;
import sila_java.library.core.sila.types.SiLAString;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
public class PauseController extends PauseControllerGrpc.PauseControllerImplBase {
    private final Set<UUID> pausedCommands = new HashSet<>();
    private final Set<Runnable> pausedCommandsListeners = new HashSet<>();

    @Override
    public void pause(
            PauseControllerOuterClass.Pause_Parameters request,
            StreamObserver<PauseControllerOuterClass.Pause_Responses> responseObserver
    ) {
        this.pausedCommands.add(UUID.fromString(request.getCommandExecutionUUID().getUUID().getValue()));
        this.notifyListeners();
        responseObserver.onNext(PauseControllerOuterClass.Pause_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void resume(
            PauseControllerOuterClass.Resume_Parameters request,
            StreamObserver<PauseControllerOuterClass.Resume_Responses> responseObserver
    ) {
        this.pausedCommands.remove(UUID.fromString(request.getCommandExecutionUUID().getUUID().getValue()));
        this.notifyListeners();
        responseObserver.onNext(PauseControllerOuterClass.Resume_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    private void notifyListeners() {
        final Set<Runnable> toRemove = new HashSet<>();
        this.pausedCommandsListeners.forEach(listener -> {
            try {
                listener.run();
            } catch (Exception e) {
                log.debug("Remove paused commands listener because of exception", e);
                toRemove.add(listener);
            }
        });
        this.pausedCommandsListeners.removeAll(toRemove);
        log.info("Paused commands listeners count {}", this.pausedCommandsListeners.size());
        log.info("Paused commands count {}", this.pausedCommands.size());
    }

    @Override
    public void subscribePausedCommands(PauseControllerOuterClass.Subscribe_PausedCommands_Parameters request, StreamObserver<PauseControllerOuterClass.Subscribe_PausedCommands_Responses> responseObserver) {
        final Runnable callback = () -> {
            final Set<PauseControllerOuterClass.DataType_UUID> siLAPausedCommands = this.pausedCommands
                    .stream()
                    .map(UUID::toString)
                    .map(uuid -> PauseControllerOuterClass.DataType_UUID.newBuilder().setUUID(SiLAString.from(uuid)).build())
                    .collect(Collectors.toSet());
            try {
                responseObserver.onNext(
                        PauseControllerOuterClass.Subscribe_PausedCommands_Responses
                                .newBuilder()
                                .addAllPausedCommands(siLAPausedCommands)
                                .build()
                );
            } catch (Exception e) {
                responseObserver.onCompleted();
                throw e;
            }
        };
        callback.run(); // send initial state
        pausedCommandsListeners.add(callback);
    }
}
