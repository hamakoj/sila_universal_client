package com.silastandard.silatest.server;

import com.silastandard.silatest.common.Constant;
import io.grpc.stub.StreamObserver;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.commands.loopbackservice.v1.LoopbackServiceGrpc;
import sila2.org.silastandard.core.commands.loopbackservice.v1.LoopbackServiceOuterClass;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAString;

@Slf4j
public class LoopbackService extends LoopbackServiceGrpc.LoopbackServiceImplBase {
    @SneakyThrows
    @Override
    public void subscribeCountTo100(LoopbackServiceOuterClass.Subscribe_CountTo100_Parameters request, StreamObserver<LoopbackServiceOuterClass.Subscribe_CountTo100_Responses> responseObserver) {
        for (int i = 0; i < 100; ++i) {
            responseObserver.onNext(
                    LoopbackServiceOuterClass.Subscribe_CountTo100_Responses
                            .newBuilder()
                            .setCountTo100(SiLAInteger.from(i + 1))
                            .build()
            );
            Thread.sleep(500);
        }
        responseObserver.onCompleted();
    }

    @Override
    public void string(LoopbackServiceOuterClass.String_Parameters request, StreamObserver<LoopbackServiceOuterClass.String_Responses> responseObserver) {
        log.info("String request: " + request.getString().getValue());
        responseObserver.onNext(LoopbackServiceOuterClass.String_Responses.newBuilder().setString(request.getString()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void integer(LoopbackServiceOuterClass.Integer_Parameters request, StreamObserver<LoopbackServiceOuterClass.Integer_Responses> responseObserver) {
        log.info("String request: " + request.getInteger().getValue());
        responseObserver.onNext(LoopbackServiceOuterClass.Integer_Responses.newBuilder().setInteger(request.getInteger()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void real(LoopbackServiceOuterClass.Real_Parameters request, StreamObserver<LoopbackServiceOuterClass.Real_Responses> responseObserver) {
        log.info("String request: " + request.getReal().getValue());
        responseObserver.onNext(LoopbackServiceOuterClass.Real_Responses.newBuilder().setReal(request.getReal()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void boolean_(LoopbackServiceOuterClass.Boolean_Parameters request, StreamObserver<LoopbackServiceOuterClass.Boolean_Responses> responseObserver) {
        log.info("String request: " + request.getBoolean().getValue());
        responseObserver.onNext(LoopbackServiceOuterClass.Boolean_Responses.newBuilder().setBoolean(request.getBoolean()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void binary(LoopbackServiceOuterClass.Binary_Parameters request, StreamObserver<LoopbackServiceOuterClass.Binary_Responses> responseObserver) {
        log.info("String request: " + request.getBinary().getValue());
        responseObserver.onNext(LoopbackServiceOuterClass.Binary_Responses.newBuilder().setBinary(request.getBinary()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void date(LoopbackServiceOuterClass.Date_Parameters request, StreamObserver<LoopbackServiceOuterClass.Date_Responses> responseObserver) {
        log.info("String request: " + request.getDate());
        //responseObserver.onNext(LoopbackServiceOuterClass.Date_Responses.newBuilder().setDate(SiLADate.from(OffsetDateTime.now())).build());
        responseObserver.onNext(LoopbackServiceOuterClass.Date_Responses.newBuilder().setDate(request.getDate()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void time(LoopbackServiceOuterClass.Time_Parameters request, StreamObserver<LoopbackServiceOuterClass.Time_Responses> responseObserver) {
        log.info("String request: " + request.getTime());
        responseObserver.onNext(LoopbackServiceOuterClass.Time_Responses.newBuilder().setTime(request.getTime()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void timestamp(LoopbackServiceOuterClass.Timestamp_Parameters request, StreamObserver<LoopbackServiceOuterClass.Timestamp_Responses> responseObserver) {
        log.info("String request: " + request.getTimestamp());
        responseObserver.onNext(LoopbackServiceOuterClass.Timestamp_Responses.newBuilder().setTimestamp(request.getTimestamp()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void any(LoopbackServiceOuterClass.Any_Parameters request, StreamObserver<LoopbackServiceOuterClass.Any_Responses> responseObserver) {
        log.info("String request: " + request.getAny().getPayload());
        responseObserver.onNext(LoopbackServiceOuterClass.Any_Responses.newBuilder().setAny(request.getAny()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getMetadata(LoopbackServiceOuterClass.GetMetadata_Parameters request, StreamObserver<LoopbackServiceOuterClass.GetMetadata_Responses> responseObserver) {
        final LoopbackServiceOuterClass.Metadata_LoopbackIdentifier loopbackIdentifier = Constant.LOOPBACK_IDENTIFIER_CTX_KEY.get();
        log.info("getMetadata: Received Loopback Identifier [{}]", loopbackIdentifier);
        responseObserver.onNext(LoopbackServiceOuterClass.GetMetadata_Responses.newBuilder().setLoopbackIdentifier(loopbackIdentifier.getLoopbackIdentifier()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getFCPAffectedByMetadataLoopbackIdentifier(
            LoopbackServiceOuterClass.Get_FCPAffectedByMetadata_LoopbackIdentifier_Parameters request,
            StreamObserver<LoopbackServiceOuterClass.Get_FCPAffectedByMetadata_LoopbackIdentifier_Responses> responseObserver
    ) {
        responseObserver.onNext(
                LoopbackServiceOuterClass.Get_FCPAffectedByMetadata_LoopbackIdentifier_Responses
                        .newBuilder()
                        .addAffectedCalls(SiLAString.from("org.silastandard/core.commands/LoopbackService/v1/Command/GetMetadata"))
                        .build()
        );
        responseObserver.onCompleted();
    }
}
