package com.silastandard.silatest.server.features.core;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.connectionconfigurationservice.v1.ConnectionConfigurationServiceGrpc;
import sila2.org.silastandard.core.connectionconfigurationservice.v1.ConnectionConfigurationServiceOuterClass;
import sila_java.library.core.sila.types.SiLABoolean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class ConnectionConfigurationService extends ConnectionConfigurationServiceGrpc.ConnectionConfigurationServiceImplBase {
    private boolean isServerInitiated = false;
    private final Set<Runnable> connectionModeListeners = new HashSet<>();
    private final Set<Runnable> clientsListeners = new HashSet<>();
    private final Map<String, ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Parameters> configuredClients = new HashMap<>();

    private void notifyConnectionModeListeners() {
        final Set<Runnable> toRemove = new HashSet<>();
        this.connectionModeListeners.forEach(listener -> {
            try {
                listener.run();
            } catch (Exception e) {
                log.debug("Remove connection mode listener because of exception", e);
                toRemove.add(listener);
            }
        });
        this.connectionModeListeners.removeAll(toRemove);
        log.info("Connection mode listeners count {}", this.connectionModeListeners.size());
    }

    private void notifyClientsListeners() {
        final Set<Runnable> toRemove = new HashSet<>();
        this.clientsListeners.forEach(listener -> {
            try {
                listener.run();
            } catch (Exception e) {
                log.debug("Remove configured clients listener because of exception", e);
                toRemove.add(listener);
            }
        });
        this.clientsListeners.removeAll(toRemove);
        log.info("Configured clients listeners count {}", this.clientsListeners.size());
        log.info("Configured clients client count {}", this.clientsListeners.size());
    }

    @Override
    public void enableServerInitiatedConnectionMode(
            ConnectionConfigurationServiceOuterClass.EnableServerInitiatedConnectionMode_Parameters request,
            StreamObserver<ConnectionConfigurationServiceOuterClass.EnableServerInitiatedConnectionMode_Responses> responseObserver
    ) {
        this.isServerInitiated = true;
        this.notifyConnectionModeListeners();
        responseObserver.onNext(
                ConnectionConfigurationServiceOuterClass.EnableServerInitiatedConnectionMode_Responses.newBuilder().build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void disableServerInitiatedConnectionMode(
            ConnectionConfigurationServiceOuterClass.DisableServerInitiatedConnectionMode_Parameters request,
            StreamObserver<ConnectionConfigurationServiceOuterClass.DisableServerInitiatedConnectionMode_Responses> responseObserver
    ) {
        this.isServerInitiated = false;
        this.notifyConnectionModeListeners();
        responseObserver.onNext(
                ConnectionConfigurationServiceOuterClass.DisableServerInitiatedConnectionMode_Responses.newBuilder().build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void connectSiLAClient(
            ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Parameters request,
            StreamObserver<ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Responses> responseObserver
    ) {
        final String clientName = request.getClientName().getValue();
        this.configuredClients.put(clientName, request);
        this.notifyClientsListeners();
        responseObserver.onNext(ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void disconnectSiLAClient(
            ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Parameters request,
            StreamObserver<ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Responses> responseObserver
    ) {
        final String clientName = request.getClientName().getValue();
        final boolean toRemove = request.getRemove().getValue();
        // todo disconnect
        if (toRemove) {
            this.configuredClients.remove(clientName);
            this.notifyClientsListeners();
        }
        responseObserver.onNext(ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void subscribeServerInitiatedConnectionModeStatus(
            ConnectionConfigurationServiceOuterClass.Subscribe_ServerInitiatedConnectionModeStatus_Parameters request,
            StreamObserver<ConnectionConfigurationServiceOuterClass.Subscribe_ServerInitiatedConnectionModeStatus_Responses> responseObserver
    ) {
        final Runnable callback = () -> {
            try {
                responseObserver.onNext(
                        ConnectionConfigurationServiceOuterClass.Subscribe_ServerInitiatedConnectionModeStatus_Responses
                                .newBuilder()
                                .setServerInitiatedConnectionModeStatus(SiLABoolean.from(this.isServerInitiated))
                                .build()
                );
            } catch (Exception e) {
                responseObserver.onCompleted();
                throw e;
            }
        };
        callback.run(); // send initial state
        this.connectionModeListeners.add(callback);
    }

    @Override
    public void subscribeConfiguredSiLAClients(
            ConnectionConfigurationServiceOuterClass.Subscribe_ConfiguredSiLAClients_Parameters request,
            StreamObserver<ConnectionConfigurationServiceOuterClass.Subscribe_ConfiguredSiLAClients_Responses> responseObserver
    ) {
        final Runnable callback = () -> {
            try {
                Set<ConnectionConfigurationServiceOuterClass.Subscribe_ConfiguredSiLAClients_Responses.ConfiguredSiLAClients_Struct> clients =
                        this.configuredClients.values()
                                .stream()
                                .map((client) -> ConnectionConfigurationServiceOuterClass.Subscribe_ConfiguredSiLAClients_Responses.ConfiguredSiLAClients_Struct
                                        .newBuilder()
                                        .setClientName(client.getClientName())
                                        .setSiLAClientHost(client.getSiLAClientHost())
                                        .setSiLAClientPort(client.getSiLAClientPort())
                                        .build()
                                ).collect(Collectors.toSet());
                responseObserver.onNext(
                        ConnectionConfigurationServiceOuterClass.Subscribe_ConfiguredSiLAClients_Responses
                                .newBuilder()
                                .addAllConfiguredSiLAClients(clients)
                                .build()
                );
            } catch (Exception e) {
                responseObserver.onCompleted();
                throw e;
            }
        };
        callback.run(); // send initial state
        this.clientsListeners.add(callback);
    }
}
