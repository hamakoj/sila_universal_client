package com.silastandard.silatest.server;

import com.google.protobuf.InvalidProtocolBufferException;
import com.silastandard.silatest.common.Constant;
import io.grpc.*;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceOuterClass;
import sila2.org.silastandard.core.commands.loopbackservice.v1.LoopbackServiceOuterClass;
import sila2.org.silastandard.core.lockcontroller.v1.LockControllerOuterClass;

import java.util.Objects;


@Slf4j
public class ServerMetadataInterceptor implements ServerInterceptor {
    private static final ServerCall.Listener NOOP_LISTENER = new ServerCall.Listener() {};

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(
            ServerCall<ReqT, RespT> serverCall, Metadata metadata,
            ServerCallHandler<ReqT, RespT> serverCallHandler
    ) {
        byte[] accessToken = metadata.get(Constant.ACCESS_TOKEN_METADATA_KEY);
        byte[] lockIdentifier = metadata.get(Constant.LOCK_IDENTIFIER_METADATA_KEY);
        byte[] loopbackIdentifier = metadata.get(Constant.LOOPBACK_IDENTIFIER_METADATA_KEY);

        Context ctx = Context.current();
        try {
            if (Objects.nonNull(lockIdentifier)) {
                ctx = ctx.withValue(Constant.LOCK_IDENTIFIER_CTX_KEY, LockControllerOuterClass.Metadata_LockIdentifier.parseFrom(lockIdentifier));
            }
            if (Objects.nonNull(accessToken)) {
                ctx = ctx.withValue(Constant.ACCESS_TOKEN_CTX_KEY, AuthorizationServiceOuterClass.Metadata_AccessToken.parseFrom(accessToken));
            }
            if (Objects.nonNull(loopbackIdentifier)) {
                ctx = ctx.withValue(Constant.LOOPBACK_IDENTIFIER_CTX_KEY, LoopbackServiceOuterClass.Metadata_LoopbackIdentifier.parseFrom(loopbackIdentifier));
            }
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }

        return Contexts.interceptCall(ctx, serverCall, metadata, serverCallHandler);
    }
}