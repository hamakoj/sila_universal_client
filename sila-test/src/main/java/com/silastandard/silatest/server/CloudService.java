package com.silastandard.silatest.server;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.com.timothy.diguiet.cloud.cloudservice.v1.CloudServiceGrpc;
import sila2.com.timothy.diguiet.cloud.cloudservice.v1.CloudServiceOuterClass;
import sila_java.library.core.sila.types.*;

import java.time.Duration;
import java.time.OffsetTime;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class CloudService extends CloudServiceGrpc.CloudServiceImplBase {
    private int stock = 0;

    @Override
    public void sum(CloudServiceOuterClass.Sum_Parameters request, StreamObserver<CloudServiceOuterClass.Sum_Responses> responseObserver) {
        responseObserver.onNext(
                CloudServiceOuterClass.Sum_Responses
                        .newBuilder()
                        .setResult(SiLAReal.from(request.getA().getValue() + request.getB().getValue()))
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void addStock(CloudServiceOuterClass.AddStock_Parameters request, StreamObserver<SiLAFramework.CommandConfirmation> responseObserver) {
        responseObserver.onNext(SiLAFramework.CommandConfirmation
                .newBuilder()
                .setCommandExecutionUUID(SiLAFramework.CommandExecutionUUID.newBuilder().setValue(UUID.randomUUID().toString()))
                .build());
        responseObserver.onCompleted();
        CompletableFuture.runAsync(() -> {
            try {
                for (int i = 0; i < request.getQuantityToAd().getValue(); i++) {
                    Thread.sleep(600);
                    ++stock;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void addStockInfo(SiLAFramework.CommandExecutionUUID request, StreamObserver<SiLAFramework.ExecutionInfo> responseObserver) {
        responseObserver.onNext(
                SiLAFramework.ExecutionInfo
                        .newBuilder()
                        .setProgressInfo(SiLAReal.from(0.0))
                        .setEstimatedRemainingTime(SiLADuration.from(Duration.ofSeconds(2)))
                        .setUpdatedLifetimeOfExecution(SiLADuration.from(Duration.ofSeconds(2)))
                        .setCommandStatus(SiLAFramework.ExecutionInfo.CommandStatus.running)
                        .build()
        );
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {}
        responseObserver.onNext(
                SiLAFramework.ExecutionInfo
                        .newBuilder()
                        .setProgressInfo(SiLAReal.from(100.0))
                        .setEstimatedRemainingTime(SiLADuration.from(Duration.ofSeconds(0)))
                        .setUpdatedLifetimeOfExecution(SiLADuration.from(Duration.ofSeconds(0)))
                        .setCommandStatus(SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully)
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void addStockIntermediate(SiLAFramework.CommandExecutionUUID request, StreamObserver<CloudServiceOuterClass.AddStock_IntermediateResponses> responseObserver) {
        try {
            for (int i = 0; i < 3; i++) {
                responseObserver.onNext(
                        CloudServiceOuterClass.AddStock_IntermediateResponses.newBuilder().setCurrentStock(SiLAInteger.from(stock)).build()
                );
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        responseObserver.onCompleted();
    }

    @Override
    public void addStockResult(SiLAFramework.CommandExecutionUUID request, StreamObserver<CloudServiceOuterClass.AddStock_Responses> responseObserver) {
        responseObserver.onNext(
                CloudServiceOuterClass.AddStock_Responses.newBuilder().setNewStock(SiLAInteger.from(stock)).build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getTime(CloudServiceOuterClass.Get_Time_Parameters request, StreamObserver<CloudServiceOuterClass.Get_Time_Responses> responseObserver) {
        responseObserver.onNext(
                CloudServiceOuterClass.Get_Time_Responses.newBuilder().setTime(SiLATime.from(OffsetTime.now())).build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void subscribeStock(CloudServiceOuterClass.Subscribe_Stock_Parameters request, StreamObserver<CloudServiceOuterClass.Subscribe_Stock_Responses> responseObserver) {
        try {
            while (true) {
                Thread.sleep(1000);
                responseObserver.onNext(CloudServiceOuterClass.Subscribe_Stock_Responses.newBuilder().setStock(SiLAInteger.from(stock)).build());
            }
        } catch (InterruptedException e) {}
        responseObserver.onCompleted();
    }

    @Override
    public void getFCPAffectedByMetadataMetadata(CloudServiceOuterClass.Get_FCPAffectedByMetadata_Metadata_Parameters request, StreamObserver<CloudServiceOuterClass.Get_FCPAffectedByMetadata_Metadata_Responses> responseObserver) {
        responseObserver.onNext(CloudServiceOuterClass.Get_FCPAffectedByMetadata_Metadata_Responses
                .newBuilder()
                .addAffectedCalls(SiLAString.from("com.timothy.diguiet/cloud/CloudService/v1/Property/Time"))
                .build()
        );
        responseObserver.onCompleted();
    }

}
