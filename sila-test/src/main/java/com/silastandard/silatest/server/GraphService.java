package com.silastandard.silatest.server;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.commands.graphservice.v1.GraphServiceGrpc;
import sila2.org.silastandard.core.commands.graphservice.v1.GraphServiceOuterClass;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAReal;
import sila_java.library.server_base.command.observable.ObservableCommandManager;
import sila_java.library.server_base.command.observable.ObservableCommandTaskRunner;

import java.time.Duration;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@Slf4j
public class GraphService extends GraphServiceGrpc.GraphServiceImplBase implements AutoCloseable {
    private final Set<Runnable> realListeners = new HashSet<>();
    private final Set<Runnable> integerListeners = new HashSet<>();
    private final ScheduledExecutorService executor;
    private final ScheduledFuture<?> task;
    private int integerValue = 0;
    private double realValue = 0;

    public GraphService() {
        this.executor = Executors.newScheduledThreadPool(1);
        this.task = this.executor.scheduleAtFixedRate(() -> {
            this.integerValue = (int) Math.round(Math.random() * 200 - 100);
            this.realValue = Math.random() * 200 - 100;
            this.notifyIntegerListeners();
            this.notifyRealListeners();
        }, 0, 2, TimeUnit.SECONDS);
    }
    private final ObservableCommandManager<GraphServiceOuterClass.GraphCommand_Parameters, GraphServiceOuterClass.GraphCommand_Responses> countToManager = new ObservableCommandManager<>(
            new ObservableCommandTaskRunner(1, 10),
            command -> {
                final double start = command.getParameter().getRangeStart().getValue();
                final double end = command.getParameter().getRangeEnd().getValue();
                final int limit = 64;
                try {
                    for (int i = 0; i < limit; ++i) {
                        final double random = Math.random() * Math.abs(start - end) + start;
                        command.setExecutionInfoAndNotify(i / (double) limit, Duration.ofSeconds(limit - i));
                        command.notifyIntermediateResponse(
                                GraphServiceOuterClass.GraphCommand_IntermediateResponses
                                        .newBuilder()
                                        .setValue(SiLAReal.from(random))
                                        .build()
                        );
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                return GraphServiceOuterClass.GraphCommand_Responses.newBuilder().build();
            }, Duration.ofMinutes(10));

    private void notifyRealListeners() {
        final Set<Runnable> toRemove = new HashSet<>();
        this.realListeners.forEach(listener -> {
            try {
                listener.run();
            } catch (Exception e) {
                log.debug("Remove Real listener because of exception", e);
                toRemove.add(listener);
            }
        });
        this.realListeners.removeAll(toRemove);
        //log.info("Real listeners count {}", this.realListeners.size());
    }

    private void notifyIntegerListeners() {
        final Set<Runnable> toRemove = new HashSet<>();
        this.integerListeners.forEach(listener -> {
            try {
                listener.run();
            } catch (Exception e) {
                log.debug("Remove Integer listener because of exception", e);
                toRemove.add(listener);
            }
        });
        this.integerListeners.removeAll(toRemove);
        //log.info("Integer listeners count {}", this.integerListeners.size());
    }

    @Override
    public void graphCommand(GraphServiceOuterClass.GraphCommand_Parameters request, StreamObserver<SiLAFramework.CommandConfirmation> responseObserver) {
        this.countToManager.addCommand(request, responseObserver);
    }

    @Override
    public void graphCommandInfo(SiLAFramework.CommandExecutionUUID request, StreamObserver<SiLAFramework.ExecutionInfo> responseObserver) {
        this.countToManager.get(request).addStateObserver(responseObserver);
    }

    @Override
    public void graphCommandIntermediate(SiLAFramework.CommandExecutionUUID request, StreamObserver<GraphServiceOuterClass.GraphCommand_IntermediateResponses> responseObserver) {
        this.countToManager.get(request).addIntermediateResponseObserver(responseObserver);
    }

    @Override
    public void graphCommandResult(SiLAFramework.CommandExecutionUUID request, StreamObserver<GraphServiceOuterClass.GraphCommand_Responses> responseObserver) {
        this.countToManager.get(request).sendResult(responseObserver);
    }

    @Override
    public void subscribeIntegerGraph(GraphServiceOuterClass.Subscribe_IntegerGraph_Parameters request, StreamObserver<GraphServiceOuterClass.Subscribe_IntegerGraph_Responses> responseObserver) {
        final Runnable callback = () -> {
            try {
                responseObserver.onNext(
                        GraphServiceOuterClass.Subscribe_IntegerGraph_Responses
                                .newBuilder()
                                .setIntegerGraph(SiLAInteger.from(this.integerValue))
                                .build()
                );
            } catch (Exception e) {
                responseObserver.onCompleted();
                throw e;
            }
        };
        callback.run(); // send initial state
        this.integerListeners.add(callback);
    }

    @Override
    public void subscribeRealGraph(GraphServiceOuterClass.Subscribe_RealGraph_Parameters request, StreamObserver<GraphServiceOuterClass.Subscribe_RealGraph_Responses> responseObserver) {
        final Runnable callback = () -> {
            try {
                responseObserver.onNext(
                        GraphServiceOuterClass.Subscribe_RealGraph_Responses
                                .newBuilder()
                                .setRealGraph(SiLAReal.from(this.realValue))
                                .build()
                );
            } catch (Exception e) {
                responseObserver.onCompleted();
                throw e;
            }
        };
        callback.run(); // send initial state
        this.realListeners.add(callback);
    }

    @Override
    public void close() throws Exception {
        this.task.cancel(true);
        this.executor.shutdownNow();
    }
}
