package com.silastandard.silatest.server;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.commands.errorservice.v1.ErrorServiceGrpc;
import sila2.org.silastandard.core.commands.errorservice.v1.ErrorServiceOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;

@Slf4j
public class ErrorService extends ErrorServiceGrpc.ErrorServiceImplBase {
    private static final String FULLY_QUALIFIED_FEATURE_IDENTIFIER = "ch.unitelabs/test/ErrorService/v1";
    @Override
    public void notImplementedCommand(ErrorServiceOuterClass.NotImplementedCommand_Parameters request, StreamObserver<ErrorServiceOuterClass.NotImplementedCommand_Responses> responseObserver) {
        super.notImplementedCommand(request, responseObserver);
    }

    @Override
    public void noResponseCommand(ErrorServiceOuterClass.NoResponseCommand_Parameters request, StreamObserver<ErrorServiceOuterClass.NoResponseCommand_Responses> responseObserver) {
        responseObserver.onCompleted();
    }

    @Override
    public void undefinedExecutionErrorCommand(ErrorServiceOuterClass.UndefinedExecutionErrorCommand_Parameters request, StreamObserver<ErrorServiceOuterClass.UndefinedExecutionErrorCommand_Responses> responseObserver) {
        throw SiLAErrors.generateUndefinedExecutionError("No more coffee");
    }

    @Override
    public void definedExecutionErrorCommand(ErrorServiceOuterClass.DefinedExecutionErrorCommand_Parameters request, StreamObserver<ErrorServiceOuterClass.DefinedExecutionErrorCommand_Responses> responseObserver) {
        throw SiLAErrors.generateDefinedExecutionError(
                FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + "StackOverflow", "Out of memory"
        );
    }

    @Override
    public void validationErrorCommand(ErrorServiceOuterClass.ValidationErrorCommand_Parameters request, StreamObserver<ErrorServiceOuterClass.ValidationErrorCommand_Responses> responseObserver) {
        throw SiLAErrors.generateValidationError(
                FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/Command/ValidationErrorCommand/Parameter/" + "Invalid", "Parameter is invalid"
        );
    }

    @Override
    public void frameworkErrorCommand(ErrorServiceOuterClass.FrameworkErrorCommand_Parameters request, StreamObserver<ErrorServiceOuterClass.FrameworkErrorCommand_Responses> responseObserver) {
        throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.COMMAND_EXECUTION_NOT_ACCEPTED, "Not allowed");
    }

    @Override
    public void connectionErrorCommand(ErrorServiceOuterClass.ConnectionErrorCommand_Parameters request, StreamObserver<ErrorServiceOuterClass.ConnectionErrorCommand_Responses> responseObserver) {
        // todo implement
        responseObserver.onNext(ErrorServiceOuterClass.ConnectionErrorCommand_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }
}
