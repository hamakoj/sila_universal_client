package com.silastandard.silatest.client.features.core;

import io.grpc.Channel;
import sila2.org.silastandard.core.authenticationservice.v1.AuthenticationServiceGrpc;
import sila2.org.silastandard.core.authenticationservice.v1.AuthenticationServiceOuterClass;

public class AuthenticationService {
    private final AuthenticationServiceGrpc.AuthenticationServiceBlockingStub stub;

    public AuthenticationService(Channel channel) {
        stub = AuthenticationServiceGrpc.newBlockingStub(channel);
    }

    public void login() {
        AuthenticationServiceOuterClass.Login_Responses responses =
                stub.login(AuthenticationServiceOuterClass.Login_Parameters.newBuilder().build());
    }

    public void logout() {
        AuthenticationServiceOuterClass.Logout_Responses responses =
                stub.logout(AuthenticationServiceOuterClass.Logout_Parameters.newBuilder().build());
    }
}
