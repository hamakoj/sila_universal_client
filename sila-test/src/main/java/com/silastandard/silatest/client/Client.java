package com.silastandard.silatest.client;

import com.silastandard.silatest.client.features.core.*;
import com.silastandard.silatest.client.features.core.commands.CancelController;
import com.silastandard.silatest.client.features.core.commands.ParameterConstraintsProvider;
import com.silastandard.silatest.client.features.core.commands.PauseController;
import io.grpc.ManagedChannel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_java.library.core.sila.clients.ChannelBuilder;
import sila_java.library.manager.ServerFinder;
import sila_java.library.manager.ServerManager;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;



@Slf4j
public class Client {
    @Getter
    private final CancelController cancelController;
    @Getter
    private final ParameterConstraintsProvider parameterConstraintsProvider;
    @Getter
    private final PauseController pauseController;
    @Getter
    private final AuthenticationService authenticationService;
    @Getter
    private final AuthorizationConfigurationService authorizationConfigurationService;
    @Getter
    private final AuthorizationProviderService authorizationProviderService;
    @Getter
    private final AuthorizationService authorizationService;
    @Getter
    private final ConnectionConfigurationService connectionConfigurationService;
    @Getter
    private final LockController lockController;
    @Getter
    private final SimulationController simulationController;

    public Client(ManagedChannel serviceChannel) {
        this.cancelController = new CancelController(serviceChannel);
        this.parameterConstraintsProvider = new ParameterConstraintsProvider(serviceChannel);
        this.pauseController = new PauseController(serviceChannel);
        this.authenticationService = new AuthenticationService(serviceChannel);
        this.authorizationConfigurationService = new AuthorizationConfigurationService(serviceChannel);
        this.authorizationProviderService = new AuthorizationProviderService(serviceChannel);
        this.authorizationService = new AuthorizationService(serviceChannel);
        this.connectionConfigurationService = new ConnectionConfigurationService(serviceChannel);
        this.lockController = new LockController(serviceChannel);
        this.simulationController = new SimulationController(serviceChannel);
    }

    public static void main(String[] args) throws InterruptedException, IOException {
        try (final ServerManager serverManager = ServerManager.getInstance()) {
            //final X509Certificate serverCertificate = readCertificate(Paths.get("./cert.pem").toFile());

            // Create Manager for clients and start discovery
            final sila_java.library.manager.models.Server server = ServerFinder
                    .filterBy(ServerFinder.Filter.type(com.silastandard.silatest.server.Server.SERVER_TYPE))
                    .scanAndFindOne(Duration.ofMinutes(1))
                    .orElseThrow(() -> new RuntimeException("No server found within time"));

            log.info("Found Server!");

            final ManagedChannel serviceChannel = ChannelBuilder
                    .withTLSEncryption(server.getHost(), server.getPort(), server.getCertificateAuthority())
                    .intercept(new ClientMetadataInterceptor()).build();
            try {
                final SiLAServiceGrpc.SiLAServiceBlockingStub serviceStub = SiLAServiceGrpc.newBlockingStub(serviceChannel);

                log.info("Found Features:");
                final List<SiLAFramework.String> featureIdentifierList = serviceStub
                        .getImplementedFeatures(SiLAServiceOuterClass.Get_ImplementedFeatures_Parameters.newBuilder().build())
                        .getImplementedFeaturesList();

                featureIdentifierList.forEach(featureIdentifier ->
                        log.info("\t" + featureIdentifier.getValue())
                );

                final Client client = new Client(serviceChannel);
                boolean simulationMode = client.getSimulationController().isSimulationMode();
                log.info("Simulation mode is " + ((simulationMode) ? "enabled" : "disabled"));
                if (simulationMode) {
                    client.getSimulationController().startRealMode();
                } else {
                    client.getSimulationController().startSimulationMode();
                }
                simulationMode = client.getSimulationController().isSimulationMode();
                log.info("Simulation mode is " + ((simulationMode) ? "enabled" : "disabled"));
                client.getLockController().lockServer();
            } finally {
                serviceChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
            }
        }
    }
}
