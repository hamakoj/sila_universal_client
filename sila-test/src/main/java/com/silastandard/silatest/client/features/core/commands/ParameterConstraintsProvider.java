package com.silastandard.silatest.client.features.core.commands;

import io.grpc.Channel;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderGrpc;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderOuterClass;

import java.util.Iterator;

public class ParameterConstraintsProvider {
    private final ParameterConstraintsProviderGrpc.ParameterConstraintsProviderBlockingStub stub;

    public ParameterConstraintsProvider(Channel channel) {
        stub = ParameterConstraintsProviderGrpc.newBlockingStub(channel);
    }

    public void subscribeParametersConstraints() {
        Iterator<ParameterConstraintsProviderOuterClass.Subscribe_ParametersConstraints_Responses> responsesIterator =
                stub.subscribeParametersConstraints(
                        ParameterConstraintsProviderOuterClass.Subscribe_ParametersConstraints_Parameters.newBuilder().build()
                );
    }
}
