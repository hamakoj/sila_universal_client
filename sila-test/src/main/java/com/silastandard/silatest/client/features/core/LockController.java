package com.silastandard.silatest.client.features.core;

import io.grpc.Channel;
// import io.grpc.Context;
import sila2.org.silastandard.core.lockcontroller.v1.LockControllerGrpc;
import sila2.org.silastandard.core.lockcontroller.v1.LockControllerOuterClass;

import java.util.Iterator;

public class LockController {
    private final LockControllerGrpc.LockControllerBlockingStub stub;

    public LockController(Channel channel) {
        stub = LockControllerGrpc.newBlockingStub(channel);
    }

    public void lockServer() {
        // Context.current().run(() -> {
        LockControllerOuterClass.LockServer_Responses responses =
                stub.lockServer(LockControllerOuterClass.LockServer_Parameters.newBuilder().build());
        // });
    }

    public void unlockServer() {
        // Context.current().run(() -> {
        LockControllerOuterClass.UnlockServer_Responses responses =
                stub.unlockServer(LockControllerOuterClass.UnlockServer_Parameters.newBuilder().build());
        // });
    }

    public void getIsLocked() {
            // todo make observable
        Iterator<LockControllerOuterClass.Subscribe_IsLocked_Responses> responses =
                stub.subscribeIsLocked(LockControllerOuterClass.Subscribe_IsLocked_Parameters.newBuilder().build());
    }

    public void getFCPAffectedByMetadataLockIdentifier() {
        LockControllerOuterClass.Get_FCPAffectedByMetadata_LockIdentifier_Responses responses =
                stub.getFCPAffectedByMetadataLockIdentifier(
                        LockControllerOuterClass.Get_FCPAffectedByMetadata_LockIdentifier_Parameters
                                .newBuilder()
                                .build()
                );
    }
}
