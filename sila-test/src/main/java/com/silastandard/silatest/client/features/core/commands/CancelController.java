package com.silastandard.silatest.client.features.core.commands;

import io.grpc.Channel;
import sila2.org.silastandard.core.commands.cancelcontroller.v1.CancelControllerGrpc;
import sila2.org.silastandard.core.commands.cancelcontroller.v1.CancelControllerOuterClass;

public class CancelController {
    private final CancelControllerGrpc.CancelControllerBlockingStub stub;

    public CancelController(Channel channel) {
        stub = CancelControllerGrpc.newBlockingStub(channel);
    }

    public void cancelCommand() {
        CancelControllerOuterClass.CancelCommand_Responses responses =
                stub.cancelCommand(CancelControllerOuterClass.CancelCommand_Parameters.newBuilder().build());
    }

    public void cancelAll() {
        CancelControllerOuterClass.CancelAll_Responses responses =
                stub.cancelAll(CancelControllerOuterClass.CancelAll_Parameters.newBuilder().build());
    }
}
