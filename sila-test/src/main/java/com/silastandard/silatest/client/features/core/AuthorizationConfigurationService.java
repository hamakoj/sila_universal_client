package com.silastandard.silatest.client.features.core;

import io.grpc.Channel;
import sila2.org.silastandard.core.authorizationconfigurationservice.v1.AuthorizationConfigurationServiceGrpc;
import sila2.org.silastandard.core.authorizationconfigurationservice.v1.AuthorizationConfigurationServiceOuterClass;

import java.util.Iterator;

public class AuthorizationConfigurationService {
    private final AuthorizationConfigurationServiceGrpc.AuthorizationConfigurationServiceBlockingStub stub;

    public AuthorizationConfigurationService(Channel channel) {
        stub = AuthorizationConfigurationServiceGrpc.newBlockingStub(channel);
    }

    public void setAuthorizationProvider() {
        AuthorizationConfigurationServiceOuterClass.SetAuthorizationProvider_Responses responses =
                stub.setAuthorizationProvider(
                        AuthorizationConfigurationServiceOuterClass.SetAuthorizationProvider_Parameters
                                .newBuilder()
                                .build()
        );
    }

    public void getAuthorizationProvider() {
        Iterator<AuthorizationConfigurationServiceOuterClass.Subscribe_AuthorizationProvider_Responses> responses =
                stub.subscribeAuthorizationProvider(
                        AuthorizationConfigurationServiceOuterClass.Subscribe_AuthorizationProvider_Parameters
                                .newBuilder()
                                .build()
                );
    }
}
