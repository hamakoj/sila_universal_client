package com.silastandard.silatest.client.features.core;

import io.grpc.Channel;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceGrpc;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceOuterClass;

public class AuthorizationService {
    private final AuthorizationServiceGrpc.AuthorizationServiceBlockingStub stub;

    public AuthorizationService(Channel channel) {
        stub = AuthorizationServiceGrpc.newBlockingStub(channel);
    }

    public void getFCPAffectedByMetadataAccessToken() {
        AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Responses responses =
                stub.getFCPAffectedByMetadataAccessToken(
                        AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Parameters
                                .newBuilder()
                                .build()
                );
    }
}
