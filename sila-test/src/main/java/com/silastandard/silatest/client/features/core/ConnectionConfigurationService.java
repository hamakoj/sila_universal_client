package com.silastandard.silatest.client.features.core;

import io.grpc.Channel;
import sila2.org.silastandard.core.connectionconfigurationservice.v1.ConnectionConfigurationServiceGrpc;
import sila2.org.silastandard.core.connectionconfigurationservice.v1.ConnectionConfigurationServiceOuterClass;

import java.util.Iterator;

public class ConnectionConfigurationService {
    private final ConnectionConfigurationServiceGrpc.ConnectionConfigurationServiceBlockingStub stub;

    public ConnectionConfigurationService(Channel channel) {
        stub = ConnectionConfigurationServiceGrpc.newBlockingStub(channel);
    }

    public void enableServerInitiatedConnectionMode() {
        ConnectionConfigurationServiceOuterClass.EnableServerInitiatedConnectionMode_Responses responses =
                stub.enableServerInitiatedConnectionMode(
                        ConnectionConfigurationServiceOuterClass.EnableServerInitiatedConnectionMode_Parameters
                                .newBuilder()
                                .build()
                );
    }

    public void disableServerInitiatedConnectionMode() {
        ConnectionConfigurationServiceOuterClass.DisableServerInitiatedConnectionMode_Responses responses =
                stub.disableServerInitiatedConnectionMode(
                        ConnectionConfigurationServiceOuterClass.DisableServerInitiatedConnectionMode_Parameters
                                .newBuilder()
                                .build()
                );
    }

    public void connectSiLAClient() {
        ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Responses responses =
                stub.connectSiLAClient(
                        ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Parameters.newBuilder().build()
                );
    }

    public void disconnectSiLAClient() {
        ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Responses responses =
                stub.disconnectSiLAClient(
                        ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Parameters.newBuilder().build()
                );
    }

    public void getServerInitiatedConnectionModeStatus() {
        // todo make observable
        Iterator<ConnectionConfigurationServiceOuterClass.Subscribe_ServerInitiatedConnectionModeStatus_Responses> serverInitiatedConnectionModeStatus =
                stub.subscribeServerInitiatedConnectionModeStatus(
                        ConnectionConfigurationServiceOuterClass.Subscribe_ServerInitiatedConnectionModeStatus_Parameters
                                .newBuilder()
                                .build()
                );
    }

    public void getConfiguredSiLAClients() {
        // todo make observable
        Iterator<ConnectionConfigurationServiceOuterClass.Subscribe_ConfiguredSiLAClients_Responses> configuredSiLAClients =
                stub.subscribeConfiguredSiLAClients(
                        ConnectionConfigurationServiceOuterClass.Subscribe_ConfiguredSiLAClients_Parameters
                                .newBuilder()
                                .build()
                );
    }
}
