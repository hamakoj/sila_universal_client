package com.silastandard.silatest.client;

import com.silastandard.silatest.common.Constant;
import io.grpc.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ClientMetadataInterceptor implements ClientInterceptor {
    @Override
    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(
            MethodDescriptor<ReqT, RespT> methodDescriptor,
            CallOptions callOptions, Channel channel
    ) {
        return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(
                channel.newCall(methodDescriptor, callOptions)
        ) {
            @Override
            public void start(Listener<RespT> responseListener, Metadata headers) {
                if (Constant.LOCK_IDENTIFIER_CTX_KEY.get() != null) {
                    headers.put(
                            Constant.LOCK_IDENTIFIER_METADATA_KEY,
                            Constant.LOCK_IDENTIFIER_CTX_KEY.get().toByteArray()
                    );
                }
                if (Constant.ACCESS_TOKEN_CTX_KEY.get() != null) {
                    headers.put(
                            Constant.ACCESS_TOKEN_METADATA_KEY,
                            Constant.ACCESS_TOKEN_CTX_KEY.get().toByteArray()
                    );
                }
                super.start(responseListener, headers);
            }
        };
    }
}