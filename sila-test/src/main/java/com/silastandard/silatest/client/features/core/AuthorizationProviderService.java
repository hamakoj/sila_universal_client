package com.silastandard.silatest.client.features.core;

import io.grpc.Channel;
import sila2.org.silastandard.core.authorizationproviderservice.v1.AuthorizationProviderServiceGrpc;
import sila2.org.silastandard.core.authorizationproviderservice.v1.AuthorizationProviderServiceOuterClass;

public class AuthorizationProviderService {
    private final AuthorizationProviderServiceGrpc.AuthorizationProviderServiceBlockingStub stub;

    public AuthorizationProviderService(Channel channel) {
        stub = AuthorizationProviderServiceGrpc.newBlockingStub(channel);
    }

    public void verify(
    ) {
        AuthorizationProviderServiceOuterClass.Verify_Responses responses =
                stub.verify(AuthorizationProviderServiceOuterClass.Verify_Parameters.newBuilder().build());
    }
}
