<!-- PROJECT SHIELDS -->
<!-- PROJECT SHIELDS -->
<!-- TODO FIX BADGES
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Last Commit][last-commit-shield]][last-commit-url]
[![Open MRs][open-mr-shield]][open-mr-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]
-->

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client">
    <img src="/images/logo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Universal SiLA Client</h3>
<p align="center">
Feature full and Universal SiLA Client that enables scientists and non technical users to control any SiLA2 Servers 
in a user friendly manner through their Phone, Tablet and PC.  
<br />
<a href="https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/wikis/home"><strong>Explore the docs »</strong></a>
<br />
<br />
<a href="https://sila.timothy.diguiet.com">View Demo</a>
·
<a href="https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/wikis/home">Open Wiki / User manual</a>
·
<a href="https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/issues">Report Bug</a>
·
<a href="https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/issues">Request Feature</a>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#run-the-pre-compiled-application">Run the application</a></li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#upgrade-migration">Upgrade-Migration</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

<a href="https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client">
    <img src="/images/usc.png" alt="Logo" width="100%" height="100%">
  </a>

### Built With

The main open source projects below:

* [Spring](https://spring.io/)
* [React](https://reactjs.org/)
* [Material UI](https://material-ui.com/)
* [TypeScript](https://www.typescriptlang.org/)

### Run the pre-compiled application

#### Note
*Latest compiled binaries are available at https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/jobs/artifacts/master/download?job=build_and_test*

#### Prerequisites
The Universal SiLA Client can be executed as is without any need for complicated setup!
The only prerequisite is to have **Java** installed (version 8 or above) installed on the computer.

If you need to install Java, go to https://adoptium.net/?variant=openjdk11

The installation guidance are available at https://adoptium.net/installation.html?variant=openjdk11

#### Java executable
1. [Download the ZIP archive](https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/jobs/artifacts/master/download?job=build_and_test)
2. Extract the downloaded ZIP archive using an archive decompressor
3. When extracted, go into the directory artifacts > back > target
4. Open a terminal in the current directory
5. Start the application by running `java -jar back-0.7.0-SNAPSHOT-app.jar`
6. You can now use the application by going to the following URL on your browser http://localhost:8080/

#### Windows executable
1. [Download the ZIP archive](https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/jobs/artifacts/master/download?job=build_and_test)
2. Extract the downloaded ZIP archive using an archive decompressor
3. When extracted, go into the directory artifacts > back > target
4. Execute usc.exe
5. You can now use the application by going to the following URL on your browser http://localhost:8080/

#### Docker image
The Docker image is located at `registry.gitlab.com/sila2/universal-sila-client/sila_universal_client:latest`

1. Open a terminal
2. Run `docker pull registry.gitlab.com/sila2/universal-sila-client/sila_universal_client:latest`
3. Run `docker run  -p 8080:8080 -p 50051:50051 -p 5353:5353/udp registry.gitlab.com/sila2/universal-sila-client/sila_universal_client`
4. You can now use the application by going to the following URL on your browser http://localhost:8080/

<!-- GETTING STARTED -->
## Getting Started

#### Development 
To develop you will need to meet the requirements below:

- Git
- Java JDK 8 or above
- Maven 3.6.3 or above (using Java JDK)
- NodeJS (v17.2.0) + NPM (8.1.4)

##### Git-LFS
This project uses Git-LFS to keep the repository size short by not keeping track of changes made to binary files

Before tracking a binary file such as a .exe, make sure to track the extension with Git-LFS like so:
```
git lfs track "*.exe"
```

### Installation

1. Open a command line terminal
2. Clone the GIT repository
   - Using HTTPS:
      ```sh
      git clone https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client.git
      ```
   - Using SSH:
      ```sh
      git clone git@gitlab.com:SiLA2/universal-sila-client/sila_universal_client.git
      ```
3. Go into the repository
      ```sh
      cd sila_universal_client
      ```
4. Clone all the submodules
   ```sh
   git submodule update --init --recursive
   ```
5. Compile the project
   ```sh
   mvn -T 1C clean install -f pom.xml -DskipTests "-Dskip.sila_java.tests=true" "-Dskip.front.lib.build=true"
   ```
That's it, you are all set!

<!-- USAGE EXAMPLES -->
## Usage

You can run the application by
- using maven:
    ```
    mvn spring-boot:run -pl back
    ```
- compiling the project and running the generated .jar file in back/target:
    ```
    java -jar .\back-0.7.0-SNAPSHOT-app.jar
    ```

The application should be accessible at
```
http://localhost:8080/
```

### Application Properties
You can specify your own properties or update existing one be providing your own configuration with the following command line argument:
```
--spring.config.additional-location=production.yaml
```

## Upgrade-Migration

### Upgrade legacy H2 database 1.4.200
Since 18/12/2022 legacy H2 database 1.4.200 is not supported anymore.
The USC Application will attempt to upgrade your database if your computer is connected to internet.
If not you can create a backup of your database and remove the data folder or follow the guide below to upgrade the database offline.

#### Offline

- Download [1.4.200 H2](https://h2database.com/h2-2019-10-14.zip) jar and [2.1.210 H2](https://github.com/h2database/h2database/releases/download/version-2.1.210/h2-2022-01-17.zip) jar.
- Unzip both jar in separate folders
- Stop the USC application if it is running
- Find the `data` folder containing the USC Database `db.h2` located in the same directory from where the USC is executed.
- Rename the `data` folder to `data_old`
- Open a terminal in `<1.4.200-folder>/bin` and run:
```
java -cp h2-1.4.200.jar org.h2.tools.Script -url jdbc:h2:/<path-to-old-db-file>/data_old/db.h2 -user sila -password sila -script backup.zip -options compression zip  
```
- If successful a `backup.zip` should be present in `<1.4.200-folder>/bin`
- Move `backup.zip` from `<1.4.200-folder>/bin` to `<2.1.210-folder>/bin`
- Open a terminal in `<2.1.210-folder>/bin` and run:
```
java -cp h2-2.1.210.jar org.h2.tools.RunScript -url jdbc:h2:/<path-to-new-db-file>/data/db.h2 -user sila -password sila -script backup.zip -options compression zip FROM_1X  
```

- The database should now be updated and you can start the USC application.

### Front

The front-end can be executed independently with npm (useful when working on the front-end for hot reload):
```
npm start --prefix front 
```

The served front-end should be accessible at
```
http://localhost:3000/
```

Note that there is a known issue in dev mode with the websocket proxy when using Firefox,
see [ERR_STREAM_WRITE_AFTER_END write after end #1342](https://github.com/http-party/node-http-proxy/issues/1342)

<!-- ROADMAP -->
## Roadmap

| Status | Goal               | Labels        | ETA         |
|:------:|:-------------------|---------------|-------------|
|        |                    |               |             |

See the [open issues](https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/issues) for a list of proposed features (and known issues).

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create.
Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

<!-- CONTACT -->
## Contact

Feel free to reach out to Timothy DIGUIET on the [SiLA Standard Slack](https://sila-standard.org/slack) channel.

<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [SiLA Standard](https://sila-standard.com/)
* [JetBrains](https://www.jetbrains.com/)
* [Adopt Open JDK](https://adoptopenjdk.net/)
* [Spring](https://spring.io/)
* [React](https://reactjs.org/)
* [Material UI](https://material-ui.com/)
* [TypeScript](https://www.typescriptlang.org/)
* [GitLab](https://gitlab.com/)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://badgen.net/gitlab/contributors/SiLA2/universal-sila-client/sila_universal_client/?scale=1.42
[contributors-url]: https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/graphs/master
[forks-shield]: https://badgen.net/gitlab/forks/SiLA2/universal-sila-client/sila_universal_client/?scale=1.42
[forks-url]: https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/forks
[stars-shield]: https://badgen.net/gitlab/stars/SiLA2/universal-sila-client/sila_universal_client/?scale=1.42
[stars-url]: https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/starrers
[issues-shield]: https://badgen.net/gitlab/open-issues/SiLA2/universal-sila-client/sila_universal_client/?scale=1.42
[issues-url]: https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/issues
[license-shield]: https://badgen.net/gitlab/license/SiLA2/universal-sila-client/sila_universal_client/?scale=1.42
[license-url]: https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/blob/master/LICENSE
[open-mr-shield]: https://badgen.net/gitlab/open-mrs/SiLA2/universal-sila-client/sila_universal_client/?scale=1.42
[open-mr-url]: https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/merge_requests
[last-commit-shield]: https://badgen.net/gitlab/last-commit/SiLA2/universal-sila-client/sila_universal_client/?scale=1.42
[last-commit-url]: https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/commits/master
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/groups/3941804/
