FROM openjdk:11-jre-slim
VOLUME /tmp

# Expose port 8080 for HTTP traffic
EXPOSE 8080
# Expose port 50051 for cloud connectivity gRPC traffic
EXPOSE 50051
# Expose port 5353 for mDNS traffic
# Used to discover SiLA servers

EXPOSE 5353/udp
ADD /back/target/back-0.7.0-SNAPSHOT-app.jar usc.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/usc.jar"]
