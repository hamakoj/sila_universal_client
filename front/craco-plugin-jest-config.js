
module.exports = {
    overrideJestConfig: ({ jestConfig, cracoConfig, pluginOptions, context: { env, paths, resolve, rootDir } }) => {
        const transformEntries = Object.entries(jestConfig.transform);
        const fileTransformEntryIndex = transformEntries.findIndex((entry) => entry[1].endsWith("fileTransform.js"));
        if (fileTransformEntryIndex === -1) {
            throw new Error("Unable to locate Jest fileTransform.js entry")
        }
        // must be before fileTransform
        transformEntries.splice(fileTransformEntryIndex, 0, ["^.+\\.proto$", "jest-raw-loader"])

        return {
            ...jestConfig,
            "verbose": true,
            "preset": "jest-puppeteer",
            "roots": [
                ...jestConfig.roots,
            ],
            "transformIgnorePatterns": [
                ...jestConfig.transformIgnorePatterns,
                "node_modules/(?!@ngrx|(?!deck.gl)|ng-dynamic)",
                "node_modules/puppeteer/.+\\.js$"
            ],
            "transform": Object.fromEntries(transformEntries),
            setupFiles: [
                "<rootDir>/setupJestEnvVars.js"
            ]
        }
    }
};