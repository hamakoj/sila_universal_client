const textEncoding = require("text-encoding");

global.TextDecoder = textEncoding.TextDecoder
global.TextEncoder = textEncoding.TextEncoder

process.env.NODE_ENV = 'test'