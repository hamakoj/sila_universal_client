module.exports = {
    overrideWebpackConfig: ({ webpackConfig, cracoConfig, pluginOptions, context: { env, paths } }) => {
        return {
            ...webpackConfig,
            module: {
                ...webpackConfig?.module,
                rules: [
                    ...webpackConfig?.module?.rules,
                    {
                        test: /\.proto/,
                        type: 'asset/source', // https://webpack.js.org/guides/asset-modules/#general-asset-type
                    },
                ]
            },
            resolve: {
                ...webpackConfig?.resolve,
                fallback: {
                    ...webpackConfig?.resolve?.fallback,
                    "http": false,
                    "https": false,
                    "os": false,
                }
            }
        }
    }
};