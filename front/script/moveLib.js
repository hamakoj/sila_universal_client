const fs = require('fs');
const path = require('path');

const libPath = path.resolve(__dirname, '../lib/');
const rootPath = path.resolve(__dirname, '../');

const copyRecursiveSync = function(src, dest) {
    const exists = fs.existsSync(src);
    const stats = exists && fs.statSync(src);
    const isDirectory = exists && stats.isDirectory();
    if (isDirectory) {
        fs.mkdirSync(dest, { recursive: true } );
        fs.readdirSync(src).forEach(function(childItemName) {
            copyRecursiveSync(path.join(src, childItemName),
                path.join(dest, childItemName));
        });
    } else {
        fs.copyFileSync(src, dest);
    }
};

fs.copyFileSync(rootPath + '/package.json', libPath + "/package.json", );
fs.copyFileSync(rootPath + '/package-lock.json', libPath + "/package-lock.json");
copyRecursiveSync(libPath + '/src/', rootPath + '/lib')
fs.rmSync(libPath + '/src/', { recursive: true, force: true });
