const protoToTs = require('@pbts/core');
const fs = require('fs');
const path = require('path');

const protoFolder = path.dirname(__dirname) + '/src/generated/';
const tsOutFolder = protoFolder;
const protoDirectory = fs.readdirSync(protoFolder);
const protoFiles = protoDirectory.filter((elm) => (elm.match(/.*\.(proto?)/ig)));
let silaFrameWorkInclude = '';
let binaryTransferInclude = '';

protoFiles.map((fileName) => {
    console.log('Converting ' + protoFolder + fileName + ' to typescript model')
    const fileContent = fs.readFileSync(protoFolder + fileName).toString();
    let tsProtoModel = protoToTs.parseProto(fileContent.replace(/ int64 /g, ' int32 '), {isParamOptional: true});

    if (fileName !== 'SiLAFramework.proto') {
        tsProtoModel = tsProtoModel
            .replace(/sila2\.org\.silastandard.*?\.(?=[A-Z])/gm, 'Proto')
    }
    tsProtoModel = tsProtoModel
        .replace(/SILA/g, 'SiLA')
        .replace(/export enum .?(?=[A-Z])/gm, 'export enum Proto')
        .replace(/export interface ?(?=[A-Z])/gm, 'export interface Proto')
        .replace(/(?=:).*?[a-z]\./gm, ': Proto')
        .replace(/\b(?!Proto)(?!Promise<|[a-z])(?!.*\?:+)(.[a-z].*?)(?=[)>;])/gm, 'Proto$1')
    if (fileName === 'SiLAFramework.proto') {
        const iterator = tsProtoModel.matchAll(/Proto\w+(?= {)/gm);
        const exported = [];
        for (let regExpMatchArray of iterator) {
            exported.push(regExpMatchArray[0]);
        }
        silaFrameWorkInclude = 'import {' + exported.filter(i => i !== 'ProtoErrorType').join(', ') + '} from \'./SiLAFramework\';\n\n';
    }
    if (fileName === 'SiLABinaryTransfer.proto') {
        const iterator = tsProtoModel.matchAll(/Proto\w+(?= {)/gm);
        const exported = [];
        for (let regExpMatchArray of iterator) {
            exported.push(regExpMatchArray[0]);
        }
        binaryTransferInclude = 'import {' + [...new Set(exported)].join(', ') + '} from \'./SiLABinaryTransfer\';\n\n';
    }
    return [fileName, tsProtoModel]
}).map((protoTs) => {
    let contentToWrite = protoTs[1];
    if (protoTs[0] !== 'SiLAFramework.proto') {
        contentToWrite = silaFrameWorkInclude + contentToWrite;
    }
    if (protoTs[0] === 'SiLACloudConnector.proto') {
        contentToWrite = binaryTransferInclude + contentToWrite;
    }
    contentToWrite = '/* tslint:disable */\n' + '/* eslint-disable */\n' + contentToWrite;
    fs.writeFileSync(
        tsOutFolder + protoTs[0].replace('proto', 'ts'),
        contentToWrite
    );
})
