const fs = require('fs');
const path = require('path');

const srcGeneratedPath = path.resolve(__dirname, '../src/generated/');
const libGeneratedPath = path.resolve(__dirname, '../lib/src/generated/');
const copyRecursiveSync = function(src, dest) {
    const exists = fs.existsSync(src);
    const stats = exists && fs.statSync(src);
    const isDirectory = exists && stats.isDirectory();
    if (isDirectory) {
        fs.mkdirSync(dest, { recursive: true } );
        fs.readdirSync(src).forEach(function(childItemName) {
            copyRecursiveSync(path.join(src, childItemName),
                path.join(dest, childItemName));
        });
    } else if (src.endsWith(".proto")) {
        fs.copyFileSync(src, dest);
    }
};

copyRecursiveSync(srcGeneratedPath, libGeneratedPath)
