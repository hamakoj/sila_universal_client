const CracoSwcPlugin = require('craco-swc');
const logWebpackConfigPlugin = require("./craco-plugin-webpack-config");
const overrideJestConfig = require("./craco-plugin-jest-config");

module.exports = {
    plugins: [
        { plugin: logWebpackConfigPlugin, options: { preText: "Will extend the webpack config:" } },
        { plugin: overrideJestConfig, options: { preText: "Will extend the jest config:" } },
        { plugin: CracoSwcPlugin }
    ],
};