const { app, BrowserWindow, screen: electronScreen, globalShortcut } = require('electron');
const path = require('path');

const argv = process.argv;
// todo validate that argument is a valid url
// todo check remote url version with electron app version
// todo handle light/dark mode properly https://www.electronjs.org/docs/latest/tutorial/dark-mode
const backUrl = (argv.length > 1) ? argv[1] : 'http://localhost:8080';

const isDev = () => {
    const isEnvSet = 'ELECTRON_IS_DEV' in process.env;
    const getFromEnv = Number.parseInt(process.env.ELECTRON_IS_DEV, 10) === 1;

    return isEnvSet ? getFromEnv : !app.isPackaged;
}

const createMainWindow = async () => {
    let mainWindow = new BrowserWindow({
        width: electronScreen.getPrimaryDisplay().workArea.width,
        height: electronScreen.getPrimaryDisplay().workArea.height,
        show: false,
        //frame: false, // todo hide frame and make title bar in the ui directly
        autoHideMenuBar: true,
        backgroundColor: '#000000',
        webPreferences: {
            nodeIntegration: false,
            devTools: true,
        }
    });
    // @ts-ignore
    globalShortcut.register('f5', () => {
        console.log('f5 is pressed')
        mainWindow.reload()
    })
    // @ts-ignore
    globalShortcut.register('f5', () => {
        console.log('CommandOrControl+R is pressed')
        mainWindow.reload()
    })

    const startURL = isDev()
        ? 'http://localhost:3000'
        : `file://${path.join(__dirname, '../build/index.html')}`;

    await mainWindow.loadURL(startURL);
    await mainWindow.webContents.executeJavaScript(`window.localStorage.setItem('backUrl', '` + backUrl + `');`);
    await mainWindow.webContents.executeJavaScript(`window.localStorage.setItem('isDesktopApp', 'true');`);
    mainWindow.once('ready-to-show', () => mainWindow.show());

    mainWindow.reload();

    mainWindow.on('closed', () => {
        mainWindow = null;
    });

    mainWindow.webContents.on('new-window', (event, url) => {
        event.preventDefault();
        mainWindow.loadURL(url);
    });
};

app.whenReady().then(() => {
    createMainWindow();

    app.on('activate', () => {
        if (!BrowserWindow.getAllWindows().length) {
            createMainWindow();
        }
    });
});

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('will-quit', () => {
    // Unregister all shortcuts.
    globalShortcut.unregisterAll()
})