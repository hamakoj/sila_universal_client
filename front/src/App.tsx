import React from 'react';
import './App.css';
import {HashRouter} from 'react-router-dom';
import ThemeProviderComponent from "./components/ThemeProviderComponent";
import store from "./store";
import {LocalizationProvider} from "@mui/x-date-pickers";
import { AdapterLuxon } from '@mui/x-date-pickers/AdapterLuxon';
import {Provider} from "react-redux";
import RootErrorBoundary from "./components/RootErrorBoundary";
import PluginsProviderComponent from "./plugin/PluginsProvider";
import RootRoutes from "./routes/RootRoutes";
import TimezoneHandler from "./components/TimezoneHandler";

export default function App() {
    return (
        <RootErrorBoundary>
            <React.StrictMode>
                    <Provider store={store}>
                        <TimezoneHandler>
                            <LocalizationProvider dateAdapter={AdapterLuxon}>
                                <HashRouter basename="/">
                                    <ThemeProviderComponent>
                                        <PluginsProviderComponent>
                                            <RootRoutes/>
                                        </PluginsProviderComponent>
                                    </ThemeProviderComponent>
                                </HashRouter>
                            </LocalizationProvider>
                        </TimezoneHandler>
                    </Provider>
            </React.StrictMode>
        </RootErrorBoundary>
    );
}