import {
    SiLAJavaDataTypeType, UscCommand,
    UscDataTypeDefinition, UscFeature,
    UscMetadata,
    UscParameter, UscProperty,
    UscResponse, UscServerStatus
} from "../generated/silaModels";
import {
    SiLATypeValueState
} from "../store/front/server/callInput/types";
import {FunctionComponent} from "react";
import {BinaryProps} from "../components/server/call/dataTypeForm/type/basic/Binary";
import {AnyProps} from "../components/server/call/dataTypeForm/type/basic/Any";
import {BooleanProps} from "../components/server/call/dataTypeForm/type/basic/Boolean";
import {DateProps} from "../components/server/call/dataTypeForm/type/basic/Date";
import {IntegerProps} from "../components/server/call/dataTypeForm/type/basic/Integer";
import {RealProps} from "../components/server/call/dataTypeForm/type/basic/Real";
import {StringProps} from "../components/server/call/dataTypeForm/type/basic/String";
import {TimeProps} from "../components/server/call/dataTypeForm/type/basic/Time";
import {TimestampProps} from "../components/server/call/dataTypeForm/type/basic/Timestamp";
import {BasicProps} from "../components/server/call/dataTypeForm/type/Basic";
import {ServerCallRequestProps} from "../components/server/call/request/ServerCallRequest";
import {DataTypeProps} from "../components/server/call/dataTypeForm/type/DataType";
import {DataTypeIdentifierProps} from "../components/server/call/dataTypeForm/type/DataTypeIdentifier";
import {StructureProps} from "../components/server/call/dataTypeForm/type/Structure";
import {ConstraintHelpTextProps} from "../components/server/call/dataTypeForm/type/constraints/ConstraintHelpText";
import {NumberUnitConstraintProps} from "../components/server/call/dataTypeForm/type/constraints/NumberUnitConstraint";
import {DataTypeFormRootProps} from "../components/server/call/dataTypeForm/DataTypeFormRoot";
import {DataTypeFormNodeProps} from "../components/server/call/dataTypeForm/DataTypeFormNode";
import {ServerCallRequestMetadatasProps} from "../components/server/call/request/ServerCallRequestMetadatas";
import {
    ServerCallRequestParameterProps
} from "../components/server/call/request/ServerCallRequestParameter";
import {ListProps} from "../components/server/call/dataTypeForm/type/List";
import { CallResponseProps } from "../components/server/call/response/CallResponse";
import { CallResponseErrorProps } from "../components/server/call/response/CallResponseError";
import { CallResponseExecInfoProps } from "../components/server/call/response/CallResponseExecInfo";
import {
    CallResponseIntermediateResponsesProps
} from "../components/server/call/response/CallResponseIntermediateResponses";
import {CallResponseMetadatasProps} from "../components/server/call/response/CallResponseMetadatas";
import {
    CallResponseObservablePropertyValuesProps
} from "../components/server/call/response/CallResponseObservablePropertyValues";
import {CallResponseParametersProps} from "../components/server/call/response/CallResponseParameters";
import { CallResponseResponsesProps } from "../components/server/call/response/CallResponseResponses";
import { CallResponsesProps } from "../components/server/call/response/CallResponses";
import {ServerCallComponentProps} from "../components/server/call/ServerCallComponent";
import {ServerCallsProps} from "../components/server/call/ServerCalls";
import {ServerCardProps} from "../components/server/ServerCard";
import {ServerCardSmallProps} from "../components/server/ServerCardSmall";
import {DateTime} from "luxon";
import {ProtoSiLAError} from "../generated/SiLAFramework";
import {FullyQualifiedCallId} from "../store/front/server/activeCall/types";
import {CommandResponse} from "../store/front/server/response/types";

export default interface PluginImplementation {
    // basic types
    PluginAnyTypeForm ?: FunctionComponent<PluginAnyTypeFormProps>,
    PluginBinaryTypeForm ?: FunctionComponent<PluginBinaryTypeFormProps>,
    PluginBooleanTypeForm ?: FunctionComponent<PluginBooleanTypeFormProps>,
    PluginDateTypeForm ?: FunctionComponent<PluginDateTypeFormProps>,
    PluginIntegerTypeForm ?: FunctionComponent<PluginIntegerTypeFormProps>,
    PluginRealTypeForm ?: FunctionComponent<PluginRealTypeFormProps>,
    PluginStringTypeForm ?: FunctionComponent<PluginStringTypeFormProps>,
    PluginTimeTypeForm ?: FunctionComponent<PluginTimeTypeFormProps>,
    PluginTimestampTypeForm ?: FunctionComponent<PluginTimestampTypeFormProps>,
    // constraints
    PluginConstraintHelpText ?: FunctionComponent<PluginConstraintHelpTextProps>,
    PluginNumberUnitConstraint ?: FunctionComponent<PluginNumberUnitConstraintProps>,
    // data types
    PluginBasicTypeForm ?: FunctionComponent<PluginBasicTypeFormProps>,
    PluginDataTypeTypeForm ?: FunctionComponent<PluginDataTypeTypeFormProps>,
    PluginDataTypeIdentifierTypeForm ?: FunctionComponent<PluginDataTypeIdentifierTypeFormProps>,
    PluginListTypeForm ?: FunctionComponent<PluginListTypeFormProps>,
    PluginStructureTypeForm ?: FunctionComponent<PluginStructureTypeFormProps>,
    // data type form
    PluginDataTypeFormRoot ?: FunctionComponent<PluginDataTypeFormRootProps>,
    PluginDataTypeFormNode?: FunctionComponent<PluginDataTypeFormNodeProps>,
    // call request
    PluginServerCallRequestBody ?: FunctionComponent<PluginServerCallRequestBodyProps>
    PluginServerCallRequestMetadatas ?: FunctionComponent<PluginServerCallRequestMetadatasProps>
    PluginServerCallRequestParameter ?: FunctionComponent<PluginServerCallRequestParameterProps>
    // call response
    PluginCallResponse ?: FunctionComponent<PluginCallResponseProps>
    PluginCallResponseError ?: FunctionComponent<PluginCallResponseErrorProps>
    PluginCallResponseExecInfo ?: FunctionComponent<PluginCallResponseExecInfoProps>
    PluginCallResponseIntermediateResponses ?: FunctionComponent<PluginCallResponseIntermediateResponsesProps>
    PluginCallResponseMetadatas ?: FunctionComponent<PluginCallResponseMetadatasProps>
    PluginCallResponseObservablePropertyValues ?: FunctionComponent<PluginCallResponseObservablePropertyValuesProps>
    PluginCallResponseParameters ?: FunctionComponent<PluginCallResponseParametersProps>
    PluginCallResponseResponses ?: FunctionComponent<PluginCallResponseResponsesProps>
    PluginCallResponses ?: FunctionComponent<PluginCallResponsesProps>
    // server call
    PluginServerCallComponent ?: FunctionComponent<PluginServerCallComponentProps>
    PluginServerCalls ?: FunctionComponent<PluginServerCallsProps>
    // server
    PluginServerCardBody ?: FunctionComponent<PluginServerCardBodyProps>
    PluginServerCardSmallBody ?: FunctionComponent<PluginServerCardSmallBodyProps>
    // todo below
    //  sidebar
    //  websocket
    //  redux store
    //  sidebar
    //  theme ?
    //  route
    //  global provider ?
    //  pages
    //   settings...

    // template Plugin ?: FunctionComponent<PluginProps>
};

export interface PluginAnyTypeFormProps extends AnyProps {
    parsedAnyDataType: SiLAJavaDataTypeType | null;
}

export interface PluginBinaryTypeFormProps extends BinaryProps {

}

export interface PluginBooleanTypeFormProps extends BooleanProps {

}

export interface PluginDateTypeFormProps extends DateProps {
    utcOffset: string
}

export interface PluginIntegerTypeFormProps extends IntegerProps {

}

export interface PluginRealTypeFormProps extends RealProps {

}

export interface PluginStringTypeFormProps extends StringProps {

}

export interface PluginTimeTypeFormProps extends TimeProps {
    utcOffset: string
}

export interface PluginTimestampTypeFormProps extends TimestampProps {
    utcOffset: string
}

export interface PluginBasicTypeFormProps extends BasicProps {

}

export interface PluginDataTypeTypeFormProps extends DataTypeProps {

}

export interface PluginDataTypeIdentifierTypeFormProps extends DataTypeIdentifierProps {

}

export interface PluginListTypeFormProps extends ListProps {

}

export interface PluginStructureTypeFormProps extends StructureProps {

}

export interface PluginConstraintHelpTextProps extends ConstraintHelpTextProps {

}

export interface PluginNumberUnitConstraintProps extends NumberUnitConstraintProps {

}

export interface PluginDataTypeFormRootProps extends DataTypeFormRootProps {

}

export interface PluginDataTypeFormNodeProps extends DataTypeFormNodeProps {

}

export interface PluginServerCallRequestBodyProps extends ServerCallRequestProps {
    parameters: SiLATypeValueState,
    metadatas: SiLATypeValueState | undefined,
    dataTypeDefinitions: UscDataTypeDefinition[],
    parametersDefinition: UscParameter[],
    metadatasDefinition: UscMetadata[],
    serverId: string,
    fullyQualifiedCallId: string
}

export interface PluginServerCallRequestMetadatasProps extends ServerCallRequestMetadatasProps {

}

export interface PluginServerCallRequestParameterProps extends ServerCallRequestParameterProps {

}

export interface PluginCallResponseProps extends CallResponseProps {
    execDate: DateTime;
}

export interface PluginCallResponseErrorProps extends CallResponseErrorProps {
    silaError: ProtoSiLAError | undefined;
}

export interface PluginCallResponseExecInfoProps extends CallResponseExecInfoProps {

}

export interface PluginCallResponseIntermediateResponsesProps extends CallResponseIntermediateResponsesProps {

}

export interface PluginCallResponseMetadatasProps extends CallResponseMetadatasProps {

}

export interface PluginCallResponseObservablePropertyValuesProps extends CallResponseObservablePropertyValuesProps {

}

export interface PluginCallResponseParametersProps extends CallResponseParametersProps {

}

export interface PluginCallResponseResponsesProps extends CallResponseResponsesProps {

}

export interface PluginCallResponsesProps extends CallResponsesProps {
    responses: CommandResponse[],
    callResponses: UscResponse[];
    callMetadatas: UscMetadata[];
    callUrl: string;
}

export interface PluginServerCallComponentProps extends ServerCallComponentProps {

}

export interface PluginServerCallsProps extends ServerCallsProps {

}

export interface PluginServerCardBodyProps extends ServerCardProps {
    bookmarked: boolean;
    serverInitials: string;
}

export interface PluginServerCardSmallBodyProps extends ServerCardSmallProps {
    name: string;
    status: UscServerStatus;
}

