import React, {useContext, FunctionComponent} from "react";
import {PluginContextType, PluginsContext} from "./PluginsProvider";
import PluginImplementation from "./PluginImplementation";

export interface PluginsComponentRendererProps<T extends Extract<PluginImplementation[keyof PluginImplementation], FunctionComponent<any>>> {
    forwardProps: React.ComponentProps<T>;
    componentName: keyof PluginImplementation;
}

export const PluginsComponentRenderer = React.memo(<T extends Extract<PluginImplementation[keyof PluginImplementation], FunctionComponent<any>>>(props: PluginsComponentRendererProps<T>): JSX.Element => {
    const pluginsContext = useContext<PluginContextType>(PluginsContext);
    const components = Object.entries(pluginsContext)
        .map(([url, plugin]) => [url, plugin[props.componentName]])
        .filter(([, component]) => !!component)
        .map(([url, component]) => {
            if (component === undefined) {
                return null;
            }
            const key = url + '/' + props.componentName;
            const Component: FunctionComponent<T> = component as unknown as FunctionComponent<T>;
            const keyProps: any = {key: key, id: key}
            return (
                <Component
                    {...keyProps}
                    {...props.forwardProps}
                />
            );
        });
    return (
        <>
            {
                components
            }
        </>
    );
});

export default PluginsComponentRenderer;