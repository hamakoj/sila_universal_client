import packageJson from '../../package.json';
import {DependencyTable} from "@paciolan/remote-component/dist/createRequires";
import {resolve} from "./remote-component.config";

export function checkCommonDependencies(): DependencyTable {
    const excludeDeps = ['react-scripts', '@paciolan/remote-component'];
    //dependencies must be hard coded since webpack cannot resolve dynamic import
    const configDeps = Object.keys(resolve);
    const packageDeps = Object.keys(packageJson.peerDependencies).filter(dep => !excludeDeps.includes(dep));
    const difference = configDeps
        .filter(x => !packageDeps.includes(x))
        .concat(packageDeps.filter(x => !configDeps.includes(x)));
    if (difference.length > 0) {
        throw new Error("Peer dependencies does not match dependencies: " + JSON.stringify(difference));
    }

    return resolve;
}

test('validate common dependencies', () => {
    checkCommonDependencies();
});
