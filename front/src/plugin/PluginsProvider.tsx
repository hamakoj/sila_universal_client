import React, { useEffect, useMemo, useState} from 'react';
import {useSelector} from "react-redux";
import {getDevSelector} from "../store/front/app/dev/selector";
import {useRemoteComponent} from "./useRemoteComponent";
import {getPluginEndpoint} from "../utils/endpoint";
import {PluginModel} from "../generated/silaModels";
import {getAppPluginsSelector} from "../store/front/app/plugin/selector";
import PluginImplementation from "./PluginImplementation";

export const PluginsContext = React.createContext<PluginContextType>({});
export const PluginsProvider = PluginsContext.Provider;

export type PluginLoadedCallback = (url: string, pi: PluginImplementation) => void;
export type PluginContextType = {[url: string]: PluginImplementation};

export interface PluginIndexProps {
    jsEntryPoint: string;
    onLoadSuccessful: PluginLoadedCallback;
}

const PluginIndex = React.memo((props: PluginIndexProps): JSX.Element | null => {
    const [loading, err, Component] = useRemoteComponent(props.jsEntryPoint, 'Index');

    useEffect(() => {
        if (loading || err || !Component) {
            if (!loading) {
                console.log('Failed to load plugin ' + props.jsEntryPoint, err)
            } else {
                console.log('Loading plugin ' + props.jsEntryPoint)
            }
            return ;
        }
        props.onLoadSuccessful(props.jsEntryPoint, Component as PluginImplementation);
    }, [props, loading, err, Component]);

    return null;
}, () => true);

function getPluginsIndex(urls: Set<string>, onLoadSuccessful: PluginLoadedCallback): JSX.Element[] {
    return Array.from(urls).map((url) => (
        <PluginIndex key={url} jsEntryPoint={url} onLoadSuccessful={onLoadSuccessful}/>
    ));
}

function getPluginsUrl(appPlugins: PluginModel[], devUrls: string[]): Set<string> {
    const urls: string[] = appPlugins
        ?.filter(p => p?.jsEntryPoint?.length > 0)
        .map(p => getPluginEndpoint(p.jsEntryPoint)) || [];
    const concatenatedUrlSet = new Set([...devUrls, ...urls]);
    if ((urls.length + devUrls.length) !== concatenatedUrlSet.size) {
        // todo handle non unique items;
    }
    return concatenatedUrlSet;
}

export interface PluginsProviderComponentProps {
    children: JSX.Element;
}

const PluginsProviderComponent = React.memo((props: PluginsProviderComponentProps): JSX.Element => {
    const dev = useSelector(getDevSelector);
    const appPlugins = useSelector(getAppPluginsSelector);
    const [plugins, setPlugins] = useState<PluginContextType>({});
    const [pluginsIndex, setPluginsIndex] = useState<JSX.Element[]>([]);
    const child = useMemo(() => props.children, [props.children]);

    const onLoadSuccessful = (url: string, component: PluginImplementation) => {
        setPlugins(prevPlugins => ({ ...prevPlugins, [url]: component}))
    };

    useEffect(() => {
        const devUrls = Object.keys(dev.frontPlugins).map((url) => (
            // todo clean up extra dash
            url.endsWith('.js') ? url : (url + 'main.js')
        ));
        const pluginsUrl = getPluginsUrl(appPlugins.plugins, devUrls);
        setPluginsIndex(getPluginsIndex(pluginsUrl, onLoadSuccessful));
    }, [appPlugins.plugins, dev.frontPlugins]);

    return (
        /* pluginsIndex is provided as a child to force react to keep it, otherwise plugins wouldn't be loaded properly */
        <PluginsProvider value={plugins}>
            {pluginsIndex}
            {child}
        </PluginsProvider>
    )

}, () => true);

export default PluginsProviderComponent;
