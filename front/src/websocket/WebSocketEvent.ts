enum WebSocketEvent {
    ON_CONNECTION = 'ON_CONNECTION',
    ON_ERROR = 'ON_ERROR'
}

export default WebSocketEvent;