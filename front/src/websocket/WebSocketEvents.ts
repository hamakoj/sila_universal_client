import {Client, Frame} from "@stomp/stompjs";
import WebSocketEvent from "./WebSocketEvent";

export default interface WebSocketsEvents {
    [WebSocketEvent.ON_CONNECTION]: (client: Client, frame: Frame | undefined) => void,
    [WebSocketEvent.ON_ERROR]: (error: string | Frame) => void,
}
