import {Client, Frame, Stomp} from "@stomp/stompjs";
import {createNanoEvents, Emitter} from "nanoevents";
import SockJS from "sockjs-client";
import WebSocketEvent from "./WebSocketEvent";
import WebSocketsEvents from "./WebSocketEvents";
import {IFrame} from "@stomp/stompjs/src/i-frame";
import {getWebsocketEndpoint} from "../utils/endpoint";
import {getRandomId} from "../utils/idGenerator";

// Class is static because of issues when using 'this', object is destroyed or going out of scope, not sure...
export default class Websocket {
    private static _client: Client | null = null;
    private static readonly _eventEmitter: Emitter<WebSocketsEvents> = createNanoEvents<WebSocketsEvents>()
    private static _initialized: boolean = false;

    static get client(): Client | null {
        return Websocket._client;
    }

    static get eventEmitter(): Emitter<WebSocketsEvents> {
        return Websocket._eventEmitter;
    }

    public static init() {
        if (!Websocket._initialized) {
            Websocket._initialized = true;
            console.log('%c Initializing websocket', 'background: #0095ff; color: #ffffff')
            this.createStompClient();
        } else {
            console.error('Attempt to initialize already initialized websocket')
        }
    }

    // todo re-fetch servers on disconnect to sync servers ?
    private static stompFailureCallback(error: string | Frame): void {
        Websocket._eventEmitter.emit(WebSocketEvent.ON_ERROR, error)
        console.log('STOMP: Disconnected...');
    }

    private static onConnectCallback(client: Client, frame: Frame | undefined): void {
        Websocket._client = client;
        Websocket._eventEmitter.emit(WebSocketEvent.ON_CONNECTION, client, frame)
    }

    private static createWebSocket(): Websocket {
        const options: SockJS.Options = {sessionId: () => getRandomId()};
        return new SockJS(getWebsocketEndpoint(), null, options);
    }

    private static createStompClient(): void {
        const stompClient: Client = Stomp.over(Websocket.createWebSocket);
        stompClient.heartbeatIncoming = 0;
        stompClient.heartbeatOutgoing = 10000;
        stompClient.reconnectDelay = 5000;
        stompClient.onConnect = (f: IFrame) => {
            console.log('STOMP: Connected!');
            Websocket.onConnectCallback(stompClient, f);
        };
        stompClient.onWebSocketError = (f: IFrame) => {
            console.error('WebSocket: error')
            console.error(f)
        };
        stompClient.onWebSocketClose = (f: IFrame) => {
            Websocket.stompFailureCallback(f)
        };
        stompClient.onStompError = (f: IFrame) => {
            console.error('STOMP: error')
            console.error(f)
        };
        stompClient.activate();
    }
}

Websocket.init();
