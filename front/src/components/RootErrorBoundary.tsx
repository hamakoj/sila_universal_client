import React, {Component, ErrorInfo, ReactNode} from "react";
import {Button, Grid, TextField, Typography} from "@mui/material";
import Git from "../generated/git.json";
import Package from "../../package.json";
import {UscVersion} from "../generated/silaModels";
import {getBackEndpoint} from "../utils/endpoint";
import {removePersistedUscState} from "../store";

interface Props {
    children: ReactNode;
}

interface State {
    hasError: boolean;
    error?: Error;
    errorInfo?: ErrorInfo;
    backVersion?: UscVersion | null;
}

class RootErrorBoundary extends Component<Props, State> {

    public state: State = {
        hasError: false
    };

    componentDidMount() {
        window?.fetch?.(getBackEndpoint('/api/version'))
            .then(response => response.json())
            .then((version) => {
                this.setState((prev) => ({...prev, backVersion: version}));
            })
            .catch((error) => {
            })
    }

    public static getDerivedStateFromError(error: Error): State {
        // Update state so the next render will show the fallback UI.
        return {hasError: true, error};
    }

    public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
        console.error("Uncaught error:", error, errorInfo);
        //return {hasError: true, error, errorInfo};
    }

    public render() {
        if (this.state.hasError) {
            const errorLocation = window.location.hash;
            const params: { [param: string]: string } = {
                'issue[title]': `[Front report][${this.state.error?.name}] ${this.state.error?.message}`.substring(0, 128),
                'issue[description]':
                    `Error location \n` +
                    `\`\`\`\n` +
                    `${errorLocation}\n` +
                    `\`\`\`\n` +
                    `\n` +
                    `Error name \n` +
                    `\`\`\`\n` +
                    `${this.state.error?.name}\n` +
                    `\`\`\`\n` +
                    `\n` +
                    `Error message \n` +
                    `\`\`\`\n` +
                    `${this.state.error?.message}\n` +
                    `\`\`\`\n` +
                    `\n` +
                    `Error stack \n` +
                    `\`\`\`\n` +
                    `${this.state.error?.stack}\n` +
                    `\`\`\`\n` +
                    `\n` +
                    `Front version \n` +
                    `\`\`\`\n` +
                    `${JSON.stringify(Git)}\n` +
                    `\`\`\`\n` +
                    `\n` +
                    `Back version \n` +
                    `\`\`\`\n` +
                    `${JSON.stringify(this.state.backVersion || 'unknown')}\n\`` +
                    `\`\`\`\n` +
                    `\n`
            };
            const encodedParams = Object.entries(params).map(kv => kv.map(encodeURIComponent).join("=")).join("&");
            return (
                <Grid container spacing={1} paddingX={4} rowSpacing={2} marginTop={1} maxWidth={'1024px'} color={"#97999e"}>
                    <Grid item xs={12}>
                        <Typography variant={'h1'}>Oops!</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant={'h2'}>There's an error</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Button variant="contained" color="primary" onClick={() => {
                            window.open('https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/issues/new?' + encodedParams)
                            //window.open('https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/issues/new?issue[title]=Validate%20new%20concept&issue[description]=Research%20idea')
                        }}>
                            Report error
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Button variant="contained" color="primary" onClick={() => {
                            window.location.reload();
                        }}>
                            Reload page
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography>You can try to clear the local cache in case the error always occurs</Typography>
                        <Button variant="contained" color="primary" onClick={() => {
                            removePersistedUscState();
                            window.location.reload();
                        }}>
                            Clear local cache & Reload page
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="Error location"
                            fullWidth
                            color="primary"
                            focused
                            InputProps={{
                                sx: {color: 'inherit'},
                                readOnly: true
                            }}
                            value={errorLocation}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="Error name"
                            fullWidth
                            color="primary"
                            focused
                            InputProps={{
                                sx: {color: 'inherit'},
                                readOnly: true
                            }}
                            value={this.state.error?.name}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="Error message"
                            multiline
                            fullWidth
                            color="primary"
                            focused
                            InputProps={{
                                sx: {color: 'inherit'},
                                readOnly: true
                            }}
                            value={this.state.error?.message}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="Error stack"
                            multiline
                            fullWidth
                            color="primary"
                            focused
                            InputProps={{
                                sx: {color: 'inherit'},
                                readOnly: true
                            }}
                            value={this.state.error?.stack}
                        />
                    </Grid>
                    <Grid item xs={12} md={1.25}>
                        <TextField
                            label="Front version"
                            fullWidth
                            color="primary"
                            focused
                            InputProps={{
                                sx: {color: 'inherit'},
                                readOnly: true
                            }}
                            value={Package.version}
                        />
                    </Grid>
                    <Grid item xs={12} md={3}>
                        <TextField
                            label="Front Git branch"
                            fullWidth
                            color="primary"
                            focused
                            InputProps={{
                                sx: {color: 'inherit'},
                                readOnly: true
                            }}
                            value={Git["git.branch"]}
                        />
                    </Grid>
                    <Grid item xs={12} md={5.5}>
                        <TextField
                            label="Front Git commit"
                            fullWidth
                            color="primary"
                            focused
                            InputProps={{
                                sx: {color: 'inherit'},
                                readOnly: true
                            }}
                            value={Git["git.commit.id"]}
                        />
                    </Grid>
                    <Grid item xs={12} md={1.25}>
                        <TextField
                            label="Front Git closest tag"
                            fullWidth
                            color="primary"
                            focused
                            InputProps={{
                                sx: {color: 'inherit'},
                                readOnly: true
                            }}
                            value={Git["git.closest.tag.name"]}
                        />
                    </Grid>
                    <Grid item xs={12} md={1}>
                        <TextField
                            label="Front Git dirty"
                            fullWidth
                            color="primary"
                            focused
                            InputProps={{
                                sx: {color: 'inherit'},
                                readOnly: true
                            }}
                            value={Git["git.dirty"]}
                        />
                    </Grid>
                    {
                        (this.state.backVersion) ? (
                            <>
                                <Grid item xs={12} md={1.25}>
                                    <TextField
                                        label="Back version"
                                        fullWidth
                                        color="primary"
                                        focused
                                        InputProps={{
                                            sx: {color: 'inherit'},
                                            readOnly: true
                                        }}
                                        value={this.state.backVersion.buildVersion}
                                    />
                                </Grid>
                                <Grid item xs={12} md={3}>
                                    <TextField
                                        label="Back Git branch"
                                        fullWidth
                                        color="primary"
                                        focused
                                        InputProps={{
                                            sx: {color: 'inherit'},
                                            readOnly: true
                                        }}
                                        value={this.state.backVersion.branch}
                                    />
                                </Grid>
                                <Grid item xs={12} md={5.5}>
                                    <TextField
                                        label="Back Git commit"
                                        fullWidth
                                        color="primary"
                                        focused
                                        InputProps={{
                                            sx: {color: 'inherit'},
                                            readOnly: true
                                        }}
                                        value={this.state.backVersion.commitId}
                                    />
                                </Grid>
                                <Grid item xs={12} md={1.25}>
                                    <TextField
                                        label="Back Git closest tag"
                                        fullWidth
                                        color="primary"
                                        focused
                                        InputProps={{
                                            sx: {color: 'inherit'},
                                            readOnly: true
                                        }}
                                        value={this.state.backVersion.closestTag}
                                    />
                                </Grid>
                                <Grid item xs={12} md={1}>
                                    <TextField
                                        label="Back Git dirty"
                                        fullWidth
                                        color="primary"
                                        focused
                                        InputProps={{
                                            sx: {color: 'inherit'},
                                            readOnly: true
                                        }}
                                        value={this.state.backVersion.dirty}
                                    />
                                </Grid>
                            </>
                        ) : (
                            <Grid item xs={12}>
                                <TextField
                                    label="Unknown back information"
                                    fullWidth
                                    color="primary"
                                    focused
                                    InputProps={{
                                        sx: {color: 'inherit'},
                                        readOnly: true
                                    }}
                                    value={"Unknown back information"}
                                />
                            </Grid>
                        )
                    }
                </Grid>
            )
        }

        return this.props.children;
    }
}

export default RootErrorBoundary;
