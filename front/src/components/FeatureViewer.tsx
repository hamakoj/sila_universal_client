import React, {useState} from 'react';
import {
    Box,
    Collapse,
    IconButton,
    Paper,
    Tab,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tabs,
    TextField,
    Typography
} from '@mui/material/';
import FeatureParser from "../utils/parser/featureParser";
import {
    SiLAJavaCommand,
    SiLAJavaDataTypeType,
    SiLAJavaDefinedExecutionError,
    SiLAJavaFeature,
    SiLAJavaMetadata,
    SiLAJavaProperty,
    SiLAJavaSiLAElement
} from "../generated/silaModels";
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import {TreeItem, TreeView} from "@mui/lab";

function getDataTypeString(type: SiLAJavaDataTypeType): string {
    if (type.basic) {
        return type.basic;
    }
    if (type.dataTypeIdentifier) {
        return "Data type identifier";
    }
    if (type.constrained) {
        return "Constrained";
    }
    if (type.structure) {
        return "Structure";
    }
    if (type.list) {
        return "List";
    }
    return "None";
}

function RichObjectTreeView(props: {elements: SiLAJavaSiLAElement[], typeDefinitions: SiLAJavaSiLAElement[]}) {
    const renderType = (nodes: {id: string, type: SiLAJavaDataTypeType}): JSX.Element => {
        let childs: JSX.Element[] = [];

        if (nodes.type.structure) {
            childs = nodes.type.structure.element.map((element): JSX.Element => (
                renderType({id: nodes.id + '/' + element.identifier, type: element.dataType})
            ));
        } else if (nodes.type.list) {
            childs = [renderType({id: nodes.id + '/list', type: nodes.type.list.dataType})];
        } else if (nodes.type.constrained) {
            childs = [renderType({id: nodes.id + '/constrained', type: nodes.type.constrained.dataType})];
        } else if (nodes.type.dataTypeIdentifier) {
            const type = props.typeDefinitions.find(t => t.identifier === nodes.type.dataTypeIdentifier)?.dataType;
            if (!type) {
                return (
                    <TreeItem key={nodes.id} nodeId={nodes.id} label={'Unknown data type identifier (' + nodes.type.dataTypeIdentifier + ')'}/>
                )
            }
            childs = [renderType({id: nodes.id + '/' + nodes.type.dataTypeIdentifier, type: type})];
        } else if (nodes.type.basic) {
            return (
                <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.type.basic}/>
            )
        } else {
            return (
                <TreeItem key={nodes.id} nodeId={nodes.id} label={'Unknown type'}/>
            )
        }
        return (
            <TreeItem key={nodes.id} nodeId={nodes.id} label={getDataTypeString(nodes.type)}>
            {
                childs.map((e) => e)
            }
            </TreeItem>
        )
    };

    const renderElement = (nodes: {id: string, element: SiLAJavaSiLAElement}): JSX.Element => {
        return (
            <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.element.displayName}>
                {
                    renderType({id: nodes.id + '/' + nodes.element.identifier, type: nodes.element.dataType})
                }
            </TreeItem>
        )
    };

    return (
        <TreeView
            aria-label="rich object"
            defaultCollapseIcon={<ExpandMoreIcon />}
            defaultExpanded={['root']}
            defaultExpandIcon={<ChevronRightIcon />}
            sx={{ height: 'auto', flexGrow: 1, overflowY: 'auto' }}
        >
            {props.elements.map((e) => renderElement({id: e.identifier, element: e}))}
        </TreeView>
    );
}

function TabPanel(props: {
    children: JSX.Element,
    index: number,
    value: number,
}) {
    const { children, value, index, ...other } = props;

    return (
        <Box
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            sx={{
                width: "100%"
            }}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    {children}
                </Box>
            )}
        </Box>
    );
}

function PropertyRow(props: {
    property: SiLAJavaProperty,
    typeDefinitions: SiLAJavaSiLAElement[]
}) {
    const [open, setOpen] = React.useState<boolean>(false);

    return (
        <>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell>
                    <IconButton
                        aria-label="expand row"
                        size="small"
                        onClick={() => setOpen((prev: boolean) => !prev)}
                    >
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    {props.property.displayName}
                </TableCell>
                <TableCell align="right">{props.property.observable}</TableCell>
                <TableCell align="right">{getDataTypeString(props.property.dataType)}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box sx={{ margin: 1 }}>
                            <Typography>{props.property.description}</Typography>
                            <RichObjectTreeView elements={[props.property]} typeDefinitions={props.typeDefinitions}/>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
}

function CommandRow(props: {
    command: SiLAJavaCommand,
    typeDefinitions: SiLAJavaSiLAElement[]
}) {
    const [open, setOpen] = React.useState<boolean>(false);

    return (
        <>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell>
                    <IconButton
                        aria-label="expand row"
                        size="small"
                        onClick={() => setOpen(!open)}
                    >
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    {props.command.displayName}
                </TableCell>
                <TableCell align="right">{props.command.observable}</TableCell>
                <TableCell align="right">{props.command.parameter.length}</TableCell>
                <TableCell align="right">{props.command.intermediateResponse.length}</TableCell>
                <TableCell align="right">{props.command.response.length}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box sx={{ margin: 1 }}>
                            <Typography>{props.command.description}</Typography>
                            <Typography>Parameters</Typography>
                            <RichObjectTreeView elements={props.command.parameter} typeDefinitions={props.typeDefinitions}/>
                            <Typography>Intermediate responses</Typography>
                            <RichObjectTreeView elements={props.command.intermediateResponse} typeDefinitions={props.typeDefinitions}/>
                            <Typography>Responses</Typography>
                            <RichObjectTreeView elements={props.command.response} typeDefinitions={props.typeDefinitions}/>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
}

function DataTypeDefinitionRow(props: {
    element: SiLAJavaSiLAElement,
    typeDefinitions: SiLAJavaSiLAElement[]
}) {
    const [open, setOpen] = React.useState(false);

    return (
        <>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell>
                    <IconButton
                        aria-label="expand row"
                        size="small"
                        onClick={() => setOpen(!open)}
                    >
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    {props.element.displayName}
                </TableCell>
                <TableCell align="right">{getDataTypeString(props.element.dataType)}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box sx={{ margin: 1 }}>
                            <Typography>{props.element.description}</Typography>
                            <RichObjectTreeView elements={[props.element]} typeDefinitions={props.typeDefinitions}/>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
}

function DefinedExecutionErrorRow(props: {
    error: SiLAJavaDefinedExecutionError
}) {
    const [open, setOpen] = React.useState(false);

    return (
        <>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell>
                    <IconButton
                        aria-label="expand row"
                        size="small"
                        onClick={() => setOpen(!open)}
                    >
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    {props.error.displayName}
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box sx={{ margin: 1 }}>
                            <Typography>{props.error.description}</Typography>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
}

function MetadataRow(props: {
    metadata: SiLAJavaMetadata,
    typeDefinitions: SiLAJavaSiLAElement[]
}) {
    const [open, setOpen] = React.useState(false);

    return (
        <>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell>
                    <IconButton
                        aria-label="expand row"
                        size="small"
                        onClick={() => setOpen(!open)}
                    >
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    {props.metadata.displayName}
                </TableCell>
                <TableCell align="right">{getDataTypeString(props.metadata.dataType)}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box sx={{ margin: 1 }}>
                            <Typography>{props.metadata.description}</Typography>
                            <RichObjectTreeView elements={[props.metadata]} typeDefinitions={props.typeDefinitions}/>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
}

function Properties(props: {properties: SiLAJavaProperty[], typeDefinitions: SiLAJavaSiLAElement[]}): JSX.Element {
    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell />
                        <TableCell>Display name</TableCell>
                        <TableCell align="right">Observable</TableCell>
                        <TableCell align="right">Root type</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.properties.map((property) => (
                        <PropertyRow key={property.identifier} property={property} typeDefinitions={props.typeDefinitions} />
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

function Commands(props: {commands: SiLAJavaCommand[], typeDefinitions: SiLAJavaSiLAElement[]}): JSX.Element {
    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell />
                        <TableCell>Display name</TableCell>
                        <TableCell align="right">Observable</TableCell>
                        <TableCell align="right">Parameters</TableCell>
                        <TableCell align="right">Intermediate responses</TableCell>
                        <TableCell align="right">Responses</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.commands.map((command) => (
                        <CommandRow key={command.identifier} command={command} typeDefinitions={props.typeDefinitions} />
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

function DataTypeDefinitions(props: {typeDefinitions: SiLAJavaSiLAElement[]}): JSX.Element {
    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell />
                        <TableCell>Display name</TableCell>
                        <TableCell align="right">Root type</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.typeDefinitions.map((element) => (
                        <DataTypeDefinitionRow key={element.identifier} element={element} typeDefinitions={props.typeDefinitions} />
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

function DefinedExecutionErrors(props: {errors: SiLAJavaDefinedExecutionError[]}): JSX.Element {
    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell />
                        <TableCell>Display name</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.errors.map((error) => (
                        <DefinedExecutionErrorRow key={error.identifier} error={error}/>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

function Metadatas(props: {metadatas: SiLAJavaMetadata[], typeDefinitions: SiLAJavaSiLAElement[]}): JSX.Element {
    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell />
                        <TableCell>Display name</TableCell>
                        <TableCell align="right">Root type</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.metadatas.map((metadata) => (
                        <MetadataRow key={metadata.identifier} metadata={metadata} typeDefinitions={props.typeDefinitions}/>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default function FeatureViewer(): JSX.Element {
    const[xml, setXml] = useState<string>('');
    const [value, setValue] = React.useState<number>(0);
    const feature: SiLAJavaFeature | null = FeatureParser.parse(xml);

    const handleChange = (event: any, newValue: number) => {
        setValue(newValue);
    };

    return (
        <Box>
            <Typography variant="h2" component="h2" gutterBottom>
                Feature Viewer
            </Typography>
            <Paper>
                <Box margin={2}>
                    <TextField
                        id="outlined-multiline-static"
                        label="Feature XML"
                        multiline
                        fullWidth
                        rows={12}
                        value={xml}
                        onChange={(e) => {
                            setXml(e.target.value);
                        }}
                    />
                </Box>
                <Box margin={2}>
                    {(!feature) ? (<Typography>The provider feature XML is invalid</Typography>) : (
                        <>
                            <Typography>{feature.displayName}</Typography>
                            <Box sx={{ flexGrow: 1, display: 'flex', height: 'auto' }}>
                                <Tabs
                                orientation="vertical"
                                variant="scrollable"
                                value={value}
                                onChange={handleChange}
                                aria-label="Vertical tabs example"
                                sx={{ borderRight: 1, borderColor: 'divider' }}
                                >
                                    <Tab label={"Property (" + feature.property.length + ")"}/>
                                    <Tab label={"Command (" + feature.command.length + ")"}/>
                                    <Tab label={"Data type definition (" + feature.dataTypeDefinition.length + ")"}/>
                                    <Tab label={"Defined execution error (" + feature.definedExecutionError.length + ")"}/>
                                    <Tab label={"Metadata (" + feature.metadata.length + ")"}/>
                                </Tabs>
                                <TabPanel value={value} index={0}>
                                    <Properties properties={feature.property} typeDefinitions={feature.dataTypeDefinition}/>
                                </TabPanel>
                                <TabPanel value={value} index={1}>
                                    <Commands commands={feature.command} typeDefinitions={feature.dataTypeDefinition}/>
                                </TabPanel>
                                <TabPanel value={value} index={2}>
                                    <DataTypeDefinitions typeDefinitions={feature.dataTypeDefinition}/>
                                </TabPanel>
                                <TabPanel value={value} index={3}>
                                    <DefinedExecutionErrors errors={feature.definedExecutionError}/>
                                </TabPanel>
                                <TabPanel value={value} index={4}>
                                    <Metadatas metadatas={feature.metadata} typeDefinitions={feature.dataTypeDefinition}/>
                                </TabPanel>
                            </Box>
                        </>
                    )}
                </Box>
            </Paper>
        </Box>
    );
}
