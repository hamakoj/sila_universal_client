import * as React from "react";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import {
    HelpCircle,
    Play,
    Settings,
    Tool,
    User,
    ChevronsRight,
    ChevronsLeft,
    Terminal,
    List as ListIcon
} from 'react-feather';
import Divider from "@mui/material/Divider";
import ListItemText from "@mui/material/ListItemText";
import {ListItemIcon, Theme, Tooltip, TooltipProps, useTheme} from "@mui/material/";
import {NavLink} from "react-router-dom";
import {getServerInitial} from "../utils/server";
import {ServerOpenKeyValueState} from "../store/front/server/open/types";
import {useDispatch, useSelector} from "react-redux";
import {getServerOpenSelector} from "../store/front/server/open/selector";
import {getServerByIdSelector} from "../store/sila/manager/server/selector";
import {UscServer} from "../generated/silaModels";
import logo from "../assets/image/logo.png";
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import {getAppSettingsSelector} from "../store/front/app/setting/selector";
import {setAppThemeMode} from "../store/front/app/setting/actions";
import {ClosedSidebarWidth, ExpandedSideBarWidth} from "./ThemeProviderComponent";
import ServerLogoBadge from "./common/ServerLogoBadge";
import {AppState} from "../store/rootReducer";
import {SxProps} from "@mui/system";
import {ListItemButton} from "@mui/material";
import {useEffect} from "react";

export default function Sidebar(): JSX.Element {
    const theme = useTheme();
    const dispatch = useDispatch();
    const {sidebarExpandedByDefault} = useSelector(getAppSettingsSelector);
    const [open, setOpen] = React.useState<boolean>(sidebarExpandedByDefault);
    const appSettings = useSelector(getAppSettingsSelector);
    const serverOpen: ServerOpenKeyValueState = useSelector(getServerOpenSelector);
    const currentServer: UscServer | undefined = useSelector((state: AppState) => getServerByIdSelector(state, serverOpen.serverUuidOpen || ''))
    const serverInitials = getServerInitial(currentServer?.name || 'NA');

    useEffect(() => {
        setOpen(sidebarExpandedByDefault);
    }, [sidebarExpandedByDefault]);

    const handleThemeSwitch = () => {
        dispatch(setAppThemeMode({darkMode: !appSettings.darkMode}))
    };

    const handleDrawerSwitch = () => {
        setOpen((prev: boolean) => !prev);
    };

    return (
        <Box sx={{display: "flex"}}>
            <MuiDrawer variant="permanent" open={open} sx={styledDrawer(open)}
                       PaperProps={{sx: {borderRight: 0, background: theme.palette.primary.main, color: 'white'}}}>
                <List sx={{paddingTop: '7px', paddingBottom: 0}}>
                    <ListItemButton
                        sx={{...listItemButtonStyle, margin: 0, padding: 0, paddingTop: '7px', paddingLeft: '13px'}}
                        key={'server/info'} component={NavLink} to={'/dashboard'}>
                        <ListItemIcon sx={{...listItemIconStyle, height: '41px'}}>
                            <img style={{height: '32px', width: '32px'}} src={logo} alt={"Universal SiLA Client Logo"}/>
                        </ListItemIcon>
                        <ListItemText primary={'Universal SiLA Client'}/>
                    </ListItemButton>
                </List>
                {(!currentServer) ? (<></>) : (
                    <>
                        <Divider/>
                        <List>
                            <ListItemButton id={'sidebar-current-server-configuration'} sx={{...listItemButtonStyle, paddingLeft: '8px'}} component={NavLink}
                                            to={'/server/' + currentServer.serverUuid + "/info"}>
                                <Tooltip {...tooltipProps} title={currentServer.name + ' Configuration'}>
                                    <ListItemIcon sx={listItemIconStyle}>
                                        <ServerLogoBadge status={currentServer.status} serverInitials={serverInitials}/>
                                    </ListItemIcon>
                                </Tooltip>
                                <ListItemText primary={'Configuration'}/>
                            </ListItemButton>
                            <ListItemButton id={'sidebar-current-server-properties'} sx={listItemButtonStyle} key={'server/properties'} component={NavLink}
                                            to={'/server/' + currentServer.serverUuid + "/property"}>
                                <Tooltip {...tooltipProps} title={currentServer.name + ' Properties'}>
                                    <ListItemIcon sx={listItemIconStyle}>
                                        <ListIcon/>
                                    </ListItemIcon>
                                </Tooltip>
                                <ListItemText primary={'Properties'}/>
                            </ListItemButton>
                            <ListItemButton id={'sidebar-current-server-commands'}  sx={listItemButtonStyle} key={'server/commands'} component={NavLink}
                                            to={'/server/' + currentServer.serverUuid + "/command"}>
                                <Tooltip {...tooltipProps} title={currentServer.name + ' Commands'}>
                                    <ListItemIcon sx={listItemIconStyle}>
                                        <Play/>
                                    </ListItemIcon>
                                </Tooltip>
                                <ListItemText primary={'Commands'}/>
                            </ListItemButton>
                        </List>
                    </>
                )}
                <Divider/>
                <List>
                    <ListItemButton sx={listItemButtonStyle} key={'help'} component={NavLink} to="/help">
                        <Tooltip {...tooltipProps} title={'Help'}>
                            <ListItemIcon sx={listItemIconStyle}>
                                <HelpCircle/>
                            </ListItemIcon>
                        </Tooltip>
                        <ListItemText primary={'Help'}/>
                    </ListItemButton>
                    {(appSettings.isAdvancedUser && (
                        <>
                            <ListItemButton sx={listItemButtonStyle} key={'tools'} component={NavLink} to="/tools">
                                <Tooltip {...tooltipProps} title={'Tools'}>
                                    <ListItemIcon sx={listItemIconStyle}>
                                        <Tool/>
                                    </ListItemIcon>
                                </Tooltip>
                                <ListItemText primary={'Tools'}/>
                            </ListItemButton>
                            <ListItemButton sx={listItemButtonStyle} key={'dev'} component={NavLink} to="/dev">
                                <Tooltip {...tooltipProps} title={'Dev'}>
                                    <ListItemIcon sx={listItemIconStyle}>
                                        <Terminal/>
                                    </ListItemIcon>
                                </Tooltip>
                                <ListItemText primary={'Dev'}/>
                            </ListItemButton>
                            <ListItemButton sx={listItemButtonStyle} key={'workspace'} component={NavLink} to="/workspace">
                                <Tooltip {...tooltipProps} title={'Workspace'}>
                                    <ListItemIcon sx={listItemIconStyle}>
                                        <User/>
                                    </ListItemIcon>
                                </Tooltip>
                                <ListItemText primary={'Workspace'}/>
                            </ListItemButton>
                        </>
                    ))}
                    <ListItemButton sx={listItemButtonStyle} key={'settings'} component={NavLink} to="/settings">
                        <Tooltip {...tooltipProps} title={'Settings'}>
                            <ListItemIcon sx={listItemIconStyle}>
                                <Settings/>
                            </ListItemIcon>
                        </Tooltip>
                        <ListItemText primary={'Settings'}/>
                    </ListItemButton>
                </List>
                <Divider/>
                <List>
                    <ListItemButton sx={listItemButtonStyle} key={'chevron'} onClick={handleThemeSwitch}>
                        <Tooltip {...tooltipProps} title={'Switch theme'}>
                            <ListItemIcon sx={listItemIconStyle}>
                                {!appSettings.darkMode ? (
                                    <Brightness4Icon/>
                                ) : (
                                    <Brightness7Icon/>
                                )}
                            </ListItemIcon>
                        </Tooltip>
                        <ListItemText primary={(appSettings.darkMode ? ('Dark') : ('Light')) + ' mode'}/>
                    </ListItemButton>
                </List>
                <Divider/>
                <List>
                    <ListItemButton sx={listItemButtonStyle} key={'chevron'} onClick={handleDrawerSwitch}>
                        <Tooltip {...tooltipProps} title={'Open sidebar'}>
                            <ListItemIcon sx={listItemIconStyle}>
                                {!open ? (
                                    <ChevronsRight/>
                                ) : (
                                    <ChevronsLeft/>
                                )}
                            </ListItemIcon>
                        </Tooltip>
                        <ListItemText primary={'Collapse sidebar'}/>
                    </ListItemButton>
                </List>
            </MuiDrawer>
        </Box>
    );
}

const openedMixin = (theme: Theme): SxProps<Theme> => ({
    width: ExpandedSideBarWidth,
    transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
    }),
    overflowX: "hidden"
});

const closedMixin = (theme: Theme): SxProps<Theme> => ({
    transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: "hidden",
    width: ClosedSidebarWidth,
});

const getDrawableStyle: (theme: Theme, open: boolean) => SxProps<Theme> = (theme, open) => {
    if (open) {
        return {
            ...openedMixin(theme),
            "& .MuiDrawer-paper": openedMixin(theme)
        }
    }
    return {
        ...closedMixin(theme),
        "& .MuiDrawer-paper": closedMixin(theme)
    };
}

const styledDrawer: (open: boolean) => SxProps<Theme> = (open) => ((theme) => {
    const style: SxProps<Theme> = {
        width: ExpandedSideBarWidth,
        flexShrink: 0,
        whiteSpace: "nowrap",
        boxSizing: "border-box",
        ...getDrawableStyle(theme, open)
    };

    return {
        ...style
    }
});

const listItemButtonStyle: SxProps<Theme> = {
    color: 'white',
    "&.active": {
        background: '#ffffff26',
    }
}

const listItemIconStyle: SxProps<Theme> = {
    color: 'white',
}

const tooltipProps: Partial<TooltipProps> = {
    placement: "right",
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};

