import React, {MouseEvent} from 'react';
import {
    Box, TextField, Typography
} from '@mui/material/';
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import {getServerInitial} from "../../utils/server";
import {Theme} from '@mui/material/styles';
import {UscServer} from "../../generated/silaModels";
import {Skeleton} from "@mui/material";
import ServerLogoBadge from "../common/ServerLogoBadge";
import {SxProps} from "@mui/system";
import PluginsComponentRenderer from "../../plugin/PluginsComponentRenderer";

export interface ServerCardSmallProps {
    serverId: string;
    server: UscServer | undefined;
    isActive: boolean;
    onClose: () => void;
    onClick?: () => void;
}

export default function ServerCardSmall(props: ServerCardSmallProps): JSX.Element {
    if (!props.server) {
        return (
            <Skeleton animation="wave" variant="rectangular" width={'100%'}>
                <TextField/>
            </Skeleton>
        )
    }
    const { name, status } = props.server;
    const serverInitials = getServerInitial(name);

    function onCloseButtonClick(e: MouseEvent) {
        e.stopPropagation();
        if (props.onClose) {
            props.onClose();
        }
    }

    return (
        <Box sx={StyledBox} display={'inline-flex'} onClick={(e) => {
            if (props.onClick && !e.isPropagationStopped()) {
                props.onClick();
            }
        }}>
            <ServerLogoBadge status={status} serverInitials={serverInitials}/>
            <Typography sx={StyledServerName} align={'left'} color="white" gutterBottom>
                {name}
            </Typography>
            <PluginsComponentRenderer componentName={'PluginServerCardSmallBody'} forwardProps={{
                ...props,
                name: name,
                status: status,
                serverInitials: serverInitials
            }}/>
            <IconButton sx={StyledCloseIcon(props.isActive || status === "UNKNOWN")} component="div" onClick={onCloseButtonClick}>
                <CloseIcon/>
            </IconButton>
        </Box>
    );
}

const StyledServerName: SxProps<Theme> = {
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    width: '100%',
    textTransform: 'initial',
    alignSelf: 'flex-end',
    marginLeft: '4px'
}

export const StyledCloseIcon: (isActive: boolean) => SxProps<Theme> = (isActive) => ((theme) => ({
    paddingTop: 0,
    zIndex: 1,
    color: 'white',
    ...((isActive) ? ({}) : ({
        [theme.breakpoints.down('sm')]: {
            display: 'none',
        },
        [theme.breakpoints.down('md')]: {
            display: 'none',
        },
    }))
}));

const StyledBox: SxProps<Theme> = (theme) => ({
    height: '100%',
    width: '100px',
    [theme.breakpoints.up('sm')]: {
        width: '150px',
    },
    [theme.breakpoints.up('md')]: {
        width: '200px',
    },
    [theme.breakpoints.up('lg')]: {
        width: '250px',
    },
    color: 'gray',
    padding: '4px',
    '&.Mui-selected': {background: 'red'},
    '& .active': {
        background: 'rgba(255,255,255,0.2)'
    },
    justifyContent: 'space-between'
});