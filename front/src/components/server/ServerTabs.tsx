import React, {useEffect} from 'react';
import {AppBar, TabProps} from "@mui/material";
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import ServerCardSmall from "./ServerCardSmall";
import {ServersTabs} from "../../store/front/server/open/types";
import {useDispatch, useSelector} from "react-redux";
import {getCurrentServerOpenSelector, getServerOpenTabsSelector} from "../../store/front/server/open/selector";
import {NavigateFunction, useLocation, useNavigate, Location} from "react-router-dom";
import {serverCurrentOpenSet, serverOpenRemoveTab} from "../../store/front/server/open/actions";
import {AppHeaderHeightPx, ServerTabsHeightPx} from "../ThemeProviderComponent";
import {ServerState} from "../../store/sila/manager/server/types";
import {getServersStateSelector} from "../../store/sila/manager/server/selector";
import {SxProps} from "@mui/system";
import {Theme} from "@mui/material/styles";
import {Dispatch} from "redux";

export interface ScrollActiveServerTabProps extends TabProps {
    unknown: boolean;
}

function onServerClose(dispatch: Dispatch, location: Location, navigate: NavigateFunction, serversTabs: ServersTabs, serverTabsKey: string[], serverUuid: string) {
    const tabIndex = serverTabsKey.indexOf(serverUuid);
    const nextOpenServer = (serverTabsKey.length > (tabIndex + 1)) ? (serverTabsKey[tabIndex + 1]) : (serverTabsKey.at(tabIndex - 1));
    if (nextOpenServer) {
        dispatch(serverCurrentOpenSet({
            serverUuid: nextOpenServer,
            tab: 'info'
        }));
        dispatch(serverOpenRemoveTab({
            serverUuid: serverUuid
        }));
    } else {
        dispatch(serverCurrentOpenSet({
            serverUuid: serverUuid,
            tab: undefined
        }));
    }
    if (location.pathname !== '/dashboard') {
        navigate((nextOpenServer && nextOpenServer !== serverUuid) ? ('/server/' + nextOpenServer + '/' + serversTabs[nextOpenServer]) : ('/dashboard/'));
    }
}

export default function ServerTabs(): JSX.Element {
    const navigate = useNavigate();
    const location = useLocation()
    const dispatch = useDispatch();
    const serversTabs: ServersTabs = useSelector(getServerOpenTabsSelector);
    const currentOpenServerId: string | undefined = useSelector(getCurrentServerOpenSelector);
    const {pending, servers}: ServerState = useSelector(getServersStateSelector);

    const serverTabsKey = Object.keys(serversTabs);

    useEffect(() => {
        if (!pending) {
            serverTabsKey
                .filter((serverId) => !servers[serverId])
                .forEach((serverId) => onServerClose(dispatch, location, navigate, serversTabs, serverTabsKey, serverId));
        }
    }, [pending, location, serverTabsKey, servers, dispatch, navigate, serversTabs]);

    function onServerChange(serverUuid: string) {
        if (!servers[serverUuid] || location.pathname.startsWith('/server/' + serverUuid)) {
            return;
        }
        const openServer = serversTabs[serverUuid] || '';
        navigate('/server/' + serverUuid + '/' + openServer);
    }


    return (
        <AppBar sx={styledAppBar} position={"static"} elevation={0}>
            {
                (serverTabsKey.length > 0 && (
                    <Tabs
                        sx={{height: ServerTabsHeightPx + 'px'}}
                        value={currentOpenServerId}
                        TabIndicatorProps={{
                            style: {
                                display: "none",
                            },
                        }}
                        selectionFollowsFocus={true}
                        //visibleScrollbar={true} // can be useful if many tabs open on PC
                        scrollButtons={'auto'}
                        allowScrollButtonsMobile
                        variant="scrollable"
                    >
                        {
                            serverTabsKey.map(serverId => {
                                const server = servers[serverId];
                                return (
                                    <Tab
                                        sx={styledTab(!server)}
                                        disableTouchRipple={!server}
                                        aria-disabled={!server}
                                        disableFocusRipple={!server}
                                        onClick={() => onServerChange(serverId)}
                                        value={serverId}
                                        key={serverId}
                                        label={
                                            <ServerCardSmall isActive={currentOpenServerId === serverId} key={serverId}
                                                             server={server}
                                                             serverId={serverId}
                                                             onClose={() => {
                                                                 onServerClose(dispatch, location, navigate, serversTabs, serverTabsKey, serverId);
                                                             }}/>
                                        }
                                    />
                                );
                            })
                        }
                    </Tabs>
                ))
            }
        </AppBar>
    );
}

const styledAppBar: SxProps<Theme> = (theme) => ({
    height: AppHeaderHeightPx + 'px',
    display: 'flex',
    justifyContent: 'flex-end',
    background: theme.palette.primary.main,
    borderBottom: '1px solid #ffffff2e'
});

const styledTab: (unknown: boolean) => SxProps<Theme> = (unknown) => ((theme) => ({
    ...((unknown) ? {cursor: 'default', background: '#4d6281 !important'} : {}) as any,
    borderRight: 'solid 1px #ffffff2e',
    borderLeft: 'solid 1px #ffffff2e',
    backgroundColor: 'rgba(0,0,0,0.11)',
    '&.Mui-selected': {
        backgroundColor: 'rgba(255,255,255,0.15)',
    },
    padding: 0, paddingRight: '1px', justifyContent: 'flex-end'
}));