import React from 'react';
import {
    SiLACallModel,
    UscCommand,
    UscFeature,
    UscMetadata,
    UscProperty,
    UscResponse
} from "../../../../generated/silaModels";
import {
    Box, CircularProgress, Tooltip, Typography
} from '@mui/material/';
import {DateTime} from "luxon";
import IconButton from "@mui/material/IconButton";
import {
    removeCommandResponse,
} from "../../../../store/front/server/response/actions";
import {useDispatch} from "react-redux";
import {CommandResponse} from "../../../../store/front/server/response/types";
import CallResponseParameters from "./CallResponseParameters";
import CallResponseExecInfo from "./CallResponseExecInfo";
import CallResponseIntermediateResponses from "./CallResponseIntermediateResponses";
import CallResponseResponses from "./CallResponseResponses";
import CallResponseMetadatas from "./CallResponseMetadatas";
import CallResponseObservablePropertyValues from "./CallResponseObservablePropertyValues";
import {getBackEndpoint} from "../../../../utils/endpoint";
import {CallType} from "../../../../store/front/server/activeCall/types";
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import CancelIcon from '@mui/icons-material/Cancel';
import StopCircleIcon from '@mui/icons-material/StopCircle';
import PauseCircleIcon from '@mui/icons-material/PauseCircle';
import PlayCircleIcon from '@mui/icons-material/PlayCircle';
import {TooltipProps} from "@mui/material";
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";
import {ProtoCancelCommand_Parameters} from "../../../../generated/CancelController";
import {ProtoPause_Parameters, ProtoResume_Parameters} from "../../../../generated/PauseController";


export interface CallResponseProps {
    feature: UscFeature;
    type: CallType;
    call: UscCommand | UscProperty | undefined;
    response: CommandResponse;
    callResponses: UscResponse[];
    callMetadatas: UscMetadata[];
    supportCommandCancel: boolean;
    supportCommandPause: boolean;
    isPaused: boolean;
}

// todo add button to redo the call with the same parameters
//  add buttons to set the parameters into the input fields
export const CallResponse = React.memo((props: CallResponseProps): JSX.Element => {
    const dispatch = useDispatch();

    if (!props.call) {
        return (<Typography>Missing call definition</Typography>);
    }

    // ignore warning below, timestamp is not necessarily a date
    const execDate = (props.response.timestamp instanceof Date) ?
        DateTime.fromJSDate(props.response.timestamp) :
        DateTime.fromISO(props.response.timestamp as any);

    const isCallInProgress = props.response.success && !props.response.value;
    const color = (isCallInProgress) ? '#777777' : (props.response.success ? 'rgb(112 146 197)' : '#cc6d6d');
    return (
        <Box padding={1} key={props.response.uuid} id={props.response.uuid}>
            <Box borderRadius={1}
                 sx={{border: '3px solid ' + color}}>
                <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    sx={(theme) => ({
                        background: color
                    })}
                >
                    <Typography paddingLeft={1} color={'black'}>{execDate.toFormat('DDD TTT')}</Typography>
                    <Box height={"inherit"}>
                        {
                            (isCallInProgress && (
                                <>
                                    {
                                        ((props.type === "command" && isCallInProgress && props.response.commandExecutionUUID && props.supportCommandCancel) && (
                                            <Tooltip {...tooltipProps} title={'Cancel command'}>
                                                <IconButton onClick={(e) => {
                                                    const parameters: ProtoCancelCommand_Parameters = {
                                                        CommandExecutionUUID: {
                                                            UUID: {
                                                                value: props.response.commandExecutionUUID
                                                            }
                                                        }
                                                    }
                                                    // todo handle loading and error
                                                    fetch(getBackEndpoint('/api/calls/execute'), {
                                                        method: 'POST',
                                                        headers: {
                                                            'Content-Type': 'application/json',
                                                        },
                                                        body: JSON.stringify({
                                                            serverId: props.response.serverUuid,
                                                            fullyQualifiedFeatureId: 'org.silastandard/core.commands/CancelController/v1',
                                                            callId: 'CancelCommand',
                                                            type: 'UNOBSERVABLE_COMMAND',
                                                            parameters: JSON.stringify(parameters),
                                                            metadatas : JSON.stringify({}),
                                                            runAsync: false
                                                        } as SiLACallModel)
                                                    })
                                                }}>
                                                    <StopCircleIcon color={'error'}/>
                                                </IconButton>
                                            </Tooltip>
                                        ))
                                    }
                                    {
                                        ((props.type === "command" && isCallInProgress && props.response.commandExecutionUUID && props.supportCommandPause && !props.isPaused) && (
                                            <Tooltip {...tooltipProps} title={'Pause command'}>
                                                <IconButton onClick={(e) => {
                                                    const parameters: ProtoPause_Parameters = {
                                                        CommandExecutionUUID: {
                                                            UUID: {
                                                                value: props.response.commandExecutionUUID
                                                            }
                                                        }
                                                    }
                                                    // todo handle loading and error
                                                    fetch(getBackEndpoint('/api/calls/execute'), {
                                                        method: 'POST',
                                                        headers: {
                                                            'Content-Type': 'application/json',
                                                        },
                                                        body: JSON.stringify({
                                                            serverId: props.response.serverUuid,
                                                            fullyQualifiedFeatureId: 'org.silastandard/core.commands/PauseController/v1',
                                                            callId: 'Pause',
                                                            type: 'UNOBSERVABLE_COMMAND',
                                                            parameters: JSON.stringify(parameters),
                                                            metadatas : JSON.stringify({}),
                                                            runAsync: false
                                                        } as SiLACallModel)
                                                    })
                                                }}>
                                                    <PauseCircleIcon color={'warning'}/>
                                                </IconButton>
                                            </Tooltip>
                                        ))
                                    }
                                    {
                                        ((props.type === "command" && isCallInProgress && props.response.commandExecutionUUID && props.supportCommandPause && props.isPaused) && (
                                            <Tooltip {...tooltipProps} title={'Resume command'}>
                                                <IconButton onClick={(e) => {
                                                    const parameters: ProtoResume_Parameters = {
                                                        CommandExecutionUUID: {
                                                            UUID: {
                                                                value: props.response.commandExecutionUUID
                                                            }
                                                        }
                                                    }
                                                    // todo handle loading and error
                                                    fetch(getBackEndpoint('/api/calls/execute'), {
                                                        method: 'POST',
                                                        headers: {
                                                            'Content-Type': 'application/json',
                                                        },
                                                        body: JSON.stringify({
                                                            serverId: props.response.serverUuid,
                                                            fullyQualifiedFeatureId: 'org.silastandard/core.commands/PauseController/v1',
                                                            callId: 'Resume',
                                                            type: 'UNOBSERVABLE_COMMAND',
                                                            parameters: JSON.stringify(parameters),
                                                            metadatas : JSON.stringify({}),
                                                            runAsync: false
                                                        } as SiLACallModel)
                                                    })
                                                }}>
                                                    <PlayCircleIcon color={'success'}/>
                                                </IconButton>
                                            </Tooltip>
                                        ))
                                    }
                                    <Tooltip {...tooltipProps} title={'Cancel ' + props.type}>
                                        <IconButton onClick={(e) => {
                                            fetch(getBackEndpoint('/api/calls/' + props.response.uuid + '/cancel'), {})
                                        }}>
                                            <CancelIcon color={'error'}/>
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip {...tooltipProps} title={'Subscription in progress'}>
                                        <span>
                                            <IconButton disabled={true}>
                                                <CircularProgress color={"warning"} size={24}/>
                                            </IconButton>
                                        </span>
                                    </Tooltip>
                                </>
                            ))
                        }
                        <Tooltip {...tooltipProps} title={((isCallInProgress) ? ('The task must be finished or cancelled to delete the response') : ('Delete response'))}>
                            <span>
                                {/* wrapped in a span so the tooltip can be displayed even if the button is disabled */}
                                <IconButton disabled={isCallInProgress} onClick={(e) => {
                                    if (isCallInProgress) {
                                        return;
                                    }
                                    const callUrl = '/api/calls/' + props.response.uuid;
                                    // todo if trying to remove a call in progress,
                                    //  add a confirmation modal and if yes then cancel the call before removing
                                    fetch(getBackEndpoint(callUrl), {method: 'DELETE'})
                                        .then((data: any) => {
                                            dispatch(removeCommandResponse({
                                                uuid: props.response.uuid
                                            }));
                                        })
                                        .catch((error) => {
                                            // todo deletion failed
                                            console.error(error);
                                        })
                                }}>
                                    <DeleteForeverIcon color={"action"}/>
                                </IconButton>
                            </span>
                        </Tooltip>
                    </Box>
                </Box>
                <Box
                    sx={{'> div:not(:last-child)': {borderBottom: '2px solid ' + color}}}>
                    <CallResponseResponses feature={props.feature} response={props.response}
                                           callResponses={props.callResponses} type={props.type}/>
                    <CallResponseMetadatas feature={props.feature} response={props.response}
                                           callMetadatas={props.callMetadatas}/>
                    {
                        (props.type === 'command' &&
                            <CallResponseParameters feature={props.feature} response={props.response}
                                                    command={props.call as UscCommand}/>
                        )
                    }
                    {
                        (props.type === 'command' && props.call?.observable &&
                            <CallResponseExecInfo response={props.response}/>
                        )
                    }
                    {
                        (props.type === 'command' && props.call?.observable && (props.call as UscCommand)?.intermediateResponses &&
                            <CallResponseIntermediateResponses feature={props.feature} response={props.response}
                                                               command={props.call as UscCommand}/>
                        )
                    }
                    {
                        (props.type === 'property' && props.call?.observable &&
                            <CallResponseObservablePropertyValues feature={props.feature} response={props.response}
                                                                  property={props.call as UscProperty}/>
                        )
                    }
                </Box>
            </Box>
            <PluginsComponentRenderer componentName={'PluginCallResponse'} forwardProps={{...props, execDate}}/>
        </Box>
    );
});

export default CallResponse

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};
