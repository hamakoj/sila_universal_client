import React, {useEffect} from 'react';
import {
    UscCall,
    UscCommand,
    UscFeature,
    UscProperty,
    UscResponse,
    UscServer
} from "../../../../generated/silaModels";
import {
    Box, Button, Paper, Typography
} from '@mui/material/';
import {useDispatch, useSelector} from "react-redux";
import {CommandResponse} from "../../../../store/front/server/response/types";
import {
    addCommandResponses, fetchResponseFromCacheRequest,
    removeCommandResponses
} from "../../../../store/front/server/response/actions";
import {CallType, FullyQualifiedCallId} from "../../../../store/front/server/activeCall/types";
import CallResponse from "./CallResponse";
import {getBackEndpoint} from "../../../../utils/endpoint";
import {SxProps} from "@mui/system";
import {Theme} from "@mui/material/styles";
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";
import {AppState} from "../../../../store/rootReducer";
import {
    getServerMetaPausedCommandsByIdSelector
} from "../../../../store/front/server/meta/selector";
import {getCallMetadatas} from "../../../../utils/call";

function callToKey(call: FullyQualifiedCallId): string {
    return (call.serverUuid + '/' + call.fullyQualifiedFeatureId + '/' + call.callType + '/' + call.callId);
}

export interface CallResponsesProps {
    server: UscServer;
    type: CallType;
    feature: UscFeature;
    responses: CommandResponse[];
    activeCall: FullyQualifiedCallId;
    command?: UscCommand;
    property?: UscProperty;
}

export const CallResponses = React.memo((props: CallResponsesProps): JSX.Element  => {
    const dispatch = useDispatch();
    const pausedCommands = useSelector((state: AppState) => getServerMetaPausedCommandsByIdSelector(state, props.server.serverUuid))
    const {activeCall, feature, command, property, responses} = props;
    const callMetadatas = React.useMemo(
        () => getCallMetadatas(props.server?.features || []),
        [props.server.features]
    );
    const callUrl = getBackEndpoint('/api/servers/' + activeCall.serverUuid + '/features/' + activeCall.fullyQualifiedFeatureId + ((props.type === "property") ? ('/properties/') : ('/commands/')) + activeCall.callId + '/calls');
    const key = callToKey(activeCall);
    const callResponses: UscResponse[] = ((props.type === 'command') ? (command?.responses) : ((property?.response) ? [property?.response] : [])) || [];
    const supportCommandCancel = props.server.features.some((f) => f.relativeIdentifier === "CancelController");
    const supportCommandPause = props.server.features.some((f) => f.relativeIdentifier === "PauseController");
    const pluginsProps = {
        ...props,
        responses: responses,
        callResponses: callResponses,
        callMetadatas: callMetadatas,
        activeCall: activeCall,
        callUrl: callUrl,
        feature: feature,
        command: command,
        property: property
    }

    useEffect(() => {
        if (activeCall && !responses.length) {
            fetch(callUrl, {method: 'GET'})
                .then((data) => data.json())
                .then(async (data: UscCall[]) => {
                    if (!data?.map) {
                        console.warn('No call response for ' + callUrl);
                        return;
                    }
                    const commandResponses: CommandResponse[] = data.map((r) => (
                        {
                            uuid: r.uuid,
                            serverUuid: activeCall.serverUuid,
                            fullyQualifiedFeatureId: activeCall.fullyQualifiedFeatureId,
                            commandExecutionUUID: r.commandExecutionUUID,
                            commandIdentifier: activeCall.callId,
                            timestamp: r.endDate || r.startDate,
                            success: !r.error,
                            value: (r.result === 'null') ? null : r.result,
                            metadatas: (r.metadatas === 'null') ? null : r.metadatas,
                            parameter: r.parameters,
                            intermediateResponse: [],
                            state: undefined,
                        }
                    ));
                    dispatch(addCommandResponses({responses: commandResponses}));
                    commandResponses.filter((r) => r.success && !r.value).forEach((r) => {
                       dispatch(fetchResponseFromCacheRequest({
                           callId: r.uuid
                       }))
                    });
                })
                .catch((error) => {
                    console.error(error);
                })
        }
    }, [activeCall, responses.length, callUrl, dispatch]);

    if (!activeCall) {
        return (<></>);
    }

    if (!feature || !responses || (!command && props.type === 'command') || (!property && props.type === 'property')) {
        return (
            <Box padding={1} key={key}>
                <Paper>
                    <Typography variant={"h6"}>
                        {(props.type === 'command') ? ('Command responses') : ('Property values')}
                    </Typography>
                    <Typography variant={"body1"}>
                        {(props.type === 'command') ? ('No responses') : ('No values')}
                    </Typography>
                    <PluginsComponentRenderer componentName={'PluginCallResponses'} forwardProps={pluginsProps}/>
                </Paper>
            </Box>
        );
    }

    return (
        <Box padding={1} key={key}>
            <Box padding={1} display={'flex'} justifyContent={'space-between'}>
                <Typography variant={"h6"}>
                    {
                        props.type === 'command' ? "Command responses" : "Property values"
                    }
                </Typography>
                <Button variant="contained" color="primary" disabled={!responses?.length} onClick={() => {
                    const callUrl = '/api/servers/' + activeCall.serverUuid + '/features/' + activeCall?.fullyQualifiedFeatureId + ((props.type === "property") ? ('/properties/') : ('/commands/')) + activeCall.callId + '/calls';
                    fetch(getBackEndpoint(callUrl), {method: 'DELETE'})
                        .then((data: any) => {
                            dispatch(removeCommandResponses({
                                serverUuid: activeCall.serverUuid,
                                fullyQualifiedFeatureId: activeCall?.fullyQualifiedFeatureId,
                                commandIdentifier: activeCall.callId
                            }));
                        })
                        .catch((error) => {
                            // todo deletion failed
                            console.error(error);
                        })
                }}>Clear all</Button>
            </Box>
            {
                (responses.length) ? (
                    responses.map((response: CommandResponse) => (
                        <CallResponse
                            key={response.uuid}
                            feature={feature}
                            type={props.type}
                            call={command || property}
                            response={response}
                            callResponses={callResponses}
                            callMetadatas={callMetadatas}
                            supportCommandCancel={supportCommandCancel}
                            supportCommandPause={supportCommandPause}
                            isPaused={pausedCommands?.PausedCommands?.some(c => c.UUID?.value === response.commandExecutionUUID) || false}
                        />
                    ))
                ) : (
                    <Typography>No response to display</Typography>
                )
            }
            <PluginsComponentRenderer componentName={'PluginCallResponses'} forwardProps={pluginsProps}/>
        </Box>
    );
});

export default CallResponses;

export const styledAccordionSummary: SxProps<Theme> = ( theme ) => ({
    backgroundColor: ((theme as any).isDarkMode) ? '#2d2d2f' : '#d3d3d3',
    color: ((theme as any).isDarkMode) ? '#a4a4a4' : '#3f3f3f',
});

export const styledAccordionDetails: SxProps<Theme> = {
    backgroundColor: 'inherit'
};