import React from 'react';
import {UscFeature, UscMetadata} from "../../../../generated/silaModels";
import {
    Accordion,
    AccordionDetails,
    Box, Typography
} from '@mui/material/';
import DataTypeForm from "../dataTypeForm/DataTypeFormRoot";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {getCommandResponseState} from "../dataTypeForm/silaToState";
import {CommandResponse} from "../../../../store/front/server/response/types";
import {styledAccordionSummary} from './CallResponses';
import {styledAccordion} from "../ServerCalls";
import {AccordionSummary, Tooltip, TooltipProps} from "@mui/material";
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";

export interface CallResponseMetadatasProps {
    feature: UscFeature;
    response: CommandResponse;
    callMetadatas: UscMetadata[];
}

export default function CallResponseMetadatas(props: CallResponseMetadatasProps): JSX.Element | null {
    const fullyQualifiedCommandId = props.response.serverUuid + '/' + props.response.fullyQualifiedFeatureId + '/' + props.response.commandIdentifier;

    if (!props.callMetadatas?.length) {
        return null
    }

    const silaValue = JSON.parse(props.response.metadatas);

    return (
        <Box key={props.response.uuid + '/metadata'}>
            {
                ((props.callMetadatas?.length > 0 && Object.keys(silaValue).length > 0) && (
                    <Accordion sx={styledAccordion}>
                        <AccordionSummary sx={styledAccordionSummary} expandIcon={<ExpandMoreIcon/>}>
                            Metadata
                        </AccordionSummary>
                        <AccordionDetails>
                            {
                                props.callMetadatas
                                    .filter(m => !!silaValue?.[m.fullyQualifiedIdentifier])
                                    .map((m) => ({...m, relativeIdentifier: m.fullyQualifiedIdentifier}))
                                    .sort((a, b) => a.displayName.localeCompare(b.displayName))
                                    .map((m: UscMetadata) => {
                                        const metadatasValue = Object.keys(silaValue).reduce((acc, key) => {
                                            const identifiers = key.split('/');
                                            const id = identifiers[5];
                                            return {
                                                ...acc,
                                                [key]: silaValue[key]?.[id]
                                            };
                                        }, {});
                                        const formValues = getCommandResponseState([], [m], metadatasValue);
                                        return (
                                            <Box key={m.relativeIdentifier + '/' + props.response.uuid}>
                                                <Box sx={{display: 'inline-flex', alignItems: 'center'}}>
                                                    <Typography>{m.displayName || m.relativeIdentifier}</Typography>
                                                    <Tooltip
                                                        {...tooltipProps}
                                                        title={m.description || 'No description'}>
                                                        <IconButton>
                                                            <InfoIcon/>
                                                        </IconButton>
                                                    </Tooltip>
                                                </Box>
                                                <DataTypeForm
                                                    dataType={m.dataType}
                                                    dataTypeDefinitions={[]}
                                                    stateId={m.relativeIdentifier}
                                                    parameterId={m.relativeIdentifier}
                                                    serverId={props.response.serverUuid}
                                                    value={formValues}
                                                    readonly={true}
                                                    fullyQualifiedCommandId={fullyQualifiedCommandId}
                                                    type={'METADATA'}
                                                />
                                            </Box>
                                        )
                                    })
                            }
                        </AccordionDetails>
                    </Accordion>
                ))
            }
            <PluginsComponentRenderer componentName={'PluginCallResponseMetadatas'} forwardProps={props}/>
        </Box>
    )
}

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};