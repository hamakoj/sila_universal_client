import React from 'react';
import {UscCommand, UscFeature} from "../../../../generated/silaModels";
import {
    Accordion,
    AccordionDetails,
    Box, Typography
} from '@mui/material/';
import DataTypeForm from "../dataTypeForm/DataTypeFormRoot";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {getCommandResponseState} from "../dataTypeForm/silaToState";
import {CommandResponse} from "../../../../store/front/server/response/types";
import {styledAccordionSummary} from './CallResponses';
import {styledAccordion} from "../ServerCalls";
import {AccordionSummary, Tooltip, TooltipProps} from "@mui/material";
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";

export interface CallResponseParametersProps {
    feature: UscFeature;
    response: CommandResponse;
    command: UscCommand;
}

export default function CallResponseParameters(props: CallResponseParametersProps): JSX.Element {
    const fullyQualifiedCommandId = props.response.serverUuid + '/' + props.response.fullyQualifiedFeatureId + '/' + props.response.commandIdentifier;

    return (
        <Box key={props.response.uuid + '/parameters'}>
            {
                (props.command.parameters && (
                    <Accordion sx={styledAccordion}>
                        <AccordionSummary sx={styledAccordionSummary} expandIcon={<ExpandMoreIcon/>}>
                            <Typography>Parameters</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            {
                                (props.command.parameters.length && props.response.parameter) ? (
                                    props.command.parameters
                                        .sort((a, b) => a.displayName.localeCompare(b.displayName))
                                        .map((p) => {
                                            const formParamValues = (typeof props.response.parameter === 'string') ?
                                                getCommandResponseState(props.feature.dataTypeDefinitions, [p], JSON.parse(props.response.parameter)) :
                                                (props.response.parameter);
                                            return (
                                                <Box key={p.relativeIdentifier + '/' + props.response.uuid}>
                                                    <Box sx={{display: 'inline-flex', alignItems: 'center'}}>
                                                        <Typography>{p.displayName || p.relativeIdentifier}</Typography>
                                                        <Tooltip
                                                            {...tooltipProps}
                                                            title={p.description || 'No description'}>
                                                            <IconButton>
                                                                <InfoIcon/>
                                                            </IconButton>
                                                        </Tooltip>
                                                    </Box>
                                                    <DataTypeForm
                                                        dataType={p.dataType}
                                                        dataTypeDefinitions={props.feature.dataTypeDefinitions}
                                                        stateId={p.relativeIdentifier}
                                                        parameterId={p.relativeIdentifier}
                                                        serverId={props.response.serverUuid}
                                                        value={formParamValues}
                                                        readonly={true}
                                                        fullyQualifiedCommandId={fullyQualifiedCommandId}
                                                        type={'PARAMETER'}
                                                    />
                                                </Box>
                                            );
                                        })) : (<Typography>No parameters</Typography>)
                            }
                        </AccordionDetails>
                    </Accordion>
                ))
            }
            <PluginsComponentRenderer componentName={'PluginCallResponseParameters'} forwardProps={props}/>
        </Box>
    );
}

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};