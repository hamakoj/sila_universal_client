import React from 'react';
import {
    Accordion,
    AccordionDetails,
    Box, Typography
} from '@mui/material/';
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {CommandResponse} from "../../../../store/front/server/response/types";
import {styledAccordionSummary} from './CallResponses';
import {styledAccordion} from "../ServerCalls";
import {AccordionSummary, Tooltip} from "@mui/material";
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";
import {ProtoDuration} from "../../../../generated/SiLAFramework";
import {protoDurationDatetime, toHuman} from "../../../../utils/duration";

export interface CallResponseExecInfoProps {
    response: CommandResponse;
}

export default function CallResponseExecInfo(props: CallResponseExecInfoProps): JSX.Element {
    function timeLeft(updatedLifetimeOfExecution: ProtoDuration): string {
        const duration = protoDurationDatetime(updatedLifetimeOfExecution);
        return toHuman(duration);
    }

    return (
        <Box key={props.response.uuid + '/exec'}>
            <Accordion sx={styledAccordion} defaultExpanded={true}>
                <AccordionSummary sx={styledAccordionSummary} expandIcon={<ExpandMoreIcon />}>
                    <Typography>Execution Info</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <>
                        <Typography>Command Execution UUID: {(props.response.commandExecutionUUID) ? props.response.commandExecutionUUID : 'Unknown'}</Typography>
                        {
                            (props.response.state) ? (
                                <Box>
                                    <Typography>Status: {(props.response.state?.commandStatus) ? (props.response.state.commandStatus) : ('unknown')}</Typography>
                                    {
                                        ((props.response.state?.commandStatus !== 'finishedSuccessfully' && props.response.state?.commandStatus !== 'finishedWithError') && (
                                            <>
                                                <Typography>Progress Info: {(props.response.state?.progressInfo?.value) ? ((props.response.state.progressInfo.value * 100).toFixed(2) + '%') : ('unknown')}</Typography>
                                                <Typography>Estimated remaining time: {(props.response.state?.estimatedRemainingTime) ? timeLeft(props.response.state.estimatedRemainingTime) : ('unknown')}</Typography>
                                                <Typography>Life time of execution: {(props.response.state?.updatedLifetimeOfExecution) ? timeLeft(props.response.state.updatedLifetimeOfExecution) : ('As long as the lifetime of the SiLA Server')}</Typography>
                                            </>
                                        ))
                                    }
                                </Box>
                            ) : (
                                <Box sx={{display: 'inline-flex', alignItems: 'center'}}>
                                    <Typography>No Execution info to display</Typography>
                                    <Tooltip
                                        componentsProps={{
                                            tooltip: {
                                                sx: {
                                                    fontSize: '14px'
                                                }
                                            }
                                        }}
                                        title={'Execution information(s) are not persistent. ' +
                                            'Execution information are lost once the web application is closed. ' +
                                            'Only the information received while the web application is open are displayed.'}>
                                        <IconButton>
                                            <InfoIcon/>
                                        </IconButton>
                                    </Tooltip>
                                </Box>
                            )
                        }
                    </>
                </AccordionDetails>
            </Accordion>
            <PluginsComponentRenderer componentName={'PluginCallResponseExecInfo'} forwardProps={props}/>
        </Box>
    );
}