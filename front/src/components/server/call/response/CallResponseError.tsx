import React from 'react';
import {
    Accordion, AccordionDetails,
    Box, Typography
} from '@mui/material/';
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {ProtoSiLAError} from "../../../../generated/SiLAFramework";
import {styledAccordionDetails, styledAccordionSummary} from './CallResponses';
import {styledAccordion} from "../ServerCalls";
import {AccordionSummary, Tooltip, TooltipProps} from "@mui/material";
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";
import {FullyQualifiedIdentifierUtils} from "../../../../utils/fullyQualifiedIdentifier";

export interface CallResponseErrorProps {
    responseValue: any; // todo set explicit type
    responseId: number | string;
}

export default function CallResponseError(props: CallResponseErrorProps): JSX.Element {
    let error = props.responseValue;
    let silaError: ProtoSiLAError | undefined = undefined;
    try {
        const json = JSON.parse(error);
        if (json.definedExecutionError || json.frameworkError || json.undefinedExecutionError || json.validationError) {
            silaError = json;
        } else {
            error = json.error;
            try {
                silaError = JSON.parse(error);
            } catch (ignored) {
            }
        }
    } catch (ignored) {
        error = props.responseValue;
    }
    let ErrorNode = <Typography>{error || 'An unknown error occurred'}</Typography>;
    let errorType = 'Error';
    if (silaError) {
        if (silaError.definedExecutionError) {
            const errorIdentifier = FullyQualifiedIdentifierUtils.getDefinedErrorIdentifier(
                silaError.definedExecutionError.errorIdentifier || ''
            )
            errorType = 'Defined execution error';
            ErrorNode = (
                <Box>
                    <Box display={'flex'} alignItems={'center'}>
                        {
                            ((errorIdentifier) && (
                                <Typography>{errorIdentifier}</Typography>
                            ))
                        }
                        <Tooltip
                            {...tooltipProps}
                            title={silaError.definedExecutionError.errorIdentifier || 'Unknown fully qualified identifier'}>
                            <IconButton>
                                <InfoIcon/>
                            </IconButton>
                        </Tooltip>
                    </Box>
                    <Typography>{silaError.definedExecutionError.message}</Typography>
                </Box>
            );
        } else if (silaError.frameworkError) {
            errorType = "Framework error";
            const error = silaError.frameworkError.errorType?.toString() || 'Unknown framework error';
            const errorTypeCapitalized = error.toString()
                .replaceAll('_', ' ')
                .toLowerCase()
                .replace(/^[^a-z]*([a-z])/, l => l.toUpperCase());
            ErrorNode = (
                <Box>
                    <Box display={'flex'} alignItems={'center'}>
                        <Typography>{errorTypeCapitalized}</Typography>
                        <Tooltip
                            {...tooltipProps}
                            title={error}>
                            <IconButton>
                                <InfoIcon/>
                            </IconButton>
                        </Tooltip>
                    </Box>
                    <Typography>{silaError.frameworkError.message}</Typography>
                </Box>
            )
        } else if (silaError.undefinedExecutionError) {
            errorType = "Undefined execution error";

            ErrorNode = (
                <Box>
                    <Typography>{silaError.undefinedExecutionError.message}</Typography>
                </Box>
            )
        } else if (silaError.validationError) {
            errorType = "Validation error";
            const parameterIdentifier = FullyQualifiedIdentifierUtils.getParameterIdentifier(
                silaError.validationError.parameter || ''
            )

            ErrorNode = (
                <Box>
                    <Box display={'flex'} alignItems={'center'}>
                        {
                            ((parameterIdentifier) && (
                                <Typography>{parameterIdentifier}</Typography>
                            ))
                        }
                        <Tooltip
                            {...tooltipProps}
                            title={silaError.validationError.parameter || 'Unknown fully qualified identifier'}>
                            <IconButton>
                                <InfoIcon/>
                            </IconButton>
                        </Tooltip>
                    </Box>
                    <Typography>{silaError.validationError.message}</Typography>
                </Box>
            )
        }
    }
    return (
        <Box key={props.responseId + '/error'}>
            <Accordion sx={styledAccordion} defaultExpanded={true}>
                <AccordionSummary sx={styledAccordionSummary} expandIcon={<ExpandMoreIcon/>}>
                    <Typography>{errorType}</Typography>
                </AccordionSummary>
                <AccordionDetails sx={styledAccordionDetails}>
                    {ErrorNode}
                </AccordionDetails>
            </Accordion>
            <PluginsComponentRenderer componentName={'PluginCallResponseError'} forwardProps={{...props, silaError}}/>
        </Box>
    )
}

const tooltipProps: Partial<TooltipProps> = {
    placement: "right",
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};
