import React from 'react';
import { UscServer} from '../../../generated/silaModels';
import {
    Accordion, AccordionDetails, AccordionSummary, Alert, Tooltip, TooltipProps,
    Typography,
} from "@mui/material";
import { useSelector} from "react-redux";
import {getServerFeaturesSelector} from "../../../store/front/server/feature/selector";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {getServerActiveCallSelector} from "../../../store/front/server/activeCall/selector";
import {styledAccordionSummary} from './response/CallResponses';
import ServerCallComponent from "./ServerCallComponent";
import {CallType} from "../../../store/front/server/activeCall/types";
import {SxProps} from "@mui/system";
import {Theme} from "@mui/material/styles";
import PluginsComponentRenderer from "../../../plugin/PluginsComponentRenderer";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";

export interface ServerCallsProps {
    server: UscServer;
    type: CallType;
}

export default function ServerCalls(props: ServerCallsProps): JSX.Element {
    const selectedServerFeatures = useSelector(getServerFeaturesSelector);
    const activeCalls = useSelector(getServerActiveCallSelector);

    function isFeatureSelected(featureIdentifier: string): boolean {
        // do not simplify or else the function can return undefined value
        if (selectedServerFeatures[props.server.serverUuid] && selectedServerFeatures[props.server.serverUuid][featureIdentifier] !== undefined) {
            return selectedServerFeatures[props.server.serverUuid][featureIdentifier];
        }
        return true;
    }

    const serverCalls = props.server.features
        .filter(f => isFeatureSelected(f.fullyQualifiedIdentifier))
        .filter(f => (props.type === 'property') ? f.properties.length : f.commands.length)
        .sort((a, b) => a.displayName.localeCompare(b.displayName))
        .map(f => (
            <Accordion sx={styledAccordion} defaultExpanded={true} key={f.fullyQualifiedIdentifier}>
                <AccordionSummary sx={styledAccordionSummary} expandIcon={<ExpandMoreIcon/>}>
                    <Typography variant={"h6"}>{f.displayName}</Typography>
                    <Tooltip
                        {...tooltipProps}
                        title={f.fullyQualifiedIdentifier || 'Unknown fully qualified identifier'}>
                        <IconButton>
                            <InfoIcon/>
                        </IconButton>
                    </Tooltip>
                </AccordionSummary>
                <AccordionDetails style={{display: 'initial'}}>
                    {
                        ((props.type === 'property') ? (f.properties) : (f.commands))
                            .sort((a, b) => a.displayName.localeCompare(b.displayName))
                            .map(call => (
                                <ServerCallComponent key={call.relativeIdentifier} server={props.server} type={props.type} feature={f} call={call} activeCalls={activeCalls}/>)
                            )
                    }
                </AccordionDetails>
            </Accordion>
        ));
    return (
        <>
            {
                (serverCalls.length) ? (serverCalls) : (
                <Alert severity={"info"}>The selected server features does contain any {(props.type === 'property') ? 'property' : 'command'}</Alert>
                )
            }
            <PluginsComponentRenderer componentName={'PluginServerCalls'} forwardProps={props}/>
        </>
    );
}

export const styledAccordion: SxProps<Theme> = (theme) => ({
    color: theme.palette.text.primary,
    backgroundColor: "initial"
});

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};