import React from 'react';
import {
    SiLAJavaConstraints,
    UscDataTypeDefinition,
} from "../../../../generated/silaModels";
import {
    Tooltip,
    Typography
} from '@mui/material/';
import DataTypeForm from "../dataTypeForm/DataTypeFormRoot";
import {FormInputType, SiLATypeValueState} from "../../../../store/front/server/callInput/types";
import {Dispatch} from "redux";
import {Box} from "@mui/material";
import {SxProps} from "@mui/system";
import {Theme} from "@mui/material/styles";
import IconButton from "@mui/material/IconButton";
import InfoIcon from '@mui/icons-material/Info';
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";
import {useSelector} from "react-redux";
import {AppState} from "../../../../store/rootReducer";
import {getServerMetaConstraintByIdSelector} from "../../../../store/front/server/meta/selector";
import SiLAXMLParser from "../../../../utils/parser/siLAXMLParser";

export interface ServerCallRequestParameterProps {
    serverUuid: string;
    dataTypes: UscDataTypeDefinition[];
    parameter: UscDataTypeDefinition;
    values: SiLATypeValueState | undefined;
    dispatch: Dispatch;
    fullyQualifiedCommandId: string;
    type: FormInputType;
}

const ServerCallRequestParameter = React.memo((props: ServerCallRequestParameterProps): JSX.Element => {
    const runtimeConstraints = useSelector((state: AppState) => getServerMetaConstraintByIdSelector(state, props.serverUuid))
    const fqi = props.fullyQualifiedCommandId.replace(props.serverUuid + '/', '') + '/Parameter/' + props.parameter.relativeIdentifier;
    // todo improve perf, re-render everytime runtime constraints changes
    const callRuntimeConstraints: SiLAJavaConstraints[] = runtimeConstraints?.ParametersConstraints
        ?.filter(r => r.CommandParameterIdentifier?.value === fqi)
        .map(r => r.Constraints?.value)
        .map(xml => SiLAXMLParser.parseConstraints(xml || ''))
        .filter((c): c is SiLAJavaConstraints => c !== null) || [];

    return (
        <Box sx={styledInputBox}>
            <Box sx={{display: 'inline-flex'}}>
                <Typography variant={"h6"}>
                    {props.parameter.displayName}
                </Typography>
                <Tooltip
                    componentsProps={{
                        tooltip: {
                            sx: {
                                fontSize: '14px'
                            }
                        }
                    }}
                    title={(props.parameter.description || 'No description')}>
                    <IconButton>
                        <InfoIcon/>
                    </IconButton>
                </Tooltip>
            </Box>
            <Box sx={styledInputBox}>
                <DataTypeForm
                    dataType={props.parameter.dataType}
                    dispatch={props.dispatch}
                    runtimeConstraints={callRuntimeConstraints}
                    dataTypeDefinitions={props.dataTypes}
                    stateId={props.parameter.relativeIdentifier}
                    parameterId={props.parameter.relativeIdentifier}
                    serverId={props.serverUuid}
                    value={props.values}
                    fullyQualifiedCommandId={props.fullyQualifiedCommandId}
                    readonly={false}
                    type={props.type}
                />
            </Box>
            <PluginsComponentRenderer componentName={'PluginServerCallRequestParameter'} forwardProps={props}/>
        </Box>
    );
});

export const styledInputBox: SxProps<Theme> = {
    marginTop: '1rem',
    marginBottom: '2rem'
};

export default ServerCallRequestParameter;