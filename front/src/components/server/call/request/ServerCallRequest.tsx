import React, {useEffect, useState} from 'react';
import {
    UscCommand,
    UscFeature,
    UscServer,
    UscProperty, UscMetadata
} from "../../../../generated/silaModels";
import {
    Box, Button, Typography
} from '@mui/material/';
import {getCommandParameterInputState} from "../dataTypeForm/silaToState";
import {useDispatch, useSelector} from "react-redux";
import {
    getCallInputMetadatasByIdSelector,
    getCallInputParametersByIdSelector,
} from "../../../../store/front/server/callInput/selector";
import {callInputInit} from "../../../../store/front/server/callInput/actions";
import ServerCallRequestParameter from "./ServerCallRequestParameter";
import {styled} from "@mui/system";
import {AppState} from "../../../../store/rootReducer";
import ServerCallRequestMetadatas from './ServerCallRequestMetadatas';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import InfoIcon from '@mui/icons-material/Info';
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";
import {getServerMetaAffectedCallsByIdSelector} from "../../../../store/front/server/meta/selector";
import IconButton from "@mui/material/IconButton";
import {Tooltip, TooltipProps} from "@mui/material";
import {executeServeCall} from "../../../../utils/call";

export const PaperCallHover = (styled(Box)(({theme}) => ({
    borderColor: 'transparent',
    boxShadow: '0px 0px 2px 1px ' + theme.palette.divider,
    borderRadius: '4px',
    color: theme.palette.text.primary,
    '&:hover': {
        boxShadow: '0px 0px 0px 3px ' + theme.palette.primary.light,
    },
    '&.active': {
        boxShadow: '0px 0px 0px 3px ' + theme.palette.primary.main,
    }
}))) as typeof Box;

function getServerMetadatasAffectedByRequest(features: UscFeature[], metadatas: string[] | undefined): UscMetadata[] {
    if (!metadatas || metadatas.length === 0) {
        return [];
    }
    return features.map(f => (
        f.metadatas.filter(m => metadatas.includes(m.fullyQualifiedIdentifier)).map((m) => ({
            ...m,
            relativeIdentifier: m.fullyQualifiedIdentifier
        }))
    )).flat();
}

export interface ServerCallRequestProps {
    server: UscServer;
    feature: UscFeature;
    call: UscCommand | UscProperty;
    active: boolean
}

const ServerCallRequest = React.memo((props: ServerCallRequestProps): JSX.Element => {
    const dispatch = useDispatch();
    const fullyQualifiedCallId = props.server.serverUuid + '/' + props.call.fullyQualifiedIdentifier;
    const callParameters = useSelector((state: AppState) => getCallInputParametersByIdSelector(state, fullyQualifiedCallId))
    const callMetadatas = useSelector((state: AppState) => getCallInputMetadatasByIdSelector(state, fullyQualifiedCallId))
    const callAffectedByMetadatas = useSelector((state: AppState) => getServerMetaAffectedCallsByIdSelector(state, props.server.serverUuid))
    const [serverMetadatas, setServerMetadatas] = useState<UscMetadata[]>([]);
    const isCommand = (props.call as UscCommand).parameters;
    const isObservable = props.call.observable;
    const pluginProps = {
        ...props,
        parameters: callParameters,
        metadatas: callMetadatas,
        dataTypeDefinitions: props.feature.dataTypeDefinitions,
        parametersDefinition: (props.call as UscCommand).parameters,
        metadatasDefinition: serverMetadatas,
        serverId: props.server.serverUuid,
        fullyQualifiedCallId: fullyQualifiedCallId
    }

    useEffect(() => {
        const affectedByMetadata = callAffectedByMetadatas?.[props.call.fullyQualifiedIdentifier] || [];
        const featureAffectedByMetadata = callAffectedByMetadatas?.[props.feature.fullyQualifiedIdentifier] || [];
        setServerMetadatas(getServerMetadatasAffectedByRequest(props.server.features, [...affectedByMetadata, ...featureAffectedByMetadata]))
    }, [props.server.features, props.feature.fullyQualifiedIdentifier, props.call.fullyQualifiedIdentifier, callAffectedByMetadatas])

    useEffect(() => {
        (async () => {
            if (isCommand && callParameters === undefined) {
                dispatch(callInputInit({
                    fullyQualifiedCommandId: fullyQualifiedCallId,
                    state: getCommandParameterInputState(props.feature.dataTypeDefinitions, (props.call as UscCommand).parameters),
                    formType: "PARAMETER",
                    serverId: props.server.serverUuid
                }))
            }
        })();
    }, [dispatch, fullyQualifiedCallId, isCommand, callParameters, props.call, props.feature.dataTypeDefinitions, props.server.serverUuid])

    function executeCall(readOnce?: boolean): Promise<Response> {
        return executeServeCall(
            props.server.serverUuid,
            props.feature,
            props.call,
            serverMetadatas,
            callMetadatas,
            callParameters,
            readOnce
        );
    }

    return (
        <Box padding={1} key={fullyQualifiedCallId} sx={{outline: 'none'}} tabIndex={0} onKeyUp={(e) => {
            if (e.key === "Enter" && !e.shiftKey) {
                executeCall();
            }
        }}>
            <PaperCallHover className={((props.active) ? ' active' : '')}>
                <Box padding={1} bgcolor={'rgba(148,148,148,0.15)'}>
                    <Box sx={{display: 'inline-flex', alignItems: 'center'}}>
                        <Typography variant={"h6"} paddingRight={1}>
                            {props.call.displayName}
                        </Typography>
                        <Tooltip
                            {...tooltipProps}
                            title={(props.call.observable) ? 'Observable' : 'Unobservable'}>
                            {(props.call.observable) ? <VisibilityIcon/> : <VisibilityOffIcon/>}
                        </Tooltip>
                        <Tooltip
                            {...tooltipProps}
                            title={props.call.fullyQualifiedIdentifier || 'Unknown fully qualified identifier'}>
                            <IconButton>
                                <InfoIcon/>
                            </IconButton>
                        </Tooltip>
                    </Box>
                    <Typography variant={"body2"}>
                        {props.call.description}
                    </Typography>
                </Box>
                <Box padding={1}>
                    {
                        ((props.call as UscCommand).parameters && (
                            <>
                                <Typography variant={'h6'}>Parameters</Typography>
                                <Box margin={2}>
                                    {
                                        (props.call as UscCommand).parameters
                                            .sort((a, b) => a.displayName.localeCompare(b.displayName))
                                            .map(p => (
                                                <ServerCallRequestParameter
                                                    key={fullyQualifiedCallId + '/Parameter/' + p.relativeIdentifier}
                                                    serverUuid={props.server.serverUuid}
                                                    dataTypes={props.feature.dataTypeDefinitions}
                                                    parameter={p}
                                                    values={callParameters}
                                                    dispatch={dispatch}
                                                    fullyQualifiedCommandId={fullyQualifiedCallId}
                                                    type={'PARAMETER'}
                                                />
                                            ))
                                    }
                                </Box>
                            </>
                        ))
                    }
                    <ServerCallRequestMetadatas serverId={props.server.serverUuid} serverMetadatas={serverMetadatas}
                                                fullyQualifiedCallId={fullyQualifiedCallId}
                                                callMetadatas={callMetadatas}/>
                    <Box margin={2}>
                        <PluginsComponentRenderer componentName={'PluginServerCallRequestBody'}
                                                  forwardProps={pluginProps}/>
                    </Box>
                    <Box margin={2}>
                        {
                            ((isObservable || isCommand) && (
                                <Button variant="contained" color="primary" disabled={props.server.status !== 'ONLINE'}
                                        onClick={() => executeCall()}>
                                    {isCommand ? "Execute" : "Subscribe"}
                                </Button>
                            ))
                        }
                        {(!isCommand) && (
                            <Button variant="contained" color="primary" disabled={props.server.status !== 'ONLINE'}
                                    sx={{marginLeft: '8px'}}
                                    onClick={() => executeCall(true)}>
                                Read once
                            </Button>
                        )}
                    </Box>
                </Box>
            </PaperCallHover>
        </Box>
    );
});

export default ServerCallRequest;

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};