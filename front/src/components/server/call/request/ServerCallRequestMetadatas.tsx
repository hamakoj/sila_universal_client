import React, {useEffect} from 'react';
import {
    UscMetadata,
    UscParameter
} from "../../../../generated/silaModels";
import {
    Box, Typography
} from '@mui/material/';
import {getCommandParameterInputState} from "../dataTypeForm/silaToState";
import {useDispatch} from "react-redux";
import {callInputInit} from "../../../../store/front/server/callInput/actions";
import ServerCallRequestParameter from "./ServerCallRequestParameter";
import {SiLATypeValueState} from "../../../../store/front/server/callInput/types";
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";

export interface ServerCallRequestMetadatasProps {
    serverId: string;
    serverMetadatas: UscMetadata[];
    callMetadatas: SiLATypeValueState | undefined;
    fullyQualifiedCallId: string;
}

const ServerCallRequestMetadatas = React.memo((props: ServerCallRequestMetadatasProps): JSX.Element => {
    const dispatch = useDispatch();

    useEffect(() => {
        if (props.serverMetadatas?.length > 0 && props.callMetadatas === undefined) {
            dispatch(callInputInit({
                fullyQualifiedCommandId: props.fullyQualifiedCallId,
                state: getCommandParameterInputState([], props.serverMetadatas as any as UscParameter[]),
                formType: "METADATA",
                serverId: props.serverId
            }))
        }
    }, [dispatch, props.serverMetadatas, props.callMetadatas, props.serverId, props.fullyQualifiedCallId])

    if (!props.serverMetadatas?.length) {
        return (<></>);
    }

    return (
        <>
            <Typography variant={'h6'}>Metadata</Typography>
            <Box padding={2}>
                {
                    (props.serverMetadatas
                            .sort((a, b) => a.displayName.localeCompare(b.displayName))
                            .map(m => (
                                <ServerCallRequestParameter
                                    key={props.fullyQualifiedCallId + '/Metadata/' + m.fullyQualifiedIdentifier}
                                    serverUuid={props.serverId}
                                    dataTypes={[]}
                                    parameter={m}
                                    values={props.callMetadatas}
                                    dispatch={dispatch}
                                    fullyQualifiedCommandId={props.fullyQualifiedCallId}
                                    type={'METADATA'}
                                />
                            ))
                    )
                }
            </Box>
            <PluginsComponentRenderer componentName={'PluginServerCallRequestMetadatas'} forwardProps={props}/>
        </>
    );
});

export default ServerCallRequestMetadatas;