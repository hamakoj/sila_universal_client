import React from 'react';
import CallResponses from "./response/CallResponses";
import SplitScreen from "../../common/SplitScreen";
import ServerCalls from "./ServerCalls";
import {UscCommand, UscFeature, UscProperty, UscServer} from "../../../generated/silaModels";
import {CallType, FullyQualifiedCallId} from "../../../store/front/server/activeCall/types";
import {useSelector} from "react-redux";
import {AppState} from "../../../store/rootReducer";
import {getServerIdActiveCallSelector} from "../../../store/front/server/activeCall/selector";
import {getFullyQualifiedCallId} from "../../../utils/server";
import {CommandResponse} from "../../../store/front/server/response/types";
import {getCommandResponseByIdSelector} from "../../../store/front/server/response/selector";
import {Alert} from "@mui/material";

export interface ServerCallResponseContainerProps {
    server: UscServer;
    type: CallType;
}

export const ServerCallResponseContainer = React.memo((props: ServerCallResponseContainerProps): JSX.Element | null => {
    const activeCall: FullyQualifiedCallId | undefined = useSelector((state: AppState) => getServerIdActiveCallSelector(state, props.server.serverUuid))
    const feature: UscFeature | undefined = activeCall && props.server.features.find(f => f.fullyQualifiedIdentifier === activeCall.fullyQualifiedFeatureId);
    const command: UscCommand | undefined = activeCall && feature?.commands.find(c => c.relativeIdentifier === activeCall.callId);
    const property: UscProperty | undefined = activeCall && feature?.properties.find(c => c.relativeIdentifier === activeCall.callId);
    const fullyQualifiedCallId = activeCall && getFullyQualifiedCallId({serverUuid: props.server.serverUuid, fullyQualifiedFeatureId: activeCall?.fullyQualifiedFeatureId, commandIdentifier: activeCall?.callId});
    const responses: CommandResponse[] = useSelector((state: AppState) => getCommandResponseByIdSelector(state, fullyQualifiedCallId))
    const memoResponses = React.useMemo(() => {
        if (props.type === "command" && command && feature && activeCall) {
            return (
                <CallResponses server={props.server} activeCall={activeCall} feature={feature} command={command} responses={responses} type={props.type}/>
            );
        } else if (props.type === "property" && property && feature && activeCall) {
            return (
                <CallResponses server={props.server} activeCall={activeCall} feature={feature} property={property} responses={responses} type={props.type}/>
            );
        }
        return <Alert severity={"info"}>Select a {((props.type === 'command') ? 'command' : 'property')} to see its responses</Alert>
    }, [props.server, activeCall, feature, command, property, responses, props.type]);
    const memoCalls = React.useMemo(() => {
        return <ServerCalls server={props.server} type={props.type}/>
    }, [props.server, props.type]);
    return (
        <SplitScreen leftName={(props.type === "command") ? ('Command') : ('Property') }
                     rightName={(props.type === "command") ? ('Response') : ('Value') }
                     leftComponent={memoCalls}
                     rightComponent={memoResponses}
        />
    );
});

export default ServerCallResponseContainer;
