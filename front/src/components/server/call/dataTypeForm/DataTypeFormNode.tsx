import React from "react";
import DataType, {DataTypeProps} from "./type/DataType";
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";
import {Box} from "@mui/material/";

export interface DataTypeFormNodeProps extends DataTypeProps {

}

const DataTypeFormNode = React.memo((props: DataTypeFormNodeProps) : JSX.Element => {
    return (
        <Box marginX={0} marginY={1} sx={{background: 'rgb(112 112 112 / 4%)', borderRadius: '4px'}}>
            <DataType
                {...props}
                value={props.rootValue}
            />
            <PluginsComponentRenderer componentName={'PluginDataTypeFormRoot'} forwardProps={props}/>
        </Box>
    );
});

export default DataTypeFormNode;
