import {SiLAJavaConstraints, SiLAJavaDataTypeType, UscSiLAElement} from "../../../../generated/silaModels";
import React from "react";
import {FormInputType, SiLATypeValueState} from "../../../../store/front/server/callInput/types";
import DataTypeFormNode from "./DataTypeFormNode";
import {Dispatch} from "redux";
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";
import {shallowEqual} from "react-redux";

export interface DataTypeFormRootProps {
    dataTypeDefinitions: UscSiLAElement[];
    dataType: SiLAJavaDataTypeType;
    stateId: string;
    readonly: boolean;
    parameterId: string;
    serverId: string;
    value: SiLATypeValueState | undefined;
    fullyQualifiedCommandId: string;
    runtimeConstraints?: SiLAJavaConstraints[]
    type: FormInputType;
    dispatch?: Dispatch;
}

const DataTypeFormRoot = React.memo((props: DataTypeFormRootProps): JSX.Element => {
    if (props.readonly || props.type === "METADATA") {
        const {runtimeConstraints, ...filteredProps} = props;
        return (
            <>
                <DataTypeFormNode
                    {...filteredProps}
                    rootValue={props.value}
                    depth={1}
                    dataTypeIdentifier={''}
                />
                <PluginsComponentRenderer componentName={'PluginDataTypeFormNode'} forwardProps={props}/>
            </>
        )
    }
    return (
        <>
            <DataTypeFormNode
                {...props}
                rootValue={props.value}
                depth={1}
                dataTypeIdentifier={''}
            />
            <PluginsComponentRenderer componentName={'PluginDataTypeFormNode'} forwardProps={props}/>
        </>
    )
}, (prev, next) => {
    if (prev.readonly && next.readonly) {
        // if readonly, ignore runtime constraints
        const {runtimeConstraints: prevRuntimeConstraints, ...prevRest} = prev;
        const {runtimeConstraints: nextRuntimeConstraints, ...nextRest} = next;
        return shallowEqual(prevRest, nextRest);
    }
    return shallowEqual(prev, next);
});

export default DataTypeFormRoot;

