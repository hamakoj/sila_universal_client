import {SiLAJavaDataTypeType, UscSiLAElement} from "../../../../generated/silaModels";
import {SiLATypeValueMap, SiLATypeValueState} from "../../../../store/front/server/callInput/types";
import SiLAXMLParser from "../../../../utils/parser/siLAXMLParser";
import {Type} from "protobufjs";
import * as Protobuf from "protobufjs";
import {ListValueType} from "./type/List";
import {ProtoAny} from "../../../../generated/SiLAFramework";

export function parametersStateToGrpcJson(dataTypeDefinition: UscSiLAElement[], parameters: UscSiLAElement[], state: SiLATypeValueState) {
    let jsonParameters = {};
    parameters.forEach((p) => {
        jsonParameters = {
            ...jsonParameters,
            [p.relativeIdentifier]: stateToGrpcJson(
                dataTypeDefinition,
                p.dataType,
                p.relativeIdentifier,
                state
            )
        };
    })
    return jsonParameters;
}

export function parametersStateToGrpcJsonFromDataType(
    dataTypeDefinition: UscSiLAElement[],
    dataTypeType: SiLAJavaDataTypeType,
    relativeIdentifier: string,
    state: SiLATypeValueState
) {
    return {
        [relativeIdentifier]: stateToGrpcJson(
            dataTypeDefinition,
            dataTypeType,
            relativeIdentifier,
            state
        )
    };
}

export function metadatasStateToGrpcJson(metadatas: UscSiLAElement[], state: SiLATypeValueState) {
    const metadatasJson: any = parametersStateToGrpcJson(
        [],
        metadatas,
        state
    );
    
    return Object.keys(metadatasJson).reduce((acc: any, key: string) => {
        const strings = key.split('/');
        const id = strings[strings.length - 1];
        if (metadatasJson[key]) {
            acc[key] = {
                [id]: metadatasJson[key]
            };
        }
        return acc;
    }, {});
}

function dataTypeIdentifierStateToGrpcJson(
    dataTypeDefinitions: UscSiLAElement[],
    dataType: SiLAJavaDataTypeType,
    stateId: string,
    state: SiLATypeValueState
) {
    const targetDataType: UscSiLAElement | undefined = dataTypeDefinitions
        .find((d) => d.relativeIdentifier === dataType.dataTypeIdentifier);
    if (targetDataType === undefined) {
        throw new Error('Unknown datatype: ' + dataType.dataTypeIdentifier + 'in ' + stateId)
    }
    return {
        [targetDataType.relativeIdentifier]: stateToGrpcJson(
            dataTypeDefinitions,
            targetDataType.dataType,
            stateId + '/' + targetDataType.relativeIdentifier,
            state
        )
    };
}

function constrainedStateToGrpcJson(
    dataTypeDefinitions: UscSiLAElement[],
    dataType: SiLAJavaDataTypeType,
    stateId: string,
    state: SiLATypeValueState
) {
    return stateToGrpcJson(
        dataTypeDefinitions,
        dataType.constrained.dataType,
        stateId,
        state,
    );
}

function structureStateToGrpcJson(
    dataType: SiLAJavaDataTypeType,
    dataTypeDefinitions: UscSiLAElement[],
    stateId: string,
    state: SiLATypeValueState
) {
    let json = {};
    dataType.structure.element.forEach(element => {
        json = {
            ...json,
            [element.identifier]: stateToGrpcJson(
                dataTypeDefinitions,
                element.dataType,
                stateId + '/' + element.identifier,
                state
            )
        }
    });
    return json;
}

function listStateToGrpcJson(
    state: SiLATypeValueState,
    stateId: string,
    dataTypeDefinitions: UscSiLAElement[],
    dataType: SiLAJavaDataTypeType
) {
    if ((state as SiLATypeValueMap)[stateId] && Array.isArray((state as SiLATypeValueMap)[stateId])) {
        return ((state as SiLATypeValueMap)[stateId] as ListValueType).map((subStateId: string) =>
            stateToGrpcJson(dataTypeDefinitions, dataType.list.dataType, subStateId, state)
        );
    }
    // todo else error?
}

export function convertGrpcJsonToProtoAny(dataType: SiLAJavaDataTypeType, silaValue: any, xml: string): ProtoAny {
    const protoMessage = new Type('');
    protoMessage.add(Protobuf.parse(SiLAXMLParser.getSiLAFrameworkProto()).root); // todo fixme need a new object each time
    protoMessage.add(SiLAXMLParser.parseProtoFieldFromDataType(protoMessage, 'any', dataType));
    const protoMessageToUse = (dataType.basic) ? protoMessage.lookupType(protoMessage.fieldsArray[0].type) : protoMessage;
    const jsonMessage = protoMessageToUse.create((dataType.basic) ? (silaValue) : ({[protoMessage.fieldsArray[0].name]: silaValue}));
    const messageBuffer = protoMessageToUse.encode(jsonMessage).finish();
    const b64encodedMessage = btoa(String.fromCharCode.apply(null, messageBuffer as unknown as number[]));

    return {
        type: xml,
        payload: b64encodedMessage
    }
}

export function anyTypeStateTpGrpcJson(state: SiLATypeValueMap | string[] | ProtoAny, stateId: string, dataTypeDefinitions: UscSiLAElement[]) {
    const dataType = SiLAXMLParser.parseXml(((state as SiLATypeValueMap)[stateId] as ProtoAny)?.type);
    if (dataType === null) {
        return {};
    }
    const silaValue = stateToGrpcJson(dataTypeDefinitions, dataType, stateId + '/any', state);
    return convertGrpcJsonToProtoAny(dataType, silaValue, ((state as SiLATypeValueMap)[stateId] as ProtoAny).type || '');
}

export const stateToGrpcJson = (
    dataTypeDefinitions: UscSiLAElement[],
    dataType: SiLAJavaDataTypeType,
    stateId: string,
    state: SiLATypeValueState
): any => {
    if (dataType.basic) {
        if (dataType.basic.toUpperCase() === "ANY") {
            return anyTypeStateTpGrpcJson(state, stateId, dataTypeDefinitions);
        }
        return (state as SiLATypeValueMap)[stateId];
    }
    if (dataType.dataTypeIdentifier) {
        return dataTypeIdentifierStateToGrpcJson(dataTypeDefinitions, dataType, stateId, state);
    }
    if (dataType.constrained) {
        return constrainedStateToGrpcJson(dataTypeDefinitions, dataType, stateId, state);
    }
    if (dataType.structure) {
        return structureStateToGrpcJson(dataType, dataTypeDefinitions, stateId, state);
    }
    if (dataType.list) {
        return listStateToGrpcJson(state, stateId, dataTypeDefinitions, dataType);
    }
    // todo error
};