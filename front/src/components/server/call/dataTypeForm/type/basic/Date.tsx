import React, {useEffect} from "react";
import DataTypeFormProps from "../dataTypeFormProps";
import {SiLAJavaBasicType} from "../../../../../../generated/silaModels";
import {Container, InputAdornment, TextField, TextFieldProps, Tooltip} from "@mui/material";
import {DateTime} from "luxon";
import {dateTimeToProtoDate, protoDateToDatetime} from "../../../../../../utils/datetime";
import {ProtoDate} from "../../../../../../generated/SiLAFramework";
import {useSelector} from "react-redux";
import {getAppTimezoneSettingsSelector} from "../../../../../../store/front/app/setting/selector";
import {
    getCurrentTimeZoneName, getCurrentTimezoneOffsetString,
    getProtoTimezoneFromZone,
    getTimezoneOffsetString,
    getZoneFromTimezoneSettings
} from "../../../../../../utils/timezone";
import PluginsComponentRenderer from "../../../../../../plugin/PluginsComponentRenderer";
import {DatePicker} from "@mui/x-date-pickers";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";

export interface DateProps extends DataTypeFormProps<SiLAJavaBasicType, ProtoDate> {

}

export default function Date(props: DateProps): JSX.Element {
    const timezoneSettings = useSelector(getAppTimezoneSettingsSelector);
    const {value, onValueChange} = props;
    const isInput = onValueChange && !props.readonly;
    const utcOffset = getTimezoneOffsetString(props.value.timezone?.hours || 0, props.value.timezone?.minutes || 0);
    const format = "yyyy-MM-dd";

    useEffect(() => {
        if (!isInput) {
            return
        }
        const protoTimezoneFromZone = getProtoTimezoneFromZone(getZoneFromTimezoneSettings(timezoneSettings));
        const isTimezoneDifferent = protoTimezoneFromZone.hours !== value.timezone?.hours || protoTimezoneFromZone.minutes !== value.timezone?.minutes;
        if (isTimezoneDifferent) {
            onValueChange({
                ...value,
                timezone: protoTimezoneFromZone
            });
        }
    }, [isInput, value, onValueChange, timezoneSettings]);

    function onChange(date: DateTime | null) {
        if (props.onValueChange && date) {
            props.onValueChange(dateTimeToProtoDate(date));
        }
    }

    function toLuxon(): DateTime {
        return protoDateToDatetime(props.value);
    }

    // todo implement min & max for Date & Time & Timestamp
    //  minDate={new Date('2012-03-01')}
    //  maxDate={new Date('2023-06-01')}
    return (
        <>
            <DatePicker
                inputFormat={format}
                mask={"____-__-__"}
                value={toLuxon()}
                onChange={onChange}
                label={'Date'}
                disabled={props.readonly}
                renderInput={(p: JSX.IntrinsicAttributes & TextFieldProps) => (
                    <TextField {...p}
                               InputLabelProps={{
                                   ...p.InputLabelProps,
                                   shrink: true,
                               }}
                               InputProps={{
                                   ...p.InputProps,
                                   disabled: props.readonly,
                                   startAdornment:
                                       <Container disableGutters sx={{display: 'flex', alignItems: 'center', width: 'auto'}}>
                                           <InputAdornment sx={{margin: 0}} position="start">{utcOffset}</InputAdornment>
                                           <Tooltip
                                               componentsProps={{tooltip: {sx: {fontSize: '14px', whiteSpace: 'pre-wrap'}}}}
                                               title={toLuxon().toLocal().toFormat(format) + '\nBased on your local/configured timezone:\n' + getCurrentTimeZoneName() + ' - ' + getCurrentTimezoneOffsetString() + '.'}>
                                               <IconButton>
                                                   <InfoIcon/>
                                               </IconButton>
                                           </Tooltip>
                                       </Container>
                               }}
                               fullWidth={true}
                    />
                )}
            />
            <PluginsComponentRenderer componentName={'PluginDateTypeForm'} forwardProps={{...props, utcOffset: utcOffset}}/>
        </>
    )
};
