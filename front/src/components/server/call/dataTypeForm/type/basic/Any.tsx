import React, {ChangeEvent} from "react";
import DataTypeFormProps from "../dataTypeFormProps";
import {SiLAJavaBasicType} from "../../../../../../generated/silaModels";
import {TextField, Typography} from "@mui/material";
import SiLAXMLParser from "../../../../../../utils/parser/siLAXMLParser";
import DataTypeFormNode from "../../DataTypeFormNode";
import {dataTypeToState} from "../../silaToState";
import {callInputUpdate} from "../../../../../../store/front/server/callInput/actions";
import {ProtoAny} from "../../../../../../generated/SiLAFramework";
import {BasicValueType} from "../Basic";
import PluginsComponentRenderer from "../../../../../../plugin/PluginsComponentRenderer";

export interface AnyProps extends DataTypeFormProps<SiLAJavaBasicType, ProtoAny> {

}

export default function Any(props: AnyProps): JSX.Element {
    const parsedAnyDataType = SiLAXMLParser.parseXml(props.value?.type || '');

    function onDataTypeChange(event: ChangeEvent<any>) {
        event.stopPropagation();
        if (props.onValueChange) {
            const dataType = SiLAXMLParser.parseXml(event.target.value);
            if (dataType && props.dispatch) {
                const newState = dataTypeToState([], dataType, props.stateId + '/any', null);
                Object.entries(newState).forEach(([k, v]: [string, BasicValueType]) => {
                    if (!props.dispatch)
                        return;
                    // todo remove previous states
                    props.dispatch(callInputUpdate({
                        fullyQualifiedCommandId: props.fullyQualifiedCommandId,
                        stateId: k,
                        updatedValue: v,
                        formType: props.type,
                        serverId: props.serverId
                    }));
                });
            }
            props.onValueChange({
                ...props.value,
                type: event.target.value,
            });
        }
    }

    return (
        <>
            {
                <TextField
                    id="outlined-multiline-static"
                    label="Data type XML"
                    multiline
                    fullWidth={true}
                    rows={4}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    InputProps={{
                        disabled: props.readonly,
                    }}
                    value={props.value.type}
                    onChange={onDataTypeChange}
                />
            }
            {
                (parsedAnyDataType !== null) ? (
                    <DataTypeFormNode
                        {...props}
                        dataType={parsedAnyDataType}
                        stateId={props.stateId + '/any'}
                    />
                ) : (<Typography>Data type is invalid</Typography>)
            }
            <PluginsComponentRenderer componentName={'PluginAnyTypeForm'} forwardProps={{...props, parsedAnyDataType: parsedAnyDataType}}/>
        </>
    );
};
