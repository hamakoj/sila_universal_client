import React, {useEffect} from "react";
import DataTypeFormProps from "../dataTypeFormProps";
import {SiLAJavaBasicType} from "../../../../../../generated/silaModels";
import {Container, InputAdornment, TextField, TextFieldProps, Tooltip} from "@mui/material";
import {DateTime} from "luxon";
import {protoTimeToDatetime} from "../../../../../../utils/datetime";
import {ProtoTime} from "../../../../../../generated/SiLAFramework";
import {TimePicker} from "@mui/x-date-pickers";
import {useSelector} from "react-redux";
import {getAppTimezoneSettingsSelector} from "../../../../../../store/front/app/setting/selector";
import {
    dateTimeToProtoTime, getCurrentTimeZoneName, getCurrentTimezoneOffsetString,
    getProtoTimezoneFromZone, getTimezoneOffsetString,
    getZoneFromTimezoneSettings
} from "../../../../../../utils/timezone";
import PluginsComponentRenderer from "../../../../../../plugin/PluginsComponentRenderer";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";

export interface TimeProps extends DataTypeFormProps<SiLAJavaBasicType, ProtoTime> {

}

export default function Time(props: TimeProps): JSX.Element {
    const timezoneSettings = useSelector(getAppTimezoneSettingsSelector);
    const {value, onValueChange} = props;
    const isInput = onValueChange && !props.readonly;
    const utcOffset = getTimezoneOffsetString(props.value.timezone?.hours || 0, props.value.timezone?.minutes || 0);
    const format = "HH:mm:ss";

    useEffect(() => {
        if (!isInput) {
            return
        }
        const protoTimezoneFromZone = getProtoTimezoneFromZone(getZoneFromTimezoneSettings(timezoneSettings));
        const isTimezoneDifferent = protoTimezoneFromZone.hours !== value.timezone?.hours || protoTimezoneFromZone.minutes !== value.timezone?.minutes;
        if (isTimezoneDifferent) {
            onValueChange({
                ...value,
                timezone: protoTimezoneFromZone
            });
        }
    }, [isInput, value, onValueChange, timezoneSettings]);

    function onChange(date: DateTime | null) {
        if (props.onValueChange && date) {
            props.onValueChange(dateTimeToProtoTime(date));
        }
    }

    function toLuxon(): DateTime {
        return protoTimeToDatetime(props.value);
    }

    return (
        <>
            <TimePicker
                views={['hours', 'minutes', 'seconds']}
                ampm={false}
                inputFormat={format}
                mask={"__:__:__"}
                value={toLuxon()}
                onChange={onChange}
                label={'Time'}
                disabled={props.readonly}
                renderInput={(p: JSX.IntrinsicAttributes & TextFieldProps) => (
                    <TextField {...p}
                               InputLabelProps={{
                                   ...p.InputLabelProps,
                                   shrink: true,
                               }}
                               InputProps={{
                                   ...p.InputProps,
                                   readOnly: props.readonly,
                                   startAdornment:
                                       <Container disableGutters sx={{display: 'flex', alignItems: 'center', width: 'auto'}}>
                                           <InputAdornment sx={{margin: 0}} position="start">{utcOffset}</InputAdornment>
                                           <Tooltip
                                               componentsProps={{tooltip: {sx: {fontSize: '14px', whiteSpace: 'pre-wrap'}}}}
                                               title={toLuxon().toLocal().toFormat(format) + '\nBased on your local/configured timezone:\n' + getCurrentTimeZoneName() + ' - ' + getCurrentTimezoneOffsetString() + '.'}>
                                               <IconButton>
                                                   <InfoIcon/>
                                               </IconButton>
                                           </Tooltip>
                                       </Container>
                                }}
                               fullWidth={true}
                    />
                )}
            />
            <PluginsComponentRenderer componentName={'PluginTimeTypeForm'} forwardProps={{...props, utcOffset: utcOffset}}/>
        </>
    )
};
