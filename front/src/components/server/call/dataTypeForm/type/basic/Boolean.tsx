import React, {ChangeEvent} from "react";
import DataTypeFormProps from "../dataTypeFormProps";
import {SiLAJavaBasicType} from "../../../../../../generated/silaModels";
import {Grid, Switch} from "@mui/material";
import {ProtoBoolean} from "../../../../../../generated/SiLAFramework";
import PluginsComponentRenderer from "../../../../../../plugin/PluginsComponentRenderer";

export interface BooleanProps extends DataTypeFormProps<SiLAJavaBasicType, ProtoBoolean> {

}

export default function Boolean(props: BooleanProps): JSX.Element {
    function onChange(event: ChangeEvent<any>, checked: boolean) {
        event.stopPropagation();
        if (props.onValueChange) {
            props.onValueChange({value: checked});
        }
    }

    return (
        <Grid component="label" container alignItems="center" spacing={1}>
            <Grid item>No</Grid>
            <Grid item>
                <Switch disabled={props.readonly} checked={props.value.value} onChange={onChange}/>
            </Grid>
            <Grid item>Yes</Grid>
            <PluginsComponentRenderer componentName={'PluginBooleanTypeForm'} forwardProps={props}/>
        </Grid>
    );
};
