import React, {useEffect} from "react";
import DataTypeFormProps from "../dataTypeFormProps";
import {SiLAJavaBasicType} from "../../../../../../generated/silaModels";
import {DateTime} from "luxon";
import {
    dateTimeToProtoTimestamp,
    protoTimestampToDatetime
} from "../../../../../../utils/datetime";
import {Container, InputAdornment, TextField, TextFieldProps, Tooltip} from "@mui/material";
import {ProtoTimestamp} from "../../../../../../generated/SiLAFramework";
import {DateTimePicker} from "@mui/x-date-pickers";
import {useSelector} from "react-redux";
import {getAppTimezoneSettingsSelector} from "../../../../../../store/front/app/setting/selector";
import {
    getCurrentTimeZoneName, getCurrentTimezoneOffsetString,
    getProtoTimezoneFromZone,
    getTimezoneOffsetString,
    getZoneFromTimezoneSettings
} from "../../../../../../utils/timezone";
import PluginsComponentRenderer from "../../../../../../plugin/PluginsComponentRenderer";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";

export interface TimestampProps extends DataTypeFormProps<SiLAJavaBasicType, ProtoTimestamp> {

}

export default function Timestamp(props: TimestampProps): JSX.Element {
    const timezoneSettings = useSelector(getAppTimezoneSettingsSelector);
    const {value, onValueChange} = props;
    const isInput = onValueChange && !props.readonly;
    const utcOffset = getTimezoneOffsetString(props.value.timezone?.hours || 0, props.value.timezone?.minutes || 0);
    const format = "yyyy-MM-dd'T'HH:mm:ss";

    useEffect(() => {
        if (!isInput) {
            return
        }
        const protoTimezoneFromZone = getProtoTimezoneFromZone(getZoneFromTimezoneSettings(timezoneSettings));
        const isTimezoneDifferent = protoTimezoneFromZone.hours !== value.timezone?.hours || protoTimezoneFromZone.minutes !== value.timezone?.minutes;
        if (isTimezoneDifferent) {
            onValueChange({
                ...value,
                timezone: protoTimezoneFromZone
            });
        }
    }, [isInput, value, onValueChange, timezoneSettings]);

    function onChange(date: DateTime | null) {
        if (props.onValueChange && date) {
            props.onValueChange(dateTimeToProtoTimestamp(date));
        }
    }

    function toLuxon(): DateTime {
        return protoTimestampToDatetime(props.value)
    }

    return (
        <>
            <DateTimePicker
                ampm={false}
                views={['year', 'month', 'day', 'hours', 'minutes', 'seconds']}
                inputFormat="yyyy-MM-dd'T'HH:mm:ss"
                mask={"____-__-__T__:__:__"}
                value={toLuxon()}
                onChange={onChange}
                label={'Timestamp'}
                disabled={props.readonly}
                renderInput={(p: JSX.IntrinsicAttributes & TextFieldProps) => (
                    <TextField {...p}
                               InputLabelProps={{
                                   ...p.InputLabelProps,
                                   shrink: true,
                               }}
                               InputProps={{
                                   ...p.InputProps,
                                   disabled: props.readonly,
                                   startAdornment:
                                       <Container disableGutters sx={{display: 'flex', alignItems: 'center', width: 'auto'}}>
                                           <InputAdornment sx={{margin: 0}} position="start">{utcOffset}</InputAdornment>
                                           <Tooltip
                                               componentsProps={{tooltip: {sx: {fontSize: '14px', whiteSpace: 'pre-wrap'}}}}
                                               title={toLuxon().toLocal().toFormat(format) + '\nBased on your local/configured timezone:\n' + getCurrentTimeZoneName() + ' - ' + getCurrentTimezoneOffsetString() + '.'}>
                                               <IconButton>
                                                   <InfoIcon/>
                                               </IconButton>
                                           </Tooltip>
                                       </Container>
                                }}
                               fullWidth={true}
                    />
                )}
            />
            <PluginsComponentRenderer componentName={'PluginTimestampTypeForm'} forwardProps={{...props, utcOffset: utcOffset}}/>
        </>
    )
};
