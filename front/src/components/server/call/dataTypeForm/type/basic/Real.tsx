import React, {ChangeEvent} from "react";
import DataTypeFormProps from "../dataTypeFormProps";
import {SiLAJavaBasicType} from "../../../../../../generated/silaModels";
import {InputAdornment, TextField} from "@mui/material";
import NumberUnitConstraint from "../constraints/NumberUnitConstraint";
import {ProtoReal} from "../../../../../../generated/SiLAFramework";
import PluginsComponentRenderer from "../../../../../../plugin/PluginsComponentRenderer";

export interface RealProps extends DataTypeFormProps<SiLAJavaBasicType, ProtoReal> {

}

export default function Real(props: RealProps): JSX.Element {
    function onChange(event: ChangeEvent<any>) {
        event.stopPropagation();
        if (props.onValueChange) {
            props.onValueChange({value: event.target.value});
        }
    }

    return (
        <>
            <TextField
                fullWidth={true}
                value={props.value.value}
                type="number"
                onChange={onChange}
                label={'Real'}
                InputLabelProps={{
                    shrink: true,
                }}
                InputProps={{
                    disabled: props.readonly,
                    endAdornment:
                        <>
                            {(!props.constraints?.unit) ? (<></>) : (
                                <InputAdornment position="end">{props.constraints?.unit.label || ''}</InputAdornment>
                            )}
                        </>,
                }}
            />
            {(props.constraints?.unit && !props.readonly) && <NumberUnitConstraint acceptDecimal={true} {...props}/>}
            <PluginsComponentRenderer componentName={'PluginRealTypeForm'} forwardProps={props}/>
        </>
    );
};
