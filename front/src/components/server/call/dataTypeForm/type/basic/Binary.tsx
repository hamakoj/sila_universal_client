import React from "react";
import DataTypeFormProps from "../dataTypeFormProps";
import {BinaryDownload, BinaryUpload, MultipartFile, SiLAJavaBasicType} from "../../../../../../generated/silaModels";
import {Button, TextField} from "@mui/material";
import {ProtoBinary} from "../../../../../../generated/SiLAFramework";
import {getBackEndpoint} from "../../../../../../utils/endpoint";
import PluginsComponentRenderer from "../../../../../../plugin/PluginsComponentRenderer";
import {DateTime} from "luxon";
import {metadatasStateToGrpcJson} from "../../stateToGrpcJson";
import {useSelector} from "react-redux";
import {AppState} from "../../../../../../store/rootReducer";
import {getCallInputMetadatasByIdSelector} from "../../../../../../store/front/server/callInput/selector";
import {
    getServerFeaturesByIdSelector
} from "../../../../../../store/sila/manager/server/selector";
import {getCallMetadatas} from "../../../../../../utils/call";

export interface BinaryProps extends DataTypeFormProps<SiLAJavaBasicType, ProtoBinary> {

}

export default function Binary(props: BinaryProps): JSX.Element {
    const features = useSelector((state: AppState) => getServerFeaturesByIdSelector(state, props.serverId));
    const callMetadatas = useSelector((state: AppState) => getCallInputMetadatasByIdSelector(state, props.fullyQualifiedCommandId))

    const MAX_CHUNK_SIZE = 2097152;
    let fileReader: FileReader;

    async function downloadFile() {
        let blob;
        if (props.value.value) {
            const base64Response = await fetch(`data:image/jpeg;base64,${props.value.value}`);
            blob = await base64Response.blob();
        } else if (props.value.binaryTransferUUID) {
            const largeBinaryResponse = await fetch(getBackEndpoint('/api/calls/binary/download'), {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    serverId: props.serverId,
                    binaryTransferUuid: props.value.binaryTransferUUID
                } as BinaryDownload)
            });
            blob = await largeBinaryResponse.blob();
        }
        if (!blob) {
            // todo error
            return;
        }
        // Create blob link to download
        const url = window.URL.createObjectURL(
            new Blob([blob]),
        );
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute(
            'download',
            `usc-binary-${DateTime.now().toString()}.bin`,
        );

        document.body.appendChild(link);

        link.click();
        link.parentNode?.removeChild(link);
    }

    function handleFileRead() {
        if (typeof fileReader.result === 'string') {
            const content = btoa(unescape(encodeURIComponent(fileReader.result)));
            console.log('encoded base64 binary length ' + content.length);
            if (props.onValueChange) {
                props.onValueChange({value: content});
            }
        } else {
            // todo handle arraybuffer
        }
        fileReader = new FileReader();
    }

    function handleFileChosen(files: FileList | null) {
        if (!files || !files.length) {
            return;
        }
        if (files[0].size > MAX_CHUNK_SIZE) {
            const fullyQualifiedCommandID = props.fullyQualifiedCommandId.replace(props.serverId + '/', '');
            const metadatas = metadatasStateToGrpcJson(getCallMetadatas(features || []), callMetadatas || {});

            const binaryUpload: BinaryUpload = {
                file: files[0] as unknown as MultipartFile,
                serverId: props.serverId,
                metadatas: JSON.stringify(metadatas),
                parameterIdentifier: fullyQualifiedCommandID + '/Parameter/' + props.parameterId
            }

            const formData = new FormData();
            for (const [key, value] of Object.entries(binaryUpload)) {
                formData.append(key, value);
            }
            fetch(getBackEndpoint('/api/calls/binary/upload'), {method: 'POST', body: formData})
                .then(response => response.text())
                .then((data) => {
                    if (props.onValueChange) {
                        props.onValueChange({
                            value: undefined,
                            binaryTransferUUID: data
                        });
                    }
                })
                .catch((error) => {
                    console.error(error);
                })
            return;
        }
        fileReader = new FileReader();
        fileReader.onloadend = handleFileRead;
        fileReader.readAsText(files[0]);
    }

    return (
        <>
            {
                (!props.readonly) ? (
                    <Button
                        variant="contained"
                        component="label"
                        sx={{marginBottom: '8px'}}
                    >
                        Upload File
                        <input
                            type="file"
                            onChange={e => handleFileChosen(e.target.files)}
                            hidden
                        />
                    </Button>
                ) : (
                    <Button
                        variant="contained"
                        component="label"
                        sx={{marginBottom: '8px'}}
                        onClick={downloadFile}
                    >
                        Download File
                    </Button>
                )
            }
            {
                (props.value.value) ? (
                    <TextField
                        label="Base64 Binary"
                        multiline
                        rows={4}
                        fullWidth={true}
                        value={props.value.value}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        InputProps={{
                            disabled: props.readonly,
                        }}
                />) : (
                    <TextField
                        label="Binary transfer UUID"
                        fullWidth={true}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        InputProps={{
                            disabled: props.readonly,
                        }}
                        value={props.value.binaryTransferUUID}
                    />
                )
            }
            <PluginsComponentRenderer componentName={'PluginBinaryTypeForm'} forwardProps={props}/>
        </>
    );
};