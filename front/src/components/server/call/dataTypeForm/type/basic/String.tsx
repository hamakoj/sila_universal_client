import React, {ChangeEvent} from "react";
import DataTypeFormProps from "../dataTypeFormProps";
import {SiLAJavaBasicType} from "../../../../../../generated/silaModels";
import {ProtoString} from "../../../../../../generated/SiLAFramework";
import {InputAdornment, TextField, Tooltip, TooltipProps} from "@mui/material";
import PluginsComponentRenderer from "../../../../../../plugin/PluginsComponentRenderer";
import KeyIcon from '@mui/icons-material/Key';
import IconButton from "@mui/material/IconButton";
import {useSelector} from "react-redux";
import {AppState} from "../../../../../../store/rootReducer";
import {
    getAppUserAccessTokenForServerIdSelector,
    getAppUserLockTokenForServerIdSelector
} from "../../../../../../store/front/app/user/selector";
import {BasicValueType} from "../Basic";
import {DateTime} from "luxon";
import {isAfter} from "../../../../../../utils/datetime";
import {getServerMetaAuthorizationProvidersByIdSelector} from "../../../../../../store/front/server/meta/selector";

export interface StringProps extends DataTypeFormProps<SiLAJavaBasicType, ProtoString> {

}

export const String = React.memo((props: StringProps): JSX.Element => {
    const isAccessToken = props.type === 'METADATA' && props.parameterId === 'org.silastandard/core/AuthorizationService/v1/Metadata/AccessToken';
    const isLockToken = props.type === 'METADATA' && props.parameterId === 'org.silastandard/core/LockController/v1/Metadata/LockIdentifier';

    function onChange(event: ChangeEvent<any>) {
        event.stopPropagation();
        props.onValueChange?.({value: event.target.value});
    }

    return (
        <>
            <TextField
                fullWidth={true}
                value={props.value.value}
                InputLabelProps={{
                    shrink: true,
                }}
                InputProps={{
                    disabled: props.readonly,
                    endAdornment:
                        <>
                            <AccessTokenEndAdornment
                                isAccessToken={isAccessToken}
                                readonly={props.readonly}
                                serverId={props.serverId}
                                onValueChange={props.onValueChange}
                            />
                            <LockTokenEndAdornment
                                isLockToken={isLockToken}
                                readonly={props.readonly}
                                serverId={props.serverId}
                                onValueChange={props.onValueChange}
                            />
                        </>
                }}
                label={'String'}
                onChange={(e) => onChange(e)}
            />
            <PluginsComponentRenderer componentName={'PluginStringTypeForm'} forwardProps={props}/>
        </>
    );
});

export default String;

export interface AccessTokenEndAdornmentProps {
    serverId: string;
    readonly: boolean;
    isAccessToken: boolean;
    onValueChange?: (newValue: BasicValueType) => void;
}

export const AccessTokenEndAdornment = React.memo((props: AccessTokenEndAdornmentProps): JSX.Element => {
    const userAccessToken = useSelector((state: AppState) => getAppUserAccessTokenForServerIdSelector(state, props.serverId));
    const authorizationProvider = useSelector((state: AppState) => getServerMetaAuthorizationProvidersByIdSelector(state, props.serverId));
    if (!props.isAccessToken || props.readonly || !userAccessToken) {
        return <></>
    }
    const authorizationProviderServerUuid = (authorizationProvider?.AuthorizationProvider) ? authorizationProvider?.AuthorizationProvider.value : props.serverId;
    const tokenMatchServerAuthProvider = authorizationProviderServerUuid === userAccessToken.authorizedByServerUuid;
    const expirationUTC = userAccessToken?.expirationUTC ? DateTime.fromISO(userAccessToken.expirationUTC) : undefined;
    const hasValidToken = (userAccessToken && expirationUTC && isAfter(expirationUTC, DateTime.now().toUTC(), 'second'))
    const hasExpiredToken = (userAccessToken && (!expirationUTC || !isAfter(expirationUTC, DateTime.now().toUTC(), 'second')))
    let toolTipText = 'Insert session access token';
    if (hasExpiredToken) {
        toolTipText = 'Insert (expired) session access token'
    } else if (!tokenMatchServerAuthProvider) {
        toolTipText = 'Insert (potentially invalid because issued by a different Authorization Provider) session access token'
    }
    return (
        <InputAdornment position="end">
            <Tooltip {...tooltipProps} title={toolTipText}>
                <IconButton
                    aria-label={toolTipText}
                    onClick={() => {
                        props.onValueChange?.({value: userAccessToken?.token || ''})
                    }}
                    onMouseDown={(event) => {
                        event.preventDefault();
                    }}
                    edge="end"
                >
                    <KeyIcon color={(!hasExpiredToken && hasValidToken && tokenMatchServerAuthProvider) ? 'success' : 'warning'}/>
                </IconButton>
            </Tooltip>
        </InputAdornment>
    );
});

export interface LockTokenEndAdornmentProps {
    serverId: string;
    readonly: boolean;
    isLockToken: boolean;
    onValueChange?: (newValue: BasicValueType) => void;
}

export const LockTokenEndAdornment = React.memo((props: LockTokenEndAdornmentProps): JSX.Element => {
    const userLockToken = useSelector((state: AppState) => getAppUserLockTokenForServerIdSelector(state, props.serverId));
    if (!props.isLockToken || props.readonly || !userLockToken) {
        return <></>
    }
    const expirationUTC = userLockToken?.expirationUTC ? DateTime.fromISO(userLockToken.expirationUTC) : undefined;
    const hasValidToken = (userLockToken && expirationUTC && isAfter(expirationUTC, DateTime.now().toUTC(), 'second'))
    const hasExpiredToken = (userLockToken && (!expirationUTC || !isAfter(expirationUTC, DateTime.now().toUTC(), 'second')))
    let toolTipText = 'Insert server lock token';
    if (hasExpiredToken) {
        toolTipText = 'Insert (expired) server lock token'
    }
    return (
        <InputAdornment position="end">
            <Tooltip {...tooltipProps} title={toolTipText}>
                <IconButton
                    aria-label={toolTipText}
                    onClick={() => {
                        props.onValueChange?.({value: userLockToken?.token || ''})
                    }}
                    onMouseDown={(event) => {
                        event.preventDefault();
                    }}
                    edge="end"
                >
                    <KeyIcon color={(!hasExpiredToken && hasValidToken) ? 'success' : 'warning'}/>
                </IconButton>
            </Tooltip>
        </InputAdornment>
    );
});


const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};