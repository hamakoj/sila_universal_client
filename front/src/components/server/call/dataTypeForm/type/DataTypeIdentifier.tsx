import React from "react";
import DataTypeFormNode from "../DataTypeFormNode";
import {Box} from "react-feather";
import {UscSiLAElement} from "../../../../../generated/silaModels";
import {findDataTypeFromIdentifier} from "../dataType";
import DataTypeFormProps from "./dataTypeFormProps";
import {DataTypeValueType} from "./DataType";
import PluginsComponentRenderer from "../../../../../plugin/PluginsComponentRenderer";

export type DataTypeIdentifierType = string;

export interface DataTypeIdentifierProps extends DataTypeFormProps<DataTypeIdentifierType, DataTypeValueType | undefined> {

}

export const DataTypeIdentifier = (props: DataTypeIdentifierProps): JSX.Element => {
    const targetDataType: UscSiLAElement | undefined = findDataTypeFromIdentifier(
        props.dataTypeDefinitions,
        props.dataTypeIdentifier
    );
    if (!targetDataType) {
        return (
            <Box>
                {"Unknown datatype " + props.dataTypeIdentifier}
                <PluginsComponentRenderer componentName={'PluginDataTypeIdentifierTypeForm'} forwardProps={props}/>
            </Box>
        );
    }
    return (
        <>
            <DataTypeFormNode
                {...props}
                stateId={props.stateId + '/' + targetDataType.relativeIdentifier}
                dataType={targetDataType.dataType}
            />
            <PluginsComponentRenderer componentName={'PluginDataTypeIdentifierTypeForm'} forwardProps={props}/>
        </>
    );
};