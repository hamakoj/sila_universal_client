import React, {useState} from "react";
import * as XsdPattern from "xsd-pattern-js";
import {XMLParser} from 'fast-xml-parser';
import Ajv2020 from "ajv/dist/2020"
import {
    SiLAJavaBasicType,
    SiLAJavaConstraints,
    SiLAJavaDataTypeType,
    SiLAJavaListType,
    SiLAJavaStructureType
} from "../../../../../../generated/silaModels";
import {DateTime} from "luxon";
import {DurationUnits} from "luxon/src/duration";
import {
    isAfter,
    isBefore,
    isSame,
    isSameOrAfter,
    isSameOrBefore,
    protoDateToDatetime, protoTimestampToDatetime,
    protoTimeToDatetime
} from "../../../../../../utils/datetime";
import {isObjectAsJSONEqual} from "../../../../../../utils/diff";
import {Box, FormHelperText} from "@mui/material/";
import {
    ProtoAny,
    ProtoBinary,
    ProtoDate,
    ProtoInteger, ProtoReal,
    ProtoString,
    ProtoTime,
    ProtoTimestamp
} from "../../../../../../generated/SiLAFramework";
import SiLAXMLParser from "../../../../../../utils/parser/siLAXMLParser";
import {getBackEndpoint} from "../../../../../../utils/endpoint";
import {getRandomId} from "../../../../../../utils/idGenerator";
import {ListValueType} from "../List";
import {BasicValueType} from "../Basic";
import PluginsComponentRenderer from "../../../../../../plugin/PluginsComponentRenderer";
import {Tooltip, TooltipProps} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";
import {removeNulls} from "../../../../../../utils/object";

export interface ConstraintHelpTextProps {
    constraints?: SiLAJavaConstraints;
    runtimeConstraints?: SiLAJavaConstraints[];
    d: SiLAJavaBasicType | SiLAJavaListType | SiLAJavaStructureType | string;
    value: BasicValueType | ListValueType;
}

export const ConstraintHelpText = React.memo((props: ConstraintHelpTextProps): JSX.Element | null => {
    const [node, setNode] = useState<JSX.Element | null>(null);

    React.useEffect(() => {
        let isSubscribed = true;
        const constraintsArray: SiLAJavaConstraints[] = [];
        if (props.constraints) {
            constraintsArray.push(props.constraints);
        }
        if (props.runtimeConstraints) {
            constraintsArray.push(...props.runtimeConstraints);
        }
        Promise.all(constraintsArray.map(constraints => getHelpStringArray(constraints, props.d, props.value))).then((constraintsText) => {
            if (!isSubscribed) {
                return;
            }
            const uniqueConstraints = Array.from(new Set<Constraint>(constraintsText.flat(1)));
            const renderedConstraints = uniqueConstraints.map((h) => (
                <FormHelperText className={'MuiFormHelperText-root MuiFormHelperText-sizeMedium'}
                                key={getRandomId()}
                                sx={{
                                    color: ((h.errorLevel !== 'warn') ? ((h.errorLevel) ? '#f44336' : '#3cc453') : '#c49d3c'),
                                    textOverflow: 'ellipsis',
                                    wordBreak: 'break-word'
                                }}
                >
                    {h.text}
                    {((h.toolTip) && (
                        <Tooltip
                            {...tooltipProps}
                            title={h.toolTip}>
                            <IconButton>
                                <InfoIcon/>
                            </IconButton>
                        </Tooltip>
                    ))}
                </FormHelperText>
            ));
            setNode(
                <Box>
                    {renderedConstraints}
                </Box>
            )
        });

        return () => {
            isSubscribed = false
        }
    }, [props.constraints, props.runtimeConstraints, props.d, props.value]);

    return (
        <>
            {node}
            <PluginsComponentRenderer componentName={'PluginConstraintHelpText'} forwardProps={props}/>
        </>
    );
});

type ErrorLevel = true | false | "warn"

type Constraint = {
    text: string;
    errorLevel: ErrorLevel;
    toolTip?: string;
}

function getBytesLengthFromBase64String(base64string: string): number {
    return base64string.length * (3 / 4) - 2;
}

function getDateTimeFromValue(d: SiLAJavaBasicType, v: ProtoTime | ProtoDate | ProtoTimestamp): DateTime | null {
    try {
        if (d === "TIME") {
            return protoTimeToDatetime(v as ProtoTime)
        }
        if (d === "DATE") {
            return protoDateToDatetime(v as ProtoDate)
        }
        if (d === "TIMESTAMP") {
            return protoTimestampToDatetime(v as ProtoTimestamp)
        }
    } catch (ignored) {
    }
    return null;
}

function dateStringContainsTimezone(dateString: string, type: SiLAJavaBasicType): boolean {
    const nbSplit = dateString.split(':').length - 1;
    return ((type === "DATE") ? (nbSplit !== 0) : (nbSplit !== 2)) || dateString.endsWith('Z');
}

function getDateTimeFromConstraintWithWarning(constraint: string, d: SiLAJavaBasicType, helpText: Constraint[]): DateTime | null {
    const useUTCTimezone = !dateStringContainsTimezone(constraint, d);
    if (useUTCTimezone) {
        helpText.push({
            text: 'Constraint with value ' + constraint + ' does not provide a timezone, using UTC as default',
            errorLevel: 'warn',
            toolTip: constraint
        });
    }
    return getDateTimeFromConstraint(constraint, d, useUTCTimezone);
}

function getDateTimeFromConstraint(constraint: string, d: SiLAJavaBasicType, useUTCTimezone: boolean): DateTime | null {
    if (constraint.endsWith('Z') && constraint.length > 1) {
        // todo fixme luxon does not seem to allow Z (Zulu) representation of UTC (+00:00)
        //  with fromFormat(...Z) as it returns null. See https://github.com/moment/luxon/issues/796.
        //  So until fixed, we remove 'Z' if present and set useUTCTimezone true as a temp fix
        constraint = constraint.substring(0, constraint.length - 1);
        useUTCTimezone = true;
    }
    if (d === "TIME") {
        if (useUTCTimezone) {
            return DateTime.fromFormat(constraint, 'hh:mm:ss', {zone: 'utc'});
        }
        return DateTime.fromFormat(constraint, 'hh:mm:ssZ');
    }
    if (d === "DATE") {
        if (useUTCTimezone) {
            return DateTime.fromFormat(constraint, 'yyyy-MM-dd', {zone: 'utc'});
        }
        return DateTime.fromFormat(constraint, 'yyyy-MM-ddZ');
    }
    if (d === "TIMESTAMP") {
        if (useUTCTimezone) {
            return DateTime.fromFormat(constraint, "yyyy-MM-dd'T'hh:mm:ss", {zone: 'utc'});
        }
        return DateTime.fromFormat(constraint, "yyyy-MM-dd'T'hh:mm:ssZ");
    }
    return null;
}

async function fetchSchema(schemaUrl: string): Promise<string> {
    const controllerUrl = getBackEndpoint('/api/schema/fetch') + '?url=' + encodeURIComponent(schemaUrl);
    const response = await fetch(controllerUrl);
    if (response.ok) {
        return await response.text();
    } else {
        throw new Error('Failed to fetch schema: ' + await response.text());
    }
}

export async function getHelpStringArray(
    c: SiLAJavaConstraints | undefined,
    d: SiLAJavaBasicType | SiLAJavaListType | SiLAJavaStructureType | string,
    value: BasicValueType | ListValueType
): Promise<Constraint[]> {
    const helpText: Constraint[] = [];
    if (!c) {
        return helpText;
    }
    if (d === "STRING" || d === "BINARY") {
        const length = (d === "STRING") ? ((value as ProtoString)?.value?.length || 0) : (getBytesLengthFromBase64String((value as (ProtoBinary)).value || ''));
        if (c.length) {
            helpText.push({text: 'The length must be ' + c.length + ' ' + (length), errorLevel: length !== c.length, toolTip: c.length.toString()});
        }
        if (c.minimalLength) {
            helpText.push({
                text: 'The length must be greater than ' + c.minimalLength,
                errorLevel: length < c.minimalLength,
                toolTip: c.minimalLength.toString()
            });
        }
        if (c.maximalLength) {
            helpText.push({
                text: 'The length must be lower than ' + c.maximalLength,
                errorLevel: length > c.maximalLength,
                toolTip: c.maximalLength.toString()
            });
        }
    }
    if (c.set && (d === "STRING" || d === "INTEGER" || d === "REAL")) {
        const val = (d === "STRING") ? (value as ProtoString).value || "" : (value as (ProtoReal | ProtoInteger)).value?.toString() || '0';
        helpText.push({
            text: 'The value must be on the following: ' + c.set.value.join(", "),
            errorLevel: !c.set.value.includes(val),
            toolTip: c.set.value.join(", ")
        });
    }
    if (c.pattern && d === "STRING") {
        const pattern = new XsdPattern.XsdPattern(c.pattern);
        const match = pattern.match((value as ProtoString).value || '');
        helpText.push({text: 'The value must respect the following pattern: ' + c.pattern, errorLevel: !match, toolTip: c.pattern});
    }
    if (d === "INTEGER" || d === "REAL") {
        const num = ((d === "INTEGER") ? (value as ProtoInteger).value : (value as ProtoReal).value) || 0;
        if (c.maximalExclusive) {
            const maximalExclusive = parseFloat(c.maximalExclusive);
            helpText.push({
                text: 'The value must be strictly lower than ' + maximalExclusive,
                errorLevel: num >= maximalExclusive,
                toolTip: maximalExclusive.toString()
            });
        }
        if (c.maximalInclusive) {
            const maximalInclusive = parseFloat(c.maximalInclusive);
            helpText.push({
                text: 'The value must be lower than ' + maximalInclusive,
                errorLevel: num > maximalInclusive,
                toolTip: maximalInclusive.toString()
            });
        }
        if (c.minimalExclusive) {
            const minimalExclusive = parseFloat(c.minimalExclusive);
            helpText.push({
                text: 'The value must be strictly greater than ' + minimalExclusive,
                errorLevel: num <= minimalExclusive,
                toolTip: minimalExclusive.toString()
            });
        }
        if (c.minimalInclusive) {
            const minimalInclusive = parseFloat(c.minimalInclusive);
            helpText.push({
                text: 'The value must be greater than ' + minimalInclusive,
                errorLevel: num < minimalInclusive,
                toolTip: minimalInclusive.toString()
            });
        }
    }
    if (d === "TIME" || d === "DATE" || d === "TIMESTAMP") {
        const timeValue = getDateTimeFromValue(d, value as ProtoTime | ProtoTimestamp | ProtoDate);
        const granularity: DurationUnits = (d === "TIME" || d === "TIMESTAMP") ? ("second") : ("day");

        if (c.set) {
            const match = c.set.value.some((constraint) => {
                const constraintDateTime = getDateTimeFromConstraintWithWarning(constraint, d, helpText);
                const error = (constraintDateTime === null || timeValue === null || !constraintDateTime.isValid || !timeValue.isValid) || !isSame(timeValue, constraintDateTime, granularity);
                return !error;
            });
            helpText.push({text: 'The value must be on the following: ' + c.set.value.join(", "), errorLevel: !match, toolTip: c.set.value.join(", ")});
        }
        if (c.maximalExclusive) {
            const constraintDateTime = getDateTimeFromConstraintWithWarning(c.maximalExclusive, d, helpText);
            const error = (constraintDateTime === null || timeValue === null || !constraintDateTime.isValid || !timeValue.isValid) || isSameOrAfter(timeValue, constraintDateTime, granularity);
            helpText.push({text: 'Time must strictly before ' + c.maximalExclusive, errorLevel: error, toolTip: c.maximalExclusive});
        }
        if (c.maximalInclusive) {
            const constraintDateTime = getDateTimeFromConstraintWithWarning(c.maximalInclusive, d, helpText);
            const error = (constraintDateTime === null || timeValue === null || !constraintDateTime.isValid || !timeValue.isValid) || isAfter(timeValue, constraintDateTime, granularity);
            helpText.push({text: 'Time must be before ' + c.maximalInclusive, errorLevel: error, toolTip: c.maximalInclusive});
        }
        if (c.minimalExclusive) {
            const constraintDateTime = getDateTimeFromConstraintWithWarning(c.minimalExclusive, d, helpText);
            const error = (constraintDateTime === null || timeValue === null || !constraintDateTime.isValid || !timeValue.isValid) || isSameOrBefore(timeValue, constraintDateTime, granularity);
            helpText.push({text: 'Time must strictly be after ' + c.minimalExclusive, errorLevel: error, toolTip: c.minimalExclusive});
        }
        if (c.minimalInclusive) {
            const constraintDateTime = getDateTimeFromConstraintWithWarning(c.minimalInclusive, d, helpText);
            const error = (constraintDateTime === null || timeValue === null || !constraintDateTime.isValid || !timeValue.isValid) || isBefore(timeValue, constraintDateTime, granularity);
            helpText.push({text: 'Time must be after ' + c.minimalInclusive, errorLevel: error, toolTip: c.minimalInclusive});
        }
    }
    if (c.unit) {
        helpText.push({
            text: `Label: ${c.unit.label} Factor: ${c.unit.factor} Offset: ${c.unit.offset}`,
            errorLevel: false,
            toolTip: `Unit component:\n ${c.unit.unitComponent.map(c => '- ' + c.siunit + ' exponent: ' + c.exponent).join('\n')}`
        });
        // (integer/real) nothing to do
    }
    if (c.contentType && (d === "STRING" || d === "BINARY")) {
        const contentTypeTooltip = `- Type: ${c.contentType.type}\n - Subtype: ${c.contentType.subtype}\n - Parameters: ${JSON.stringify(c.contentType.parameters)}\n`;
        if (c.contentType.type === 'application') {
            if (c.contentType.subtype === 'xml' || c.contentType.subtype === 'x-animl') {
                try {
                    const parse = new XMLParser().parse((d === "BINARY") ? atob((value as ProtoBinary).value || '') : ((value as ProtoString).value || ''));
                    helpText.push({
                        text: 'Content type constraint ' + c.contentType.type + '/' + c.contentType.subtype,
                        errorLevel: Object.keys(parse || {}).length === 0,
                        toolTip: contentTypeTooltip
                    });
                } catch (e) {
                    helpText.push({
                        text: 'Content type constraint ' + c.contentType.type + '/' + c.contentType.subtype,
                        errorLevel: true,
                        toolTip:  contentTypeTooltip
                    });
                }
            } else if (c.contentType.subtype === 'json') {
                try {
                    JSON.parse((d === "BINARY") ? atob((value as ProtoBinary).value || '') : ((value as ProtoString).value || ''));
                    helpText.push({
                        text: 'Content type constraint ' + c.contentType.type + '/' + c.contentType.subtype,
                        errorLevel: false,
                        toolTip:  contentTypeTooltip
                    });
                } catch (e) {
                    helpText.push({
                        text: 'Content type constraint ' + c.contentType.type + '/' + c.contentType.subtype,
                        errorLevel: true,
                        toolTip:  contentTypeTooltip
                    });
                }
            } else {
                helpText.push({
                    text: 'Content type constraint ' + c.contentType.type + '/' + c.contentType.subtype,
                    errorLevel: false,
                    toolTip:  contentTypeTooltip
                });
            }
        } else {
            helpText.push({
                text: 'Content type constraint ' + c.contentType.type + '/' + c.contentType.subtype,
                errorLevel: false,
                toolTip:  contentTypeTooltip
            });
        }
    }
    if ((d as SiLAJavaListType).dataType) {
        const count = (value as ListValueType)?.length || 0;
        if (c.elementCount) {
            helpText.push({
                text: 'There must be ' + c.elementCount + " elements",
                errorLevel: c.elementCount !== count,
                toolTip: c.elementCount.toString()
            });
        }
        if (c.minimalElementCount) {
            helpText.push({
                text: 'There must be at least ' + c.minimalElementCount + " elements",
                errorLevel: c.minimalElementCount > count,
                toolTip: c.minimalElementCount.toString()
            });
        }
        if (c.maximalElementCount) {
            helpText.push({
                text: 'There must be at most ' + c.maximalElementCount + " elements",
                errorLevel: c.maximalElementCount < count,
                toolTip: c.maximalElementCount.toString()
            });
        }
    }
    if (c.fullyQualifiedIdentifier && d === "STRING") {
        const featureIdentifierRegexp = '^\\w+(\\.)\\w+\\/\\w+\\/\\w+\\/v\\w+';
        const commandIdentifierRegexp = featureIdentifierRegexp + '\\/Command\\/\\w+';
        const fqi: { [fullyQualifiedIdentifier: string]: { help: string, example: string, regexp: RegExp } } = {
            FeatureIdentifier: {
                help: 'Originator/Category/Feature Identifier/v Major Feature Version',
                example: 'org.silastandard/core/SiLAService/v1',
                regexp: new RegExp(featureIdentifierRegexp)
            },
            CommandIdentifier: {
                help: 'Originator/Category/Feature Identifier/v Major Feature Version/Command/Command Identifier',
                example: 'org.silastandard/core/SiLAService/v1/Command/HelloWorld',
                regexp: new RegExp(commandIdentifierRegexp)
            },
            CommandParameterIdentifier: {
                help: 'Originator/Category/Feature Identifier/v Major Feature Version/Command/Command Identifier/Parameter/Command Parameter Identifier',
                example: 'org.silastandard/core/SiLAService/v1/Command/HelloWorld/Parameter/WorldName',
                regexp: new RegExp(commandIdentifierRegexp + '\\/Parameter\\/\\w+')
            },
            CommandResponseIdentifier: {
                help: 'Originator/Category/Feature Identifier/v Major Feature Version/Command/Command Identifier/Response/Command Response Identifier',
                example: 'org.silastandard/core/SiLAService/v1/Command/HelloWorld/Response/HelloResponse',
                regexp: new RegExp(commandIdentifierRegexp + '\\/Response\\/\\w+')
            },
            IntermediateCommandResponseIdentifier: {
                help: 'Originator/Category/Feature Identifier/v Major Feature Version/Command/Command Identifier/IntermediateResponse/Intermediate Command Response Identifier',
                example: 'org.silastandard/core/SiLAService/v1/Command/HelloWorld/IntermediateResponse/LettersToSpell',
                regexp: new RegExp(commandIdentifierRegexp + '\\/IntermediateResponse\\/\\w+')
            },
            DefinedExecutionErrorIdentifier: {
                help: 'Originator/Category/Feature Identifier/v Major Feature Version/DefinedExecutionError/Defined Execution Error Identifier',
                example: 'org.silastandard/core/SiLAService/v1/DefinedExecutionError/InvalidWorldName',
                regexp: new RegExp(featureIdentifierRegexp + '\\/DefinedExecutionError\\/\\w+')
            },
            PropertyIdentifier: {
                help: 'Originator/Category/Feature Identifier/v Major Feature Version/Property/Property Identifier',
                example: 'org.silastandard/core/SiLAService/v1/Property/WorldName',
                regexp: new RegExp(featureIdentifierRegexp + '\\/Property\\/\\w+')
            },
            DataTypeIdentifier: {
                help: 'Originator/Category/Feature Identifier/v Major Feature Version/DataType/DataType Identifier',
                example: 'org.silastandard/core/SiLAService/v1/DataType/WorldType',
                regexp: new RegExp(featureIdentifierRegexp + '\\/DataType\\/\\w+')
            },
            MetadataIdentifier: {
                help: 'Originator/Category/Feature Identifier/v Major Feature Version/Metadata/Metadata Identifier',
                example: 'org.silastandard/core/SiLAService/v1/Metadata/MetaVerse',
                regexp: new RegExp(featureIdentifierRegexp + '\\/Metadata\\/\\w+')
            },
        }
        const fqiElement = fqi[c.fullyQualifiedIdentifier];
        if (fqiElement) {
            const match = fqiElement.regexp.test((value as ProtoString).value || '');
            helpText.push({
                text: 'The string must respect the following semantic ' + fqiElement.help + ' example: ' + fqiElement.example,
                errorLevel: !match,
                toolTip: c.fullyQualifiedIdentifier + ': ' + fqiElement.help
            });
        }
    }
    if (c.schema && (d === "STRING" || d === "BINARY")) {
        let externalSchema = null;
        if (c.schema.url) {
            try {
                externalSchema = await fetchSchema(c.schema.url);
            } catch (e) {
                console.error('Failed to load schema located at ' + c.schema.url);
                helpText.push({text: (e || 'Failed to fetch') + ' located at ' + c.schema.url, errorLevel: true, toolTip: c.schema.url});
            }
        }
        const schema = (c.schema.inline) ? (c.schema.inline) : externalSchema;
        const schemaTooltip = c.schema.type + ':' + (schema || 'No schema');
        if (!schema) {
            // no schema or failed to load
        } else if (c.schema.type === "Json") {
            try {
                const ajv = new Ajv2020()
                const schema1 = JSON.parse(schema);
                delete schema1.$id;
                delete schema1.$schema;
                // todo handle schema properly, issue when using specific one
                //const validate = ajv.getSchema(schema1.$id) || ajv.compile(schema1)
                const validate = ajv.compile(schema1)
                const valid = validate(JSON.parse((d === "BINARY") ? atob((value as ProtoBinary).value || '') : ((value as ProtoString).value || '')));
                if (valid) {
                    helpText.push({text: 'Schema constraints', errorLevel: false, toolTip: schemaTooltip});
                } else {
                    console.error(ajv.errors);
                    helpText.push({text: 'Schema constraints', errorLevel: true, toolTip: schemaTooltip});
                }
            } catch (e: any) {
                // todo handle
                //console.error(e);
                helpText.push({text: 'Schema constraints', errorLevel: true, toolTip: schemaTooltip});
            }
        } else if (c.schema.type === "Xml") {
            const response = await fetch(getBackEndpoint('/api/schema/validate-xml'), {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    xml: (d === "BINARY") ? atob((value as ProtoBinary).value || '') : ((value as ProtoString).value || ''),
                    schema: schema
                })
            });
            if (await response.text() === 'true') {
                helpText.push({text: 'Schema constraints', errorLevel: false, toolTip: schemaTooltip});
            } else {
                helpText.push({text: 'Schema constraints', errorLevel: true, toolTip: schemaTooltip});
            }
        } else {
            helpText.push({text: 'Schema constraints', errorLevel: false, toolTip: schemaTooltip});
        }
    }
    if (c.allowedTypes && d === "ANY") {
        const typeTooltip = c.allowedTypes.dataType.map(dt => '- ' + JSON.stringify(removeNulls(dt))).join('\n\n');
        try {
            const dataType: SiLAJavaDataTypeType | null = SiLAXMLParser.parseXml((value as ProtoAny).type);
            if (!dataType) {
                throw new Error("Invalid datatype");
            }
            // todo check if isObjectAsJSONEqual works
            const match = c.allowedTypes.dataType.some((allowedType) => isObjectAsJSONEqual(allowedType, dataType));
            helpText.push({text: 'Type constraints', errorLevel: !match, toolTip: typeTooltip});
        } catch (e: any) {
            helpText.push({text: 'Type constraints', errorLevel: false, toolTip: typeTooltip});
        }
    }
    return helpText;
}

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem',
                whiteSpace: 'pre-line',
                maxHeight: '50vh',
                overflow: 'auto'
            }
        }
    }
};