import DataTypeFormProps from "../dataTypeFormProps";
import {SiLAJavaBasicType} from "../../../../../../generated/silaModels";
import {Box, Button} from "@mui/material";
import React from "react";
import {ProtoInteger, ProtoReal} from "../../../../../../generated/SiLAFramework";
import PluginsComponentRenderer from "../../../../../../plugin/PluginsComponentRenderer";

export interface NumberUnitConstraintProps extends DataTypeFormProps<SiLAJavaBasicType, ProtoInteger | ProtoReal> {
    acceptDecimal: boolean;
}

export default function NumberUnitConstraint(props: NumberUnitConstraintProps): JSX.Element {
    return (
        <>
            <Box style={{marginTop: '1rem'}} display={'flex'} alignItems={'center'} flexWrap={'wrap'}>
                <Button style={{marginRight: '0.5rem', marginBottom: '0.5rem'}} variant="contained" color="inherit"
                        onClick={() => {
                            const unit = props.constraints?.unit;
                            if (unit && props.onValueChange) {
                                const convertedValue = ((props.value?.value || 0) * unit.factor) + unit.offset;
                                props.onValueChange({value: (props.acceptDecimal) ? convertedValue : Math.round(convertedValue)});
                            }
                        }}>
                    {props.constraints?.unit.label + ' → ' + props.constraints?.unit.unitComponent[0].siunit}
                </Button>
                <Button style={{marginRight: '0.5rem', marginBottom: '0.5rem'}} variant="contained" color="inherit"
                        onClick={() => {
                            const unit = props.constraints?.unit;
                            if (unit && props.onValueChange) {
                                const convertedValue = ((props.value?.value || 0) - unit.offset) / unit.factor;
                                props.onValueChange({value: (props.acceptDecimal) ? convertedValue : Math.round(convertedValue)});
                            }
                        }}>
                    {props.constraints?.unit.unitComponent[0].siunit + ' → ' + props.constraints?.unit.label}
                </Button>
            </Box>
            <PluginsComponentRenderer componentName={'PluginNumberUnitConstraint'} forwardProps={props}/>
        </>
    );
}