import React from "react";
import Typography from "@mui/material/Typography";
import DataTypeFormNode from '../DataTypeFormNode'
import DataTypeFormProps from "./dataTypeFormProps";
import {SiLAJavaStructureType} from "../../../../../generated/silaModels";
import {DataTypeValueType} from "./DataType";
import PluginsComponentRenderer from "../../../../../plugin/PluginsComponentRenderer";
import {Accordion, AccordionDetails, AccordionSummary} from "@mui/material";
import {styledAccordion} from "../../ServerCalls";
import {styledAccordionDetails, styledAccordionSummary} from "../../response/CallResponses";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

export interface StructureProps extends DataTypeFormProps<SiLAJavaStructureType, DataTypeValueType | undefined> {

}

export default function Structure(props: StructureProps): JSX.Element {
    return (
        <Accordion sx={styledAccordion} defaultExpanded={true}>
            <AccordionSummary sx={styledAccordionSummary} expandIcon={<ExpandMoreIcon/>}>
                <Typography>Structure</Typography>
            </AccordionSummary>
            <AccordionDetails sx={styledAccordionDetails}>
                <>
                    {props.dataType.element.map((element) => (
                        <React.Fragment key={props.stateId + '/' + element.identifier}>
                            <Typography>{element.displayName}</Typography>
                            <DataTypeFormNode
                                {...props}
                                dataType={element.dataType}
                                depth={props.depth + 1}
                                stateId={props.stateId + '/' + element.identifier}
                            />
                        </React.Fragment>
                    ))}
                    <PluginsComponentRenderer componentName={'PluginStructureTypeForm'} forwardProps={props}/>
                </>
            </AccordionDetails>
        </Accordion>
    );
};