import React from "react";
import IconButton from "@mui/material/IconButton";
import RemoveCircleOutline from "@mui/icons-material/RemoveCircleOutline";
import AddCircleOutline from "@mui/icons-material/AddCircleOutline";
import DataTypeFormNode from '../DataTypeFormNode';
import DataTypeFormProps from "./dataTypeFormProps";
import {SiLAJavaListType} from "../../../../../generated/silaModels";
import {dataTypeToState} from "../silaToState";
import {
    callInputListPush,
    callInputListRemove
} from "../../../../../store/front/server/callInput/actions";
import {Accordion, AccordionDetails, AccordionSummary, Box} from "@mui/material";
import {ConstraintHelpText} from "./constraints/ConstraintHelpText";
import {getRandomId} from "../../../../../utils/idGenerator";
import PluginsComponentRenderer from "../../../../../plugin/PluginsComponentRenderer";
import {Typography} from "@mui/material/";
import {styledAccordion} from "../../ServerCalls";
import {styledAccordionDetails, styledAccordionSummary} from "../../response/CallResponses";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

export type ListValueType = string[];

export interface ListProps extends DataTypeFormProps<SiLAJavaListType, ListValueType> {

}

export const List = React.memo((props: ListProps): JSX.Element => {
    function onAddElement() {
        if (props.dispatch) {
            const stateIdToPush = props.stateId + '/' + getRandomId();
            const newState = dataTypeToState(
                props.dataTypeDefinitions,
                props.dataType.dataType,
                stateIdToPush
            );
            props.dispatch(callInputListPush({
                fullyQualifiedCommandId: props.fullyQualifiedCommandId,
                stateId: props.stateId,
                stateIdToPush: stateIdToPush,
                valueToPush: newState,
                formType: props.type,
                serverId: props.serverId
            }));
        }
    }

    function onRemoveElement(stateIdToRemove: string) {
        if (props.dispatch) {
            if ((props.value?.length || 0) < 1) {
                return;
            }
            props.dispatch(callInputListRemove({
                fullyQualifiedCommandId: props.fullyQualifiedCommandId,
                stateId: props.stateId,
                stateIdToRemove: stateIdToRemove,
                formType: props.type,
                serverId: props.serverId
            }));
        }
    }

    const stateIdArray: string[] = (props.value?.length) ? (props.value) : [];
    // todo handle index
    return (
        <>
            <Accordion sx={styledAccordion} defaultExpanded={true}>
                <AccordionSummary sx={styledAccordionSummary} expandIcon={<ExpandMoreIcon/>}>
                    <Typography>List</Typography>
                </AccordionSummary>
                <AccordionDetails sx={styledAccordionDetails}>
                    <>
                        {stateIdArray.map((stateId: string, idx) => {
                            return (
                                <React.Fragment key={stateId + '/' + idx}>
                                    <DataTypeFormNode
                                        {...props}
                                        stateId={stateId}
                                        dataType={props.dataType.dataType}
                                        depth={props.depth + 1}
                                    />
                                    {
                                        (props.readonly) ? (<></>) : (
                                            <Box style={{display: 'inline-flex'}}>
                                                <IconButton onClick={() => onRemoveElement(stateId)}>
                                                    <RemoveCircleOutline />
                                                </IconButton>
                                                <IconButton onClick={() => onAddElement()}>
                                                    <AddCircleOutline />
                                                </IconButton>
                                            </Box>
                                        )
                                    }
                                </React.Fragment>
                            );
                        })}
                        {
                            (!stateIdArray?.length && (
                                (props.readonly) ? (
                                    <Typography>(Empty list)</Typography>
                                ) : (
                                    <IconButton onClick={() => onAddElement()}>
                                        <AddCircleOutline color={'primary'} />
                                    </IconButton>
                                )
                            ))
                        }
                        <Box className={"MuiFormHelperText-root"}>
                            <ConstraintHelpText constraints={props.constraints} runtimeConstraints={props.runtimeConstraints} d={props.dataType} value={props.value}/>
                        </Box>
                        <PluginsComponentRenderer componentName={'PluginListTypeForm'} forwardProps={props}/>
                    </>
                </AccordionDetails>
            </Accordion>
        </>
    );
});

export default List;