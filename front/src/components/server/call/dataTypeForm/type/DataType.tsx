import React from "react";
import {
    SiLAJavaBasicType, SiLAJavaConstrainedType,
    SiLAJavaDataTypeType,
    SiLAJavaListType
} from "../../../../../generated/silaModels";
import List, {ListValueType} from "./List";
import {DataTypeIdentifier} from "./DataTypeIdentifier";
import Structure from "./Structure";
import DataTypeFormProps from "./dataTypeFormProps";
import {getUnwrappedConstraints, getUnwrappedDataTypeIdentifier, getUnwrappedType, isUnwrappedTypeEqual} from "../dataType";
import {Skeleton} from "@mui/material";
import {TextField} from "@mui/material";
import Basic, {BasicValueType} from "./Basic";
import {SiLATypeValueMap, SiLATypeValueState} from "../../../../../store/front/server/callInput/types";
import PluginsComponentRenderer from "../../../../../plugin/PluginsComponentRenderer";

export type DataTypeValueType = SiLATypeValueState;

export type DataTypeType = SiLAJavaDataTypeType | SiLAJavaConstrainedType | string;

export interface DataTypeProps extends DataTypeFormProps<DataTypeType, DataTypeValueType | undefined> {

}

const DataType = React.memo((props: DataTypeProps): JSX.Element => {
    const pluginsDataType = <PluginsComponentRenderer componentName={'PluginDataTypeTypeForm'} forwardProps={props}/>
    const loading = (
        <>
            <Skeleton animation="pulse" variant={"rectangular"} width={'100%'}>
                <TextField/>
            </Skeleton>
            {pluginsDataType}
        </>
    );
    const commonProps: DataTypeProps = {
        ...props,
        constraints: getUnwrappedConstraints(props.dataType as SiLAJavaDataTypeType, props.constraints),
    };
    if (isUnwrappedTypeEqual(props.dataType as SiLAJavaDataTypeType, 'basic')) {
        if ((props.value as SiLATypeValueMap)?.[props.stateId] === undefined) {
            return (loading)
        }
        return (
            <>
                <Basic
                    {...commonProps}
                    value={(props.value as SiLATypeValueMap)[props.stateId] as BasicValueType}
                    dataType={getUnwrappedType(props.dataType as SiLAJavaDataTypeType, "basic") as SiLAJavaBasicType}
                />
                {pluginsDataType}
            </>
        );
    }
    if (isUnwrappedTypeEqual(props.dataType as SiLAJavaDataTypeType, 'dataTypeIdentifier')) {
        if (props.value === undefined) {
            return (loading)
        }
        return (
            <>
                <DataTypeIdentifier
                    {...commonProps}
                    dataType={props.dataType as string}
                    dataTypeIdentifier={getUnwrappedDataTypeIdentifier(props.dataType as SiLAJavaDataTypeType)}
                />
                {pluginsDataType}
            </>
        );
    }
    if (isUnwrappedTypeEqual(props.dataType as SiLAJavaDataTypeType, 'list')) {
        if ((props.value as SiLATypeValueState as SiLATypeValueMap)?.[props.stateId] === undefined) {
            return (loading)
        }
        return (
            <>
                <List
                    {...commonProps}
                    value={(props.value as SiLATypeValueState as SiLATypeValueMap)[props.stateId] as ListValueType}
                    dataType={getUnwrappedType(props.dataType as SiLAJavaDataTypeType, "list") as SiLAJavaListType}
                />
                {pluginsDataType}
            </>
        );
    }
    if (isUnwrappedTypeEqual(props.dataType as SiLAJavaDataTypeType, 'structure')) {
        if (props.value === undefined) {
            return (loading)
        }
        return (
            <>
                <Structure
                    {...commonProps}
                    dataType={(props.dataType as SiLAJavaDataTypeType).structure}
                />
                {pluginsDataType}
            </>
        );
    }
    console.error('unknown datatype ' + JSON.stringify(props.dataType));
    return (
        <>
            {"Unknown datatype " + JSON.stringify(props.dataType)}
            {pluginsDataType}
        </>
    );
});

export default DataType;