import {
    SiLAJavaBasicType, SiLAJavaConstraints,
    SiLAJavaDataTypeType,
    SiLAJavaListType,
    SiLAJavaStructureType,
    UscSiLAElement
} from "../../../../generated/silaModels";

export function isUnwrappedTypeEqual(dataType: SiLAJavaDataTypeType, type: keyof SiLAJavaDataTypeType): boolean {
    return !!(dataType[type] || (dataType.constrained && dataType.constrained.dataType[type]));
}

export function getUnwrappedType(
    dataType: SiLAJavaDataTypeType,
    type: keyof SiLAJavaDataTypeType
): SiLAJavaBasicType | SiLAJavaListType | SiLAJavaStructureType | string {
    return (dataType.constrained ? dataType.constrained.dataType[type] : dataType[type]);
}

export function getUnwrappedDataTypeIdentifier(dataType: SiLAJavaDataTypeType): string {
    return (dataType.constrained ? dataType.constrained.dataType.dataTypeIdentifier : dataType.dataTypeIdentifier);
}

export function getUnwrappedConstraints(dataType: SiLAJavaDataTypeType, constraints?: SiLAJavaConstraints) {
    return (dataType.constrained ? dataType.constrained.constraints : constraints);
}

export function findDataTypeFromIdentifier(
    dataTypeDefinitions: UscSiLAElement[],
    identifier: string
): UscSiLAElement | undefined {
    return dataTypeDefinitions.find((d) => d.relativeIdentifier === identifier);
}

export function findDataTypeFromDataTypeIdentifier(
    dataTypeDefinitions: UscSiLAElement[],
    dataType: SiLAJavaDataTypeType
): UscSiLAElement | undefined {
    return findDataTypeFromIdentifier(dataTypeDefinitions, dataType.dataTypeIdentifier);
}