import {
    SiLAJavaBasicType,
    SiLAJavaDataTypeType,
    UscParameter, UscResponse, UscSiLAElement
} from "../../../../generated/silaModels";
import {findDataTypeFromDataTypeIdentifier} from "./dataType";
import {SiLATypeValueState} from "../../../../store/front/server/callInput/types";
import {
    ProtoAny, ProtoBinary,
    ProtoBoolean,
    ProtoDate,
    ProtoInteger,
    ProtoReal,
    ProtoString,
    ProtoTime,
    ProtoTimestamp
} from "../../../../generated/SiLAFramework";
import SiLAXMLParser from "../../../../utils/parser/siLAXMLParser";
import {
    getProtoDateWithDefault,
    getProtoTimestampWithDefault,
    getProtoTimeWithDefault,
} from "../../../../utils/datetime";
import {getRandomId} from "../../../../utils/idGenerator";

export function getCommandParameterInputState(
    dataTypeDefinitions: UscSiLAElement[],
    parameters: UscParameter[]
): SiLATypeValueState {
    let state = {};
    parameters.forEach((p) => {
        state = {
            ...state,
            ...dataTypeToState(dataTypeDefinitions, p.dataType, p.relativeIdentifier),
        };
    })
    return state;
}

export function getCommandParameterInputStateFromDataType(
    dataTypeDefinitions: UscSiLAElement[],
    dataTypeType: SiLAJavaDataTypeType,
    relativeIdentifier: string
): SiLATypeValueState {
    return {
        ...dataTypeToState(dataTypeDefinitions, dataTypeType, relativeIdentifier),
    };
}

export function getCommandResponseState(
    dataTypeDefinitions: UscSiLAElement[],
    responses: UscResponse[],
    silaValue: any
): SiLATypeValueState {
    let state = {};
    responses.forEach((r) => {
        state = {
            ...state,
            ...dataTypeToState(dataTypeDefinitions, r.dataType, r.relativeIdentifier, silaValue[r.relativeIdentifier]),
        };
    });
    return state;
}

// todo handle constraints
export function getValueForBasicType(type: SiLAJavaBasicType): ProtoString | ProtoInteger | ProtoReal | ProtoBoolean | ProtoTime | ProtoTimestamp | ProtoDate | ProtoBinary | ProtoAny {
    if (type === "STRING") {
        return {
            value: ''
        } as ProtoString;
    }
    if (type === "INTEGER") {
        return {
            value: 1
        } as ProtoInteger;
    }
    if (type === "REAL") {
        return {
            value: 1.0
        } as ProtoReal;
    }
    if (type === "BOOLEAN") {
        return {
            value: false
        } as ProtoBoolean;
    }
    if (type === "TIME") {
        return getProtoTimeWithDefault();
    }
    if (type === "TIMESTAMP") {
        return getProtoTimestampWithDefault();
    }
    if (type === "DATE") {
        return getProtoDateWithDefault();
    }
    if (type === "BINARY") {
        return {
            binaryTransferUUID: ''
        } as ProtoBinary
    }
    if (type === "ANY") {
        return {
            payload: '',
            type: '',
        } as ProtoAny
    }
    throw new Error('Unknown type: ' + type);
}

export function dataTypeIdentifierToState(dataTypeDefinitions: UscSiLAElement[], dataType: SiLAJavaDataTypeType, stateId: string, silaValue: any): SiLATypeValueState {
    const targetDataType: UscSiLAElement | undefined = findDataTypeFromDataTypeIdentifier(dataTypeDefinitions, dataType);
    if (targetDataType === undefined) {
        throw new Error('Unknown datatype: ' + dataType.dataTypeIdentifier + ' in ' + stateId)
    }
    const subStateId: string = stateId + '/' + targetDataType.relativeIdentifier;
    let v = undefined
    if (silaValue && silaValue[targetDataType.relativeIdentifier]) {
        v = silaValue[targetDataType.relativeIdentifier];
    }
    return {
        [stateId]: subStateId,
        ...dataTypeToState(
            dataTypeDefinitions,
            targetDataType.dataType,
            subStateId,
            v
        ),
    };
}

function constrainedToState(dataTypeDefinitions: UscSiLAElement[], dataType: SiLAJavaDataTypeType, stateId: string, silaValue: any): SiLATypeValueState {
    return dataTypeToState(
        dataTypeDefinitions,
        dataType.constrained.dataType,
        stateId,
        silaValue,
    );
}

function structureToState(dataType: SiLAJavaDataTypeType, stateId: string, dataTypeDefinitions: UscSiLAElement[], silaValue: any): SiLATypeValueState {
    let json = {};
    dataType.structure.element.forEach(element => {
        const subStateId = stateId + '/' + element.identifier;
        let v = undefined
        if (silaValue && silaValue[element.identifier]) {
            v = silaValue[element.identifier];
        }
        json = {
            ...json,
            ...dataTypeToState(
                dataTypeDefinitions,
                element.dataType,
                subStateId,
                v
            ),
        }
    });
    return json;
}

function listToState(stateId: string, dataTypeDefinitions: UscSiLAElement[], dataType: SiLAJavaDataTypeType, silaValue: any): SiLATypeValueState {
    const subStateId = stateId + '/' + getRandomId();
    if (!silaValue) {
        return {
            [stateId]: [subStateId],
            ...dataTypeToState(
                dataTypeDefinitions,
                dataType.list.dataType,
                subStateId
            ),
        };
    }
    const subStateIds: string[] = [];
    let newState = {};
    silaValue.forEach((v: UscSiLAElement) => {
        const subStateId = stateId + '/' + getRandomId();
        subStateIds.push(subStateId);
        newState = {
            ...newState,
            ...dataTypeToState(dataTypeDefinitions, dataType.list.dataType, subStateId, v),
        }
    })
    return { [stateId]: subStateIds, ...newState };
}

export function anyTypeToState(dataTypeDefinitions: UscSiLAElement[], dataType: SiLAJavaDataTypeType, stateId: string, silaValue: any): SiLATypeValueState {
    const subStateId = stateId + '/any';
    const dt = SiLAXMLParser.parseXml(silaValue.type);
    if (dt) {
        if (silaValue.payload instanceof Uint8Array) {
            // todo fixme this happen when having an any type list of any type that is parsed, the payload becomes a uint8array
            silaValue.payload = btoa(String.fromCharCode.apply(null, silaValue.payload));
        }
        const decodeAnyType = SiLAXMLParser.decodeAnyType(dt, silaValue.payload);
        const parameterInputState = dataTypeToState(
            dataTypeDefinitions,
            dt,
            subStateId,
            decodeAnyType
        );
        return {
            [stateId]: {
                type: silaValue.type,
                payload: silaValue.payload,
            },
            ...parameterInputState,
        };
    }
    // todo error?
    return {};
}


export function dataTypeToState(
    dataTypeDefinitions: UscSiLAElement[],
    dataType: SiLAJavaDataTypeType,
    stateId: string,
    silaValue?: any,
): SiLATypeValueState {
    if (dataType.basic) {
        if (dataType.basic.toUpperCase() === "ANY" && silaValue && silaValue.type && silaValue.payload) {
            return anyTypeToState(dataTypeDefinitions, dataType, stateId, silaValue);
        }
        return {[stateId]:(silaValue) ? (silaValue) : (getValueForBasicType(dataType.basic.toUpperCase() as SiLAJavaBasicType))};
    }
    if (dataType.dataTypeIdentifier) {
        return dataTypeIdentifierToState(dataTypeDefinitions, dataType, stateId, silaValue);
    }
    if (dataType.constrained) {
        return constrainedToState(dataTypeDefinitions, dataType, stateId, silaValue);
    }
    if (dataType.structure) {
        return structureToState(dataType, stateId, dataTypeDefinitions, silaValue);
    }
    if (dataType.list) {
        return listToState(stateId, dataTypeDefinitions, dataType, silaValue);
    }
    console.error('Unknown data type ', dataType);
    return {};
}