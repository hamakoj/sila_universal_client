import React from 'react';
import {
    Grid
} from '@mui/material/';
import ServerCard from "./ServerCard";

export interface ServerGridProps {
    serversId:string[];
    onServerChange: Function;
}

export default function ServerGrid(props: ServerGridProps): JSX.Element {
    return (
        <Grid container spacing={2} marginTop={2} width={'auto'}>
            {
                props.serversId.map(id => {
                    return (
                        <Grid item xs={12} md={6} lg={4} xl={3} key={id}>
                            <ServerCard serverId={id} onClick={() => {
                                props.onServerChange(id);
                            }}/>
                        </Grid>
                    )
                })
            }
        </Grid>
    );
}
