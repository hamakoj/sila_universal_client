import React from 'react';
import {
    Box, Card, CardContent, Grid, Tooltip, Typography
} from '@mui/material/';
import IconButton from "@mui/material/IconButton";
import ThrashIcon from '@mui/icons-material/Delete';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import RefreshIcon from '@mui/icons-material/Refresh';
import {useDispatch, useSelector} from "react-redux";
import {serverBookmarkAddRequest, serverBookmarkDeleteRequest} from "../../store/front/server/bookmark/actions";
import {ServerBookmarkKeyValueState} from "../../store/front/server/bookmark/types";
import {getServerBookmarksSelector} from "../../store/front/server/bookmark/selector";
import {getServerByIdSelector} from "../../store/sila/manager/server/selector";
import {ServersTabs} from "../../store/front/server/open/types";
import {getServerOpenTabsSelector} from "../../store/front/server/open/selector";
import ServerLogoBadge from "../common/ServerLogoBadge";
import {getServerInitial} from "../../utils/server";
import {AppState} from "../../store/rootReducer";
import {getBackEndpoint} from "../../utils/endpoint";
import {SxProps} from "@mui/system";
import {Theme} from "@mui/material/styles";
import PluginsComponentRenderer from "../../plugin/PluginsComponentRenderer";
import {TooltipProps} from "@mui/material";
import {toast} from "react-toastify";
import {ServerHostPortCertificate} from "../../generated/silaModels";

export interface ServerCardProps {
    serverId: string;
    onClick?: () => void;
}

export default function ServerCard(props: ServerCardProps): JSX.Element {
    const dispatch = useDispatch();
    const serverBookmark: ServerBookmarkKeyValueState = useSelector(getServerBookmarksSelector);
    const serversTabs: ServersTabs = useSelector(getServerOpenTabsSelector);
    const server = useSelector((state: AppState) => getServerByIdSelector(state, props.serverId));
    const bookmarked = serverBookmark[props.serverId];
    const serverInitials = getServerInitial(server?.name || 'NA');

    return (
        <Card sx={styledCard} className={((serversTabs[props.serverId]) ? ' active' : '')}>
            <CardContent onClick={(e) => {
                if (props.onClick) {
                    props.onClick();
                }
            }}>
                <Grid container direction="row" alignItems="center">
                    <Grid item>
                        <ServerLogoBadge status={server.status} serverInitials={serverInitials}/>
                    </Grid>
                    <Grid item>
                        <Typography sx={{marginLeft: '8px'}} color="textSecondary" gutterBottom>
                            {server.name}
                        </Typography>
                    </Grid>
                    <Grid item sx={{marginLeft: 'auto', display: 'inline-flex', alignItems: 'end'}}>
                        {
                            ((server.status === "UNKNOWN" && server.connectionType !== "SERVER_INITIATED") && (
                                <Tooltip {...tooltipProps} title={'Retry connection'}>
                                    <IconButton style={{paddingTop: 0}} onClick={(e) => {
                                        e.stopPropagation();
                                        toast.promise(
                                            fetch(getBackEndpoint('/api/servers'), {
                                                method: 'POST',
                                                headers: {
                                                    'Content-Type': 'application/json',
                                                },
                                                body: JSON.stringify({host: server.serverHost, port: server.port, certificate: server.certificateAuthority} as ServerHostPortCertificate)
                                            }).then((r) => {
                                                if (!r.ok) {
                                                    throw new Error('Server response error')
                                                }
                                            }),
                                            {
                                                pending: 'Connecting to server...',
                                                success: 'Successfully reconnected to server',
                                                error: 'Failed to reconnect to server'
                                            },
                                            {position: 'bottom-center', theme: 'dark'}
                                        )
                                    }}>
                                        <RefreshIcon color={"warning"}/>
                                    </IconButton>
                                </Tooltip>
                            ))
                        }
                        <IconButton style={{paddingTop: 0}} onClick={(e) => {
                            e.stopPropagation();
                            if (bookmarked) {
                                dispatch(serverBookmarkDeleteRequest({serverUuid: server.serverUuid}));
                            } else {
                                dispatch(serverBookmarkAddRequest({serverUuid: server.serverUuid}));
                            }
                        }}>
                            {
                                (bookmarked) ? (<BookmarkIcon/>) : (<BookmarkBorderIcon/>)
                            }
                        </IconButton>
                        <IconButton style={{paddingTop: 0}} onClick={(e) => {
                            e.stopPropagation();
                            fetch(getBackEndpoint('/api/servers/' + server.serverUuid), {
                                method: 'DELETE'
                            })
                                .catch((error) => {
                                    console.error(error);
                                    //setAlertMessage(JSON.stringify(error));
                                    //setAlertSeverity('error');
                                    //setCloseAlert(false);
                                })
                        }}>
                            <ThrashIcon/>
                        </IconButton>
                    </Grid>
                </Grid>
                <Box>
                    <Typography variant="body2" component="p">
                        {server.description}
                    </Typography>
                </Box>
                <PluginsComponentRenderer componentName={'PluginServerCardBody'} forwardProps={{
                    ...props,
                    bookmarked: bookmarked,
                    serverInitials: serverInitials
                }}/>
            </CardContent>
        </Card>
    );
}

const styledCard: SxProps<Theme> = (theme) => ({
    height: '100%',
    cursor: 'pointer',
    border: '3px solid',
    '&:hover': {
        borderColor: theme.palette.primary.light,
    },
    '&.active': {
        borderColor: theme.palette.primary.main,
    },
    borderColor: 'transparent',
});

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};