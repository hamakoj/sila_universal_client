import React, {useEffect} from 'react';
import {useSelector} from "react-redux";
import {getAppTimezoneSettingsSelector} from "../store/front/app/setting/selector";
import {getCurrentTimeZoneName, getZoneFromTimezoneSettings} from "../utils/timezone";
import {Settings} from "luxon";

const ThemeProviderComponent = React.memo((props: {children: JSX.Element}): JSX.Element => {
    const timezoneSettings = useSelector(getAppTimezoneSettingsSelector);

    useEffect(() => {
        const prev = getCurrentTimeZoneName();
        Settings.defaultZone = getZoneFromTimezoneSettings(timezoneSettings);
        const next = getCurrentTimeZoneName();
        console.log('Timezone changed from ' + prev + ' to ' + next);
    }, [timezoneSettings]);

    return props.children;
});

export default ThemeProviderComponent;