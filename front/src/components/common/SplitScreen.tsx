import React from 'react';
import {
    Box, BoxProps, Grid, Tab, Tabs, useTheme
} from '@mui/material/';
import SwipeableViews from 'react-swipeable-views';
import useWindowDimensions from "../../hooks/useWindowDimension/useWindowDimensions";
import {ContentHeightPx, AppHeaderHeightPx} from "../ThemeProviderComponent";
import {SxProps} from "@mui/system";
import {Theme} from "@mui/material/styles";

export interface SplitScreenProps {
    leftName: string;
    rightName: string;
    leftComponent: JSX.Element;
    rightComponent: JSX.Element;
}

const SplitScreen = React.memo((props: SplitScreenProps): JSX.Element => {
    const theme = useTheme();
    const [value, setValue] = React.useState<any>(0);
    const { width } = useWindowDimensions();

    const handleChange = (event:any, newValue:number) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index: number) => {
        setValue(index);
    };

    const isScreenBelowMd = theme.breakpoints.values.md < width;

    if (isScreenBelowMd && value !== 0) {
        setValue(0);
    }

    return (
        <>
            <Grid container sx={rootGrid}>
                <SwipeableViews
                    index={value}
                    disabled={isScreenBelowMd}
                    style={{
                        display: 'flex',
                        height: '100%',
                        width: '100%',
                    }}
                    containerStyle={{
                        width: isScreenBelowMd ? ('50%') : ('100%')
                    }}
                    onChangeIndex={handleChangeIndex}
                >
                    <TabPanel value={value} index={0}>
                        <Box margin={1}>
                            {props.leftComponent}
                        </Box>
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        <Box margin={1}>
                            {props.rightComponent}
                        </Box>
                    </TabPanel>
                </SwipeableViews>
            </Grid>
            {
                !isScreenBelowMd && (
                    <Tabs sx={styledTabs} variant={"fullWidth"} value={value} onChange={handleChange} aria-label="basic tabs example">
                        <Tab label={props.leftName} />
                        <Tab label={props.rightName} />
                    </Tabs>
                )
            }
        </>
    )
});

export default SplitScreen;

interface TabPanelProps extends BoxProps {
    value: string;
    index: number;
    children: JSX.Element;
}

function TabPanel(props: TabPanelProps): JSX.Element {
    const { children, value, index, ...other } = props;

    return (
        <Box
            role="tabpanel"
            sx={splitGrid(props.index)}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            <>{children}</>
        </Box>
    );
}

const splitGrid: (index: number) => SxProps<Theme> = (index: number) => ((theme) => ({
    width: '100%',
    maxWidth: '100%',
    [theme.breakpoints.up('md')]: {
        minHeight: '100%',
        maxHeight: '100%',
        overflowX: 'hidden',
        overflowY: 'auto',
        borderRight: ((index === 0) ? 'solid 1px #9d9d9d30' : 'none'),
        overflowWrap: 'anywhere'
    }
}));

const rootGrid: SxProps<Theme> = (theme) => ({
    overflowY: 'auto',
    height: '100%',
    [theme.breakpoints.down('md')]: {
        height: 'calc(100% - ' + (ContentHeightPx) + 'px)',
    }
});

const styledTabs: SxProps<Theme> = (theme) => ({
    width: '100%',
    height: AppHeaderHeightPx + 'px',
    [theme.breakpoints.up('md')]: {
        display: 'none'
    }
});

