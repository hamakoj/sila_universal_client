import React, {useEffect, useMemo, useState} from "react";
import {debounce} from "@mui/material";
import TextField from "@mui/material/TextField";
import {DatePicker, DateTimePicker, TimePicker} from "@mui/x-date-pickers";

export const debounceComponent = (
    Component: any, onChangeValue?: (e: any) => void, timeout = 300
) => ({ onChange, value, ...props }:any) => {
    const [debouncedValue, setValue] = useState<any>(value);
    const handler = useMemo(
        () => debounce((newValue:any) => {
            onChange(newValue)
        }, timeout),
        [onChange]
    );

    // listen for external changes
    useEffect(() => {
        setValue(value);
    }, [value]);

    function handleTextChange(e: any) {
        const v = onChangeValue ? onChangeValue(e) : e;
        setValue(v);
        handler(e);
    }

    return <Component {...props} onChange={handleTextChange} value={debouncedValue} />;
};

export const DebouncedTextField = debounceComponent(TextField, (e:any) => e.target.value) as typeof TextField;
export const DebouncedDatePicker = debounceComponent(DatePicker) as typeof DatePicker;
export const DebouncedDateTimePicker = debounceComponent(DateTimePicker) as typeof DateTimePicker;
export const DebouncedTimePicker = debounceComponent(TimePicker) as typeof TimePicker;