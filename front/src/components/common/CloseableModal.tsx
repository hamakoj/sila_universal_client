import {
    Container,
    Typography,
    Paper,
    Grid, SxProps
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import React from "react";
import {Theme} from "@mui/material/styles";

const RootGridStyle: SxProps<Theme> = {
    height: '80vh',
    maxHeight: '80vh',
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    overflowY: 'auto',
    display: 'flex',
    flexWrap: 'nowrap',
    flexDirection: 'column',
    maxWidth: 'lg',
}

export interface CloseableModalProps {
    title: string;
    handleClose: () => void;
    children: JSX.Element;
    gridStyle?: SxProps<Theme>;
    id?: string;
}

export default function CloseableModal(props: CloseableModalProps): JSX.Element {
    const gridStyle: SxProps<Theme> = {...RootGridStyle, ...(props.gridStyle || {})} as SxProps<Theme>;
    return (
        <Grid id={props.id} component={Paper} container justifyContent={"space-between"} sx={gridStyle} columns={{xs: 4, sm: 8, md: 12}}>
            <>
                <Grid sx={{backgroundColor: 'rgba(65,65,65,0.38)'}} container justifyContent={"space-between"} columns={{ xs: 4, sm: 8, md: 12 }}>
                    <div/>
                    <Typography fontSize={27} >
                        {props.title}
                    </Typography>
                    <IconButton sx={{float: "right", borderRadius: 0, background: '#ffffff1a'}} onClick={(e) => {
                        props.handleClose();
                    }}>
                        <CloseIcon fontSize="large"/>
                    </IconButton>
                </Grid>
                <Container sx={{width: '100%', height: '100%', overflowY: 'auto'}}>
                    {props.children}
                </Container>
            </>
        </Grid>
    );
}