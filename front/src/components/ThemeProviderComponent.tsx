import React, {useEffect} from 'react';
import '../App.css';

import {darkScrollbar, ThemeProvider} from "@mui/material";
import {createTheme, CssBaseline, PaletteMode} from "@mui/material";
import {useSelector} from "react-redux";
import {getAppSettingsSelector} from "../store/front/app/setting/selector";
import {Theme} from "@mui/material/";
import {grey} from "@mui/material/colors";
import {Palette} from "@mui/material/styles/createPalette";

//https://material.io/design/color/the-color-system.html#tools-for-picking-colors

const USCBlue = {
    50: '#e5f2fb',
    100: '#bfddf7',
    200: '#99c9f2',
    300: '#75b3ec',
    400: '#5ea3e8',
    500: '#2196f3',
    600: '#4d94e4',
    700: '#4074c3',
    800: '#3a64b1',
    900: '#2f4691',
    A100: '#82b1ff',
    A200: '#448aff',
    A400: '#2979ff',
    A700: '#2962ff'
};

export const AppHeaderHeightPx = 56;
export const ServerTabsHeightPx = 48;
export const ExpandedSideBarWidth = 240;
export const ClosedSidebarWidth = 56;
export const ServerTabsHeaderMarginPx = 5;
export const SidebarMarginPx = 5;
export const ContentHeightPx = AppHeaderHeightPx + ServerTabsHeaderMarginPx;
export const ContentWidthPx = ClosedSidebarWidth + SidebarMarginPx;
const getDesignTokens = (mode: PaletteMode): Theme & any => {
    const palette: Palette & any = {
        mode,
        ...(mode === 'light'
            ? {
                // palette values for light mode
                primary: {
                    main: USCBlue[700],
                    contrastText: "#ffffff",
                },
                text: {
                    primary: grey[800],
                    secondary: grey[900],
                },
                background: {
                    default: grey[200],
                    paper: grey[300],
                },
                divider: 'rgba(0,0,0,0.2)',
            }
            : {
                // palette values for dark mode
                primary: {
                    main: USCBlue[700],
                    contrastText: grey[300],
                },
                text: {
                    primary: grey[500],
                    secondary: grey[400],
                },
                secondary: {
                    main: '#4074C3',
                    contrastText: grey[300],
                },
                background: {
                    paper: '#161618',
                    default: '#161618'
                },
                divider: 'rgba(255,255,255,0.2)',
            }),
    };
    // todo see if we need https://mui.com/customization/theming/#responsivefontsizes-theme-options-theme
    const isDarkMode = mode === 'dark';
    return ({
        isDarkMode: isDarkMode,
        breakpoint: {
            values: {
                xs: 0,
                sm: 600,
                md: 900,
                lg: 1200,
                xl: 1536,
            },
        },
        typography: {
            h1: {
                fontSize: 60
            },
            h2: {
                fontSize: 45
            }
        },
        palette: palette,
        components: {
            MuiCssBaseline: {
                styleOverrides: {
                    body: {
                        ...darkScrollbar(),
                        '&::-webkit-scrollbar, & *::-webkit-scrollbar': {
                            width: '14px',
                        },
                        '&::-webkit-scrollbar:hover, & *::-webkit-scrollbar:hover': {
                            background: 'rgba(0,0,0, 0.1)',
                        },
                        '&::-webkit-scrollbar-thumb:hover, & *::-webkit-scrollbar-thumb:hover': {
                            backgroundColor: USCBlue[700],
                        },
                        '&::-webkit-scrollbar-thumb, & *::-webkit-scrollbar-thumb': {
                            ...darkScrollbar()['&::-webkit-scrollbar-thumb, & *::-webkit-scrollbar-thumb'],
                            border: '4px solid rgba(0, 0, 0, 0)',
                            backgroundClip: 'padding-box'
                        }
                    }
                },
            },
            MuiIconButton: {
                styleOverrides: {
                    root: {
                        color: palette.text.primary
                    }
                }
            },
            MuiListItemIcon: {
                styleOverrides: {
                    root: {
                    }
                }
            },
            MuiAccordionSummary: {
                styleOverrides: {
                    root: {
                    },
                    expandIconWrapper: {
                    }
                }
            },
            ...((isDarkMode) ? ({
                MuiAlert: {
                    styleOverrides: {
                        standardSuccess: {
                            background: 'rgb(37 57 39)'
                        },
                        standardInfo: {
                            background: 'rgb(5 51 71)'
                        },
                        standardWarning: {
                            background: 'rgb(65 47 18)'
                        },
                        standardError: {
                            background: 'rgb(69 35 35)'
                        }
                    }
                }
            }) : ({})),
        },
    });
};

export default function ThemeProviderComponent(props: {children: JSX.Element}): JSX.Element {
    const { darkMode } = useSelector(getAppSettingsSelector);
    const [theme, setTheme] = React.useState<Theme>(createTheme(getDesignTokens(darkMode ? 'dark' : 'light')));

    useEffect(() => {
        setTheme(createTheme(getDesignTokens(darkMode ? 'dark' : 'light')))
    }, [darkMode]);

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            {props.children}
        </ThemeProvider>
  );
}