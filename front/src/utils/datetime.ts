import {DateTime, DurationUnit, Zone} from "luxon";
import {ProtoDate, ProtoTime, ProtoTimestamp, ProtoTimezone} from "../generated/SiLAFramework";
import {getCurrentTimeZoneOffset, getTimezoneOffsetString, protoTimezoneFromZone} from "./timezone";

export function getProtoDateWithDefault(protoTs?: ProtoDate): ProtoDate {
    const dateTime = DateTime.now();
    return {
        year: protoTs?.year || dateTime.year,
        month: protoTs?.month || dateTime.month,
        day: protoTs?.day || dateTime.day,
        timezone: getProtoTimezoneWithDefault(protoTs?.timezone),
    }
}

export function getProtoTimeWithDefault(protoTs?: ProtoTime): ProtoTime {
    const dateTime = DateTime.now();
    return {
        hour: protoTs?.hour || dateTime.hour,
        minute: protoTs?.minute || dateTime.minute,
        second: protoTs?.second || dateTime.second,
        timezone: getProtoTimezoneWithDefault(protoTs?.timezone),
    }
}

export function getProtoTimestampWithDefault(protoTs?: ProtoTimestamp): ProtoTimestamp {
    return {
        ...getProtoDateWithDefault(protoTs),
        ...getProtoTimeWithDefault(protoTs),
    }
}

export function getProtoTimezoneWithDefault(protoTs?: ProtoTimezone): ProtoTimezone {
    const currentTimeZoneOffset = getCurrentTimeZoneOffset();
    return {
        hours: protoTs?.hours || currentTimeZoneOffset.hours,
        minutes: protoTs?.minutes || currentTimeZoneOffset.minutes,
    }
}

export function protoTimestampToDatetime(protoTs: ProtoTimestamp): DateTime {
    try {
        return DateTime.fromObject({
            year: protoTs.year, // a year, such as 1987
            month: protoTs.month, // a month, 1-12
            day: protoTs.day, // a day of the month, 1-31, depending on the month
            hour: protoTs.hour, // hour of the day, 0-23
            minute: protoTs.minute, // minute of the hour, 0-59
            second: protoTs.second, // second of the minute, 0-59
            millisecond: 0, // millisecond of the second, 0-999
        }, {
            zone: getTimezoneOffsetString(protoTs.timezone?.hours || 0, protoTs.timezone?.minutes || 0)
        });
    } catch (e: any) {
        return DateTime.invalid(e);
    }
}

export function protoDateToDatetime(protoDate: ProtoDate): DateTime {
    try {
        return DateTime.fromObject({
            year: protoDate.year, // a year, such as 1987
            month: protoDate.month, // a month, 1-12
            day: protoDate.day, // a day of the month, 1-31, depending on the month
        }, {
            zone: getTimezoneOffsetString(protoDate.timezone?.hours || 0, protoDate.timezone?.minutes || 0)
        });
    } catch (e: any) {
        return DateTime.invalid(e);
    }
}

export function protoTimeToDatetime(protoTime: ProtoTime): DateTime {
    try {
        return DateTime.fromObject({
            hour: protoTime.hour, // hour of the day, 0-23
            minute: protoTime.minute, // minute of the hour, 0-59
            second: protoTime.second, // second of the minute, 0-59
            millisecond: 0, // millisecond of the second, 0-999
        }, {
            zone: getTimezoneOffsetString(protoTime.timezone?.hours || 0, protoTime.timezone?.minutes || 0)
        });
    } catch (e: any) {
        return DateTime.invalid(e);
    }
}

export function dateTimeToProtoTimestamp(dateTime: DateTime, zone?: Zone): ProtoTimestamp {
    return {
        year: dateTime.year,
        month: dateTime.month,
        day: dateTime.day,
        hour: dateTime.hour,
        minute: dateTime.minute,
        second: dateTime.second,
        timezone: protoTimezoneFromZone(zone ? zone : dateTime.zone)
    }
}

export function dateTimeToProtoDate(dateTime: DateTime, zone?: Zone): ProtoDate {
    return {
        year: dateTime.year,
        month: dateTime.month,
        day: dateTime.day,
        timezone: protoTimezoneFromZone(zone ? zone : dateTime.zone)
    }
}

export function isSame(a: DateTime, b: DateTime, granularity: DurationUnit): boolean {
    return a.diff(b, granularity).get(granularity) === 0;
}

export function isSameOrAfter(a: DateTime, b: DateTime, granularity: DurationUnit): boolean {
    return a.diff(b, granularity).get(granularity) >= 0;
}

export function isAfter(a: DateTime, b: DateTime, granularity: DurationUnit): boolean {
    return a.diff(b, granularity).get(granularity) > 0;
}

export function isSameOrBefore(a: DateTime, b: DateTime, granularity: DurationUnit): boolean {
    return a.diff(b, granularity).get(granularity) <= 0;
}

export function isBefore(a: DateTime, b: DateTime, granularity: DurationUnit): boolean {
    return a.diff(b, granularity).get(granularity) < 0;
}