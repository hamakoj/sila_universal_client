import {SiLAJavaDataTypeType, SiLAJavaFeature} from '../../generated/silaModels';
import {XMLParser} from 'fast-xml-parser';
import {stringToCamelCase} from "../string";

export default class FeatureParser {
    static setKeyToArray(obj: any, key:string): void {
        if (!Array.isArray(obj[key])) {
            if (obj[key]) {
                obj[key] = [obj[key]];
            } else {
                obj[key] = [];
            }
        }
    }

    static camelizeKeys(obj: any): any {
        if (Array.isArray(obj)) {
            return obj.map(v => FeatureParser.camelizeKeys(v));
        } else if (obj !== null && obj.constructor === Object) {
            return Object.keys(obj).reduce(
                (result, key) => ({
                    ...result,
                    [stringToCamelCase(key)]: FeatureParser.camelizeKeys(obj[key]),
                }),
                {},
            );
        }
        return obj;
    }

    static setSiLAElementToArray(obj:any): void {
        const dt: SiLAJavaDataTypeType = {
            basic: null, constrained: null, dataTypeIdentifier: null, list: null, structure: null
        } as any as SiLAJavaDataTypeType;
        obj.dataType = {
            ...dt,
            ...obj.dataType
        }
        if (obj.dataType.basic) {
            obj.dataType.basic = obj.dataType.basic.toUpperCase();
        }
        if (obj.dataType.list) {
            FeatureParser.setSiLAElementToArray(obj.dataType.list);
        }
        if (obj.dataType.structure) {
            FeatureParser.setKeyToArray(obj.dataType.structure, 'element');
            obj.dataType.structure.element.forEach(FeatureParser.setSiLAElementToArray);
        }
        if (obj.dataType.constrained) {
            FeatureParser.setSiLAElementToArray(obj.dataType.constrained);
            if (obj.dataType.constrained.constraints.set) {
                FeatureParser.setKeyToArray(obj.dataType.constrained.constraints.set, 'value');
            }
            if (obj.dataType.constrained.constraints.unit) {
                FeatureParser.setKeyToArray(obj.dataType.constrained.constraints.unit, 'unitComponent');
            }
            if (obj.dataType.constrained.constraints.contentType) {
                if (obj.dataType.constrained.constraints.contentType.parameters) {
                    FeatureParser.setKeyToArray(obj.dataType.constrained.constraints.contentType.parameters, 'parameter');
                } else {
                    obj.dataType.constrained.constraints.contentType.parameters = [];
                }
            }
            if (obj.dataType.constrained.constraints.allowedTypes) {
                FeatureParser.setKeyToArray(obj.dataType.constrained.constraints.allowedTypes, 'dataType');
                obj.dataType.constrained.constraints.allowedTypes.dataType.forEach((a: any) => {
                    // todo check if it is changed by ref still
                    FeatureParser.setSiLAElementToArray({dataType: a})
                });
            }
        }
    }

    static displayType(d: SiLAJavaDataTypeType, message: string, depth: number): void {
        if (d.basic) {
            console.log(`${'-'.repeat(depth)} ${message}${d.basic}`);
        }
        if (d.list) {
            FeatureParser.displayType(d.list.dataType, message + 'list of ', depth + 1);
        }
        if (d.constrained) {
            FeatureParser.displayType(d.constrained.dataType, message + 'constraint on ', depth + 1);
        }
        if (d.structure) {
            d.structure.element.forEach(e => FeatureParser.displayType(e.dataType, message + 'structure with ', depth + 1))
        }
    }

    static getParser<T>(): (xml: string) => T {
        const options = {
            //attributeNamePrefix : "@_",
            //attrNodeName: "attr", //default is 'false'
            //textNodeName : "#text",
            ignoreAttributes : false,
            ignoreNameSpace : false,
            allowBooleanAttributes : false,
            parseNodeValue : true,
            parseAttributeValue : true,
            trimValues: true,
            //cdataTagName: "__cdata", //default is 'false'
            //cdataPositionChar: "\\c",
            parseTrueNumberOnly: true,
            arrayMode: false, //"strict".
            //attrValueProcessor: (val:any, attrName:any) => {
            //    return val;
            //},
            //tagValueProcessor : (val:any, tagName:any) => {
            //    return val;
            //},
            //stopNodes: ["Command", "Property", "DataTypeDefinition", "DefinedExecutionError", "Metadata"]
        };
        return ((xml) => (new XMLParser(options).parse(xml, true)));
    }

    static parse(xml: string): SiLAJavaFeature | null {
        let parsedXml
        try {
            parsedXml = this.getParser<SiLAJavaFeature>()(xml);
        } catch(error) {
            // todo handle
            console.log(error)
            return null;
        }

        parsedXml = FeatureParser.camelizeKeys(parsedXml);
        const f: SiLAJavaFeature = parsedXml.feature as SiLAJavaFeature;
        FeatureParser.setKeyToArray(f, 'command')
        FeatureParser.setKeyToArray(f, 'property')
        FeatureParser.setKeyToArray(f, 'metadata')
        FeatureParser.setKeyToArray(f, 'definedExecutionError')
        FeatureParser.setKeyToArray(f, 'dataTypeDefinition')
        f.dataTypeDefinition.forEach(dt => {
            FeatureParser.setSiLAElementToArray(dt);
        })
        f.definedExecutionError.forEach(d => {
            FeatureParser.setKeyToArray(d, 'identifier');
        })
        f.metadata.forEach(m => {
            FeatureParser.setSiLAElementToArray(m);
            if (m.definedExecutionErrors) {
                FeatureParser.setKeyToArray(m.definedExecutionErrors, 'identifier');
            }
        })
        f.command.forEach((c) => {
            FeatureParser.setKeyToArray(c, 'parameter')
            FeatureParser.setKeyToArray(c, 'response')
            FeatureParser.setKeyToArray(c, 'intermediateResponse')
            c.parameter.forEach(FeatureParser.setSiLAElementToArray)
            c.response.forEach(FeatureParser.setSiLAElementToArray)
            c.intermediateResponse.forEach(FeatureParser.setSiLAElementToArray)
            if (c.definedExecutionErrors) {
                FeatureParser.setKeyToArray(c.definedExecutionErrors, 'identifier');
            }
        })
        f.property.forEach((p) => {
            FeatureParser.setSiLAElementToArray(p);
            if (p.definedExecutionErrors) {
                FeatureParser.setKeyToArray(p.definedExecutionErrors, 'identifier');
            }
        })
        return f;
    }
};