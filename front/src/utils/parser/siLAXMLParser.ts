import * as Protobuf from "protobufjs";
import {Field, IParserResult, Type} from "protobufjs";
import FeatureParser from "./featureParser";
import {SiLAJavaConstraints, SiLAJavaDataTypeType} from "../../generated/silaModels";
import {getRandomId} from "../idGenerator";
import SiLAFramework from "./../../generated/SiLAFramework.proto"

export default class SiLAXMLParser {
    // todo replace with getSiLAFramework to return a new instance of Protobuf.parse(AnyTypeParser.getSiLAFrameworkProto()) everytime it is called
    static readonly SILA_FRAMEWORK: IParserResult = Protobuf.parse(SiLAXMLParser.getSiLAFrameworkProto());

    static parseXml(xml: string | undefined): SiLAJavaDataTypeType | null {
        if (xml === undefined) {
            return null;
        }
        const parseAnyType = SiLAXMLParser.parseAnyType(xml);
        if (!parseAnyType) {
            // todo handle error
            return null;
        }

        const containsType = Object.values(parseAnyType).some(v => v !== null);
        if (!containsType) {
            // todo handle error
            return null;
        }

        return parseAnyType;
    }

    static parseConstraints(xml: string): SiLAJavaConstraints | null {
        let parsedXml
        try {
            parsedXml = FeatureParser.getParser<{Constraints: SiLAJavaConstraints}>()(xml);
        } catch(error) {
            // todo handle
            //console.log(error)
            return null;
        }
        return FeatureParser.camelizeKeys(parsedXml.Constraints);
    }

    static parseAnyType(xml: string): SiLAJavaDataTypeType | null {
        let parsedXml
        try {
            parsedXml = FeatureParser.getParser<{dataType: SiLAJavaDataTypeType, dataTypeType?: SiLAJavaDataTypeType}>()(xml);
            if (parsedXml.dataTypeType) {
                parsedXml.dataType = parsedXml.dataTypeType;
            }
        } catch(error) {
            // todo handle
            //console.log(error)
            return null;
        }
        parsedXml = FeatureParser.camelizeKeys(parsedXml);
        FeatureParser.setSiLAElementToArray(parsedXml)
        return parsedXml.dataType;
    }

    static decodeAnyType(silaType: SiLAJavaDataTypeType, base64EncodedProtoMessage: string): any {
        const message = new Type('');
        message.add(SiLAXMLParser.SILA_FRAMEWORK.root);
        message.add(SiLAXMLParser.parseProtoFieldFromDataType(message, 'any', silaType));
        //const newMessage = rootMessage.create({ value: "test"} );
        //const array = rootMessage.encode(newMessage).finish();
        //const b64encoded = btoa(String.fromCharCode.apply(null, array as unknown as number[]));
        //const base64EncodedProtoMessage = "Cc3MzMzMzB5A";
        const buffer = Uint8Array.from(atob(base64EncodedProtoMessage), c => c.charCodeAt(0));
        let decoded;
        try {
            // try with wrapped type
            decoded = message.decode(buffer);
        } catch (e) {
            // try with unwrapped type
            const rootMessage = message.lookupType(message.fieldsArray[0].type);
            decoded = rootMessage.decode(buffer);
        }
        if (silaType.structure || silaType.list) {
            // @ts-ignore
            return decoded[message.fieldsArray[0].name]; // unwrap result
        }
        return decoded;
    }

    // todo move to utils
    static capitalizeFirstLetter(str: string) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    static parseProtoFieldFromDataType(
        message: Protobuf.Type,
        identifier: string,
        dt: SiLAJavaDataTypeType,
        id: number = 1,
        repeated: boolean = false
    ): Field {
        if (dt.basic) {
            return new Field(
                identifier,
                id,
                "sila2.org.silastandard." + SiLAXMLParser.capitalizeFirstLetter(dt.basic.toLowerCase()),
                (repeated) ? "repeated" : undefined
            )
        } else if (dt.dataTypeIdentifier) {
            throw new Error('Data type identifier not allowed in any type');
        } else if (dt.constrained) {
            return SiLAXMLParser.parseProtoFieldFromDataType(message, identifier, dt.constrained.dataType, id, repeated);
        } else if (dt.structure) {
            const struct = new Type(identifier + getRandomId() + '_Struct');
            for (let i = 0; i < dt.structure.element.length; i++) {
                const element = dt.structure.element[i];
                struct.add(SiLAXMLParser.parseProtoFieldFromDataType(message, element.identifier, element.dataType, i, false))
            }
            message.add(struct);
            return new Field(
                identifier + '_Struct',
                id,
                struct.fullName,
                (repeated) ? "repeated" : undefined
            );
        } else if (dt.list) {
            return SiLAXMLParser.parseProtoFieldFromDataType(message, identifier, dt.list.dataType, 1, true);
        } else {
            throw new Error('Unknown data type');
        }
    }

    static getSiLAFrameworkProto(): string {
        return SiLAFramework;
    }
};