import {DateTime, IANAZone, SystemZone, Zone} from 'luxon';
import tzdata from 'tzdata';
import {ProtoTime, ProtoTimezone} from "../generated/SiLAFramework";
import {AppTimezoneSettings} from "../store/front/app/setting/types";

const validLuxonTimezones: string[] = Array.from(new Set<string>(
    Object.keys(tzdata.zones).filter(
        tz => tz.includes('/') && DateTime.local().setZone(tz).isValid
    )
)).sort((a, b) => (a < b ? -1 : 1));

export function getZoneFromTimezoneSettings(settings: AppTimezoneSettings): Zone {
    return (settings.useSystemTimezone) ? SystemZone.instance : IANAZone.create(settings.customTimezoneName)
}

export function getTimezones(): string[] {
    return validLuxonTimezones;
}

export function getCurrentTimeZoneName(): string {
    return DateTime.now().zoneName
}

export function getCurrentTimeZoneOffsetNameShort(): string {
    return DateTime.now().offsetNameShort
}

export function getSystemDefaultTimezoneName(): string {
    return SystemZone.instance.name
}

export function getProtoTimezoneFromZone(zone: Zone): ProtoTimezone {
    const offset = zone.offset(0);
    const offsetHours = offset / 60;
    const offsetMinutes = offset % 60;
    return {
        hours: offsetHours,
        minutes: offsetMinutes
    }
}

export function getCurrentTimeZoneOffset(): ProtoTimezone {
    return getProtoTimezoneFromZone(DateTime.now().zone);
}

function numberToPaddedString(n: number, pad: number): string {
    return String(n).padStart(pad, '0');
}

export function getTimezoneOffsetString(hours: number, minutes: number): string {
    const sign = (hours === 0) ? (minutes) : (hours);
    return 'UTC' + ((sign >= 0) ? ('+') : ('-'))
        + numberToPaddedString(Math.abs(hours), 2)
        + ':' + numberToPaddedString(Math.abs(minutes), 2);
}

export function getCurrentTimezoneOffsetString(): string {
    const timeZoneOffset = getCurrentTimeZoneOffset();
    return getTimezoneOffsetString(timeZoneOffset.hours || 0, timeZoneOffset.minutes || 0);
}

export function protoTimezoneFromZone(zone: Zone): ProtoTimezone {
    const offsetInMinutes = zone.offset(0);
    return {
        hours: offsetInMinutes / 60,
        minutes: offsetInMinutes % 60
    }
}

export function dateTimeToProtoTime(dateTime: DateTime, zone?: Zone): ProtoTime {
    return {
        hour: dateTime.hour,
        minute: dateTime.minute,
        second: dateTime.second,
        timezone: protoTimezoneFromZone(zone ? zone : dateTime.zone)
    }
}