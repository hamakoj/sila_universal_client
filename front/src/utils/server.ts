export function getServerInitial(name: string): string {
    return (getServerInitialCustom(name, 3));
}

export function getServerInitialCustom(name: string, nbInitial: number): string {
    return name.split(' ').map(i => i.charAt(0)).join('').toUpperCase().substr(0, nbInitial);
}

export function getFullyQualifiedCallId(call: {serverUuid: string, fullyQualifiedFeatureId: string | undefined, commandIdentifier: string | undefined}): string {
    return call.serverUuid + '/' + (call.fullyQualifiedFeatureId || '') + '/' + (call.commandIdentifier || '');
}