const backUrlKey = 'backUrl';

export function getBackUrl(): string | null {
    return window.localStorage.getItem('backUrl');
}

export function setBackUrl(url: string): void {
    window.localStorage.setItem(backUrlKey, url);
}