// Compact arrays with null entries; delete keys from objects with null value
export function removeNulls(obj: any): any {
    const isArray = obj instanceof Array;
    for (const k in obj){
        if (obj[k]===null) {
            isArray ? obj.splice(k as any as number, 1) : delete obj[k];
        } else if (typeof obj[k]=="object") {
            removeNulls(obj[k]);
        }
    }
    return obj;
}