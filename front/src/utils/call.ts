import {
    metadatasStateToGrpcJson,
    parametersStateToGrpcJson
} from "../components/server/call/dataTypeForm/stateToGrpcJson";
import {
    SiLACallModel,
    SiLAJavaType,
    UscCommand,
    UscFeature,
    UscMetadata,
    UscProperty,

} from "../generated/silaModels";
import {getBackEndpoint} from "./endpoint";
import {
    SiLATypeValueState
} from "../store/front/server/callInput/types";


export function isCommand(call: UscCommand | UscProperty): boolean {
    return !!((call as UscCommand).parameters);
}

export function executeServeCall(
    serverUUID: string,
    feature: UscFeature,
    call: UscCommand | UscProperty,
    serverMetadatas: UscMetadata[],
    callMetadatas: SiLATypeValueState | undefined,
    callParameters: SiLATypeValueState,
    readOnce: boolean = false
): Promise<Response> {
    // todo add option to execute call without persistence

    const isCmd = isCommand(call);
    const parameters = (!isCmd) ? {} : parametersStateToGrpcJson(
        feature.dataTypeDefinitions,
        (call as UscCommand).parameters,
        callParameters
    );
    const metadatas: any = (!callMetadatas) ? {} : metadatasStateToGrpcJson(serverMetadatas, callMetadatas);

    const type: SiLAJavaType =
        ((call.observable) ? "OBSERVABLE_" : "UNOBSERVABLE_")
        + (isCmd ? "COMMAND" : "PROPERTY")
        + ((!isCmd && call.observable && readOnce) ? '_READ' : '') as SiLAJavaType
    const callModel: SiLACallModel = {
        serverId: serverUUID,
        fullyQualifiedFeatureId: feature.fullyQualifiedIdentifier,
        callId: call.relativeIdentifier,
        type,
        ...(isCmd) ? {parameters: JSON.stringify(parameters)} : {},
        ...(serverMetadatas) ? {metadatas: JSON.stringify(metadatas)} : {},
        runAsync: true
    };
    return executeServerCallModel(callModel);
}

export function executeServerCallModel(callModel: SiLACallModel): Promise<Response> {
    return fetch(getBackEndpoint('/api/calls/execute'), {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(callModel)
    })
}

export function getCallMetadatas(features: UscFeature[]) {
    /*
    Even if it seems like an error it's not.
    relativeIdentifier is being used internally as a key for a map
    Metadata with the same relative identifier will clash, hence why fully qualified id is used instead.
    */
    return features.flatMap(f => f.metadatas).map((m) => ({...m, relativeIdentifier: m.fullyQualifiedIdentifier}))
}
