export class FullyQualifiedIdentifierUtils {
    private static readonly ORIGINATOR_REGEX: string = "[a-z][a-z.]*";
    private static readonly CATEGORY_REGEX: string = "[a-z][a-z.]*";
    private static readonly IDENTIFIER_REGEX: string = "[A-Z][a-zA-Z0-9]*";
    private static readonly MAJOR_VERSION_REGEX: string = "v\\d+";

    private static readonly FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX: string =
        `${FullyQualifiedIdentifierUtils.ORIGINATOR_REGEX}/${FullyQualifiedIdentifierUtils.CATEGORY_REGEX}/${FullyQualifiedIdentifierUtils.IDENTIFIER_REGEX}/${FullyQualifiedIdentifierUtils.MAJOR_VERSION_REGEX}`;
    private static readonly FULLY_QUALIFIED_COMMAND_IDENTIFIER_REGEX: string =
        `${FullyQualifiedIdentifierUtils.FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX}/Command/${FullyQualifiedIdentifierUtils.IDENTIFIER_REGEX}`;

    public static readonly FullyQualifiedFeatureIdentifierPattern
        = new RegExp(FullyQualifiedIdentifierUtils.FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX);
    public static readonly FullyQualifiedCommandIdentifierPattern
        = new RegExp(FullyQualifiedIdentifierUtils.FULLY_QUALIFIED_COMMAND_IDENTIFIER_REGEX);
    public static readonly FullyQualifiedCommandParameterIdentifierPattern
        = new RegExp(`${FullyQualifiedIdentifierUtils.FULLY_QUALIFIED_COMMAND_IDENTIFIER_REGEX}/Parameter/${FullyQualifiedIdentifierUtils.IDENTIFIER_REGEX}`);

    public static readonly FullyQualifiedCommandResponseIdentifierPattern
        = new RegExp(`${FullyQualifiedIdentifierUtils.FULLY_QUALIFIED_COMMAND_IDENTIFIER_REGEX}/Response/${FullyQualifiedIdentifierUtils.IDENTIFIER_REGEX}`);
    public static readonly FullyQualifiedIntermediateCommandResponseIdentifierPattern
        = new RegExp(`${FullyQualifiedIdentifierUtils.FULLY_QUALIFIED_COMMAND_IDENTIFIER_REGEX}/IntermediateResponse/${FullyQualifiedIdentifierUtils.IDENTIFIER_REGEX}`);
    public static readonly FullyQualifiedDefinedExecutionErrorIdentifierPattern
        = new RegExp(`${FullyQualifiedIdentifierUtils.FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX}/DefinedExecutionError/${FullyQualifiedIdentifierUtils.IDENTIFIER_REGEX}`);
    public static readonly FullyQualifiedPropertyIdentifierPattern
        = new RegExp(`${FullyQualifiedIdentifierUtils.FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX}/Property/${FullyQualifiedIdentifierUtils.IDENTIFIER_REGEX}`);
    public static readonly FullyQualifiedCustomDataTypeIdentifierPattern
        = new RegExp(`${FullyQualifiedIdentifierUtils.FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX}/DataType/${FullyQualifiedIdentifierUtils.IDENTIFIER_REGEX}`);
    public static readonly FullyQualifiedMetadataIdentifierPattern
        = new RegExp(`${FullyQualifiedIdentifierUtils.FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX}/Metadata/${FullyQualifiedIdentifierUtils.IDENTIFIER_REGEX}`);

    public static getDefinedErrorIdentifier(fullyQualifiedDefinedErrorIdentifier: string): string | null {
        const isValidErrorIdentifier = FullyQualifiedIdentifierUtils.FullyQualifiedDefinedExecutionErrorIdentifierPattern.test(
            fullyQualifiedDefinedErrorIdentifier
        );
        return isValidErrorIdentifier ? fullyQualifiedDefinedErrorIdentifier.split('/')[5] : null
    }

    public static getParameterIdentifier(fullyQualifiedDefinedErrorIdentifier: string): string | null {
        const isValidParameterIdentifier = FullyQualifiedIdentifierUtils.FullyQualifiedCommandParameterIdentifierPattern.test(
            fullyQualifiedDefinedErrorIdentifier
        );
        return isValidParameterIdentifier ? fullyQualifiedDefinedErrorIdentifier.split('/')[7] : null
    }
}