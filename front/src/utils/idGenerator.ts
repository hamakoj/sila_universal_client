import hexoid from "hexoid";

const generator = hexoid(36)

export function getRandomId(): string {
    return generator();
}