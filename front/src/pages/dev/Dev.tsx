import React, {useEffect, useState} from 'react';
import {
    Alert,
    Box, Button, Grid, Paper, TextField, Typography
} from '@mui/material/';
import SiLAXMLParser from "../../utils/parser/siLAXMLParser";
import FeatureViewer from "../../components/FeatureViewer";
import {useDispatch, useSelector} from "react-redux";
import {getDevSelector} from "../../store/front/app/dev/selector";
import {addDevFrontPlugin, removeDevFrontPlugin} from "../../store/front/app/dev/actions";
import Package from "../../../package.json";
import Git from "../../generated/git.json";
import {UscVersion} from "../../generated/silaModels";
import {getBackEndpoint} from "../../utils/endpoint";
import Divider from "@mui/material/Divider";
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import PluginsComponentRenderer from "../../plugin/PluginsComponentRenderer";

export default function Dev(): JSX.Element {
    const[xml, setXml] = useState<string>('');
    const[value, setValue] = useState<string>('');
    const[decoded, setDecoded] = useState<any>('');
    const[componentUrl, setComponentUrl] = useState<any>('');
    const[componentName, setComponentName] = useState<any>('');
    const[jsonProps, setJsonProps] = useState<any>('{}');
    // todo check if front version is compatible with back version
    const[backVersion, setBackVersion] = useState<UscVersion | null>(null);
    const dev = useSelector(getDevSelector);
    const dispatch = useDispatch();
    useEffect(() => {
        try {
            const dataType = SiLAXMLParser.parseXml(xml);
            if (dataType) {
                const decodeAnyType = SiLAXMLParser.decodeAnyType(dataType, value);
                setDecoded(JSON.stringify(decodeAnyType));
            } else {
                setDecoded('Invalid Data Type');
            }
        } catch (e) {
            setDecoded('Invalid Payload');
        }
    }, [xml, value]);

    useEffect(() => {
        fetch(getBackEndpoint('/api/version'))
            .then(response => response.json())
            .then((version) => {
                setBackVersion(version)
            })
            .catch((error) => {
                console.error(error);
            })
    }, [setBackVersion]);


    let propsToForward: any = {};
    let error = '';
    try {
        propsToForward = JSON.parse(jsonProps);
    } catch (e) {
        error = 'JSON Props is invalid'
    }

    return (
        <Box>
            <Typography variant="h1" component="h1" gutterBottom>
                Dev
            </Typography>
            <Typography variant="h2" component="h2" gutterBottom>
                Version
            </Typography>
            <Box>
                <Box>
                    Front version: {Package.version}
                </Box>
                <Box>
                    Front Git branch: {Git["git.branch"]}
                </Box>
                <Box>
                    Front Git commit: {Git["git.commit.id"]}
                </Box>
                <Box>
                    Front Git closest tag: {Git["git.closest.tag.name"]}
                </Box>
                <Box>
                    Front Git dirty: {Git["git.dirty"]}
                </Box>
            </Box>
            <Divider/>
            <Box>
                {
                    (backVersion != null) ? (
                        <>
                            <Box>
                                Back version: {backVersion.buildVersion}
                            </Box>
                            <Box>
                                Back Git branch: {backVersion.branch}
                            </Box>
                            <Box>
                                Back Git commit: {backVersion.commitId}
                            </Box>
                            <Box>
                                Back Git closest tag: {backVersion.closestTag}
                            </Box>
                            <Box>
                                Back Git dirty: {backVersion.dirty}
                            </Box>
                        </>
                    ) : (
                        <>
                            <Box>
                                Unknown back version
                            </Box>
                        </>
                    )
                }
            </Box>
            <Typography variant="h2" component="h2" gutterBottom>
                Data type to Protobuf
            </Typography>
            <Paper>
                <Box margin={2}>
                    <TextField
                        id="outlined-multiline-static"
                        label="XML"
                        multiline
                        rows={4}
                        value={xml}
                        onChange={(e) => {
                            setXml(e.target.value);
                        }}
                    />
                </Box>
                <Box margin={2}>
                    <TextField
                        id="outlined-multiline-static"
                        label="Base64 Encoded Proto Value"
                        multiline
                        rows={4}
                        value={value}
                        onChange={(e) => {
                            setValue(e.target.value);
                        }}
                    />
                </Box>
                <Box margin={2}>
                    <TextField
                        id="outlined-multiline-static"
                        label="Decoded"
                        multiline
                        rows={4}
                        value={decoded}
                    />
                </Box>
            </Paper>
            <FeatureViewer/>
            <Typography variant="h2" component="h2" gutterBottom>
                Remote component
            </Typography>
            <Paper>
                <Box padding={2}>
                    <Typography variant={'h6'}>Add Front Dev Plugin</Typography>
                    <TextField
                        id="outlined-multiline-static"
                        label="Front dev plugin URL"
                        value={componentUrl}
                        onChange={(e) => {
                            setComponentUrl(e.target.value);
                        }}
                    />
                    <Button variant="contained" color="primary" onClick={() => {
                        dispatch(addDevFrontPlugin({url: componentUrl}))
                    }}>
                        Add dev front plugin
                    </Button>
                </Box>
                <Divider/>
                <Box padding={2}>
                    <Typography variant={'h6'}>Loaded Front Dev Plugins</Typography>
                    <Grid container direction={'column'} spacing={1}>
                        {
                            Object.keys(dev.frontPlugins).map((url) => (
                                <Grid item display={'inline-flex'} key={url}>
                                    <Typography>{url}</Typography>
                                    <IconButton style={{paddingTop: 0}} onClick={() => {
                                        dispatch(removeDevFrontPlugin({url: url}));
                                    }}>
                                        <CloseIcon/>
                                    </IconButton>
                                </Grid>
                            ))
                        }
                    </Grid>
                </Box>
                <Divider/>
                <Box padding={2}>
                    <Typography variant={'h6'}>Display plugin component with prop</Typography>
                    <TextField
                        id="outlined-multiline-static"
                        label="Component name"
                        value={componentName}
                        sx={{marginBottom: '16px'}}
                        onChange={(e) => {
                            setComponentName(e.target.value);
                        }}
                    />
                    <TextField
                        id="outlined-multiline-static"
                        label="JSON props"
                        multiline
                        fullWidth
                        value={jsonProps}
                        onChange={(e) => {
                            setJsonProps(e.target.value);
                        }}
                    />
                    <PluginsComponentRenderer componentName={componentName} forwardProps={propsToForward}/>
                    {((error && (
                        <Alert severity={"warning"}>{error}</Alert>
                    )))}
                </Box>
            </Paper>
        </Box>
    );
}