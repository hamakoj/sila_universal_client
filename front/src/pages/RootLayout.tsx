import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {getIsConnectedSelector, getLostConnectionSelector} from "../store/sila/websocket/selector";
import {Outlet, useLocation} from "react-router-dom";
import {Alert, Button, Grid, Snackbar} from "@mui/material";
import Sidebar from "../components/Sidebar";
import ServerTabs from "../components/server/ServerTabs";
import OpenServers from "./server/OpenServers";
import {ContentHeightPx, ServerTabsHeaderMarginPx, SidebarMarginPx} from "../components/ThemeProviderComponent";
import {SxProps} from "@mui/system";
import {Theme} from "@mui/material/styles";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

const RootLayout = React.memo((): JSX.Element => {
    const allOpenServers = React.useMemo(() => <OpenServers/>, []);
    const isWsConnected: boolean = useSelector(getIsConnectedSelector);
    const lostConnection: boolean = useSelector(getLostConnectionSelector);
    const [isOnlineAlertOpen, setIsOnlineAlertOpen] = useState<boolean>(false);
    const [isOfflineAlertOpen, setIsOfflineAlertOpen] = useState<boolean>(false);
    const [displayDesyncAlert, setDisplayDesyncAlert] = useState<boolean>(lostConnection);
    const {pathname} = useLocation();

    useEffect(() => {
        setIsOfflineAlertOpen(!isWsConnected);
        setIsOnlineAlertOpen(isWsConnected)
    }, [isWsConnected]);

    useEffect(() => {
        setDisplayDesyncAlert(lostConnection)
    }, [lostConnection]);

    const snackbarAlert = (
        <>
            <Snackbar
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center"
                }}
                open={displayDesyncAlert && isWsConnected}
            >
                <Alert severity={'warning'} action={
                    <>
                        <Button sx={{minWidth: '100px'}} color={"warning"} variant={'contained'}
                                size="small" onClick={() => {
                            setDisplayDesyncAlert(false);
                            window.location.reload();
                        }}>
                            Reload page
                        </Button>
                        <IconButton sx={{marginLeft: '8px', padding: '4px'}} onClick={() => {
                            setDisplayDesyncAlert(false);
                        }}>
                            <CloseIcon/>
                        </IconButton>
                    </>
                }>
                    {'The application might be desynchronized, it is advised to reload the page'}
                </Alert>
            </Snackbar>
            <Snackbar
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center"
                }}
                open={!isWsConnected && !lostConnection}
            >
                <Alert
                    severity={'info'}
                >
                    {'Loading...'}
                </Alert>
            </Snackbar>
            <Snackbar
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center"
                }}
                open={isOfflineAlertOpen && !isWsConnected && lostConnection}
            >
                <Alert
                    severity={'error'}
                >
                    {'You are offline, attempting to reconnect...'}
                </Alert>
            </Snackbar>
            <Snackbar
                autoHideDuration={2000}
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center"
                }}
                onClose={() => setIsOnlineAlertOpen(false)}
                open={isOnlineAlertOpen && isWsConnected}
            >
                <Alert
                    severity={'success'}
                    onClose={() => setIsOnlineAlertOpen(false)}
                >
                    {'You are now online'}
                </Alert>
            </Snackbar>
        </>
    );

    // force to render all open server once
    return (
        <Grid sx={{display: "flex", width: '100vw'}}>
            <Sidebar/>
            <Grid sx={{width: '100%', overflowX: 'auto'}}>
                <ServerTabs/>
                <Grid sx={rootGrid}>
                    <Outlet/>
                    <div id={'servers-render'} style={{...(pathname.includes('server')) ? {height: '100%'} : {display: 'none', height: 0}}}>
                        {allOpenServers}
                    </div>
                    <ToastContainer />
                    {snackbarAlert}
                </Grid>
            </Grid>
        </Grid>
    )
});

export default RootLayout;

export const rootGrid: SxProps<Theme> = {
    overflowY: 'auto',
    height: 'calc(100vh - ' + ContentHeightPx + 'px)',
    marginTop: ServerTabsHeaderMarginPx + 'px',
    marginLeft: SidebarMarginPx + 'px',
    marginRight: SidebarMarginPx + 'px'
}