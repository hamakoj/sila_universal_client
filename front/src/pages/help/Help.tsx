import React from 'react';
import {
    Box, Button, Typography
} from '@mui/material/';
import Divider from "@mui/material/Divider";
import {SxProps} from "@mui/material";

export default function Help(): JSX.Element {
    return (
        <Box>
            <Typography variant="h1" component="h1" gutterBottom>
                Help
            </Typography>
            <Typography variant="h2" component="h2" gutterBottom>
                Useful links
            </Typography>
            <Divider sx={DividerStyle}/>
            <Typography variant="h5" component="h5" gutterBottom>
                SiLA Consortium
            </Typography>
            <Box>
                <Button style={{margin: '0.5rem'}} variant="contained" color="primary" onClick={() => {
                    window.open('https://sila-standard.com/')
                }}>
                    SiLA Consortium Website
                </Button>
                <Button style={{margin: '0.5rem'}} variant="contained" color="primary" onClick={() => {
                    window.open('https://sila-standard.slack.com/join/shared_invite/enQtNDI0ODcxMDg5NzkzLTBhOTU3N2I0NTc4NDcyMjg2ZDIwZDc1Yjg4N2FmYjZkMzljZDAyZjAwNTc5OTVjYjIwZWJjYjA0YTY0NTFiNDA')
                }}>
                    SiLA Consortium Slack
                </Button>
            </Box>
            <Divider sx={DividerStyle}/>
            <Typography variant="h5" component="h5" gutterBottom>
                SiLA 2
            </Typography>
            <Box>
                <Button style={{margin: '0.5rem'}} variant="contained" color="primary" onClick={() => {
                    window.open('https://sila2.gitlab.io/sila_base/')
                }}>
                    SiLA 2 Wiki
                </Button>
                <Button style={{margin: '0.5rem'}} variant="contained" color="primary" onClick={() => {
                    window.open('https://gitlab.com/SiLA2/sila_base')
                }}>
                    SiLA Base repository
                </Button>
            </Box>
            <Divider sx={DividerStyle}/>
            <Typography variant="h5" component="h5" gutterBottom>
                SiLA 2 Specification
            </Typography>
            <Box>
                <Button style={{margin: '0.5rem'}} variant="contained" color="primary" onClick={() => {
                    window.open('https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit')
                }}>
                    Specification Part A
                </Button>
                <Button style={{margin: '0.5rem'}} variant="contained" color="primary" onClick={() => {
                    window.open('https://docs.google.com/document/d/1-shgqdYW4sgYIb5vWZ8xTwCUO_bqE13oBEX8rYY_SJA/edit')
                }}>
                    Specification Part B
                </Button>
                <Button style={{margin: '0.5rem'}} variant="contained" color="primary" onClick={() => {
                    window.open('https://docs.google.com/document/d/1J9gypD6HofLQZ8cPgLWljRuO0V8l5dS22TWQxFy4bhY/edit')
                }}>
                    Specification Part C
                </Button>
            </Box>
            <Divider sx={DividerStyle}/>
            <Typography variant="h5" component="h5" gutterBottom>
                Universal SiLA Client (USC)
            </Typography>
            <Box>
                <Button style={{margin: '0.5rem'}} variant="contained" color="primary" onClick={() => {
                    window.open('https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client')
                }}>
                    USC repository
                </Button>
                <Button style={{margin: '0.5rem'}} variant="contained" color="primary" onClick={() => {
                    window.open('https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/wikis/home')
                }}>
                    USC Wiki
                </Button>
            </Box>
            {
                /*
                <Typography variant="h2" component="h2" gutterBottom>
                        Icon description
                </Typography>
                <Box>
                    <Box>
                        <BookmarkIcon/>
                        Bookmark server
                    </Box>
                    <Box>
                        <ThrashIcon/>
                        Remove server from application
                    </Box>
                    <Box>
                        <Codesandbox/>
                        Server is in simulation mode
                    </Box>
                    <Box>
                        <Lock/>
                        Server is locked
                    </Box>
                    <Box>
                        <Key/>
                        Server requires authentication
                    </Box>
                    <Box>
                        <Clock/>
                        Server has calls in progress
                    </Box>
                </Box>
                 */
            }
        </Box>
    );
}

const DividerStyle: SxProps = {
    marginY: '16px'
}