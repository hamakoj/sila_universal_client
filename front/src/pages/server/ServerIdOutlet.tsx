import {Outlet, useLocation, useParams} from "react-router-dom";
import {useDispatch} from "react-redux";
import {Tab} from "../../store/front/server/open/types";
import {serverCurrentOpenSet} from "../../store/front/server/open/actions";
import React, {useEffect} from "react";

export default function ServerIdOutlet(): JSX.Element {
    const {id} = useParams();
    const {pathname} = useLocation();
    const dispatch = useDispatch();

    useEffect(() => {
        if (id) {
            let tab: Tab = 'info';
            if (pathname.endsWith('property')) {
                tab = 'property';
            } else if (pathname.endsWith('command')) {
                tab = 'command';
            }
            dispatch(serverCurrentOpenSet({
                serverUuid: id,
                tab: tab
            }))
        }
    }, [id, pathname, dispatch])

    return (<Outlet/>);
}