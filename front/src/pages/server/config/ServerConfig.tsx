import React, {useEffect} from 'react';
import {ServerHostPortCertificate, SiLACallModel, UscCall, UscServer} from '../../../generated/silaModels';
import {
    Accordion, AccordionDetails,
    AccordionSummary, Alert,
    Box, Button, Checkbox, Divider,
    FormControlLabel, Grid, InputAdornment, Link, Modal, TextField, Tooltip, TooltipProps,
    Typography,
} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {
    serverFeatureSelection, serverFeatureSelections,
} from "../../../store/front/server/feature/actions";
import {getServerFeaturesSelector} from "../../../store/front/server/feature/selector";
import {ServerFeatureSelectedKeyValue} from "../../../store/front/server/feature/types";
import SplitScreen from "../../../components/common/SplitScreen";
import {styledBadge} from "../../../components/common/ServerLogoBadge";
import HttpsOutlinedIcon from '@mui/icons-material/HttpsOutlined';
import WarningAmberOutlinedIcon from '@mui/icons-material/WarningAmberOutlined';
import CloudOutlinedIcon from '@mui/icons-material/CloudOutlined';
import CloudOffOutlinedIcon from '@mui/icons-material/CloudOffOutlined';
import {SxProps} from "@mui/system";
import {Theme} from "@mui/material/styles";
import Badge from "@mui/material/Badge";
import {urlToHREF} from "../../../utils/url";
import {ProtoCancelAll_Parameters} from "../../../generated/CancelController";
import {getBackEndpoint} from "../../../utils/endpoint";
import {fetchResponseFromCacheRequest} from "../../../store/front/server/response/actions";
import {ProtoSubscribe_ParametersConstraints_Parameters} from "../../../generated/ParameterConstraintsProvider";
import {ProtoSubscribe_PausedCommands_Parameters} from "../../../generated/PauseController";
import AccessToken from "./AccessToken";
import ConnectionConfigurationModal from "./modal/ConnectionConfiguration";
import {AppState} from "../../../store/rootReducer";
import {
    getServerMetaRecoverableErrorsByIdSelector
} from "../../../store/front/server/meta/selector";
import {ProtoSubscribe_RecoverableErrors_Parameters} from "../../../generated/ErrorRecoveryService";
import {ProtoSubscribe_ConfiguredSiLAClients_Parameters} from "../../../generated/ConnectionConfigurationService";
import {ProtoSubscribe_AuthorizationProvider_Parameters} from "../../../generated/AuthorizationConfigurationService";
import {ProtoSubscribe_IsLocked_Parameters} from "../../../generated/LockController";
import LockToken from "./LockToken";
import IconButton from "@mui/material/IconButton";
import EditIcon from '@mui/icons-material/Edit';
import DoneIcon from '@mui/icons-material/Done';
import ClearIcon from '@mui/icons-material/Clear';
import {ProtoSetServerName_Parameters} from "../../../generated/SiLAService";
import {
    fetchCallAffectedByMetadataRequest
} from "../../../store/front/server/meta/actions";
import ErrorRecoveryModal from "./modal/ErrorRecovery";
import SimulationControllerModal from "./modal/SimulationController";

export interface ServerConfigurationContainerProps {
    server: UscServer;
}

const ServerConfigurationContainer = React.memo((props: ServerConfigurationContainerProps): JSX.Element => {
    const selectedServerFeatures = useSelector(getServerFeaturesSelector);
    const recoverableErrors = useSelector((state: AppState) => getServerMetaRecoverableErrorsByIdSelector(state, props.server.serverUuid));
    const dispatch = useDispatch();
    const [openConnectionConfigurationModal, setOpenConnectionConfigurationModal] = React.useState<boolean>(false);
    const [openErrorRecoveryModal, setOpenErrorRecoveryModal] = React.useState<boolean>(false);
    const [openSimulationControllerModal, setOpenSimulationControllerModal] = React.useState<boolean>(false);

    useEffect(() => {
        dispatch(fetchCallAffectedByMetadataRequest({
            serverUuid: props.server.serverUuid
        }))
    }, [dispatch, props.server.serverUuid]);

    useEffect(() => {
        if (props.server.status === "ONLINE" && props.server.features.some(f => f.relativeIdentifier === 'ParameterConstraintsProvider')) {
            fetch(getBackEndpoint('/api/servers/' + props.server.serverUuid + '/features/org.silastandard/core.commands/ParameterConstraintsProvider/v1/calls/ParametersConstraints/running'))
                .then(response => response.json())
                .then((data: string[]) => {
                    if (data.length) {
                        dispatch(fetchResponseFromCacheRequest({
                            callId: data[0]
                        }))
                    } else {
                        const parameters: ProtoSubscribe_ParametersConstraints_Parameters = {}
                        fetch(getBackEndpoint('/api/calls/execute'), {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                serverId: props.server.serverUuid,
                                fullyQualifiedFeatureId: 'org.silastandard/core.commands/ParameterConstraintsProvider/v1',
                                callId: 'ParametersConstraints',
                                type: 'OBSERVABLE_PROPERTY',
                                parameters: JSON.stringify(parameters),
                                metadatas: JSON.stringify({})
                            } as SiLACallModel)
                        })
                        // todo handle fail
                    }
                })
                .catch((error) => {
                    // todo handle
                });
        }
    }, [dispatch, props.server.features, props.server.status, props.server.serverUuid]);

    useEffect(() => {
        if (props.server.status === "ONLINE" && props.server.features.some(f => f.relativeIdentifier === 'PauseController')) {
            fetch(getBackEndpoint('/api/servers/' + props.server.serverUuid + '/features/org.silastandard/core.commands/PauseController/v1/calls/PausedCommands/running'))
                .then(response => response.json())
                .then((data: string[]) => {
                    if (data.length) {
                        dispatch(fetchResponseFromCacheRequest({
                            callId: data[0]
                        }))
                    } else {
                        const parameters: ProtoSubscribe_PausedCommands_Parameters = {}
                        fetch(getBackEndpoint('/api/calls/execute'), {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                serverId: props.server.serverUuid,
                                fullyQualifiedFeatureId: 'org.silastandard/core.commands/PauseController/v1',
                                callId: 'PausedCommands',
                                type: 'OBSERVABLE_PROPERTY',
                                parameters: JSON.stringify(parameters),
                                metadatas: JSON.stringify({}),
                            } as SiLACallModel)
                        })
                        // todo handle fail
                    }
                })
                .catch((error) => {
                    // todo handle
                });
        }
    }, [dispatch, props.server.features, props.server.status, props.server.serverUuid]);

    useEffect(() => {
        if (props.server.status === "ONLINE" && props.server.features.some(f => f.relativeIdentifier === 'AuthorizationConfigurationService')) {
            fetch(getBackEndpoint('/api/servers/' + props.server.serverUuid + '/features/org.silastandard/core/AuthorizationConfigurationService/v1/calls/AuthorizationProvider/running'))
                .then(response => response.json())
                .then((data: string[]) => {
                    if (data.length) {
                        dispatch(fetchResponseFromCacheRequest({
                            callId: data[0]
                        }))
                    } else {
                        const parameters: ProtoSubscribe_AuthorizationProvider_Parameters = {}
                        fetch(getBackEndpoint('/api/calls/execute'), {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                serverId: props.server.serverUuid,
                                fullyQualifiedFeatureId: 'org.silastandard/core/AuthorizationConfigurationService/v1',
                                callId: 'AuthorizationProvider',
                                type: 'OBSERVABLE_PROPERTY',
                                parameters: JSON.stringify(parameters),
                                metadatas: JSON.stringify({})
                            } as SiLACallModel)
                        })
                        // todo handle fail
                    }
                })
                .catch((error) => {
                    // todo handle
                });
        }
    }, [dispatch, props.server.features, props.server.status, props.server.serverUuid]);

    useEffect(() => {
        if (props.server.status === "ONLINE" && props.server.features.some(f => f.relativeIdentifier === 'ConnectionConfigurationService')) {
            fetch(getBackEndpoint('/api/servers/' + props.server.serverUuid + '/features/org.silastandard/core/ConnectionConfigurationService/v1/calls/ConfiguredSiLAClients/running'))
                .then(response => response.json())
                .then((data: string[]) => {
                    if (data.length) {
                        dispatch(fetchResponseFromCacheRequest({
                            callId: data[0]
                        }))
                    } else {
                        const parameters: ProtoSubscribe_ConfiguredSiLAClients_Parameters = {}
                        fetch(getBackEndpoint('/api/calls/execute'), {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                serverId: props.server.serverUuid,
                                fullyQualifiedFeatureId: 'org.silastandard/core/ConnectionConfigurationService/v1',
                                callId: 'ConfiguredSiLAClients',
                                type: 'OBSERVABLE_PROPERTY',
                                parameters: JSON.stringify(parameters),
                                metadatas: JSON.stringify({})
                            } as SiLACallModel)
                        })
                        // todo handle fail
                    }
                })
                .catch((error) => {
                    // todo handle
                });
        }
    }, [dispatch, props.server.features, props.server.status, props.server.serverUuid]);

    useEffect(() => {
        if (props.server.status === "ONLINE" && props.server.features.some(f => f.relativeIdentifier === 'ErrorRecoveryService')) {
            fetch(getBackEndpoint('/api/servers/' + props.server.serverUuid + '/features/org.silastandard/core/ErrorRecoveryService/v1/calls/RecoverableErrors/running'))
                .then(response => response.json())
                .then((data: string[]) => {
                    if (data.length) {
                        dispatch(fetchResponseFromCacheRequest({
                            callId: data[0]
                        }))
                    } else {
                        const parameters: ProtoSubscribe_RecoverableErrors_Parameters = {}
                        fetch(getBackEndpoint('/api/calls/execute'), {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                serverId: props.server.serverUuid,
                                fullyQualifiedFeatureId: 'org.silastandard/core/ErrorRecoveryService/v1',
                                callId: 'RecoverableErrors',
                                type: 'OBSERVABLE_PROPERTY',
                                parameters: JSON.stringify(parameters),
                                metadatas: JSON.stringify({})
                            } as SiLACallModel)
                        })
                        // todo handle fail
                    }
                })
                .catch((error) => {
                    // todo handle
                });
        }
    }, [dispatch, props.server.features, props.server.status, props.server.serverUuid]);

    useEffect(() => {
        if (props.server.status === "ONLINE" && props.server.features.some(f => f.relativeIdentifier === 'LockController')) {
            fetch(getBackEndpoint('/api/servers/' + props.server.serverUuid + '/features/org.silastandard/core/LockController/v1/calls/IsLocked/running'))
                .then(response => response.json())
                .then((data: string[]) => {
                    if (data.length) {
                        dispatch(fetchResponseFromCacheRequest({
                            callId: data[0]
                        }))
                    } else {
                        const parameters: ProtoSubscribe_IsLocked_Parameters = {}
                        fetch(getBackEndpoint('/api/calls/execute'), {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                serverId: props.server.serverUuid,
                                fullyQualifiedFeatureId: 'org.silastandard/core/LockController/v1',
                                callId: 'IsLocked',
                                type: 'OBSERVABLE_PROPERTY',
                                parameters: JSON.stringify(parameters),
                                metadatas: JSON.stringify({})
                            } as SiLACallModel)
                        })
                        // todo handle fail
                    }
                })
                .catch((error) => {
                    // todo handle
                });
        }
    }, [dispatch, props.server.features, props.server.status, props.server.serverUuid]);

    function isSelected(serverUuid: string, featureIdentifier: string): boolean {
        if (selectedServerFeatures && selectedServerFeatures[serverUuid] && selectedServerFeatures[serverUuid][featureIdentifier] !== undefined) {
            return selectedServerFeatures[serverUuid][featureIdentifier];
        }
        return true;
    }

    function setAllFeatureSelection(s: UscServer | undefined, selected: boolean) {
        if (s !== undefined) {
            const serverUuid = s.serverUuid;
            let featureSelections: ServerFeatureSelectedKeyValue = {};
            s.features.forEach((f) => {
                featureSelections = {
                    ...featureSelections,
                    [f.fullyQualifiedIdentifier]: selected
                };
            });
            dispatch(serverFeatureSelections({
                serverUuid: serverUuid,
                featuresSelection: featureSelections
            }));
        }
    }

    const renderServer = (s: UscServer) => {
        const informations = {
            'Status': <Badge
                sx={{...styledBadge(s.status), '& .MuiBadge-badge': {bottom: '50%'}}}
                overlap="circular"
                anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
                variant="dot"
            ><Typography marginRight={3}>{s.status}</Typography></Badge>,
            'Type': s.serverType,
            'Description': (s.description) ? s.description : 'No description',
            ...((s.connectionType === "CLIENT_INITIATED") ? ({'Host': s.serverHost, 'Port': s.port}) : ({})),
            'UUID': s.serverUuid,
            'Connection security': ((s.negotiationType === 'TLS') ?
                <Typography display={"flex"}>Secured <HttpsOutlinedIcon color={'success'}/></Typography> :
                <Typography display={"flex"}>Insecure <WarningAmberOutlinedIcon color={"warning"}/></Typography>),
            'Connection type': ((s.connectionType === 'CLIENT_INITIATED') ?
                <Typography display={"flex"}>Client initiated <CloudOffOutlinedIcon color={'info'}/></Typography> :
                <Typography display={"flex"}>Server initiated <CloudOutlinedIcon color={"success"}/></Typography>),
            'Added on': s.joined,
            'Vendor URL': <Link underline="hover" href={urlToHREF(s.vendorUrl)} target="_blank"
                                rel="noopener">{s.vendorUrl}</Link>,
            'Version': s.version,
            'Certificate': (s.certificateAuthority?.length) ? (<TextField
                variant={'filled'}
                multiline
                maxRows={4}
                onFocus={event => {
                    event.target.select();
                }}
                fullWidth
                InputProps={{
                    readOnly: true,
                }}
                label={'Server Certificate'}
                value={s.certificateAuthority}
            />) : 'None'
        }
        const supportCommandCancel = props.server.features.some((f) => f.relativeIdentifier === "CancelController");
        const supportConnectionConfiguration = props.server.features.some((f) => f.relativeIdentifier === "ConnectionConfigurationService");
        const supportErrorRecoveryService = props.server.features.some((f) => f.relativeIdentifier === "ErrorRecoveryService");
        const supportSimulationController = props.server.features.some((f) => f.relativeIdentifier === "SimulationController");
        const isServerOnline = props.server.status === "ONLINE";

        return (
            <Box key={s.serverUuid}>
                <Box sx={{my: 2}} key={'server-name'}>
                    <EditableServerName name={s.name} serverUuid={s.serverUuid} isOnline={s.status === "ONLINE"}/>
                    <Divider/>
                </Box>
                <Box sx={{my: 2}} key={'server-configuration'}>
                    <Typography style={{flex: 1, marginBottom: 7}} variant={"h5"}>Configuration</Typography>
                    <Divider/>
                    <Grid container spacing={1} marginTop={1} marginBottom={4}>
                        {
                            ((props.server.status === "UNKNOWN" && props.server.connectionType !== "SERVER_INITIATED") && (
                                <Grid item xs={12}>
                                    <Alert severity={'warning'} sx={{maxWidth: '600px'}} action={
                                        <Button sx={{minWidth: '100px'}} color={"warning"} variant={'contained'} size="small" onClick={() => {
                                            fetch(getBackEndpoint('/api/servers'), {
                                                method: 'POST',
                                                headers: {
                                                    'Content-Type': 'application/json',
                                                },
                                                body: JSON.stringify({host: props.server.serverHost, port: props.server.port, certificate: props.server.certificateAuthority} as ServerHostPortCertificate)
                                            })
                                        }}>
                                            Retry connection
                                        </Button>
                                    }>
                                        Server is not reachable
                                    </Alert>
                                </Grid>
                            ))
                        }
                        <LockToken server={props.server}/>
                        <AccessToken server={props.server}/>
                        {
                            ((isServerOnline && supportErrorRecoveryService && recoverableErrors?.RecoverableErrors?.length) && (
                                <Grid item xs={12}>
                                    <Alert severity={'warning'} sx={{maxWidth: '600px'}} action={
                                        <Button sx={{minWidth: '100px'}} color={"warning"} variant={'contained'}
                                                size="small" onClick={() => {
                                            setOpenErrorRecoveryModal(true);
                                        }}>
                                            Recover
                                        </Button>
                                    }>
                                        {recoverableErrors?.RecoverableErrors?.length} command execution errors can be
                                        recovered!
                                    </Alert>
                                </Grid>
                            ))
                        }
                        {
                            ((isServerOnline && supportCommandCancel) && (
                                <Grid item>
                                    <Button variant="contained" color="primary" onClick={(e) => {
                                        const parameters: ProtoCancelAll_Parameters = {}
                                        // todo handle loading and error
                                        fetch(getBackEndpoint('/api/calls/execute'), {
                                            method: 'POST',
                                            headers: {
                                                'Content-Type': 'application/json',
                                            },
                                            body: JSON.stringify({
                                                serverId: props.server.serverUuid,
                                                fullyQualifiedFeatureId: 'org.silastandard/core.commands/CancelController/v1',
                                                callId: 'CancelAll',
                                                type: 'UNOBSERVABLE_COMMAND',
                                                parameters: JSON.stringify(parameters),
                                                metadatas: JSON.stringify({}),
                                                runAsync: false
                                            } as SiLACallModel)
                                        })
                                    }}>
                                        Cancel all commands
                                    </Button>
                                </Grid>
                            ))
                        }
                        {
                            ((isServerOnline && supportConnectionConfiguration) && (
                                <Grid item>
                                    <Button variant="contained" color="primary" onClick={(e) => {
                                        setOpenConnectionConfigurationModal(true);
                                    }}>
                                        Configure connection
                                    </Button>
                                </Grid>
                            ))
                        }
                        {
                            ((isServerOnline && supportSimulationController) && (
                                <Grid item>
                                    <Button variant="contained" color="primary" onClick={(e) => {
                                        setOpenSimulationControllerModal(true);
                                    }}>
                                        Simulation controller
                                    </Button>
                                </Grid>
                            ))
                        }
                    </Grid>
                </Box>
                <Box sx={{my: 2}} key={'server-information'}>
                    <Typography style={{flex: 1, marginBottom: 7}} variant={"h5"}>Information</Typography>
                    <Divider/>
                    <Grid container rowSpacing={{xs: 2, sm: 1}} alignItems={'baseline'} maxWidth={'1000px'}>
                        {
                            Object.entries(informations).map(([k, v]) => (
                                <React.Fragment key={k}>
                                    <Grid item xs={12} sm={4} sx={{'&.MuiGrid-item ': {maxWidth: '200px'}}}>
                                        <Typography variant={"h6"}>{k}</Typography>
                                    </Grid>
                                    <Grid item xs={12} sm={8} paddingLeft={{xs: 0, sm: 1, md: 2, lg: 4}} sx={{'&.MuiGrid-item ': {paddingTop: '0px'}}}>
                                        <Box>{v}</Box>
                                    </Grid>
                                </React.Fragment>
                            ))
                        }
                    </Grid>
                </Box>
            </Box>
        );
    }

    return (
        <>
            <SplitScreen leftName={'Configuration'} rightName={'Feature'} leftComponent={
                props.server !== undefined ? renderServer(props.server) : (
                    <Typography>'No server to display'</Typography>)
            } rightComponent={
                <Box>
                    <Typography variant={"h5"}>Selected features</Typography>
                    <Grid container spacing={1} marginTop={1} marginBottom={4}>
                        <Grid item>
                            <Button variant="contained" color="primary" onClick={() => {
                                setAllFeatureSelection(props.server, true);
                            }}>
                                Select all
                            </Button>
                        </Grid>
                        <Grid item>
                            <Button variant="contained" color="primary" onClick={() => {
                                setAllFeatureSelection(props.server, false);
                            }}>
                                Select none
                            </Button>
                        </Grid>
                    </Grid>
                    <Grid container direction={'column'} spacing={1}>
                        {
                            props.server?.features
                                .sort((a, b) => a.displayName.localeCompare(b.displayName))
                                .map((f) => {
                                    return (
                                        <Grid item key={f.fullyQualifiedIdentifier + '/checkbox'}>
                                            <FormControlLabel
                                                sx={styledFormControlLabel}
                                                control={
                                                    <Checkbox
                                                        color="primary"
                                                        inputProps={{'aria-label': 'secondary checkbox'}}
                                                        checked={isSelected(props.server?.serverUuid, f.fullyQualifiedIdentifier)}
                                                        onChange={(e) => {
                                                            dispatch(serverFeatureSelection({
                                                                serverUuid: props.server?.serverUuid,
                                                                featureIdentifier: f.fullyQualifiedIdentifier,
                                                                selected: e.target.checked
                                                            }))
                                                        }}
                                                    />
                                                }
                                                label={
                                                    <Accordion sx={{color: 'text.secondary'}} onClick={(e) => {
                                                        e.preventDefault();
                                                    }}>
                                                        <AccordionSummary expandIcon={<ExpandMoreIcon/>} sx={{
                                                            backgroundColor: "initial",
                                                            color: 'text.secondary'
                                                        }}>
                                                            <Typography>{f.displayName}</Typography>
                                                        </AccordionSummary>
                                                        <AccordionDetails sx={{color: 'text.secondary'}}>
                                                            <Typography>
                                                                {f.description}
                                                            </Typography>
                                                        </AccordionDetails>
                                                    </Accordion>
                                                }
                                            />
                                        </Grid>
                                    );
                                })
                        }
                    </Grid>
                </Box>
            }/>
            <Modal
                key={'connection-configuration-modal'}
                id={'connection-configuration-modal'}
                open={openConnectionConfigurationModal}
                onClose={() => setOpenConnectionConfigurationModal(false)}
            >
                <div>
                    <ConnectionConfigurationModal
                        handleClose={() => setOpenConnectionConfigurationModal(false)}
                        server={props.server}
                    />
                </div>
            </Modal>
            <Modal
                key={'error-recovery-modal'}
                id={'error-recovery-modal'}
                open={openErrorRecoveryModal}
                onClose={() => setOpenErrorRecoveryModal(false)}
            >
                <div>
                    <ErrorRecoveryModal
                        handleClose={() => setOpenErrorRecoveryModal(false)}
                        server={props.server}
                        recoverableErrors={recoverableErrors}
                    />
                </div>
            </Modal>
            <Modal
                key={'simulation-controller-modal'}
                id={'simulation-controller-modal'}
                open={openSimulationControllerModal}
                onClose={() => setOpenSimulationControllerModal(false)}
            >
                <div>
                    <SimulationControllerModal
                        handleClose={() => setOpenSimulationControllerModal(false)}
                        serverUuid={props.server.serverUuid}
                        isCurrentServerOnline={props.server.status === "ONLINE"}
                    />
                </div>
            </Modal>
        </>
    );
});

export default ServerConfigurationContainer;

const styledFormControlLabel: SxProps<Theme> = {
    '& .MuiTypography-root': {
        width: '100%',
    },
    width: '100%',
};

export interface EditableServerNameProps {
    serverUuid: string;
    name: string;
    isOnline: boolean;
}

export interface EditableServerNameRequest {
    loading: boolean;
    error?: string;
}

export function EditableServerName(props: EditableServerNameProps) {
    const [isEditing, setIsEditing] = React.useState<boolean>(false);
    const [newName, setNewName] = React.useState<string>(props.name);
    const [request, setRequest] = React.useState<EditableServerNameRequest>({loading: false});

    function changeName() {
        const parameters: ProtoSetServerName_Parameters = {
            ServerName: {
                value: newName
            }
        }
        fetch(getBackEndpoint('/api/calls/execute'), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                serverId: props.serverUuid,
                fullyQualifiedFeatureId: 'org.silastandard/core/SiLAService/v1',
                callId: 'SetServerName',
                type: 'UNOBSERVABLE_COMMAND',
                parameters: JSON.stringify(parameters),
                metadatas: JSON.stringify({}),
                runAsync: false
            } as SiLACallModel)
        }).then(response => response.json()).then((response: UscCall) => {
            if (!response.error) {
                setRequest({
                    loading: false,
                    error: undefined
                })
                setIsEditing(false)
            } else {
                setRequest({
                    loading: false,
                    error: response.result || response.error as any || 'Unknown error'
                })
            }
        }).catch((error) => {
            setRequest({
                loading: false,
                error: error
            })
        })
    }

    if (isEditing) {
        return (
            <>
                {((!request.loading && request.error) && (
                    <Alert severity={"warning"}>{request.error}</Alert>
                ))}
                <TextField
                    fullWidth={false}
                    variant={"filled"}
                    value={newName}
                    label={'Server name'}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <Tooltip {...tooltipProps} title={'Cancel'}>
                                    <IconButton
                                        sx={{margin: 1}}
                                        aria-label="Cancel"
                                        disabled={request.loading}
                                        onClick={() => {
                                            setIsEditing(false)
                                        }}
                                        onMouseDown={(event) => {
                                            event.preventDefault();
                                        }}
                                        edge="end"
                                    >
                                        <ClearIcon/>
                                    </IconButton>
                                </Tooltip>
                                <Tooltip {...tooltipProps} title={'Validate'}>
                                    <IconButton
                                        aria-label="Validate"
                                        disabled={!props.isOnline || request.loading || newName.length < 1 || newName.length > 64}
                                        onClick={() => {
                                            changeName();
                                        }}
                                        onMouseDown={(event) => {
                                            event.preventDefault();
                                        }}
                                        edge="end"
                                    >
                                        <DoneIcon/>
                                    </IconButton>
                                </Tooltip>
                            </InputAdornment>
                        )
                    }}
                    onChange={(e) => setNewName(e.target.value)}
                />
            </>
        )
    }
    return (
        <Box display={'flex'}>
            <Typography variant={"h4"}>{props.name}</Typography>
            <Tooltip {...tooltipProps} title={'Edit server name'}>
                <IconButton
                    aria-label="Edit server name"
                    disabled={!props.isOnline}
                    onClick={() => {
                        setIsEditing(true)
                    }}
                    onMouseDown={(event) => {
                        event.preventDefault();
                    }}
                    edge="end"
                >
                    <EditIcon/>
                </IconButton>
            </Tooltip>
        </Box>
    )
}

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};