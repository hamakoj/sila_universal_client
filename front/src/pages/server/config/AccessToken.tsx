import React, {useEffect, useState} from "react";
import {DateTime} from "luxon";
import {isAfter} from "../../../utils/datetime";
import {SiLACallModel, SiLACallResult, UscServer} from "../../../generated/silaModels";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../../store/rootReducer";
import {getAppUserAccessTokenForServerIdSelector} from "../../../store/front/app/user/selector";
import {Alert, Button, Grid, InputAdornment, Modal, TextField} from "@mui/material";
import {ProtoLogout_Parameters} from "../../../generated/AuthenticationService";
import {getBackEndpoint} from "../../../utils/endpoint";
import LoginModal from "./modal/LoginModal";
import {removeUserAccessToken, setExpiredUserAccessToken} from "../../../store/front/app/user/actions";
import IconButton from "@mui/material/IconButton";
import {Visibility, VisibilityOff} from "@mui/icons-material";
import {getServerMetaAuthorizationProvidersByIdSelector} from "../../../store/front/server/meta/selector";
import {ServerKeyValue} from "../../../store/sila/manager/server/types";
import {getServersSelector} from "../../../store/sila/manager/server/selector";
import AuthorizationProviderModal from "./modal/AuthorizationProviderModal";

export interface AccessTokenProps {
    server: UscServer;
}

export const AccessToken = React.memo((props: AccessTokenProps): JSX.Element => {
    const dispatch = useDispatch();
    const authorizationProvider = useSelector((state: AppState) => getServerMetaAuthorizationProvidersByIdSelector(state, props.server.serverUuid));
    const authorizationProviderServerUuid = (authorizationProvider?.AuthorizationProvider) ? authorizationProvider?.AuthorizationProvider.value : props.server.serverUuid;
    const userAccessToken = useSelector((state: AppState) => getAppUserAccessTokenForServerIdSelector(state, props.server.serverUuid));
    const [openLoginModal, setOpenLoginModal] = React.useState<boolean>(false);
    const [openAuthorizationProviderModal, setOpenAuthorizationProviderModal] = React.useState<boolean>(false);
    const [timeDeltaString, setTimeDeltaString] = useState<string>('');
    const [showAccessToken, setShowAccessToken] = useState<boolean>(false);
    const expirationUTC = userAccessToken?.expirationUTC ? DateTime.fromISO(userAccessToken.expirationUTC) : undefined;
    const hasValidToken = (userAccessToken && expirationUTC && isAfter(expirationUTC, DateTime.now().toUTC(), 'second'))
    const hasExpiredToken = (userAccessToken && (!expirationUTC || !isAfter(expirationUTC, DateTime.now().toUTC(), 'second')))
    ////
    const availableServers: ServerKeyValue = useSelector(getServersSelector);
    const isCurrentServerOnline = props.server.status === "ONLINE";
    const useCustomAuthorizationProvider = props.server.features.some(f => f.relativeIdentifier === 'AuthorizationConfigurationService');
    const serverUuidUsedForAuth: string | undefined = (useCustomAuthorizationProvider) ? authorizationProvider?.AuthorizationProvider?.value : props.server.serverUuid;
    const serverUsedForAuth: UscServer | undefined = serverUuidUsedForAuth ? availableServers[serverUuidUsedForAuth] : undefined;
    const hasAuthorizationProviderAuthenticationService = !!serverUsedForAuth?.features.some(f => f.relativeIdentifier === 'AuthenticationService');
    const isAuthorizationProviderPresentAndOnline = !!(serverUsedForAuth && hasAuthorizationProviderAuthenticationService && serverUsedForAuth.status === "ONLINE");
    const isUserTokenServerAuthorizerPresentAndOnline = userAccessToken?.authorizedByServerUuid && availableServers[userAccessToken?.authorizedByServerUuid]?.status === "ONLINE";

    useEffect(() => {
        const nowUtc = DateTime.now().toUTC();
        if (!userAccessToken || !expirationUTC || !isAfter(expirationUTC, nowUtc, 'second')) {
            if (userAccessToken && expirationUTC) {
                dispatch(setExpiredUserAccessToken({
                    serverUuid: props.server.serverUuid,
                    token: userAccessToken.token
                }))
            }
            return;
        }
        const deltaSeconds = expirationUTC.diff(nowUtc, 'second').as('seconds');
        const delay = (deltaSeconds >= 120) ? (60000) : (deltaSeconds >= 60 ? (deltaSeconds % 60 * 1000) : 1000);
        const callback = () => {
            // + 1 just to re-render to display that the token has expired
            if (isAfter(expirationUTC, DateTime.now().minus({second: 1}).toUTC(), 'second')) {
                setTimeDeltaString(expirationUTC?.toLocal().toRelative() || '')
            } else {
                setTimeDeltaString('')
            }
        };
        // in case it the first update with a delay longer than 1s
        if (delay > 1000) {
            callback();
        }
        const timeout = setTimeout(callback, delay);
        return () => {
            clearTimeout(timeout);
        }
    }, [userAccessToken, expirationUTC, dispatch, props.server.serverUuid]);

    return (
        <>
            <InvalidAuthorizationProviderAlert
                isAuthorizationProviderPresentAndOnline={isAuthorizationProviderPresentAndOnline}
                serverUsedForAuth={serverUsedForAuth}
                serverUuidUsedForAuth={serverUuidUsedForAuth}
                hasAuthorizationProviderAuthenticationService={hasAuthorizationProviderAuthenticationService}
                useCustomAuthorizationProvider={useCustomAuthorizationProvider}
                isCurrentServerOnline={isCurrentServerOnline}
            />
            {
                ((userAccessToken) && ((hasExpiredToken || !hasValidToken) ? (
                    <Grid item xs={12} lg={10}>
                        <Alert
                            onClose={() => {
                                dispatch(removeUserAccessToken({serverUuid: props.server.serverUuid}))
                            }}
                            severity={'warning'}>
                            Your authenticated session has expired!
                        </Alert>
                    </Grid>
                ) : (
                    <Grid item xs={12} lg={10}>
                        <Alert severity={'info'}>Your authenticated session will expire {timeDeltaString}.</Alert>
                    </Grid>
                )))
            }
            {
                ((useCustomAuthorizationProvider && hasValidToken && authorizationProviderServerUuid !== userAccessToken?.authorizedByServerUuid) && (
                    <Grid item xs={12} lg={10}>
                        <Alert severity={'warning'}>
                            Your session token has been issued by a different server than the one currently
                            specified by the Authorization Provider Configuration.
                            It is advised to logout and login using the current Authorization Provider.
                        </Alert>
                    </Grid>
                ))
            }
            {
                ((hasValidToken && !isUserTokenServerAuthorizerPresentAndOnline) && (
                    <Grid item xs={12}>
                        <Alert severity={'warning'} sx={{maxWidth: '600px'}} action={
                            <Button sx={{minWidth: '100px'}} color={"warning"} variant={'contained'} size="small" onClick={() => {
                                dispatch(removeUserAccessToken({
                                    serverUuid: props.server.serverUuid
                                }));
                            }}>
                                Logout locally
                            </Button>
                        }>
                            The server that provided the token has been removed from the Universal SiLA Client or is offline.
                            Please add it back or make it online in order to properly logout.
                            You can also logout locally without notifying the server.
                        </Alert>
                    </Grid>
                ))
            }
            {
                ((isCurrentServerOnline && useCustomAuthorizationProvider) && (
                    <Grid item xs={12} lg={10}>
                        <Button variant="contained" color="primary"
                                onClick={() => setOpenAuthorizationProviderModal(true)}>
                            Change authorization provider
                        </Button>
                    </Grid>
                ))
            }
            {
                (isAuthorizationProviderPresentAndOnline && (hasExpiredToken || !hasValidToken) && (
                    <Grid item xs={12} lg={10}>
                        <Button variant="contained" color="primary" onClick={() => setOpenLoginModal(true)}>
                            Login
                        </Button>
                    </Grid>
                ))
            }
            {
                ((hasValidToken && expirationUTC) && (
                    <>
                        <Grid item xs={12} sm={8}>
                            <TextField
                                id={'access-token-form'}
                                key={'access-token-form'}
                                fullWidth={true}
                                type={showAccessToken ? 'text' : 'password'}
                                value={userAccessToken?.token || ''}
                                label={'Access token'}
                                helperText={'The access token to provide to use restricted features'}
                                InputProps={{
                                    readOnly: true,
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={() => {
                                                    setShowAccessToken((prev) => (!prev))
                                                }}
                                                onMouseDown={(event) => {
                                                    event.preventDefault();
                                                }}
                                                edge="end"
                                            >
                                                {showAccessToken ? <VisibilityOff/> : <Visibility/>}
                                            </IconButton>
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>
                        <Grid item xs={4} sm={3}>
                            <Button
                                disabled={!isUserTokenServerAuthorizerPresentAndOnline}
                                variant="contained"
                                color="primary"
                                onClick={(e) => {
                                    const parameters: ProtoLogout_Parameters = {
                                        AccessToken: {value: userAccessToken?.token || ''}
                                    }
                                    fetch(getBackEndpoint('/api/runtime/calls/execute'), {
                                        method: 'POST',
                                        headers: {
                                            'Content-Type': 'application/json',
                                        },
                                        body: JSON.stringify({
                                            serverId: userAccessToken?.authorizedByServerUuid,
                                            fullyQualifiedFeatureId: 'org.silastandard/core/AuthenticationService/v1',
                                            callId: 'Logout',
                                            type: 'UNOBSERVABLE_COMMAND',
                                            parameters: JSON.stringify(parameters),
                                            metadatas: JSON.stringify({}),
                                            runAsync: false
                                        } as SiLACallModel)
                                    }).then(response => response.json()).then((response: SiLACallResult) => {
                                        if (response.success) {
                                            dispatch(removeUserAccessToken({
                                                serverUuid: props.server.serverUuid
                                            }));
                                        } else {
                                            // todo handle
                                        }
                                    }).catch((error) => {
                                        // todo handle
                                    })
                                }}>
                                Logout
                            </Button>
                        </Grid>
                    </>
                ))
            }
            <Modal
                key={'authorization-provider-modal'}
                id={'authorization-provider-modal'}
                open={openAuthorizationProviderModal}
                onClose={() => setOpenAuthorizationProviderModal(false)}
            >
                <div>
                    <AuthorizationProviderModal
                        handleClose={() => setOpenAuthorizationProviderModal(false)}
                        server={props.server}
                        availableServers={availableServers}
                        isAuthorizationProviderPresentAndOnline={isAuthorizationProviderPresentAndOnline}
                        serverUsedForAuth={serverUsedForAuth}
                        serverUuidUsedForAuth={serverUuidUsedForAuth}
                        hasAuthorizationProviderAuthenticationService={hasAuthorizationProviderAuthenticationService}
                        useCustomAuthorizationProvider={useCustomAuthorizationProvider}
                        isCurrentServerOnline={isCurrentServerOnline}
                    />
                </div>
            </Modal>
            <Modal
                key={'authentication-login-modal'}
                id={'authentication-login-modal'}
                open={openLoginModal}
                onClose={() => setOpenLoginModal(false)}
            >
                <div>
                    <LoginModal
                        handleClose={() => setOpenLoginModal(false)}
                        server={props.server}
                        isAuthorizationProviderPresentAndOnline={isAuthorizationProviderPresentAndOnline}
                        serverUsedForAuth={serverUsedForAuth}
                        serverUuidUsedForAuth={serverUuidUsedForAuth}
                        hasAuthorizationProviderAuthenticationService={hasAuthorizationProviderAuthenticationService}
                        useCustomAuthorizationProvider={useCustomAuthorizationProvider}
                        isCurrentServerOnline={isCurrentServerOnline}
                    />
                </div>
            </Modal>
        </>
    )
});

export default AccessToken;

export interface InvalidAuthorizationProviderAlertProps {
    isAuthorizationProviderPresentAndOnline: boolean;
    hasAuthorizationProviderAuthenticationService: boolean;
    useCustomAuthorizationProvider: boolean;
    isCurrentServerOnline: boolean;
    serverUuidUsedForAuth?: string;
    serverUsedForAuth?: UscServer;
}

export function InvalidAuthorizationProviderAlert(props: InvalidAuthorizationProviderAlertProps): JSX.Element {
    const alerts = [];
    if (!props.isCurrentServerOnline) {
        return <></>;
    }
    if (props.isAuthorizationProviderPresentAndOnline && props.serverUsedForAuth) {
        alerts.push(
            <Grid item xs={12} lg={10} key={'using-server-alert'}>
                <Alert severity={'info'}>
                    Using server "{props.serverUsedForAuth.name}"
                    ({props.serverUsedForAuth.serverUuid})
                    as authorization provider
                </Alert>
            </Grid>
        );
    }
    if (props.useCustomAuthorizationProvider) {
        if (!props.serverUuidUsedForAuth) {
            alerts.push(
                <Grid item xs={12} lg={10}  key={'missing-server-for-auth-alert'}>
                    <Alert severity={'error'}>
                        This server uses the feature "Authorization Configuration Service"
                        but does not specify which server to use for authentication!
                    </Alert>
                </Grid>
            )
        } else if (!props.isAuthorizationProviderPresentAndOnline) {
            if (props.serverUsedForAuth?.status === "ONLINE") {
                alerts.push(
                    <Grid item xs={12} lg={10} key={'missing-feature-alert'}>
                        <Alert severity={'error'}>
                            Server {props.serverUsedForAuth?.name} with UUID {props.serverUuidUsedForAuth}
                            to use as authorization provider is invalid because it does not expose the feature
                            "Authentication Service"!
                        </Alert>
                    </Grid>
                )
            } else if (props.serverUsedForAuth) {
                alerts.push(
                    <Grid item xs={12} lg={10} key={'server-offline-alert'}>
                        <Alert severity={'error'}>
                            Server {props.serverUsedForAuth?.name} with UUID {props.serverUuidUsedForAuth}
                            to use as authorization provider is not online. Turn it online or try to re-add it.
                        </Alert>
                    </Grid>
                )
            } else {
                alerts.push(
                    <Grid item xs={12} lg={10} key={'missing-authorization-server'}>
                        <Alert severity={'error'}>
                            Server with UUID {props.serverUuidUsedForAuth}
                            to use as authorization provider is not present
                            in the Universal SiLA Client or offline. Please add it or change the authorization provider!
                        </Alert>
                    </Grid>
                )
            }
        }
    } else if (!props.isAuthorizationProviderPresentAndOnline) {
        if (props.isCurrentServerOnline) {
            // server does not support authentication
        } else if (props.hasAuthorizationProviderAuthenticationService) {
            alerts.push(
                <Grid item xs={12} lg={10} key={'this-server-offline-alert'}>
                    <Alert severity={'error'}>
                        This server needs to be online to authenticate!
                    </Alert>
                </Grid>
            )
        }
    }

    return (
        <>
            {
                alerts
            }
        </>
    )
}