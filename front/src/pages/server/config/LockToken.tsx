import React, {useEffect, useState} from "react";
import {DateTime} from "luxon";
import {isAfter} from "../../../utils/datetime";
import {UscServer} from "../../../generated/silaModels";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../../store/rootReducer";
import {
    getAppUserLockTokenForServerIdSelector
} from "../../../store/front/app/user/selector";
import {Alert, Button, Grid, Modal} from "@mui/material";
import {
    removeUserLockToken,
    setExpiredUserLockToken
} from "../../../store/front/app/user/actions";
import {
    getServerMetaIsLockedByIdSelector
} from "../../../store/front/server/meta/selector";
import LockControllerModal from "./modal/LockControllerModal";

export interface LockTokenProps {
    server: UscServer;
}

export const LockToken = React.memo((props: LockTokenProps): JSX.Element => {
    const dispatch = useDispatch();
    const [openLockControllerModal, setOpenLockControllerModal] = React.useState<boolean>(false);
    const userLockToken = useSelector((state: AppState) => getAppUserLockTokenForServerIdSelector(state, props.server.serverUuid));
    const isLocked = useSelector((state: AppState) => getServerMetaIsLockedByIdSelector(state, props.server.serverUuid));
    const supportLock = props.server.features.some((f) => f.relativeIdentifier === "LockController");
    const isServerOnline = props.server.status === "ONLINE";
    const expirationUTC = userLockToken?.expirationUTC ? DateTime.fromISO(userLockToken.expirationUTC) : undefined;
    const hasExpiredToken = (userLockToken && (!isLocked || !expirationUTC || !isAfter(expirationUTC, DateTime.now().toUTC(), 'second')))
    const hasValidToken = (userLockToken && isLocked && expirationUTC && isAfter(expirationUTC, DateTime.now().toUTC(), 'second'))
    const [timeDeltaString, setTimeDeltaString] = useState<string>('');

    useEffect(() => {
        const nowUtc = DateTime.now().toUTC();
        if (!userLockToken || !expirationUTC || !isAfter(expirationUTC, nowUtc, 'second')) {
            if (userLockToken && expirationUTC) {
                dispatch(setExpiredUserLockToken({
                    serverUuid: props.server.serverUuid,
                    token: userLockToken.token
                }))
            }
            return;
        }
        const deltaSeconds = expirationUTC.diff(nowUtc, 'second').as('seconds');
        const delay = (deltaSeconds >= 120) ? (60000) : (deltaSeconds >= 60 ? (deltaSeconds % 60 * 1000) : 1000);
        const callback = () => {
            // + 1 just to re-render to display that the token has expired
            if (isAfter(expirationUTC, DateTime.now().minus({second: 1}).toUTC(), 'second')) {
                setTimeDeltaString(expirationUTC?.toLocal().toRelative() || '')
            } else {
                setTimeDeltaString('')
            }
        };
        // in case it the first update with a delay longer than 1s
        if (delay > 1000) {
            callback();
        }
        const timeout = setTimeout(callback, delay);
        return () => {
            clearTimeout(timeout);
        }
    }, [userLockToken, expirationUTC, dispatch, props.server.serverUuid]);

    return (
        <>
            {
                ((supportLock && isLocked?.IsLocked?.value) && (
                    <Grid item xs={12} lg={10}>
                        <Alert severity={'warning'}>
                            Server is locked!
                        </Alert>
                    </Grid>
                ))
            }
            {
                ((userLockToken) && ((hasExpiredToken || !hasValidToken) ? (
                    <Grid item xs={12} lg={10}>
                        <Alert
                            onClose={() => {
                                dispatch(removeUserLockToken({serverUuid: props.server.serverUuid}))
                            }}
                            severity={'warning'}>
                            Your server lock token has expired!
                        </Alert>
                    </Grid>
                ) : (
                    <Grid item xs={12} lg={10}>
                        <Alert severity={'info'}>Your server lock token will expire {timeDeltaString}.</Alert>
                    </Grid>
                )))
            }
            {
                ((isServerOnline && supportLock) && (
                    <Grid item xs={4}>
                        <Button variant="contained" color="primary" onClick={(e) => {
                            setOpenLockControllerModal(true);
                        }}>
                            Lock controller
                        </Button>
                    </Grid>
                ))
            }
            <Modal
                key={'lock-controller-modal'}
                id={'lock-controller-modal'}
                open={openLockControllerModal}
                onClose={() => setOpenLockControllerModal(false)}
            >
                <div>
                    <LockControllerModal
                        isLocked={isLocked?.IsLocked?.value === true}
                        handleClose={() => setOpenLockControllerModal(false)}
                        server={props.server}
                    />
                </div>
            </Modal>
        </>
    )
});

export default LockToken;