import {
    Accordion, AccordionDetails,
    Alert, Button,
    Grid, TextField,
    Typography,
} from "@mui/material";
import React, {useEffect} from "react";
import CloseableModal from "../../../../components/common/CloseableModal";
import {
    SiLACallModel,
    SiLAJavaDataTypeType, UscCall, UscServer
} from "../../../../generated/silaModels";
import {AccordionSummary, Paper} from "@mui/material/";
import {
    ProtoAbortErrorHandling_Parameters,
    ProtoContinuationOption_Struct,
    ProtoDataType_RecoverableError, ProtoExecuteContinuationOption_Parameters, ProtoSetErrorHandlingTimeout_Parameters,
    ProtoSubscribe_RecoverableErrors_Responses
} from "../../../../generated/ErrorRecoveryService";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import SiLAXMLParser from "../../../../utils/parser/siLAXMLParser";
import DataTypeForm from "../../../../components/server/call/dataTypeForm/DataTypeFormRoot";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../../../store/rootReducer";
import {getCallInputParametersByIdSelector} from "../../../../store/front/server/callInput/selector";
import {callInputInit} from "../../../../store/front/server/callInput/actions";
import {
    getCommandParameterInputStateFromDataType
} from "../../../../components/server/call/dataTypeForm/silaToState";
import StarIcon from '@mui/icons-material/Star';
import {
    convertGrpcJsonToProtoAny,
    parametersStateToGrpcJsonFromDataType
} from "../../../../components/server/call/dataTypeForm/stateToGrpcJson";
import {getBackEndpoint} from "../../../../utils/endpoint";


export interface ErrorRecoveryModalProps {
    handleClose: () => void;
    server: UscServer;
    recoverableErrors: ProtoSubscribe_RecoverableErrors_Responses | undefined;
}

export interface SetErrorHandlingTimeout {
    timeout: number;
}

export interface ErrorRecoveryFormRequest {
    loading: boolean;
}

export default function ErrorRecoveryModal(props: ErrorRecoveryModalProps): JSX.Element {
    const [setErrorHandlingTimeoutForm, setSetErrorHandlingTimeoutForm] = React.useState<SetErrorHandlingTimeout>({timeout: 0});
    const [formRequest, setFormRequest] = React.useState<ErrorRecoveryFormRequest>({loading: false});
    const [abortRecoveryAlert, setAbortRecoveryAlert] = React.useState<string | null>(null);
    const [setTimeoutAlert, setSetTimeoutAlert] = React.useState<string | null>(null);
    const isServerOnline = props.server.status === "ONLINE";

    return (
        <CloseableModal title={'Error Recovery'} handleClose={props.handleClose}>
            <>
                <Grid container spacing={1} rowSpacing={2} marginTop={1} marginBottom={4} flexDirection={'column'}
                      alignContent={'space-around'}>
                    <Grid item xs={12}>
                        <Typography variant={'h4'}>Error handling timeout</Typography>
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            id={'set-error-handling-timeout-form'}
                            key={'set-error-handling-timeout-form'}
                            fullWidth={true}
                            value={setErrorHandlingTimeoutForm.timeout}
                            label={'Timeout in seconds'}
                            helperText="Timeout in seconds, a timeout of 0 means no timeout"
                            type={"number"}
                            onChange={(e) => setSetErrorHandlingTimeoutForm((prev) => ({
                                ...prev,
                                timeout: parseInt(e.target.value)
                            }))}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        {
                            (setTimeoutAlert !== null && (
                                <Alert severity="warning" onClose={() => setSetTimeoutAlert(null)}>{setTimeoutAlert}</Alert>
                            ))
                        }
                        <Button disabled={!isServerOnline || formRequest.loading}
                                variant="contained"
                                color="primary" onClick={(e) => {
                            setFormRequest({loading: true})
                            const parameters: ProtoSetErrorHandlingTimeout_Parameters = {
                                ErrorHandlingTimeout: {
                                    value: setErrorHandlingTimeoutForm.timeout
                                }
                            }
                            fetch(getBackEndpoint('/api/calls/execute'), {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify({
                                    serverId: props.server.serverUuid,
                                    fullyQualifiedFeatureId: 'org.silastandard/core/ErrorRecoveryService/v1',
                                    callId: 'SetErrorHandlingTimeout',
                                    type: 'UNOBSERVABLE_COMMAND',
                                    parameters: JSON.stringify(parameters),
                                    metadatas: JSON.stringify({}),
                                    runAsync: false
                                } as SiLACallModel)
                            }).then(response => response.json()).then((response: UscCall) => {
                                if (!response.error) {
                                    setFormRequest({
                                        loading: false,
                                    })
                                    return null;
                                } else {
                                    setFormRequest({
                                        loading: false,
                                    })
                                    return response.result || response.error;
                                }
                            }).catch((error) => {
                                setFormRequest({
                                    loading: false,
                                })
                                return error;
                            }).then((alert) => setSetTimeoutAlert(alert))
                        }}>
                            Set error handling timeout
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant={'h4'}>Recoverable errors</Typography>
                    </Grid>
                    {
                        (!isServerOnline && (
                            <Grid item xs={8}>
                                <Alert severity="warning">Server is offline!</Alert>
                            </Grid>
                        ))
                    }
                    <Grid item xs={8}>
                        {props.recoverableErrors?.RecoverableErrors?.map((recoverableError, index) => {
                            return (
                                <Paper elevation={4} key={index}>
                                    <Grid container paddingX={2} spacing={1} rowSpacing={2} marginTop={1}
                                          paddingBottom={2} marginBottom={4}>
                                        <Grid item xs={12}>
                                            <Typography variant={"h5"}>Error {index + 1}</Typography>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                fullWidth
                                                InputLabelProps={{shrink: true}} InputProps={{readOnly: true}}
                                                label={'Command Identifier'}
                                                value={recoverableError.RecoverableError?.CommandIdentifier?.value || ''}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                fullWidth
                                                InputLabelProps={{shrink: true}} InputProps={{readOnly: true}}
                                                label={'Command Execution UUID'}
                                                value={recoverableError.RecoverableError?.CommandExecutionUUID?.value || ''}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                fullWidth
                                                InputLabelProps={{shrink: true}} InputProps={{readOnly: true}}
                                                label={'Error Message'}
                                                value={recoverableError.RecoverableError?.ErrorMessage?.value || ''}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                fullWidth
                                                InputLabelProps={{shrink: true}} InputProps={{readOnly: true}}
                                                label={'Automatic Execution Timeout'}
                                                value={recoverableError.RecoverableError?.AutomaticExecutionTimeout?.value || ''}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                fullWidth
                                                InputLabelProps={{shrink: true}} InputProps={{readOnly: true}}
                                                label={'Default Option'}
                                                value={recoverableError.RecoverableError?.DefaultOption?.value || ''}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography variant={"h6"}>Continuation options</Typography>
                                        </Grid>
                                        <Grid item xs={12}>
                                            {
                                                recoverableError.RecoverableError?.ContinuationOptions?.map(option => option?.ContinuationOption).map((option) => (
                                                    <Accordion
                                                        defaultExpanded={recoverableError.RecoverableError?.DefaultOption?.value === option?.Identifier?.value}
                                                        key={'continuation-option-' + index + '-' + option?.Identifier?.value}>
                                                        <AccordionSummary
                                                            expandIcon={<ExpandMoreIcon/>}
                                                            aria-controls="panel1a-content"
                                                            id="panel1a-header"
                                                        >
                                                            {(recoverableError.RecoverableError?.DefaultOption?.value === option?.Identifier?.value) ? (
                                                                <>
                                                                    <StarIcon color={'info'}/>
                                                                    <Typography>{option?.Identifier?.value} (default)</Typography>
                                                                </>
                                                            ) : (
                                                                <Typography>{option?.Identifier?.value}</Typography>
                                                            )}
                                                        </AccordionSummary>
                                                        <AccordionDetails>
                                                            <Typography>
                                                                {option?.Description?.value}
                                                            </Typography>
                                                            <TextField
                                                                fullWidth
                                                                sx={{marginTop: '1rem'}}
                                                                InputLabelProps={{shrink: true}}
                                                                InputProps={{readOnly: true}}
                                                                label={'Required input data'}
                                                                value={option?.RequiredInputData?.value || 'None'}
                                                            />
                                                            <ExecuteContinuationOption
                                                                serverUuid={props.server.serverUuid}
                                                                isServerOnline={isServerOnline}
                                                                loading={formRequest.loading}
                                                                errorIndex={index}
                                                                recoverableError={recoverableError}
                                                                continuationOption={option}
                                                                onExecute={(parameters) => {
                                                                    setFormRequest({loading: true})
                                                                    return fetch(getBackEndpoint('/api/calls/execute'), {
                                                                        method: 'POST',
                                                                        headers: {
                                                                            'Content-Type': 'application/json',
                                                                        },
                                                                        body: JSON.stringify({
                                                                            serverId: props.server.serverUuid,
                                                                            fullyQualifiedFeatureId: 'org.silastandard/core/ErrorRecoveryService/v1',
                                                                            callId: 'ExecuteContinuationOption',
                                                                            type: 'UNOBSERVABLE_COMMAND',
                                                                            parameters: JSON.stringify(parameters),
                                                                            metadatas: JSON.stringify({}),
                                                                            runAsync: false
                                                                        } as SiLACallModel)
                                                                    }).then(response => response.json()).then((response: UscCall) => {
                                                                        if (!response.error) {
                                                                            setFormRequest({
                                                                                loading: false,
                                                                            })
                                                                            return null;
                                                                        } else {
                                                                            setFormRequest({
                                                                                loading: false,
                                                                            })
                                                                            return response.result || response.error;
                                                                        }
                                                                    }).catch((error) => {
                                                                        setFormRequest({
                                                                            loading: false,
                                                                        })
                                                                        return error;
                                                                    })
                                                                }}
                                                            />
                                                        </AccordionDetails>
                                                    </Accordion>
                                                ))
                                            }
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography variant={"h6"}>Actions</Typography>
                                        </Grid>
                                        <Grid item xs={4}>
                                            {
                                                (abortRecoveryAlert !== null && (
                                                    <Alert severity="warning" onClose={() => setAbortRecoveryAlert(null)}>{abortRecoveryAlert}</Alert>
                                                ))
                                            }
                                            <Button disabled={!isServerOnline || formRequest.loading}
                                                    variant="contained"
                                                    color="primary" onClick={(e) => {
                                                setFormRequest({loading: true})
                                                const parameters: ProtoAbortErrorHandling_Parameters = {
                                                    CommandExecutionUUID: {
                                                        value: recoverableError.RecoverableError?.CommandExecutionUUID?.value || ''
                                                    }
                                                }
                                                fetch(getBackEndpoint('/api/calls/execute'), {
                                                    method: 'POST',
                                                    headers: {
                                                        'Content-Type': 'application/json',
                                                    },
                                                    body: JSON.stringify({
                                                        serverId: props.server.serverUuid,
                                                        fullyQualifiedFeatureId: 'org.silastandard/core/ErrorRecoveryService/v1',
                                                        callId: 'AbortErrorHandling',
                                                        type: 'UNOBSERVABLE_COMMAND',
                                                        parameters: JSON.stringify(parameters),
                                                        metadatas: JSON.stringify({}),
                                                        runAsync: false
                                                    } as SiLACallModel)
                                                }).then(response => response.json()).then((response: UscCall) => {
                                                    if (!response.error) {
                                                        setFormRequest({
                                                            loading: false,
                                                        })
                                                        return null;
                                                    } else {
                                                        setFormRequest({
                                                            loading: false,
                                                        })
                                                        return response.result || response.error;
                                                    }
                                                }).catch((error) => {
                                                    setFormRequest({
                                                        loading: false,
                                                    })
                                                    return error;
                                                }).then((alert) => setAbortRecoveryAlert(alert))
                                            }}>
                                                Abort error handling
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            )
                        })}
                    </Grid>
                </Grid>
            </>
        </CloseableModal>
    );
}

export interface ExecutionContinuationOptionProps {
    serverUuid: string;
    errorIndex: number;
    isServerOnline: boolean;
    loading: boolean;
    recoverableError: ProtoDataType_RecoverableError;
    continuationOption: ProtoContinuationOption_Struct | undefined;
    onExecute?: (parameters: ProtoExecuteContinuationOption_Parameters) => Promise<string | null>
}

export function ExecuteContinuationOption(props: ExecutionContinuationOptionProps) {
    const dispatch = useDispatch();
    const requiredInputValueXML = props.continuationOption?.RequiredInputData?.value;
    const optionIdentifier = props.continuationOption?.Identifier?.value || '';
    const fullyQualifiedCommandId = 'org.silastandard/core/ErrorRecoveryService/v1/Command/ExecuteContinuationOption/' + props.errorIndex + '/' + optionIdentifier;
    const relativeParameterIdentifier = 'RequiredInputData';
    const callParameters = useSelector((state: AppState) => getCallInputParametersByIdSelector(state, fullyQualifiedCommandId))
    const parsedDataType: SiLAJavaDataTypeType | null | undefined = requiredInputValueXML ? SiLAXMLParser.parseXml(requiredInputValueXML) : undefined;
    const [alert, setAlert] = React.useState<string | null>(null);

    useEffect(() => {
        if (props.continuationOption && callParameters === undefined && parsedDataType) {
            dispatch(callInputInit({
                fullyQualifiedCommandId: fullyQualifiedCommandId,
                state: getCommandParameterInputStateFromDataType([], parsedDataType, relativeParameterIdentifier),
                formType: "PARAMETER",
                serverId: props.serverUuid
            }))
        }
    }, [dispatch, callParameters, parsedDataType, props.serverUuid, props.continuationOption, relativeParameterIdentifier, fullyQualifiedCommandId])

    if (parsedDataType === null) {
        // todo display warning
    }

    return (
        <>
            {(parsedDataType) && (
                <DataTypeForm
                    dataType={parsedDataType}
                    dispatch={dispatch}
                    runtimeConstraints={[]}
                    dataTypeDefinitions={[]}
                    stateId={relativeParameterIdentifier}
                    parameterId={relativeParameterIdentifier}
                    serverId={props.serverUuid}
                    value={callParameters}
                    fullyQualifiedCommandId={fullyQualifiedCommandId}
                    readonly={false}
                    type='PARAMETER'
                />
            )}
            {
                (alert !== null && (
                    <Alert severity="warning" onClose={() => setAlert(null)}>{alert}</Alert>
                ))
            }
            <Button disabled={!props.isServerOnline || props.loading}
                    variant="contained"
                    color="primary"
                    onClick={(e) => {
                        const hasRequiredInputData = parsedDataType && requiredInputValueXML;
                        const parameters = (!parsedDataType) ? {} : parametersStateToGrpcJsonFromDataType(
                            [],
                            parsedDataType,
                            relativeParameterIdentifier,
                            callParameters
                        );
                        const protoAny = hasRequiredInputData ? convertGrpcJsonToProtoAny(parsedDataType, parameters[relativeParameterIdentifier], requiredInputValueXML) : ({
                            type: '',
                            payload: ''
                        });
                        const executionContinuationOptionParameters: ProtoExecuteContinuationOption_Parameters = {
                            CommandExecutionUUID: {value: props.recoverableError.RecoverableError?.CommandExecutionUUID?.value || ''},
                            ContinuationOption: {value: optionIdentifier},
                            InputData: protoAny,
                        }
                        props.onExecute?.(executionContinuationOptionParameters).then((error) => {
                            setAlert(error);
                        });
                    }}
            >
                Execute option
            </Button>
        </>
    );
}