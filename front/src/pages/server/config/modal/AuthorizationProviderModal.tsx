import {
    Alert,
    Button,
    CircularProgress, FormControl,
    Grid, InputLabel, MenuItem, Select,
} from "@mui/material";
import React from "react";
import CloseableModal from "../../../../components/common/CloseableModal";
import {SiLACallModel, SiLACallResult, UscServer} from "../../../../generated/silaModels";
import {getBackEndpoint} from "../../../../utils/endpoint";
import {InvalidAuthorizationProviderAlert} from "../AccessToken";
import {ProtoSetAuthorizationProvider_Parameters} from "../../../../generated/AuthorizationConfigurationService";
import {ServerKeyValue} from "../../../../store/sila/manager/server/types";

export interface AuthorizationProviderModalProps {
    handleClose: () => void;
    server: UscServer;
    availableServers: ServerKeyValue;
    isAuthorizationProviderPresentAndOnline: boolean;
    serverUuidUsedForAuth?: string;
    serverUsedForAuth?: UscServer;
    hasAuthorizationProviderAuthenticationService: boolean;
    useCustomAuthorizationProvider: boolean;
    isCurrentServerOnline: boolean;
}

export interface AuthorizationProviderForm {
    loading: boolean;
    error?: string;
}

export default function AuthorizationProviderModal(props: AuthorizationProviderModalProps): JSX.Element {
    const [selectedServer, setSelectedServer] = React.useState<string>('');
    const [authorizationProviderRequest, setAuthorizationProviderRequest] = React.useState<AuthorizationProviderForm>({loading: false});

    return (
        <CloseableModal title={'Authorization Provider'} handleClose={props.handleClose}>
            <Grid container spacing={1} rowSpacing={2} marginTop={1} marginBottom={4} flexDirection={'column'}
                  alignContent={'space-around'}>
                <Grid item xs={6}>
                    <InvalidAuthorizationProviderAlert
                        isAuthorizationProviderPresentAndOnline={props.isAuthorizationProviderPresentAndOnline}
                        serverUsedForAuth={props.serverUsedForAuth}
                        serverUuidUsedForAuth={props.serverUuidUsedForAuth}
                        hasAuthorizationProviderAuthenticationService={props.hasAuthorizationProviderAuthenticationService}
                        useCustomAuthorizationProvider={props.useCustomAuthorizationProvider}
                        isCurrentServerOnline={props.isCurrentServerOnline}
                    />
                </Grid>
                <Grid item xs={4}>
                    <FormControl fullWidth>
                        <InputLabel id="auth-provider-server-form-label">Online server with authorization
                            provider</InputLabel>
                        <Select
                            labelId="auth-provider-server-form-label"
                            id={'auth-provider-server-form'}
                            key={'auth-provider-server-form'}
                            value={selectedServer}
                            label="Online server with authorization provider"
                            onChange={(e) => setSelectedServer(e.target.value)}
                        >
                            {
                                Object.values(props.availableServers)
                                    .filter(server => server.status === "ONLINE")
                                    .filter(server => server.features.some(f => f.relativeIdentifier === 'AuthenticationService'))
                                    .map(server => (
                                        <MenuItem key={server.serverUuid} value={server.serverUuid}>
                                            {server.name} ({server.serverUuid})
                                        </MenuItem>
                                    ))
                            }
                            <MenuItem key={'none'} disabled={true} value={''}>None (invalid)</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={4}>
                    {
                        ((!authorizationProviderRequest.loading && authorizationProviderRequest.error) && (
                            <Alert severity="warning">{authorizationProviderRequest.error}</Alert>
                        ))
                    }
                </Grid>
                <Grid item xs={4}>
                    {(!authorizationProviderRequest.loading) ? (
                        <Button
                            disabled={!selectedServer || !props.useCustomAuthorizationProvider || !props.isCurrentServerOnline}
                            variant="contained" color="primary" onClick={(e) => {
                            const parameters: ProtoSetAuthorizationProvider_Parameters = {
                                AuthorizationProvider: {
                                    value: selectedServer
                                }
                            }
                            setAuthorizationProviderRequest(prev => ({
                                ...prev,
                                loading: true,
                                error: undefined
                            }))
                            fetch(getBackEndpoint('/api/runtime/calls/execute'), {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify({
                                    serverId: props.server.serverUuid,
                                    fullyQualifiedFeatureId: 'org.silastandard/core/AuthorizationConfigurationService/v1',
                                    callId: 'SetAuthorizationProvider',
                                    type: 'UNOBSERVABLE_COMMAND',
                                    parameters: JSON.stringify(parameters),
                                    metadatas: JSON.stringify({}),
                                    runAsync: false
                                } as SiLACallModel)
                            }).then(response => response.json()).then((response: SiLACallResult) => {
                                if (response.success) {
                                    setAuthorizationProviderRequest(prev => ({
                                        loading: false,
                                        error: undefined
                                    }))
                                    props.handleClose();
                                } else {
                                    setAuthorizationProviderRequest(prev => ({
                                        ...prev,
                                        loading: false,
                                        error: 'Set authorization failed!'
                                    }))
                                }
                            }).catch((error) => {
                                setAuthorizationProviderRequest({
                                    loading: false,
                                    error: 'Set authorization failed!'
                                })
                            })
                        }}>
                            Set authorization provider
                        </Button>
                    ) : (
                        <CircularProgress/>
                    )}
                </Grid>
            </Grid>
        </CloseableModal>
    );
}