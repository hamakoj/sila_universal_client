import {
    Alert,
    Button,
    CircularProgress, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
    Grid,
    Switch,
    Table,
    TableCell,
    TableContainer,
    TableRow,
    TextField,
    Tooltip, TooltipProps,
    Typography,
} from "@mui/material";
import React from "react";
import CloseableModal from "../../../../components/common/CloseableModal";
import {SiLACallModel, UscCall, UscServer} from "../../../../generated/silaModels";
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import {getBackEndpoint} from "../../../../utils/endpoint";
import {useSelector} from "react-redux";
import {
    ProtoConnectSiLAClient_Parameters,
    ProtoDisableServerInitiatedConnectionMode_Parameters,
    ProtoDisconnectSiLAClient_Parameters,
    ProtoEnableServerInitiatedConnectionMode_Parameters
} from "../../../../generated/ConnectionConfigurationService";
import {AppState} from "../../../../store/rootReducer";
import {
    getServerMetaConfiguredSiLAClientsByIdSelector
} from "../../../../store/front/server/meta/selector";
import {Paper, TableBody, TableHead} from "@mui/material/";
import LogoutIcon from '@mui/icons-material/Logout';
import IconButton from "@mui/material/IconButton";
import ClearIcon from '@mui/icons-material/Clear';

export interface ConnectionConfigurationModalProps {
    handleClose: () => void;
    server: UscServer;
}

export interface ConnectionConfigurationForm {
    clientName: string;
    clientHost: string;
    clientPort: number;
    clientPersist: boolean;
}

export interface ConnectionConfigurationFormRequest {
    loading: boolean;
    error?: string;
}

export interface DisconnectionConfigurationFormRequest {
    loading: boolean;
    error?: string;
}

export interface ChangeConnectionModeFormRequest {
    loading: boolean;
    error?: string;
}

export interface ConnectionModeDialog {
    open: boolean;
    title?: string;
    description?: string;
    onOk?: () => void;
}

export default function ConnectionConfigurationModal(props: ConnectionConfigurationModalProps): JSX.Element {
    const configuredSiLAClients = useSelector((state: AppState) => getServerMetaConfiguredSiLAClientsByIdSelector(state, props.server.serverUuid));
    const [addClientRequest, setAddClientRequest] = React.useState<ConnectionConfigurationFormRequest>({loading: false});
    const [disconnectClientRequest, setDisconnectClientRequest] = React.useState<DisconnectionConfigurationFormRequest>({loading: false});
    const [changeConnectionModeFormRequest, setChangeConnectionModeFormRequest] = React.useState<ChangeConnectionModeFormRequest>({loading: false});
    const [connectionModeDialog, setConnectionModeDialog] = React.useState<ConnectionModeDialog>({open: false});
    const [addClientForm, setAddClientForm] = React.useState<ConnectionConfigurationForm>(
        {
            clientName: 'Universal SiLA Client (localhost:8080)',
            clientHost: 'localhost',
            clientPort: 8080,
            clientPersist: false
        }
    );
    const existingNames = configuredSiLAClients?.ConfiguredSiLAClients?.map(client => client.ClientName?.value) || [];
    const useAlreadyInUseName = existingNames.length > 0 && existingNames.includes(addClientForm.clientName);
    // todo check if port is valid
    const isServerOnline = props.server.status === "ONLINE";

    function disconnectClient(clientName: string, remove: boolean) {
        // todo display warning if trying to remove our own client
        const parameters: ProtoDisconnectSiLAClient_Parameters = {
            ClientName: {value: clientName},
            Remove: {value: remove}
        }
        setDisconnectClientRequest({loading: true})
        fetch(getBackEndpoint('/api/calls/execute'), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                serverId: props.server.serverUuid,
                fullyQualifiedFeatureId: 'org.silastandard/core/ConnectionConfigurationService/v1',
                callId: 'DisconnectSiLAClient',
                type: 'UNOBSERVABLE_COMMAND',
                parameters: JSON.stringify(parameters),
                metadatas: JSON.stringify({}),
                runAsync: false
            } as SiLACallModel)
        }).then(response => response.json()).then((response: UscCall) => {
            if (!response.error) {
                setDisconnectClientRequest({
                    loading: false,
                })
            } else {
                setDisconnectClientRequest({
                    loading: false,
                    error: 'Client disconnect failed, make sure that this client is connected'
                })
            }
        }).catch((error) => {
            setDisconnectClientRequest({
                loading: false,
                error: 'Client disconnect failed, make sure that this client is connected'
            })
        })
    }

    function switchConnectionMode(changeToServerInitiatedMode: boolean) {
        const parameters: ProtoEnableServerInitiatedConnectionMode_Parameters | ProtoDisableServerInitiatedConnectionMode_Parameters = {}
        setChangeConnectionModeFormRequest({loading: true})
        fetch(getBackEndpoint('/api/calls/execute'), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                serverId: props.server.serverUuid,
                fullyQualifiedFeatureId: 'org.silastandard/core/ConnectionConfigurationService/v1',
                callId: (changeToServerInitiatedMode) ? 'EnableServerInitiatedConnectionMode' : 'DisableServerInitiatedConnectionMode',
                type: 'UNOBSERVABLE_COMMAND',
                parameters: JSON.stringify(parameters),
                metadatas: JSON.stringify({}),
                runAsync: false
            } as SiLACallModel)
        }).then(response => response.json()).then((response: UscCall) => {
            if (!response.error) {
                setChangeConnectionModeFormRequest({
                    loading: false,
                })
            } else {
                setChangeConnectionModeFormRequest({
                    loading: false,
                    error: 'Failed to change connection mode'
                })
            }
        }).catch((error) => {
            setChangeConnectionModeFormRequest({
                loading: false,
                error: 'Failed to change connection mode'
            })
        })
    }

    return (
        <CloseableModal title={'Connection configuration'} handleClose={props.handleClose}>
            <>
                <Grid container spacing={1} rowSpacing={2} marginTop={1} marginBottom={4} flexDirection={'column'}
                      alignContent={'space-around'}>
                    <Grid item xs={12}>
                        <Typography variant={'h4'}>Connected clients</Typography>
                    </Grid>
                    {
                        (!isServerOnline && (
                            <Grid item xs={8}>
                                <Alert severity="warning">Server is offline!</Alert>
                            </Grid>
                        ))
                    }
                    {
                        (!disconnectClientRequest.loading && disconnectClientRequest.error && (
                            <Grid item xs={8}>
                                <Alert severity="warning">{disconnectClientRequest.error}</Alert>
                            </Grid>
                        ))
                    }
                    <Grid item xs={8}>
                        <TableContainer component={Paper}>
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Name</TableCell>
                                        <TableCell align="right">Host</TableCell>
                                        <TableCell align="right">Port</TableCell>
                                        <TableCell align="center">Action</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {configuredSiLAClients?.ConfiguredSiLAClients?.map((client) => (
                                        <TableRow
                                            key={client.ClientName?.value}
                                            sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                        >
                                            <TableCell component="th" scope="row">
                                                {client.ClientName?.value}
                                            </TableCell>
                                            <TableCell align="right">{client.SiLAClientHost?.value}</TableCell>
                                            <TableCell align="right">{client.SiLAClientPort?.value}</TableCell>
                                            <TableCell align="right">
                                                <Tooltip {...tooltipProps} title={'Disconnect client'}>
                                                    <IconButton
                                                        disabled={!isServerOnline || disconnectClientRequest.loading}
                                                        onClick={() => {
                                                            disconnectClient(client.ClientName?.value || '', false);
                                                        }}
                                                        onMouseDown={(event) => {
                                                            event.preventDefault();
                                                        }}
                                                        edge="end"
                                                    >
                                                        <LogoutIcon color={'warning'}/>
                                                    </IconButton>
                                                </Tooltip>
                                                <Tooltip {...tooltipProps} title={'Disconnect & Remove client'}>
                                                    <IconButton
                                                        disabled={!isServerOnline || disconnectClientRequest.loading}
                                                        onClick={() => {
                                                            disconnectClient(client.ClientName?.value || '', true);
                                                        }}
                                                        onMouseDown={(event) => {
                                                            event.preventDefault();
                                                        }}
                                                        edge="end"
                                                    >
                                                        <ClearIcon color={'warning'}/>
                                                    </IconButton>
                                                </Tooltip>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant={'h4'}>Connect client</Typography>
                    </Grid>
                    {
                        (useAlreadyInUseName && (
                            <Grid item xs={4}>
                                <Alert severity="warning">This name is already in use!</Alert>
                            </Grid>
                        ))
                    }
                    <Grid item xs={4}>
                        <TextField
                            id={'connection-configuration-client-name-form'}
                            key={'connection-configuration-client-name-form'}
                            fullWidth={true}
                            value={addClientForm.clientName}
                            label={'Client name'}
                            onChange={(e) => setAddClientForm((prev) => ({...prev, clientName: e.target.value}))}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            id={'connection-configuration-client-host-form'}
                            key={'connection-configuration-client-host-form'}
                            fullWidth={true}
                            value={addClientForm.clientHost}
                            label={'Client host'}
                            onChange={(e) => setAddClientForm((prev) => ({...prev, clientHost: e.target.value}))}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            id={'connection-configuration-client-port-form'}
                            key={'connection-configuration-client-host-form'}
                            fullWidth={true}
                            type={"number"}
                            value={addClientForm.clientPort}
                            label={'Client port'}
                            onChange={(e) => setAddClientForm((prev) => ({
                                ...prev,
                                clientPort: parseInt(e.target.value)
                            }))}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <FormGroup>
                            <FormControlLabel
                                control={<Switch
                                    checked={addClientForm.clientPersist}
                                    onChange={(e) => setAddClientForm((prev) => ({
                                        ...prev,
                                        clientPersist: e.target.checked
                                    }))}
                                />}
                                id={'connection-configuration-client-persist-form'}
                                key={'connection-configuration-client-persist-form'}
                                label={'Persistent'}
                            />
                        </FormGroup>
                    </Grid>
                    {
                        ((!addClientRequest.loading && addClientRequest.error) && (
                            <Grid item xs={4}>
                                <Alert severity="warning">{addClientRequest.error}</Alert>
                            </Grid>
                        ))
                    }
                    <Grid item xs={4}>
                        {(!addClientRequest.loading) ? (
                            <Button disabled={!isServerOnline || useAlreadyInUseName} variant="contained"
                                    color="primary" onClick={(e) => {
                                const parameters: ProtoConnectSiLAClient_Parameters = {
                                    ClientName: {value: addClientForm.clientName},
                                    SiLAClientHost: {value: addClientForm.clientHost},
                                    SiLAClientPort: {value: addClientForm.clientPort},
                                    Persist: {value: addClientForm.clientPersist},
                                }
                                setAddClientRequest({loading: true})
                                fetch(getBackEndpoint('/api/calls/execute'), {
                                    method: 'POST',
                                    headers: {
                                        'Content-Type': 'application/json',
                                    },
                                    body: JSON.stringify({
                                        serverId: props.server.serverUuid,
                                        fullyQualifiedFeatureId: 'org.silastandard/core/ConnectionConfigurationService/v1',
                                        callId: 'ConnectSiLAClient',
                                        type: 'UNOBSERVABLE_COMMAND',
                                        parameters: JSON.stringify(parameters),
                                        metadatas: JSON.stringify({}),
                                        runAsync: false
                                    } as SiLACallModel)
                                }).then(response => response.json()).then((response: UscCall) => {
                                    if (!response.error) {
                                        setAddClientRequest({
                                            loading: false,
                                        })
                                    } else {
                                        setAddClientRequest({
                                            loading: false,
                                            error: 'Client connect failed, make sure that this client is not already connected'
                                        })
                                    }
                                }).catch((error) => {
                                    setAddClientRequest({
                                        loading: false,
                                        error: 'Client connect failed, make sure that this client is not already connected'
                                    })
                                })
                            }}>
                                Connect client
                            </Button>
                        ) : (
                            <CircularProgress/>
                        )}
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant={'h4'}>Switch connection mode</Typography>
                    </Grid>
                    {
                        ((!changeConnectionModeFormRequest.loading && changeConnectionModeFormRequest.error) && (
                            <Grid item xs={4}>
                                <Alert severity="warning">{changeConnectionModeFormRequest.error}</Alert>
                            </Grid>
                        ))
                    }
                    <Grid item xs={4}>
                        {
                            (props.server.connectionType === "SERVER_INITIATED") ? (
                                <Button disabled={!isServerOnline} variant="contained" color="primary" onClick={(e) => {
                                    setConnectionModeDialog({
                                        open: true,
                                        title: 'Change connection mode to be client initiated',
                                        description: 'The server will become offline and you will need to add it back through the server manager since the Universal SiLA Client does not know the host and port of the server',
                                        onOk: () => switchConnectionMode(false)
                                    })
                                }}>
                                    Use client initiated connection
                                </Button>
                            ) : (
                                <Button disabled={!isServerOnline} variant="contained" color="primary" onClick={(e) => {
                                    setConnectionModeDialog({
                                        open: true,
                                        title: 'Change connection mode to be server initiated',
                                        description: 'Make sure that you connected the Universal SiLA Client before switching, otherwise the server will become offline and will not connect back to the Universal SiLA Client',
                                        onOk: () => switchConnectionMode(true)
                                    })
                                }}>
                                    Use server initiated connection
                                </Button>
                            )
                        }
                    </Grid>
                </Grid>
                <div>
                    <Dialog
                        open={connectionModeDialog.open}
                        onClose={() => setConnectionModeDialog(prev => ({...prev, open: false}))}
                        aria-labelledby="connection-mode-dialog-title"
                        aria-describedby="connection-mode-dialog-description"
                    >
                        <DialogTitle id="connection-mode-dialog-title">
                            {connectionModeDialog.title}
                        </DialogTitle>
                        <DialogContent>
                            <DialogContentText id="connection-mode-dialog-description">
                                {connectionModeDialog.description}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                onClick={() => setConnectionModeDialog(prev => ({...prev, open: false}))}>Abort</Button>
                            <Button onClick={() => {
                                setConnectionModeDialog(prev => ({...prev, open: false}))
                                connectionModeDialog.onOk?.();
                            }} autoFocus>Proceed</Button>
                        </DialogActions>
                    </Dialog>
                </div>
            </>
        </CloseableModal>
    );
}

const tooltipProps: Partial<TooltipProps> = {
    sx: {
        marginLeft: '0.5rem',
        marginRight: '0.5rem',
    },
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};