import {
    Alert,
    Button,
    CircularProgress,
    Grid,
} from "@mui/material";
import React from "react";
import CloseableModal from "../../../../components/common/CloseableModal";
import {SiLACallModel, UscCall} from "../../../../generated/silaModels";
import {getBackEndpoint} from "../../../../utils/endpoint";
import {
    ProtoGet_SimulationMode_Responses,
    ProtoStartRealMode_Parameters,
    ProtoStartSimulationMode_Parameters
} from "../../../../generated/SimulationController";

export interface SimulationControllerModalProps {
    handleClose: () => void;
    serverUuid: string;
    isCurrentServerOnline: boolean;
}

export interface SimulationModeRequest {
    loading: boolean;
    error?: string;
    result?: ProtoGet_SimulationMode_Responses;
}

export default function SimulationControllerModal(props: SimulationControllerModalProps): JSX.Element {
    const [simulationModeRequest, setSimulationModeRequest] = React.useState<SimulationModeRequest>({loading: false});

    function runSimulationCall(callId: string) {
        const parameters: ProtoStartSimulationMode_Parameters | ProtoStartRealMode_Parameters = {}
        const isSimulationModePropertyCall = callId === 'SimulationMode';
        fetch(getBackEndpoint('/api/calls/execute'), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                serverId: props.serverUuid,
                fullyQualifiedFeatureId: 'org.silastandard/core/SimulationController/v1',
                callId: callId,
                type: isSimulationModePropertyCall ? 'UNOBSERVABLE_PROPERTY' : 'UNOBSERVABLE_COMMAND',
                parameters: JSON.stringify(parameters),
                metadatas: JSON.stringify({}),
                runAsync: false
            } as SiLACallModel)
        }).then(response => response.json()).then((response: UscCall) => {
            if (!response.error) {
                if (isSimulationModePropertyCall) {
                    const simulationMode: ProtoGet_SimulationMode_Responses = JSON.parse(response.result);
                    setSimulationModeRequest({
                        loading: false,
                        error: undefined,
                        result: simulationMode
                    })
                } else {
                    setSimulationModeRequest({
                        loading: false,
                        error: undefined
                    })
                }
            } else {
                setSimulationModeRequest({
                    loading: false,
                    error: response.result || response.error as any || 'Unknown error'
                })
            }
        }).catch((error) => {
            setSimulationModeRequest({
                loading: false,
                error: error
            })
        })
    }

    return (
        <CloseableModal title={'Simulation Controller'} handleClose={props.handleClose}>
            <Grid container spacing={1} rowSpacing={2} marginTop={1} marginBottom={4} flexDirection={'column'}
                  alignContent={'space-around'}>
                {
                    ((!simulationModeRequest.loading && simulationModeRequest.error) && (
                        <Grid item xs={4}>
                            <Alert severity="warning">{simulationModeRequest.error}</Alert>
                        </Grid>
                    ))
                }
                {
                    ((!simulationModeRequest.loading && simulationModeRequest.result) && (
                        <Grid item xs={4}>
                            <Alert severity="info">
                                {
                                    (simulationModeRequest.result.SimulationMode?.value) ?
                                        'Server is currently in simulation mode' :
                                        'Server is currently in real mode'
                                }
                            </Alert>
                        </Grid>
                    ))
                }
                {((simulationModeRequest.loading) && (
                    <Grid item xs={12}>
                        <CircularProgress/>
                    </Grid>
                ))}
                {((!simulationModeRequest.loading) && (
                    <Grid item xs={4}>
                        <Button
                            disabled={!props.isCurrentServerOnline}
                            variant="contained" color="primary" onClick={(e) => {
                            runSimulationCall('SimulationMode')
                        }}>
                            Get current mode
                        </Button>
                    </Grid>
                ))}
                {((!simulationModeRequest.loading) && (
                    <Grid item xs={4}>
                        <Button
                            disabled={!props.isCurrentServerOnline}
                            variant="contained" color="primary" onClick={(e) => {
                            runSimulationCall('StartSimulationMode')
                        }}>
                            Start simulation mode
                        </Button>
                    </Grid>
                ))}
                {((!simulationModeRequest.loading) && (
                    <Grid item xs={4}>
                        <Button
                            disabled={!props.isCurrentServerOnline}
                            variant="contained" color="primary" onClick={(e) => {
                            runSimulationCall('StartRealMode')
                        }}>
                            Start real mode
                        </Button>
                    </Grid>
                ))}
            </Grid>
        </CloseableModal>
    );
}