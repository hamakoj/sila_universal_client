import {
    Alert,
    Button,
    Checkbox, CircularProgress, Divider, Grid, InputAdornment,
    TextField, Typography,
} from "@mui/material";
import React from "react";
import CloseableModal from "../../../../components/common/CloseableModal";
import {SiLACallModel, SiLACallResult, UscServer} from "../../../../generated/silaModels";
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import {getBackEndpoint} from "../../../../utils/endpoint";
import {ProtoLogin_Parameters, ProtoLogin_Responses} from "../../../../generated/AuthenticationService";
import {DateTime} from "luxon";
import {useDispatch} from "react-redux";
import {setUserAccessToken} from "../../../../store/front/app/user/actions";
import {Visibility, VisibilityOff} from "@mui/icons-material";
import IconButton from "@mui/material/IconButton";
import {InvalidAuthorizationProviderAlert} from "../AccessToken";

export interface LoginModalProps {
    handleClose: () => void;
    server: UscServer;
    isAuthorizationProviderPresentAndOnline: boolean;
    serverUuidUsedForAuth?: string;
    serverUsedForAuth?: UscServer;
    isCurrentServerOnline: boolean;
    hasAuthorizationProviderAuthenticationService: boolean;
    useCustomAuthorizationProvider: boolean;
}

export interface LoginForm {
    username: string;
    password: string;
    features: string[];
    showPassword: boolean;
}

export interface LoginFormRequest {
    loading: boolean;
    error?: string;
}

export default function LoginModal(props: LoginModalProps): JSX.Element {
    const dispatch = useDispatch();
    const [loginRequest, setLoginRequest] = React.useState<LoginFormRequest>({loading: false});
    const [loginForm, setLoginForm] = React.useState<LoginForm>({username: '', password: '', features: [], showPassword: false});
    return (
        <CloseableModal title={'Login'} handleClose={props.handleClose}>
            <Grid container spacing={1} rowSpacing={2} marginTop={1} marginBottom={4} flexDirection={'column'}
                  alignContent={'space-around'}>
                <Grid item xs={6}>
                    <InvalidAuthorizationProviderAlert
                        isAuthorizationProviderPresentAndOnline={props.isAuthorizationProviderPresentAndOnline}
                        serverUsedForAuth={props.serverUsedForAuth}
                        serverUuidUsedForAuth={props.serverUuidUsedForAuth}
                        hasAuthorizationProviderAuthenticationService={props.hasAuthorizationProviderAuthenticationService}
                        useCustomAuthorizationProvider={props.useCustomAuthorizationProvider}
                        isCurrentServerOnline={props.isCurrentServerOnline}
                    />
                </Grid>
                <Grid item xs={4}>
                    <TextField
                        id={'auth-login-username-form'}
                        key={'auth-login-username-form'}
                        fullWidth={true}
                        value={loginForm.username}
                        label={'Username'}
                        onChange={(e) => setLoginForm((prev) => ({...prev, username: e.target.value}))}
                    />
                </Grid>
                <Grid item xs={4}>
                    <TextField
                        id={'auth-password-username-form'}
                        key={'auth-password-username-form'}
                        fullWidth={true}
                        type={loginForm.showPassword ? 'text' : 'password'}
                        value={loginForm.password}
                        label={'Password'}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={() => {
                                            setLoginForm((prevState) => ({
                                                ...prevState,
                                                showPassword: !prevState.showPassword
                                            }))
                                        }}
                                        onMouseDown={(event) => {
                                            event.preventDefault();
                                        }}
                                        edge="end"
                                    >
                                        {loginForm.showPassword ? <VisibilityOff/> : <Visibility/>}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                        onChange={(e) => setLoginForm((prev) => ({...prev, password: e.target.value}))}
                    />
                </Grid>
                <Grid item xs={4} sm={6}>
                    <Typography variant={'h6'}>Features to request access (ignore to request all)</Typography>
                    <Divider/>
                    <FormGroup sx={{maxHeight: 256, flexWrap: 'nowrap', overflow: 'auto'}}>
                        {
                            props.server.features.map(f => (
                                <FormControlLabel key={f.fullyQualifiedIdentifier + '/checkbox'} control={
                                    <Checkbox
                                        checked={loginForm.features.includes(f.fullyQualifiedIdentifier)}
                                        onChange={(e) => {
                                            if (e.target.checked) {
                                                setLoginForm(prev => ({
                                                    ...prev,
                                                    features: [...prev.features, f.fullyQualifiedIdentifier]
                                                }));
                                            } else {
                                                setLoginForm(prev => ({
                                                    ...prev,
                                                    features: prev.features.filter(p => p !== f.fullyQualifiedIdentifier)
                                                }));
                                            }
                                        }}
                                    />
                                } label={f.fullyQualifiedIdentifier}/>
                            ))
                        }
                    </FormGroup>
                </Grid>
                <Grid item xs={4}>
                    {
                        ((!loginRequest.loading && loginRequest.error) && (
                            <Alert severity="warning">{loginRequest.error}</Alert>
                        ))
                    }
                </Grid>
                <Grid item xs={4}>
                    {(!loginRequest.loading) ? (
                        <Button disabled={!props.isAuthorizationProviderPresentAndOnline}
                                variant="contained" color="primary" onClick={(e) => {
                            const authorizationProvider = props.serverUsedForAuth?.serverUuid;
                            if (!authorizationProvider) {
                                console.error('Authorization provider is undefined, this should not happen!');
                                return;
                            }
                            const parameters: ProtoLogin_Parameters = {
                                UserIdentification: {value: loginForm.username},
                                Password: {value: loginForm.password},
                                RequestedServer: {value: props.server.serverUuid},
                                RequestedFeatures: loginForm.features.map((f) => ({value: f}))
                            }
                            setLoginRequest({loading: true})
                            fetch(getBackEndpoint('/api/runtime/calls/execute'), {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify({
                                    serverId: authorizationProvider,
                                    fullyQualifiedFeatureId: 'org.silastandard/core/AuthenticationService/v1',
                                    callId: 'Login',
                                    type: 'UNOBSERVABLE_COMMAND',
                                    parameters: JSON.stringify(parameters),
                                    metadatas: JSON.stringify({}),
                                    runAsync: false
                                } as SiLACallModel)
                            }).then(response => response.json()).then((response: SiLACallResult) => {
                                if (response.success) {
                                    const loginResponses: ProtoLogin_Responses = JSON.parse(response.value);
                                    const expireUTCDateTime = DateTime.now().toUTC().plus({seconds: loginResponses.TokenLifetime?.value || 0});
                                    const accessToken = loginResponses.AccessToken?.value || '';
                                    dispatch(setUserAccessToken({
                                        serverUuid: props.server.serverUuid,
                                        authorizedByServerUuid: authorizationProvider,
                                        expirationUTC: expireUTCDateTime,
                                        token: accessToken
                                    }))
                                    setLoginRequest({
                                        loading: false,
                                    })
                                    props.handleClose();
                                } else {
                                    setLoginRequest({
                                        loading: false,
                                        error: 'Login failed, make sure you provided the right credentials'
                                    })
                                }
                            }).catch((error) => {
                                setLoginRequest({
                                    loading: false,
                                    error: 'Login failed, make sure you provided the right credentials'
                                })
                            })
                        }}>
                            Login
                        </Button>
                    ) : (
                        <CircularProgress/>
                    )}
                </Grid>
            </Grid>
        </CloseableModal>
    );
}