import {
    Alert, Button,
    Grid, InputAdornment, Switch, TextField, Tooltip, TooltipProps
} from "@mui/material";
import React, {useEffect} from "react";
import CloseableModal from "../../../../components/common/CloseableModal";
import {SiLACallModel, UscCall, UscServer} from "../../../../generated/silaModels";
import {getBackEndpoint} from "../../../../utils/endpoint";
import {ProtoLockServer_Parameters, ProtoUnlockServer_Parameters} from "../../../../generated/LockController";
import AutorenewIcon from '@mui/icons-material/Autorenew';
import IconButton from "@mui/material/IconButton";
import {generateUnsafeSlowUUID} from "../../../../utils/unsafeUUID";
import {useDispatch, useSelector} from "react-redux";
import {removeUserLockToken, setUserLockToken} from "../../../../store/front/app/user/actions";
import {DateTime} from "luxon";
import {AppState} from "../../../../store/rootReducer";
import {getAppUserLockTokenForServerIdSelector} from "../../../../store/front/app/user/selector";

export interface LockControllerModalProps {
    handleClose: () => void;
    server: UscServer;
    isLocked: boolean;
}

export interface LockControllerRequest {
    loading: boolean;
    error?: string;
}

export default function LockControllerModal(props: LockControllerModalProps): JSX.Element {
    const dispatch = useDispatch();
    const isServerOnline = props.server.status === "ONLINE";
    const userLockToken = useSelector((state: AppState) => getAppUserLockTokenForServerIdSelector(state, props.server.serverUuid));
    const [lockControllerRequest, setLockControllerRequest] = React.useState<LockControllerRequest>({loading: false});
    const [lockToken, setLockToken] = React.useState<string>('');
    const [lockTimeout, setLockTimeout] = React.useState<number>(0);
    const [useCustomLockToken, setUseCustomLockToken] = React.useState<boolean>(false);

    useEffect(() => {
        if (props.isLocked) {
            if (useCustomLockToken) {
                setLockToken('')
            } else {
                setLockToken(userLockToken?.token || '')
            }
        }
    }, [userLockToken?.token, props.isLocked, useCustomLockToken]);

    return (
        <CloseableModal title={'Lock Controller'} handleClose={props.handleClose}>
            <Grid container spacing={1} rowSpacing={2} marginTop={1} marginBottom={4} flexDirection={'column'} alignContent={'space-around'}>
                {
                    (!isServerOnline && (
                        <Grid item xs={12}>
                            <Alert severity={'warning'}>
                                Server is offline!
                            </Alert>
                        </Grid>
                    ))
                }
                {
                    (isServerOnline && props.isLocked && (
                        <Grid item xs={12}>
                            <Alert severity={'warning'}>
                                Server is locked!
                            </Alert>
                        </Grid>
                    ))
                }
                {
                    ((lockControllerRequest.error && !lockControllerRequest.loading) && (
                        <Grid item xs={4}>
                            <Alert severity="warning">{lockControllerRequest.error}</Alert>
                        </Grid>
                    ))
                }
                <Grid item xs={6}>
                    <TextField
                        id={'lock-controller-token-form'}
                        key={'lock-controller-token-form'}
                        fullWidth={true}
                        value={lockToken}
                        label={'Lock token'}
                        helperText="The token to provide to use when the locked server"
                        InputProps={{
                            ...((props.isLocked && !useCustomLockToken) && ({
                                readOnly: true
                            })),
                            ...((!props.isLocked) && ({
                                endAdornment: <InputAdornment position="end">
                                    <Tooltip {...tooltipProps} title={'Generate random token'}>
                                        <IconButton
                                            aria-label="Generate random token"
                                            onClick={() => {
                                                setLockToken(generateUnsafeSlowUUID())
                                            }}
                                            onMouseDown={(event) => {
                                                event.preventDefault();
                                            }}
                                            edge="end"
                                        >
                                            <AutorenewIcon/>
                                        </IconButton>
                                    </Tooltip>
                                </InputAdornment>
                            })),
                        }}
                        onChange={(e) => setLockToken(e.target.value)}
                    />
                </Grid>
                {
                    (props.isLocked && (
                        <Grid item xs={12}>
                            <Grid component="label" container alignItems="center" spacing={1}>
                                <Grid item xs={5}>Unlock with own token</Grid>
                                <Grid item xs={2}>
                                    <Switch checked={useCustomLockToken} onChange={(event, checked) => setUseCustomLockToken(checked)}/>
                                </Grid>
                                <Grid item xs={5}>Unlock with custom token</Grid>
                            </Grid>
                        </Grid>
                    ))
                }
                {
                    ((!props.isLocked) && (
                        <Grid item xs={6}>
                            <TextField
                                id={'lock-controller-timeout-form'}
                                key={'lock-controller-timeout-form'}
                                fullWidth={true}
                                value={lockTimeout}
                                helperText="Timeout in seconds, a timeout of 0 means no timeout"
                                label={'Lock timeout'}
                                type={"number"}
                                onChange={(e) => setLockTimeout(parseInt(e.target.value))}
                            />
                        </Grid>
                    ))
                }
                <Grid item xs={4}>
                    <Button disabled={!isServerOnline || lockControllerRequest.loading || !lockToken}
                            variant="contained"
                            color="primary" onClick={(e) => {
                        setLockControllerRequest({loading: true})
                        const isLockRequest = !props.isLocked;
                        const parameters: ProtoLockServer_Parameters | ProtoUnlockServer_Parameters = {
                            LockIdentifier: {value: lockToken},
                            ...(!isLockRequest) ? ({}) : ({Timeout: {value: lockTimeout}})
                        }
                        fetch(getBackEndpoint('/api/calls/execute'), {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                serverId: props.server.serverUuid,
                                fullyQualifiedFeatureId: 'org.silastandard/core/LockController/v1',
                                callId: (props.isLocked) ? 'UnlockServer' : 'LockServer',
                                type: 'UNOBSERVABLE_COMMAND',
                                parameters: JSON.stringify(parameters),
                                metadatas: JSON.stringify({}),
                                runAsync: false
                            } as SiLACallModel)
                        }).then(response => response.json()).then((response: UscCall) => {
                            if (!response.error) {
                                if (isLockRequest) {
                                    const timeout = (parameters as ProtoLockServer_Parameters)?.Timeout?.value;
                                    const timeoutUTCDatetime = (timeout === 0) ? (DateTime.now().toUTC().plus({year: 100})) : (
                                        DateTime.now().toUTC().plus({seconds: (parameters as ProtoLockServer_Parameters)?.Timeout?.value || (5 * 60)})
                                    )
                                    dispatch(setUserLockToken({
                                        serverUuid: props.server.serverUuid,
                                        token: parameters.LockIdentifier?.value || '',
                                        expirationUTC: timeoutUTCDatetime
                                    }))
                                } else {
                                    dispatch(removeUserLockToken({
                                        serverUuid: props.server.serverUuid,
                                    }))
                                }
                                setLockControllerRequest({
                                    loading: false,
                                    error: undefined
                                })
                            } else {
                                setLockControllerRequest({
                                    loading: false,
                                    error: response.result || response.error as any || 'Unknown error'
                                })
                            }
                        }).catch((error) => {
                            setLockControllerRequest({
                                loading: false,
                                error: error
                            })
                        })
                    }}>
                        {(props.isLocked) ? 'Unlock server' : 'Lock server'}
                    </Button>
                </Grid>
            </Grid>
        </CloseableModal>
    );
}

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};