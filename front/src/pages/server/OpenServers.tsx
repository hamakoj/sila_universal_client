import React from "react";
import {ServerOpenKeyValueState} from "../../store/front/server/open/types";
import {useSelector} from "react-redux";
import {getServerOpenSelector} from "../../store/front/server/open/selector";
import {AppSettings} from "../../store/front/app/setting/types";
import {getAppSettingsSelector} from "../../store/front/app/setting/selector";
import {getServersSelector} from "../../store/sila/manager/server/selector";
import ServerConfigurationContainer from "./config/ServerConfig";
import ServerProperty from "./property/ServerProperty";
import ServerCommand from "./command/ServerCommand";

const OpenServers = React.memo((): JSX.Element => {
    const serverOpen: ServerOpenKeyValueState = useSelector(getServerOpenSelector);
    const settings: AppSettings = useSelector(getAppSettingsSelector);
    const servers = useSelector(getServersSelector); // todo select only those open
    const getVisibilityStyle = (hiddenCondition: boolean): any => {
        if (!hiddenCondition) {
            return {
                display: 'none',
                height: 0,
            };
        }
        return {
            height: '100%'
        };
    };

    const elements = Object.keys(serverOpen.serversTabs)
        .filter((serverId) => (settings.serverTabsStoreInMemory) ? true : serverOpen.serverUuidOpen === serverId)
        .map((serverId) => {
            const server = servers[serverId];
            if (!server) {
                return null;
            }
            const isCurrentServerOpen = serverOpen.serverUuidOpen === serverId;
            const currentServerTab = serverOpen.serversTabs[serverId];
            return (
                <React.Fragment key={serverId}>
                    <div key={'info'} style={getVisibilityStyle(isCurrentServerOpen && currentServerTab === 'info')}>
                        <ServerConfigurationContainer server={server}/>
                    </div>
                    <div key={'property'}
                         style={getVisibilityStyle(isCurrentServerOpen && currentServerTab === 'property')}>
                        <ServerProperty server={server}/>
                    </div>
                    <div key={'command'}
                         style={getVisibilityStyle(isCurrentServerOpen && currentServerTab === 'command')}>
                        <ServerCommand server={server}/>
                    </div>
                </React.Fragment>
            )
        });
    return (
        <>
            {elements}
        </>
    );
});

export default OpenServers;