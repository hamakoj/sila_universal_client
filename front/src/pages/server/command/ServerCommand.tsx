import React from 'react';

import ServerCallResponseContainer from "../../../components/server/call/ServerCallResponseContainer";
import {UscServer} from "../../../generated/silaModels";

export interface ServerCommandProps {
    server: UscServer
}

const ServerCommand = React.memo((props: ServerCommandProps): JSX.Element => {
    return (
        <ServerCallResponseContainer server={props.server} type={'command'}/>
    )
});

export default ServerCommand