import React from 'react';

import ServerCallResponseContainer from "../../../components/server/call/ServerCallResponseContainer";
import {UscServer} from "../../../generated/silaModels";

export interface ServerPropertyProps {
    server: UscServer;
}

const ServerProperty = React.memo((props: ServerPropertyProps): JSX.Element => {
    return (
        <ServerCallResponseContainer server={props.server} type={'property'}/>
    )
});

export default ServerProperty;