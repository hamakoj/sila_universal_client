import {ServerKeyValue} from "../../../store/sila/manager/server/types";
import {
    Typography,
    Paper,
    TableCell, Tooltip, Theme, Container,
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {Table, TableBody, TableContainer, TableHead, TableRow} from "@mui/material/";
import {getRunningCallSelector} from "../../../store/sila/runningCall/selector";
import {RunningCallState} from "../../../store/sila/runningCall/types";
import DangerousIcon from "@mui/icons-material/Dangerous";
import GpsFixedIcon from '@mui/icons-material/GpsFixed';
import CloseableModal from "../../../components/common/CloseableModal";
import {getBackEndpoint} from "../../../utils/endpoint";
import {useNavigate} from "react-router-dom";
import {serverActiveCall} from "../../../store/front/server/activeCall/actions";
import {SxProps} from "@mui/system";

export interface CallManagerProps {
    handleClose: () => void;
    serversReal: ServerKeyValue;
}

export default function CallManager(props: CallManagerProps): JSX.Element {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const runningCalls: RunningCallState = useSelector(getRunningCallSelector);
    const runningCallsValues = Object.values(runningCalls.callUuidMap);

    return (
        <CloseableModal title={'Task Manager'} handleClose={props.handleClose}>
            <>
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Task UUID</TableCell>
                                <TableCell>Server identifier</TableCell>
                                <TableCell>Server name</TableCell>
                                <TableCell>Feature identifier</TableCell>
                                <TableCell>Task identifier</TableCell>
                                <TableCell>Timestamp</TableCell>
                                <TableCell>Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                runningCallsValues.map(call => {
                                    const server = props.serversReal[call.serverUuid];
                                    const feature = server?.features?.find?.(f => f.fullyQualifiedIdentifier === call.featureIdentifier);
                                    const property = feature?.properties?.find?.(p => p.relativeIdentifier === call.commandIdentifier);
                                    const command = feature?.commands?.find?.(v => v.relativeIdentifier === call.commandIdentifier);
                                    const cellSx: SxProps<Theme> = {
                                        overflow: 'hidden',
                                        overflowWrap: 'break-word',
                                        maxWidth: '180px'
                                    };
                                    return (
                                        <TableRow
                                            key={call.uuid}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <TableCell component="th" scope="row">
                                                {call.uuid}
                                            </TableCell>
                                            <TableCell sx={cellSx}>{call.serverUuid}</TableCell>
                                            <TableCell sx={cellSx}>{server?.name || 'Unknown'}</TableCell>
                                            <TableCell sx={cellSx}>{call.featureIdentifier}</TableCell>
                                            <TableCell sx={cellSx}>{call.commandIdentifier}</TableCell>
                                            <TableCell sx={cellSx}>{call.timestamp.toISOString()}</TableCell>
                                            <TableCell sx={cellSx}>
                                                <Container sx={{display: 'flex'}}>
                                                    <Tooltip title={'Cancel ' + call.commandIdentifier + ' task'}>
                                                        <IconButton onClick={(e) => {
                                                            fetch(getBackEndpoint('/api/calls/' + call.uuid + '/cancel'), {})
                                                        }}>
                                                            <DangerousIcon color={'error'}/>
                                                        </IconButton>
                                                    </Tooltip>
                                                    {
                                                        ((feature && (command || property)) && (
                                                            <Tooltip title={'Go to task'}>
                                                                <IconButton onClick={(e) => {
                                                                    const callType = (command) ? 'command' : 'property';
                                                                    navigate('/server/' + call.serverUuid + '/' + callType);
                                                                    dispatch(serverActiveCall({
                                                                        serverUuid: call.serverUuid,
                                                                        fullyQualifiedFeatureId: feature.fullyQualifiedIdentifier,
                                                                        callType: callType,
                                                                        callId: call.commandIdentifier
                                                                    }));
                                                                    // todo scroll call.uuid into view
                                                                }}>
                                                                    <GpsFixedIcon color={'primary'}/>
                                                                </IconButton>
                                                            </Tooltip>
                                                        ))
                                                    }
                                                </Container>
                                            </TableCell>
                                        </TableRow>
                                    )}
                                )
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
                {
                    (runningCallsValues.length === 0 && (
                        <Typography>No task in progress</Typography>
                    ))
                }
            </>
        </CloseableModal>
    );
}