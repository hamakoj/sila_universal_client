import {ServerKeyValue} from "../../../store/sila/manager/server/types";
import {
    Alert,
    Button,
    Snackbar,
    TextField,
    Typography,
    AlertColor,
    InputLabel, MenuItem, Select, Grid, FormControl, Autocomplete, Chip, Modal
} from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import {Wifi} from "react-feather";
import ServerGrid from "../../../components/server/ServerGrid";
import React, {useState} from "react";
import {useSelector} from "react-redux";
import {getIsConnectedSelector} from "../../../store/sila/websocket/selector";
import {useNavigate} from "react-router-dom";
import {UscServerStatus} from "../../../generated/silaModels";
import CloseableModal from "../../../components/common/CloseableModal";
import {getBackEndpoint} from "../../../utils/endpoint";
import {SxProps} from "@mui/system";
import {Theme} from "@mui/material/styles";
import AddServer from "./AddServer";

export interface ServerManagerProps {
    handleClose: () => void;
    serversReal: ServerKeyValue;
}

export default function ServerManager(props: ServerManagerProps): JSX.Element {
    const navigate = useNavigate();
    const [openAddServer, setOpenAddServer] = React.useState<boolean>(false);
    const [featureSearch, setFeatureSearch] = useState<string>('');
    const [filters, setFilters] = useState<{name: string, status: UscServerStatus | "ANY", features: string[]}>({
        name: '',
        status: "ANY",
        features: []
    });
    const [closeAlert, setCloseAlert] = useState<boolean>(true);
    const [loadingInProgress, setLoadingInProgress] = useState<string>('');
    const [alertMessage, setAlertMessage] = useState<string>('');
    const [alertSeverity, setAlertSeverity] = useState<AlertColor>('success');
    const isWsConnected: boolean = useSelector(getIsConnectedSelector);
    const serversName = Object.values(props.serversReal).reduce((acc: {[serverName: string]: null}, server) => {
        acc[server.name] = null;
        return acc;
    }, {});
    const features = Object.values(props.serversReal).reduce((acc, server) => {
        return {
            ...acc,
            ...server.features.reduce((localAcc: {[featureName: string]: null}, feature) => {
                localAcc[feature.displayName] = null;
                return localAcc;
            }, {})
        };
    }, {});

    // todo optimize by keeping a map for each predicate with the server as key and predicate as value
    const filteredServersId = Object.entries(props.serversReal).filter(([k, v]) => {
        if (!v.name.toLowerCase().includes(filters.name.toLowerCase())) {
            return false;
        }
        if (filters.status !== 'ANY' && !v.status.toLowerCase().includes(filters.status.toLowerCase())) {
            return false;
        }
        const serverFeatures = new Set(v.features.map(f => f.displayName));
        if (!filters.features.every((featureName => serverFeatures.has(featureName)))) {
            return false;
        }
        return true;
    }).map(([k, v]) => k);

    return (
        <CloseableModal title={'SiLA Server Manager'} handleClose={props.handleClose}>
            <div>
                <Typography variant="h5" gutterBottom>
                    Actions
                </Typography>
                <Grid container spacing={1} columns={{ xs: 4, sm: 8, md: 12 }}>
                    <Snackbar
                        open={!closeAlert}
                        autoHideDuration={6000}
                        onClose={() => {
                            setCloseAlert(true)
                        }}
                    >
                        <Alert
                            onClose={() => {
                                setCloseAlert(true)
                            }}
                            severity={alertSeverity}
                        >
                            {alertMessage}
                        </Alert>
                    </Snackbar>
                    <Snackbar
                        open={loadingInProgress.length > 0}
                        autoHideDuration={20000}
                    >
                        <Alert
                            severity={'info'}
                        >
                            {loadingInProgress}
                        </Alert>
                    </Snackbar>
                    <Grid item xl={1} key={'server-search'}>
                        <Button sx={{height: '100%'}} id={"add-server-manager-btn"} variant="contained" color="primary"
                                disabled={!isWsConnected}
                                onClick={() => {
                                    setOpenAddServer(true);
                                }}
                        >
                            <AddIcon/>
                            <Typography marginLeft={1}>Add server</Typography>
                        </Button>
                    </Grid>
                    <Grid item xl={1} key={'server-scan'}>
                        <Button sx={{height: '100%'}} variant="contained" color="primary" onClick={() => {
                            setLoadingInProgress('Scan in progress');
                            fetch(getBackEndpoint('/api/servers/scan'), {
                                method: 'GET',
                            })
                                .then((data) => {
                                    setLoadingInProgress('');
                                    if (!data.ok) {
                                        setAlertMessage('Server scan failed');
                                        setAlertSeverity('error');
                                        setCloseAlert(false);
                                        return;
                                    }
                                    setAlertMessage('Server scan done');
                                    setAlertSeverity('success');
                                    setCloseAlert(false);
                                })
                                .catch((error) => {
                                    setLoadingInProgress('');
                                    console.error(error);
                                    setAlertMessage('Server scan failed');
                                    setAlertSeverity('error');
                                    setCloseAlert(false);
                                })
                        }}>
                            <Wifi/>
                            <Typography marginLeft={1}>Discover Servers</Typography>
                        </Button>
                    </Grid>
                </Grid>
                <Grid container sx={{paddingTop: '1rem'}} spacing={1} columns={{ xs: 4, sm: 8, md: 12 }}>
                    <Grid item xs={12} key={'title'}>
                        <Typography variant="h5" gutterBottom>
                            Filters
                        </Typography>
                    </Grid>
                    <Grid item xs={12} sm={4} md={4} key={'name-filter'}>
                        <Autocomplete
                            disablePortal
                            options={Object.keys(serversName)}
                            value={filters.name}
                            onChange={(e, value) => {
                                setFilters({
                                    ...filters,
                                    name: value || ''
                                })
                            }}
                            renderInput={(params) => <TextField
                                sx={styledTextFieldInput}
                                {...params}
                                variant={'filled'}
                                label={'Filter by name'}
                                value={filters.name}
                                onChange={(e) => {
                                    setFilters({
                                        ...filters,
                                        name: e.target.value
                                    })
                                }}
                            />}
                        />
                    </Grid>
                    <Grid item xs={12} sm={4} md={4} key={'feature-filter'}>
                        <Autocomplete
                            value={filters.features}
                            onChange={(event, newValue) => {
                                setFilters({
                                    ...filters,
                                    features: newValue
                                })
                            }}
                            inputValue={featureSearch}
                            onInputChange={(event, newInputValue) => {
                                setFeatureSearch(newInputValue)
                            }}
                            freeSolo
                            multiple
                            renderTags={(value: string[], getTagProps) =>
                                value.map((option: string, index: number) => (
                                    <Chip
                                        variant="outlined"
                                        label={option}
                                        {...getTagProps({ index })}
                                    />
                                ))
                            }
                            id="controllable-states-demo"
                            options={Object.keys(features)}
                            renderInput={(params) =>
                                <TextField sx={styledTextFieldInput} {...params} InputProps={{
                                    ...params.InputProps,
                                    sx: {
                                        '> input': {
                                            width: "100% !important"
                                        }
                                    }
                                }} variant={'filled'} placeholder="Search feature..." label="Features" />
                            }
                        />
                    </Grid>
                    <Grid item xs={12} sm={4} md={4} key={'status-filter'}>
                        <FormControl>
                            <InputLabel id="demo-simple-select-label">Status</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                value={filters.status}
                                label="Server Status"
                                variant={'filled'}
                                onChange={(e) => {
                                    setFilters({
                                        ...filters,
                                        status: e.target.value as any
                                    })
                                }}
                            >
                                <MenuItem value={'ANY'}>Any</MenuItem>
                                <MenuItem value={'ONLINE'}>Online</MenuItem>
                                <MenuItem value={'OFFLINE'}>Offline</MenuItem>
                                <MenuItem value={'UNKNOWN'}>Unknown</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
                <ServerGrid serversId={filteredServersId} onServerChange={(serverId: string) => {
                    navigate('/server/' + serverId)
                }}/>
                <Modal
                    keepMounted
                    open={openAddServer}
                    onClose={() => setOpenAddServer(false)}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                >
                    <div>
                        <AddServer handleClose={() => setOpenAddServer(false)}/>
                    </div>
                </Modal>
            </div>
        </CloseableModal>
    );
}

const styledTextFieldInput: SxProps<Theme> = {
    color: '#ffffff',
    borderColor: '#ffffff',
    '&::before': {
        borderColor: '#ffffff',
    },
    '&::after': {
        borderColor: '#ffffff',
    },
    '&:hover': {
        borderColor: '#ffffff',
    },
    width: '100%',
}