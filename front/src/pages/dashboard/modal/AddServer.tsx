import {
    Alert,
    Button,
    Snackbar,
    TextField,
    Typography,
    AlertColor,
    Grid, AccordionSummary, AccordionDetails, Accordion, Container
} from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import UploadIcon from '@mui/icons-material/Upload';
import React, {useState} from "react";
import {useSelector} from "react-redux";
import {getIsConnectedSelector} from "../../../store/sila/websocket/selector";
import CloseableModal from "../../../components/common/CloseableModal";
import {getBackEndpoint} from "../../../utils/endpoint";
import {SxProps} from "@mui/system";
import {Theme} from "@mui/material/styles";
import {ServerHostPortCertificate} from "../../../generated/silaModels";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

export interface AddServerProps {
    handleClose: () => void;
}

export default function AddServer(props: AddServerProps): JSX.Element {
    const [serverHost, setServerHost] = useState<string>('127.0.0.1');
    const [serverPort, setServerPort] = useState<number>(50052);
    const [serverCertificate, setServerCertificate] = useState<string>('');
    const [closeAlert, setCloseAlert] = useState<boolean>(true);
    const [loadingInProgress, setLoadingInProgress] = useState<string>('');
    const [alertMessage, setAlertMessage] = useState<{severity: AlertColor, title: string, description: string} | undefined>(undefined);
    const isWsConnected: boolean = useSelector(getIsConnectedSelector);
    let fileReader: FileReader;

    function handleFileChosen(files: FileList | null) {
        if (!files || !files.length) {
            return;
        }
        fileReader = new FileReader();
        fileReader.onloadend = handleFileRead;
        fileReader.readAsText(files[0]);
    }

    function handleFileRead() {
        if (typeof fileReader.result === 'string') {
            setServerCertificate(fileReader.result)
        } else {
            // todo handle arraybuffer
        }
        fileReader = new FileReader();
    }

    return (
        <CloseableModal id={'add-server-modal'} title={'Add SiLA Server'} handleClose={props.handleClose} gridStyle={{maxWidth: 'md', maxHeight: '600px', height: '100%'}}>
            <>
                <Typography variant="h5" gutterBottom>
                    Add server
                </Typography>
                <Grid container spacing={1} columns={{ xs: 4, sm: 8, md: 8 }}>
                    <Snackbar
                        open={!closeAlert}
                        autoHideDuration={12000}
                        onClose={() => {
                            setCloseAlert(true)
                        }}
                    >
                        <Container>
                            <Alert
                                onClose={() => {
                                    setCloseAlert(true)
                                }}
                                severity={alertMessage?.severity}
                                sx={{
                                    maxWidth: 'sm',
                                    '@media (max-width: 700px)': {
                                        '& > *': {
                                            paddingRight: '0px',
                                            paddingLeft: '0px'
                                        }
                                    }
                                }}
                            >
                                <Container sx={{padding: 0}}>
                                    {alertMessage?.title}
                                </Container>
                                {((alertMessage?.description) && (
                                    <Container sx={{marginTop: '1rem', padding: 0}}>
                                        <Accordion sx={{
                                            color: 'text.secondary',
                                            backgroundColor: "transparent",
                                        }} onClick={(e) => {
                                            e.preventDefault();
                                        }}>
                                            <AccordionSummary expandIcon={<ExpandMoreIcon/>} sx={{
                                                backgroundColor: "initial",
                                                color: 'text.secondary',
                                            }}>
                                                <Typography fontSize={'inherit'}>Details</Typography>
                                            </AccordionSummary>
                                            <AccordionDetails sx={{color: 'text.secondary', overflow: 'scroll',  maxHeight: '300px', whiteSpace: 'pre-line'}}>
                                                <Typography fontSize={'inherit'}>
                                                    {alertMessage?.description}
                                                </Typography>
                                            </AccordionDetails>
                                        </Accordion>
                                    </Container>
                                ))}
                            </Alert>
                        </Container>
                    </Snackbar>
                    <Snackbar
                        open={loadingInProgress.length > 0}
                        autoHideDuration={20000}
                    >
                        <Alert
                            severity={'info'}
                        >
                            {loadingInProgress}
                        </Alert>
                    </Snackbar>
                    <Grid item xs={4} sm={6} md={6} key={'server-ip'}>
                        <TextField
                            sx={styledTextFieldInput}
                            variant={'filled'}
                            id={'server-add-ip-input'}
                            label={'Server Host'}
                            value={serverHost}
                            onChange={(txt) => {
                                setServerHost(txt.target.value)
                            }}
                        />
                    </Grid>
                    <Grid item xs={4} sm={2} md={2} key={'server-port'}>
                        <TextField
                            sx={styledTextFieldInput}
                            variant={'filled'}
                            id={'server-add-port-input'}
                            label={'Server Port'}
                            type={"number"}
                            aria-valuemin={0}
                            aria-valuemax={65535}
                            value={serverPort}
                            onChange={(txt) => {
                                const number = parseInt(txt.target.value);
                                if (isFinite(number)) {
                                    setServerPort(number)
                                }
                            }}
                        />
                    </Grid>
                    <Grid item xs={4} sm={8} md={8} key={'server-certificate'}>
                        <TextField
                            sx={styledTextFieldInput}
                            variant={'filled'}
                            multiline
                            maxRows={8}
                            id={'server-add-certificate-input'}
                            label={'Server Certificate'}
                            helperText={'The server certificate must be provided for self-signed or untrusted certificate.'}
                            value={serverCertificate}
                            onChange={(txt) => {
                                setServerCertificate(txt.target.value)
                            }}
                        />
                    </Grid>
                    <Grid item xl={1} key={'server-load-certificate'}>
                        <Button id={'load-certificate-btn'} variant="contained" color="primary" component="label" sx={{marginBottom: '8px', height: '100%'}}>
                            <UploadIcon/>
                            <Typography marginLeft={1}>Load certificate</Typography>
                            <input
                                type="file"
                                accept=".cer,.pem,.cert,text/plain"
                                onChange={e => handleFileChosen(e.target.files)}
                                hidden
                            />
                        </Button>
                    </Grid>
                    <Grid item xl={1} key={'server-search'}>
                        <Button sx={{marginBottom: '8px', height: '100%'}} id={'search-server-btn'} variant="contained" color="primary"
                                disabled={!isWsConnected}
                                onClick={() => {
                                    setLoadingInProgress('Server addition in progress');
                                    fetch(getBackEndpoint('/api/servers'), {
                                        method: 'POST', // or 'PUT'
                                        headers: {
                                            'Content-Type': 'application/json',
                                        },
                                        body: JSON.stringify({
                                            host: serverHost,
                                            port: serverPort,
                                            certificate: serverCertificate.length === 0 ? null : serverCertificate
                                        } as ServerHostPortCertificate)
                                    })
                                        .then(async (data) => {
                                            setLoadingInProgress('');
                                            if (!data.ok) {
                                                setAlertMessage({severity: 'error', title: 'Server addition failed', description: await data?.text?.() || ''});
                                                setCloseAlert(false);
                                                return;
                                            }
                                            setAlertMessage({severity: 'success', title: 'Server added successfully', description: ''});
                                            setCloseAlert(false);
                                        })
                                        .catch((error) => {
                                            setAlertMessage({severity: 'error', title: 'Server addition failed', description: error});
                                            console.error(error);
                                            setCloseAlert(false);
                                        })
                                }}
                        >
                            <AddIcon/>
                            <Typography marginLeft={1}>Add server</Typography>
                        </Button>
                    </Grid>
                </Grid>
            </>
        </CloseableModal>
    );
}

const styledTextFieldInput: SxProps<Theme> = {
    color: '#ffffff',
    borderColor: '#ffffff',
    '&::before': {
        borderColor: '#ffffff',
    },
    '&::after': {
        borderColor: '#ffffff',
    },
    '&:hover': {
        borderColor: '#ffffff',
    },
    width: '100%',
}