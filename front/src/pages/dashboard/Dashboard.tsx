import React, {useMemo} from 'react';
import {
    Button, Modal, Typography
} from '@mui/material/';
import ServerGrid from "../../components/server/ServerGrid";
import {useSelector} from "react-redux";
import {getServersSelector} from "../../store/sila/manager/server/selector";
import {getServerBookmarksSelector} from "../../store/front/server/bookmark/selector";
import {ServerBookmarkKeyValueState} from "../../store/front/server/bookmark/types";
import {ServerKeyValue} from "../../store/sila/manager/server/types";
import {useNavigate} from "react-router-dom";
import ServerManager from "./modal/ServerManager";
import CallManager from "./modal/CallManager";
import {Dns, VideoSettings} from "@mui/icons-material";
import {Grid} from "@mui/material";

export default function Dashboard(): JSX.Element {
    const navigate = useNavigate();
    const serversReal: ServerKeyValue = useSelector(getServersSelector);
    const serverBookmark: ServerBookmarkKeyValueState = useSelector(getServerBookmarksSelector);
    const [open, setOpen] = React.useState<boolean>(false);
    const [openCallManager, setOpenCallManager] = React.useState<boolean>(false);

    const ServerManagerMemo = useMemo(
        // must be wrapped in div
        // see https://stackoverflow.com/questions/56347839/material-ui-v4-0-1-warning-expected-an-element-that-can-hold-a-ref
        () => (
            <div><ServerManager handleClose={() => setOpen(false)} serversReal={serversReal}/></div>
        ), [setOpen, serversReal]
    );

    const CallManagerMemo = useMemo(
        () => (
            <div><CallManager handleClose={() => setOpenCallManager(false)} serversReal={serversReal}/></div>
        ), [setOpenCallManager, serversReal]
    );

    return (
        <>
            <Typography variant="h1" component="h2" gutterBottom>
                Dashboard
            </Typography>
                <Grid container spacing={2} marginTop={2} width={'auto'}>
                    <Grid item>
                        <Button variant="contained" color="primary"
                                id={"open-server-manager-btn"}
                                onClick={() => setOpen(true)}
                        >
                            <Dns/>
                            <Typography marginLeft={1}>Manage SiLA Servers</Typography>
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button variant="contained" color="primary"
                                id={"open-call-manager-btn"}
                                onClick={() => setOpenCallManager(true)}
                        >
                            <VideoSettings/>
                            <Typography marginLeft={1}>Manage tasks</Typography>
                        </Button>
                    </Grid>
                    <Grid item xs={12}>
                        <ServerGrid serversId={Object.keys(serversReal).filter((id) => serverBookmark[id])} onServerChange={(serverId: string) => {navigate('/server/' + serverId)}}/>
                    </Grid>
                </Grid>
            <Modal
                open={open}
                onClose={() => setOpen(false)}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                {ServerManagerMemo}
            </Modal>
            <Modal
                open={openCallManager}
                onClose={() => setOpenCallManager(false)}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                {CallManagerMemo}
            </Modal>
        </>
    );
}