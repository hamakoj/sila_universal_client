import React from 'react';
import {
    Box, Typography
} from '@mui/material/';

export default function Tools(): JSX.Element {
    return (
        <Box>
            <Typography variant="h1" component="h1" gutterBottom>
                Tools
            </Typography>
            <Typography variant="h2" component="h2" gutterBottom>
                Feature viewer
            </Typography>
            <Box>
                Future functionality
            </Box>
            <Typography variant="h2" component="h2" gutterBottom>
                Feature builder
            </Typography>
            <Box>
                Future functionality
            </Box>
        </Box>
    );
}