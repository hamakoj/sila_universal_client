import React from 'react';
import {
    Box, Typography
} from '@mui/material';

export default function Workspace(): JSX.Element {
    return (
        <Box>
            <Typography variant="h1" component="h2" gutterBottom>
                Workspace
            </Typography>
            <Box>
                Future functionality
            </Box>
            <Box>
                The workspace will allow users to execute commands, properties and receive responses relatively to the current workspace.
            </Box>
        </Box>
    );
}