import React from 'react';
import puppeteer, {Browser, ElementHandle, Page} from 'puppeteer';
import fs from "fs";
import path from "path";

const sleep = (ms: number) => new Promise(r => setTimeout(r, ms));

async function expectTry(times: number, delayMs: number, func: () => void): Promise<void> {
  let tries = 0;
  while (true) {
    try {
      await func();
      return ;
    } catch (e) {
      ++times
      if (tries >= times) {
        throw e;
      }
      await sleep(delayMs)
    }
  }
}


async function scrollToMakeElementVisible(elementHandle: ElementHandle<Element>) {
  await elementHandle.evaluate((el) => {
    function closestScrollableParent(element: Element) {
      if (!element) return null;

      let currentElement = element.parentElement;
      while (currentElement && currentElement !== document.body) {
        const style = window.getComputedStyle(currentElement);
        const overflow = style.getPropertyValue('overflow') + style.getPropertyValue('overflow-y');

        if (overflow.includes('scroll') || overflow.includes('auto')) {
          return currentElement;
        }

        currentElement = currentElement.parentElement;
      }

      return document.documentElement; // Fallback to documentElement if no other scrollable parent found
    }

    const scrollableParent = closestScrollableParent(el);
    if (scrollableParent) {
      const elementRect = el.getBoundingClientRect();
      const parentRect = scrollableParent.getBoundingClientRect();

      // Scroll to make the element visible in the parent's viewport
      scrollableParent.scrollTo({
        top: elementRect.top + scrollableParent.scrollTop - parentRect.top,
      });
    }
  });
}
async function pageWaitForOneScrollableVisibleXPath(page: Page, selector: string): Promise<ElementHandle<Element> | undefined> {
  const element: ElementHandle<Node> | null = await page.waitForXPath(selector, {visible: false});
  if (element) {
    await scrollToMakeElementVisible(element as ElementHandle<Element>);
  }
  return pageWaitForOneVisibleXPath(page, selector);
}

async function pageWaitForOneVisibleXPath(page: Page, selector: string): Promise<ElementHandle<Element> | undefined> {
  const visibleElement: ElementHandle<Node> | null = await page.waitForXPath(selector, {visible: true});
  if (visibleElement) {
    return visibleElement as ElementHandle<Element>;
  }
  return undefined;
}


async function getAllParents(elementHandle: ElementHandle<Element>): Promise<ElementHandle<HTMLElement>[]> {
  const allParents: ElementHandle<HTMLElement>[] = [];
  let currentElement = elementHandle;

  while (true) {
    const parentElement = await currentElement.evaluateHandle((element) => element.parentElement);
    if (!parentElement || (await (parentElement as any).evaluate((element: any) => !element))) {
      break;
    }
    allParents.push(parentElement as ElementHandle<HTMLElement>);
    currentElement = parentElement as ElementHandle<HTMLElement>;
  }

  return allParents;
}

async function findSubChildByXPath(elementHandle: ElementHandle<Element>, xpath: string): Promise<ElementHandle<Element> | null> {
  const parentElements = await getAllParents(elementHandle);
  for (const parentElement of parentElements) {
    const match = await parentElement.$x(xpath);
    if (match.length > 0) {
      return match[0] as ElementHandle<Element>;
    }
  }

  return null;
}

async function openSelectServer(page: Page) {
  const openServerManagerButton = await pageWaitForOneVisibleXPath(page, " //*[@id=\"open-server-manager-btn\"]");
  await openServerManagerButton?.click();
  await sleep(1000); // wait end of transition
  await pageWaitForOneVisibleXPath(page,"//p[contains(string(), \"SiLA Server Manager\")]");
  const uscTestServer = await pageWaitForOneVisibleXPath(page,"//p[contains(string(), \"USCTestServer\")]");
  await uscTestServer?.click();
}

async function goToServerInfoPage(page: Page) {
  const serverPropertiesButton = await page.waitForSelector('#sidebar-current-server-configuration');
  await serverPropertiesButton?.click();
  await expectTry(20, 1000, async () => {
    await expect(page.url().endsWith('/info')).toBe(true)
  });
}

async function selectAllFeatures(page: Page) {
  const selectAllFeaturesButton = await pageWaitForOneVisibleXPath(page, "//button[text()='Select all']");
  await selectAllFeaturesButton?.click();
}

async function goToServerProperties(page: Page) {
  const serverPropertiesButton = await page.waitForSelector('#sidebar-current-server-properties');
  await serverPropertiesButton?.click();
}

async function goToServerCommands(page: Page) {
  const serverPropertiesButton = await page.waitForSelector('#sidebar-current-server-commands');
  await serverPropertiesButton?.click();
}

async function selectObservableFeatureService(page: Page) {
  const selectNoneFeaturesButton = await pageWaitForOneVisibleXPath(page, "//button[text()='Select none']");
  await selectNoneFeaturesButton?.click();
  const featuresCheckbox = await page.$x("//*[@id='simple-tabpanel-1']/div/div/div[2]/div/label/span/input")
  await expect(featuresCheckbox.length).toBe(18)
  for (const checkbox of featuresCheckbox) {
    const checked = await (await checkbox.getProperty("checked")).jsonValue();
    await expect(checked).toBeFalsy()
  }
  await (featuresCheckbox?.[13] as ElementHandle<Element>).click(); // observable service
  const checked = await (await featuresCheckbox[13].getProperty("checked")).jsonValue();
  await expect(checked).toBe(true);
}

async function runUptimeObservableProperty(page: Page) {
  const upTimeProperty = await pageWaitForOneScrollableVisibleXPath(page, "//h6[text()=\"Uptime\"]");
  await upTimeProperty?.click();
  const readPropertyButton = await findSubChildByXPath(upTimeProperty as ElementHandle<Element>, "//button[text()='Read once']");
  await readPropertyButton?.click();
  await sleep(5000); // wait to receive value
  await page.screenshot({path: 'observable_property_value.jpg'});
  const value = await pageWaitForOneVisibleXPath(page, "//input[@type='number' and @disabled]");
  if (value) {
    const inputValue = parseInt((await page.evaluate((x) => (x as any).value || -1, value)));
    await expect(inputValue).toBeGreaterThanOrEqual(0);
  } else {
    throw new Error("No property value")
  }
}

async function runServerTypeUnobservableProperty(page: Page) {
  const serverTypeProperty = await pageWaitForOneScrollableVisibleXPath(page, "//h6[text()=\"Server Type\"]");
  await serverTypeProperty?.click();
  const readPropertyButton = await findSubChildByXPath(serverTypeProperty as ElementHandle<Element>,  "//button[text()='Read once']");
  await readPropertyButton?.click();
  await sleep(5000); // wait to receive value
  await page.screenshot({path: 'unobservable_property_value.jpg'});
  const value = await pageWaitForOneVisibleXPath(page, "//input[@type='text' and @disabled]");
  if (value) {
    const serverName = await page.evaluate((x) => (x as any).value || '', value);
    await expect(serverName).toMatch('USCTestServer');
  } else {
    throw new Error("No property value")
  }
}

async function runIntegerLoopBackUnobservableCommand(page: Page) {
  const integerCommand = await pageWaitForOneScrollableVisibleXPath(page, "//h6[text()=\"Integer\"]");
  await integerCommand?.click();
  const executeCommandButton = await findSubChildByXPath(integerCommand as ElementHandle<Element>,  "//button[text()='Execute']");
  await executeCommandButton?.click();
  await sleep(5000); // wait to receive value
  await page.screenshot({path: 'unobservable_command_value.jpg'});
  const value = await pageWaitForOneVisibleXPath(page, "//input[@disabled]");  // todo reduce scope
  if (value) {
    const inputValue = parseInt((await page.evaluate((x) => (x as any).value || -1, value)));
    await expect(inputValue).toBe(1);
  } else {
    throw new Error("No response value")
  }
}
async function runCountToObservableCommand(page: Page) {
  const countToCommand = await pageWaitForOneScrollableVisibleXPath(page, "//h6[text()=\"Count To\"]");
  await countToCommand?.click();
  const executeCommandButton = await findSubChildByXPath(countToCommand as ElementHandle<Element>,  "//button[text()='Execute']");
  await executeCommandButton?.click();
  await sleep(5000); // wait to receive value
  await page.screenshot({path: 'observable_command_value.jpg'});
  const value = await pageWaitForOneVisibleXPath(page, "//input[@disabled]"); // todo reduce scope
  if (value) {
    const response = await page.evaluate((x) => (x as any).value || '', value);
    await expect(response).toMatch('00:00:00');
  } else {
    throw new Error("No response value")
  }
}
describe('Page content check', () => {
  let page:Page;
  let browser:Browser;
  let serverCertificate: string | null;

  beforeAll(async () => {
    // to fix [0403/015546.303053:ERROR:zygote_host_impl_linux.cc(89)] Running as root without --no-sandbox is not supported. See https://crbug.com/638180.
    browser = await puppeteer.launch({
      headless: true,
      args:['--no-sandbox'],
      defaultViewport: {
        width: 1680,
        height: 1050,
      }
    });
    page = await browser.newPage();
    await page.setDefaultTimeout(60000);
    await page.setDefaultNavigationTimeout(60000);
    await page.goto('http://localhost:8080/');
    serverCertificate = null;
  });

  it('should find server certificate"', async () => {
    const content = fs.readFileSync(path.resolve(__dirname, './../../../cert.pem'));
    expect(content.toString().startsWith("-----BEGIN CERTIFICATE-----")).toBeTruthy();
    serverCertificate = content.toString();
  });

  it('should add server"', async () => {
    const openServerManagerButton = await page.waitForSelector("#open-server-manager-btn");
    expect(openServerManagerButton?.click).toBeDefined()
    await openServerManagerButton?.click();

    await new Promise((r) => setTimeout(r, 1000));

    await page.screenshot({path: 'server_manager.jpg'});

    const openServerAddButton = await page.waitForSelector("#add-server-manager-btn");
    expect(openServerAddButton?.click).toBeDefined()
    await openServerAddButton?.click();

    await new Promise((r) => setTimeout(r, 1000));

    await page.screenshot({path: 'add_server_modal.jpg'});

    const ipInput = await page.waitForSelector("#server-add-ip-input");
    expect(ipInput?.type).toBeDefined()
    const portInput = await page.waitForSelector("#server-add-port-input");
    expect(portInput?.type).toBeDefined()

    await ipInput?.click({
      clickCount: 3
    })

    const ip = '127.0.0.1';
    await ipInput?.type(ip)
    expect((await ipInput?.evaluate(el => el.textContent)) === ip);

    await portInput?.click({
      clickCount: 3
    })

    const port = '50052';
    await portInput?.type(port)
    expect((await portInput?.evaluate(el => el.textContent)) === port);

    if (serverCertificate) {
      const certificateInput = await page.waitForSelector("#server-add-certificate-input");
      expect(certificateInput?.focus).toBeDefined()
      await certificateInput?.focus();

      await new Promise((r) => setTimeout(r, 2000));

      await certificateInput?.type(serverCertificate)
      expect((await certificateInput?.evaluate(el => el.textContent)) === serverCertificate);
    }
    await page.screenshot({path: 'server_add_form.jpg'});

    const addServerButton = await page.waitForXPath("(//*[@id=\"search-server-btn\" and not(@disabled)])");
    expect(addServerButton?.focus).toBeDefined()
    await (addServerButton as ElementHandle)?.focus();
    await page.screenshot({path: 'server_add_button_focus.jpg'});
    await (addServerButton as ElementHandle)?.click();
    await page.keyboard.press('Escape')
    await page.screenshot({path: 'server_wait.jpg'});
    await page.waitForXPath("//p[contains(., \"USCTestServer\")]")
    //close modal
    await openServerManagerButton?.click();
  });

  it('should open the server manager', async () => {
    await openSelectServer(page)
  });

  it('should open the server info page', async () => {
    await goToServerInfoPage(page);
  });

  it('should select observable feature service', async () => {
    await selectObservableFeatureService(page);
  });

  it('should read Up time observable property', async () => {
    await goToServerProperties(page)
    await runUptimeObservableProperty(page);
  });

  it('should select all features', async () => {
    await goToServerInfoPage(page);
    await selectAllFeatures(page);
  });

  it('should read Server Name unobservable property', async () => {
    await goToServerProperties(page)
    await runServerTypeUnobservableProperty(page);
  });

  it('should run Integer Loopback unobservable command', async () => {
    await goToServerCommands(page)
    await runIntegerLoopBackUnobservableCommand(page);
  });

  it('should run Count To observable command', async () => {
    await goToServerCommands(page)
    await runCountToObservableCommand(page);
  });

  afterAll(async() => {
    await page.close();
    await browser.close();
    serverCertificate = null;
  })
});