import React from "react";
import {Navigate, Outlet, Route, Routes} from "react-router-dom";
import Dashboard from "../pages/dashboard/Dashboard";
import Tools from "../pages/tools/Tools";
import Dev from "../pages/dev/Dev";
import Help from "../pages/help/Help";
import Workspace from "../pages/workspace/Workspace";
import Settings from "../pages/settings/Settings";
import RootLayout from "../pages/RootLayout";
import ServerIdOutlet from "../pages/server/ServerIdOutlet";

export default function RootRoutes(): JSX.Element {
    return (
        <Routes>
            <Route path="/" element={<RootLayout/>}>
                <Route index element={<Navigate to={"/dashboard"}/>}/>
                <Route path="dashboard/*" element={<Dashboard/>}/>
                <Route path="server" element={<Outlet/>}>
                    <Route index element={<Navigate to={"/dashboard"}/>}/>
                    <Route path=":id" element={<ServerIdOutlet/>}>
                        <Route index element={<Navigate to={"info"}/>}/>
                        <Route path="info" element={null}/>
                        <Route path="property" element={null}/>
                        <Route path="command" element={null}/>
                    </Route>
                </Route>
                <Route path="tools" element={<Tools/>}/>
                <Route path="dev" element={<Dev/>}/>
                <Route path="help" element={<Help/>}/>
                <Route path="workspace" element={<Workspace/>}/>
                <Route path="settings" element={<Settings/>}/>
            </Route>
        </Routes>
    )
}