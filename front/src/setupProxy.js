const { createProxyMiddleware } = require('http-proxy-middleware');

//only for dev
module.exports = app => {
    app.use(
        createProxyMiddleware(
            "/websocketsila",
            {
                target: 'http://localhost:8080',
                ws: true
            })
    );
    app.use(
        createProxyMiddleware(
            "/api",
            {
                target: 'http://localhost:8080',
                ws: false
            })
    );
    app.use(
        /* proxy for usc plugin */
        createProxyMiddleware(
            ['**/main.js'],
            {
                target: 'http://localhost:8080',
                ws: false
            })
    );
}
