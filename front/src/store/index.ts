import {applyMiddleware, createStore} from "redux";
import createSagaMiddleware from "redux-saga";
import logger from "redux-logger";

import rootReducer, {AppState} from "./rootReducer";
import {rootSaga} from "./rootSaga";
import {debounce} from "@mui/material";
import {initialServerFeatureState} from "./front/server/feature/reducer";
import {initialServerOpenState} from "./front/server/open/reducer";
import {initialActiveServerCallsState} from "./front/server/activeCall/reducer";
import {initialAppSettingsState} from "./front/app/setting/reducer";
import {initialDevState} from "./front/app/dev/reducer";
import merge from 'deepmerge';
import {initialAppUserState} from "./front/app/user/reducer";

const PersistedStateLocalStorageKey = "usc_persisted_state";

export function removePersistedUscState() {
    localStorage.removeItem(PersistedStateLocalStorageKey);
}

function saveToLocalStorage(state: AppState) {
    try {
        // todo compare the previous and new state and check if an updated is required
        const serialisedState = JSON.stringify({
            serverFeature: state.serverFeature,
            serverOpen: state.serverOpen,
            // todo could be added in the future but needs to keep track of the eventual divergence between the previous and current datatype
            //commandParameterInput: state.commandParameterInput,
            appSettings: state.appSettings,
            serverActiveCall: state.serverActiveCall,
            dev: state.dev,
            appUser: state.appUser
        });
        localStorage.setItem(PersistedStateLocalStorageKey, serialisedState);
    } catch (e) {
        console.warn(e);
    }
}

function loadFromLocalStorage() {
    try {
        const serialisedState = localStorage.getItem(PersistedStateLocalStorageKey);
        if (!serialisedState) {
            return undefined;
        }
        const loadedState = JSON.parse(serialisedState);
        // todo clear app user expired access token on load
        // load initial states before the one saved in case new properties got added since the last save
        const initialState = {
            serverFeature: initialServerFeatureState,
            serverOpen: initialServerOpenState,
            appSettings: initialAppSettingsState,
            serverActiveCall: initialActiveServerCallsState,
            dev: initialDevState,
            appUser: initialAppUserState,
        };
        return merge(initialState, loadedState);
    } catch (e) {
        console.warn(e);
        return undefined;
    }
}

// Create the saga middleware
const sagaMiddleware = createSagaMiddleware();

let isTest = false;
try {
    // @ts-ignore
    isTest = process?.env?.NODE_ENV === 'test';
} catch (ignored) {}

// Mount it on the Store
const store = createStore(
    rootReducer,
    loadFromLocalStorage(),
    (!isTest) ? applyMiddleware(sagaMiddleware, logger) : applyMiddleware(sagaMiddleware)
);
// update localstorage on store change
store.subscribe(debounce(() => saveToLocalStorage(store.getState()), 1000));

// Run the saga
sagaMiddleware.run(rootSaga);

export default store;