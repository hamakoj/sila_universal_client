import { combineReducers } from "redux";

import serverReducer from "./sila/manager/server/reducer";
import commandResponseReducer from "./front/server/response/reducer";
import commandInputReducer from "./front/server/callInput/reducer";
import wsReducer from "./sila/websocket/reducer";
import serverFeatureReducer from "./front/server/feature/reducer";
import serverBookmarkReducer from "./front/server/bookmark/reducer";
import serverOpenReducer from "./front/server/open/reducer";
import serverMetaReducer from "./front/server/meta/reducer";
import appSettingsReducer from "./front/app/setting/reducer";
import serverActiveCallReducer from "./front/server/activeCall/reducer";
import runningCallReducer from "./sila/runningCall/reducer";
import devReducer from "./front/app/dev/reducer";
import pluginReducer from "./front/app/plugin/reducer";
import appUserReducer from "./front/app/user/reducer";

const rootReducer = combineReducers({
    server: serverReducer,
    ws: wsReducer,
    serverFeature: serverFeatureReducer,
    serverBookmark: serverBookmarkReducer,
    serverOpen: serverOpenReducer,
    serverMeta: serverMetaReducer,
    commandResponse: commandResponseReducer,
    callInput: commandInputReducer,
    appSettings: appSettingsReducer,
    serverActiveCall: serverActiveCallReducer,
    runningCall: runningCallReducer,
    dev: devReducer,
    appPlugins: pluginReducer,
    appUser: appUserReducer
});

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;