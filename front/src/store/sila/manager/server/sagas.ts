import axios, {AxiosResponse} from "axios";
import { all, call, put, StrictEffect, takeLatest } from "redux-saga/effects";

import {UscServer} from "../../../../generated/silaModels";
import {fetchServersFailure, fetchServersSuccess} from "./actions";
import {FETCH_SERVERS_REQUEST} from "./actionTypes";
import {ServerKeyValue} from "./types";
import {getBackEndpoint} from "../../../../utils/endpoint";

const getServers = ():Promise<AxiosResponse<ServerKeyValue>> => {
    return axios.get<ServerKeyValue>(getBackEndpoint("/api/back/servers"));
}

export function* fetchServersSaga(): Generator<StrictEffect, undefined, AxiosResponse<ServerKeyValue>> {
    try {
        const response = yield call(getServers);
        yield put(
            fetchServersSuccess({
                servers: Object.values<UscServer>(response.data),
            })
        );
    } catch (e: any) {
        yield put(
            fetchServersFailure({
                error: e.message,
            })
        );
    }
    return undefined;
}

export default function* serverSaga(): Generator<StrictEffect, undefined, AxiosResponse<ServerKeyValue>> {
    yield all([takeLatest(FETCH_SERVERS_REQUEST, fetchServersSaga)]);
    return undefined;
}
