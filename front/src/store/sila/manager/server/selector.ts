import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {UscFeature, UscServer} from "../../../../generated/silaModels";
import {ServerKeyValue, ServerState} from "./types";

const getPending = (state: AppState) => state.server.pending;

const getError = (state: AppState) => state.server.error;

const getServersState = (state: AppState) => state.server;
const getServers = (state: AppState) => state.server.servers;
const getServerById = (state: AppState, uuid: string) => uuid;

export const getPendingSelector: Selector<AppState, boolean> = createSelector(getPending, (pending) => pending);

export const getErrorSelector: Selector<AppState, string | null> = createSelector(getError, (error) => error);
export const getServersStateSelector : Selector<AppState, ServerState>= createSelector(getServersState, (state) => state);
export const getServersSelector : Selector<AppState, ServerKeyValue>= createSelector(getServers, (servers) => servers);
export const getServerByIdSelector: Selector<AppState, UscServer> = createSelector(
    [getServers, getServerById],
    (servers, serverUuid) => servers[serverUuid]
);

export const getServerFeaturesByIdSelector: Selector<AppState, UscFeature[]> = createSelector(
    [getServers, getServerById],
    (servers, serverUuid) => (servers[serverUuid]?.features || [])
);
