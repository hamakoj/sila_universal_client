import {
    FETCH_SERVERS_REQUEST,
    FETCH_SERVERS_FAILURE,
    FETCH_SERVERS_SUCCESS,
} from "./actionTypes";
import {
    FetchServersRequest,
    FetchServersSuccess,
    FetchServersSuccessPayload,
    FetchServersFailure,
    FetchServersFailurePayload,
} from "./types";

export const fetchServersRequest = (): FetchServersRequest => ({
    type: FETCH_SERVERS_REQUEST,
});

export const fetchServersSuccess = (
    payload: FetchServersSuccessPayload
): FetchServersSuccess => ({
    type: FETCH_SERVERS_SUCCESS,
    payload,
});

export const fetchServersFailure = (
    payload: FetchServersFailurePayload
): FetchServersFailure => ({
    type: FETCH_SERVERS_FAILURE,
    payload,
});