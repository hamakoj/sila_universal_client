import {
    FETCH_SERVERS_REQUEST,
    FETCH_SERVERS_SUCCESS,
    FETCH_SERVERS_FAILURE,
} from "./actionTypes";
import {UscServer} from "../../../../generated/silaModels";
import {WsServerAddition, WsServerDelete, WsServerUpdate} from "../../websocket/types";

export interface ServerKeyValue {
    [serverUuid: string]: UscServer;
}

export interface ServerState {
    servers: ServerKeyValue;
    pending: boolean;
    error: string | null;
}

export interface FetchServersSuccessPayload {
    servers: UscServer[];
}

export interface FetchServersFailurePayload {
    error: string;
}

export interface FetchServersRequest {
    type: typeof FETCH_SERVERS_REQUEST;
}

export type FetchServersSuccess = {
    type: typeof FETCH_SERVERS_SUCCESS;
    payload: FetchServersSuccessPayload;
};

export type FetchServersFailure = {
    type: typeof FETCH_SERVERS_FAILURE;
    payload: FetchServersFailurePayload;
};

export type ServerActions =
    | FetchServersRequest
    | FetchServersSuccess
    | FetchServersFailure
    | WsServerAddition
    | WsServerUpdate
    | WsServerDelete;