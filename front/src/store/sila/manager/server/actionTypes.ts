export const FETCH_SERVERS_REQUEST = "sila/manager/server/FETCH_SERVERS_REQUEST";
export const FETCH_SERVERS_SUCCESS = "sila/manager/server/FETCH_SERVERS_SUCCESS";
export const FETCH_SERVERS_FAILURE = "sila/manager/server/FETCH_SERVERS_FAILURE";