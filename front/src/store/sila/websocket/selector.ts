import {createSelector, Selector} from "reselect";
import {AppState} from "../../rootReducer";
import {Client} from "@stomp/stompjs";

const getIsConnected = (state: AppState) => state.ws.isConnected;
const getLostConnection = (state: AppState) => state.ws.lostConnection;
const getClient = (state: AppState) => state.ws.client;

export const getIsConnectedSelector: Selector<AppState, boolean> = createSelector(getIsConnected, (isConnected) => isConnected);

export const getLostConnectionSelector: Selector<AppState, boolean> = createSelector(getLostConnection, (lostConnection) => lostConnection);

export const getClientSelector: Selector<AppState, Client | null> = createSelector(getClient, (client) => client);
