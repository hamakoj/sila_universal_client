import {
    WS_CALL_COMPLETE, WS_CALL_ERROR, WS_CALL_EXEC_INFO, WS_CALL_INIT, WS_CALL_INTERMEDIATE,
    WS_CALL_PROGRESS,
    WS_CONNECTED, WS_DISCONNECTED, WS_PROPERTY_UPDATE,
    WS_SERVER_ADDITION, WS_SERVER_DELETE, WS_SERVER_UPDATE,
} from "./actionTypes";
import {
    ObservableCommandInit, ObservableExecutionInfo, ObservableIntermediateResponse, ObservablePropertyResponse,
    SiLAJavaCallCompleted, SiLAJavaCallErrored,
    SiLAJavaCallStarted,
    UscServer
} from "../../../generated/silaModels";
import {Client} from "@stomp/stompjs";

export interface WsState {
    client: Client | null;
    isConnected: boolean;
    lostConnection: boolean;
}

export interface WsServerAdditionPayload {
    server: UscServer;
}

export interface WsServerUpdatePayload {
    server: UscServer;
}

export interface WsServerDeletePayload {
    uuid: string;
}

export interface WsConnectedPayload {
    client: Client;
}

export interface WsDisconnectedPayload {
    error: string;
}

export type WsServerAddition = {
    type: typeof WS_SERVER_ADDITION;
    payload: WsServerAdditionPayload;
};

export type WsServerUpdate = {
    type: typeof WS_SERVER_UPDATE;
    payload: WsServerUpdatePayload;
};

export type WsServerDelete = {
    type: typeof WS_SERVER_DELETE;
    payload: WsServerDeletePayload;
};

export type WsConnected = {
    type: typeof WS_CONNECTED;
    payload: WsConnectedPayload;
};

export type WsDisconnected = {
    type: typeof WS_DISCONNECTED;
    payload: WsDisconnectedPayload;
};

export type WsCallProgress = {
    type: typeof WS_CALL_PROGRESS;
    payload: SiLAJavaCallStarted;
};

export type WsCallComplete = {
    type: typeof WS_CALL_COMPLETE;
    payload: SiLAJavaCallCompleted;
};

export type WsCallError = {
    type: typeof WS_CALL_ERROR;
    payload: SiLAJavaCallErrored;
};

export type WsCallInit = {
    type: typeof WS_CALL_INIT;
    payload: ObservableCommandInit;
};

export type WsCallExecInfo = {
    type: typeof WS_CALL_EXEC_INFO;
    payload: ObservableExecutionInfo;
};

export type WsCallIntermediate = {
    type: typeof WS_CALL_INTERMEDIATE;
    payload: ObservableIntermediateResponse;
};

export type WsPropertyUpdate = {
    type: typeof WS_PROPERTY_UPDATE;
    payload: ObservablePropertyResponse;
};


export type WsAction =
    | WsCallProgress
    | WsCallComplete
    | WsCallError
    | WsCallInit
    | WsCallExecInfo
    | WsCallIntermediate
    | WsPropertyUpdate
    | WsServerAddition
    | WsServerUpdate
    | WsServerDelete
    | WsConnected
    | WsDisconnected
