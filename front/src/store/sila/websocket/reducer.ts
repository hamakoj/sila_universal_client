import {WsAction, WsState} from "./types";
import {WS_CONNECTED, WS_DISCONNECTED} from "./actionTypes";

const initialState: WsState = {
    client: null,
    isConnected: false,
    lostConnection: false
};

export default function Reducer(state = initialState, action: WsAction): WsState {
    switch (action.type) {
        case WS_CONNECTED:
            return {
                ...state,
                isConnected: true,
                client: action.payload.client,
            };
        case WS_DISCONNECTED:
            const wasConnected = state.isConnected;
            return {
                ...state,
                client: null,
                isConnected: false,
                lostConnection: wasConnected ? true : state.lostConnection
            };
    }
    return state;
};