export const ADD_RUNNING_CALL = "sila/manager/runningCall/ADD_RUNNING_CALL";
export const ADD_RUNNING_CALLS = "sila/manager/runningCall/ADD_RUNNING_CALLS";
export const FETCH_RUNNING_CALLS_REQUEST = "sila/manager/runningCall/FETCH_RUNNING_CALLS_REQUEST";
export const FETCH_RUNNING_CALLS_SUCCESS = "sila/manager/runningCall/FETCH_RUNNING_CALLS_SUCCESS";
export const FETCH_RUNNING_CALLS_FAILURE = "sila/manager/runningCall/FETCH_RUNNING_CALLS_FAILURE";
