import axios, {AxiosResponse} from "axios";
import { all, call, put, StrictEffect, takeLatest } from "redux-saga/effects";

import {SiLAJavaSiLACall} from "../../../generated/silaModels";
import {FETCH_RUNNING_CALLS_REQUEST} from "./actionTypes";
import {fetchRunningCallsFailure, fetchRunningCallsSuccess} from "./actions";
import {getBackEndpoint} from "../../../utils/endpoint";

const getServers = ():Promise<AxiosResponse<SiLAJavaSiLACall[]>> => {
    return axios.get<SiLAJavaSiLACall[]>(getBackEndpoint("/api/calls/running"));
}

export function* fetchRunningCallsSaga(): Generator<StrictEffect, undefined, AxiosResponse<SiLAJavaSiLACall[]>> {
    try {
        const response = yield call(getServers);
        yield put(
            fetchRunningCallsSuccess({
                calls: Object.values<SiLAJavaSiLACall>(response.data),
            })
        );
    } catch (e: any) {
        yield put(
            fetchRunningCallsFailure({
                error: e.message,
            })
        );
    }
    return undefined;
}

export default function* runningCallsSaga(): Generator<StrictEffect, undefined, AxiosResponse<SiLAJavaSiLACall[]>> {
    yield all([takeLatest(FETCH_RUNNING_CALLS_REQUEST, fetchRunningCallsSaga)]);
    return undefined;
}
