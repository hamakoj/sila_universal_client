import {
    ADD_RUNNING_CALL,
    ADD_RUNNING_CALLS,
    FETCH_RUNNING_CALLS_FAILURE,
    FETCH_RUNNING_CALLS_SUCCESS
} from "./actionTypes";
import {
    AddRunningCallResponse,
    AddRunningCallResponses, FetchRunningCallsFailure, FetchRunningCallsFailurePayload,
    FetchRunningCallsSuccess, FetchRunningCallsSuccessPayload,
    RunningCallResponse,
    RunningCallResponses
} from "./types";

export const addRunningCall = (
    payload: RunningCallResponse
): AddRunningCallResponse => ({
    type: ADD_RUNNING_CALL,
    payload: payload
});

export const addRunningCalls = (
    payload: RunningCallResponses
): AddRunningCallResponses => ({
    type: ADD_RUNNING_CALLS,
    payload: payload
});

export const fetchRunningCallsSuccess = (
    payload: FetchRunningCallsSuccessPayload
): FetchRunningCallsSuccess => ({
    type: FETCH_RUNNING_CALLS_SUCCESS,
    payload,
});

export const fetchRunningCallsFailure = (
    payload: FetchRunningCallsFailurePayload
): FetchRunningCallsFailure => ({
    type: FETCH_RUNNING_CALLS_FAILURE,
    payload,
});