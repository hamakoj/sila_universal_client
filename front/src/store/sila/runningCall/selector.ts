import {createSelector, Selector} from "reselect";

import { AppState } from "../../rootReducer";
import {RunningCallState} from "./types";

const getRunningCall = (state: AppState) => state.runningCall;

export const getRunningCallSelector: Selector<AppState, RunningCallState> = createSelector(getRunningCall, (runningCall) => runningCall);