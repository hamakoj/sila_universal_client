import {
    ADD_RUNNING_CALL,
    ADD_RUNNING_CALLS,
    FETCH_RUNNING_CALLS_FAILURE,
    FETCH_RUNNING_CALLS_REQUEST,
    FETCH_RUNNING_CALLS_SUCCESS,
} from "./actionTypes";
import {
    WsCallComplete,
    WsCallError,
    WsCallExecInfo,
    WsCallInit,
    WsCallIntermediate,
    WsCallProgress, WsPropertyUpdate
} from "../websocket/types";
import {SiLAJavaSiLACall} from "../../../generated/silaModels";

export type CallUUIDMap = {[uuid: string]: RunningCall};

export interface RunningCallState {
    callUuidMap: CallUUIDMap
}

export interface RunningCall {
    id: number | string
    uuid: string;
    serverUuid: string;
    featureIdentifier: string;
    commandIdentifier: string;
    timestamp: Date;
}

export interface RunningCallResponse {
    response: SiLAJavaSiLACall
}

export interface RunningCallResponses {
    responses: SiLAJavaSiLACall[]
}

export interface CallIdentifier {
    serverUuid: string;
    featureIdentifier: string;
    commandIdentifier: string;
}

export type AddRunningCallResponse = {
    type: typeof ADD_RUNNING_CALL;
    payload: RunningCallResponse;
};

export type AddRunningCallResponses = {
    type: typeof ADD_RUNNING_CALLS;
    payload: RunningCallResponses;
};

export interface FetchRunningCallsSuccessPayload {
    calls: SiLAJavaSiLACall[];
}

export interface FetchRunningCallsFailurePayload {
    error: string;
}

export interface FetchRunningCallsRequest {
    type: typeof FETCH_RUNNING_CALLS_REQUEST;
}

export type FetchRunningCallsSuccess = {
    type: typeof FETCH_RUNNING_CALLS_SUCCESS;
    payload: FetchRunningCallsSuccessPayload;
};

export type FetchRunningCallsFailure = {
    type: typeof FETCH_RUNNING_CALLS_FAILURE;
    payload: FetchRunningCallsFailurePayload;
};

export type RunningCallActions =
    | FetchRunningCallsRequest
    | FetchRunningCallsSuccess
    | FetchRunningCallsFailure
    | WsCallProgress
    | WsCallComplete
    | WsCallError
    | WsCallInit
    | WsCallExecInfo
    | WsCallIntermediate
    | WsPropertyUpdate
    | AddRunningCallResponse
    | AddRunningCallResponses;