import {CallUUIDMap, RunningCall, RunningCallActions, RunningCallState} from "./types";
import {
    WS_CALL_COMPLETE,
    WS_CALL_ERROR,
    WS_CALL_EXEC_INFO,
    WS_CALL_INTERMEDIATE,
    WS_CALL_PROGRESS,
    WS_PROPERTY_UPDATE
} from "../websocket/actionTypes";
import {SiLAJavaSiLACall} from "../../../generated/silaModels";
import {
    ADD_RUNNING_CALL,
    ADD_RUNNING_CALLS,
    FETCH_RUNNING_CALLS_FAILURE,
    FETCH_RUNNING_CALLS_REQUEST, FETCH_RUNNING_CALLS_SUCCESS
} from "./actionTypes";

const initialState: RunningCallState = {
    callUuidMap: {},
};

function silaCallToRunningCall(silaCall: SiLAJavaSiLACall): RunningCall {
    return {
        id: silaCall.identifier,
        uuid: silaCall.identifier,
        serverUuid: silaCall.serverId,
        featureIdentifier: silaCall.fullyQualifiedFeatureId,
        commandIdentifier: silaCall.callId,
        timestamp: new Date()
    }
}

export default function Reducer(state: RunningCallState = initialState, action: RunningCallActions): RunningCallState {
    switch (action.type) {
        case FETCH_RUNNING_CALLS_REQUEST:
            return {
                ...state,
            };
        case FETCH_RUNNING_CALLS_SUCCESS:
            return {
                ...state,
                callUuidMap: action.payload.calls.reduce((acc: CallUUIDMap, call) => {
                    acc[call.identifier] = silaCallToRunningCall(call)
                    return acc;
                }, state.callUuidMap)
            }
        case FETCH_RUNNING_CALLS_FAILURE:
            return {
                ...state,
            };
        case WS_CALL_PROGRESS: {
            return {
                ...state,
                callUuidMap: {
                    ...state.callUuidMap,
                    [action.payload.siLACall.identifier]: silaCallToRunningCall(action.payload.siLACall)
                }
            }
        }
        case WS_CALL_COMPLETE: {
            const {[action.payload.siLACall.identifier]: remove, ...rest}: CallUUIDMap = state.callUuidMap;
            return {
                ...state,
                callUuidMap: rest,
            }
        }
        case WS_CALL_ERROR: {
            const {[action.payload.siLACall.identifier]: remove, ...rest}: CallUUIDMap = state.callUuidMap;
            return {
                ...state,
                callUuidMap: rest,
            }
        }
        case WS_CALL_EXEC_INFO: {
            return {
                ...state,
                callUuidMap: {
                    ...state.callUuidMap,
                    [action.payload.baseCall.identifier]: silaCallToRunningCall(action.payload.baseCall)
                }
            }
        }
        case WS_CALL_INTERMEDIATE: {
            return {
                ...state,
                callUuidMap: {
                    ...state.callUuidMap,
                    [action.payload.baseCall.identifier]: silaCallToRunningCall(action.payload.baseCall)
                }
            }
        }
        case WS_PROPERTY_UPDATE: {
            return {
                ...state,
                callUuidMap: {
                    ...state.callUuidMap,
                    [action.payload.baseCall.identifier]: silaCallToRunningCall(action.payload.baseCall)
                }
            }
        }
        case ADD_RUNNING_CALL: {
            return {
                ...state,
                callUuidMap: {
                    ...state.callUuidMap,
                    [action.payload.response.identifier]: silaCallToRunningCall(action.payload.response)
                }
            }
        }
        case ADD_RUNNING_CALLS: {
            return {
                ...state,
                callUuidMap: action.payload.responses.reduce((acc: CallUUIDMap, call) => {
                    acc[call.identifier] = silaCallToRunningCall(call)
                    return acc;
                }, state.callUuidMap)
            }
        }
    }
    return state;
};