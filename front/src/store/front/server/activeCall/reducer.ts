import {ServerActiveCallActions, ServerActiveCalls} from "./types";
import {SET_ACTIVE_CALL} from "./actionTypes";

export const initialActiveServerCallsState: ServerActiveCalls = {};

export default function Reducer(state: ServerActiveCalls = initialActiveServerCallsState, action: ServerActiveCallActions): ServerActiveCalls {
    switch (action.type) {
        case SET_ACTIVE_CALL:
            return {
                ...state,
                [action.payload.serverUuid]: action.payload
            }
    }
    return state;
};