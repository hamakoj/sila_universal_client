import {
    FullyQualifiedCallId,
    ServerCurrentOpenSet,
} from "./types";
import {
    SET_ACTIVE_CALL
} from "./actionTypes";

export const serverActiveCall = (
    payload: FullyQualifiedCallId
): ServerCurrentOpenSet => ({
    type: SET_ACTIVE_CALL,
    payload: payload
});

