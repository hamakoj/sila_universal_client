import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {FullyQualifiedCallId, ServerActiveCalls} from "./types";

const getServerActiveCall = (state: AppState): ServerActiveCalls => state.serverActiveCall;
const getServerIdActiveCall = (state: AppState, serverUuid: string) => serverUuid;

export const getServerActiveCallSelector: Selector<AppState, ServerActiveCalls> = createSelector(getServerActiveCall, (serverOpen) => serverOpen);

export const getServerIdActiveCallSelector: Selector<AppState, FullyQualifiedCallId> = createSelector(
    [getServerActiveCall, getServerIdActiveCall],
    (activeCalls, serverUuid) => activeCalls[serverUuid]
);