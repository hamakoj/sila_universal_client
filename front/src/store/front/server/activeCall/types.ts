import {
    SET_ACTIVE_CALL,
} from "./actionTypes";

export interface ServerActiveCalls {
    [serverUuid: string]: FullyQualifiedCallId;
}

export type CallType = 'command' | 'property';

export interface FullyQualifiedCallId {
    serverUuid: string,
    fullyQualifiedFeatureId: string,
    callType: CallType,
    callId: string
}

export type ServerCurrentOpenSet = {
    type: typeof SET_ACTIVE_CALL;
    payload: FullyQualifiedCallId;
};

export type ServerActiveCallActions =
    | ServerCurrentOpenSet;
