import {
    WsPropertyUpdate
} from "../../../sila/websocket/types";
import {
    ProtoSubscribe_ParametersConstraints_Responses
} from "../../../../generated/ParameterConstraintsProvider";
import {ProtoSubscribe_PausedCommands_Responses} from "../../../../generated/PauseController";
import {
    ProtoSubscribe_AuthorizationProvider_Responses
} from "../../../../generated/AuthorizationConfigurationService";
import {ProtoSubscribe_ConfiguredSiLAClients_Responses} from "../../../../generated/ConnectionConfigurationService";
import {ProtoSubscribe_RecoverableErrors_Responses} from "../../../../generated/ErrorRecoveryService";
import {ProtoSubscribe_IsLocked_Responses} from "../../../../generated/LockController";
import {
    FETCH_CALL_AFFECTED_BY_METADATA_FAILURE,
    FETCH_CALL_AFFECTED_BY_METADATA_REQUEST,
    FETCH_CALL_AFFECTED_BY_METADATA_SUCCESS
} from "./actionTypes";

export interface ServersMetaState {
    [serverUuid: string]: ServerMeta;
}

export interface ServerMetadataAffectingCalls {
    [fullyQualifiedMetadata: string]: string[];
}

export interface ServerCallsAffectedByMetadata {
    [fullyQualifiedCall: string]: string[];
}

export interface ServerMeta {
    runtimeConstraints?: ProtoSubscribe_ParametersConstraints_Responses;
    pausedCommands?: ProtoSubscribe_PausedCommands_Responses;
    authorizationProvider?: ProtoSubscribe_AuthorizationProvider_Responses;
    configuredSiLAClients?: ProtoSubscribe_ConfiguredSiLAClients_Responses;
    recoverableErrors?: ProtoSubscribe_RecoverableErrors_Responses;
    isLocked?: ProtoSubscribe_IsLocked_Responses;
    callAffectedByMetadata?: ServerCallsAffectedByMetadata
}

export interface FetchCallAffectedByMetadataRequestPayload {
    serverUuid: string
}

export interface FetchCallAffectedByMetadataSuccessPayload {
    serverUuid: string;
    serverMetadataAffectingCalls: ServerMetadataAffectingCalls;
}

export interface FetchCallAffectedByMetadataFailurePayload {
    serverUuid: string;
    error: string;
}

export interface FetchCallAffectedByMetadataRequest {
    type: typeof FETCH_CALL_AFFECTED_BY_METADATA_REQUEST;
    payload: FetchCallAffectedByMetadataRequestPayload;
}

export type FetchCallAffectedByMetadataSuccess = {
    type: typeof FETCH_CALL_AFFECTED_BY_METADATA_SUCCESS;
    payload: FetchCallAffectedByMetadataSuccessPayload;
};

export type FetchCallAffectedByMetadataFailure = {
    type: typeof FETCH_CALL_AFFECTED_BY_METADATA_FAILURE;
    payload: FetchCallAffectedByMetadataFailurePayload;
};

export type ServerMetaActions =
    | WsPropertyUpdate
    | FetchCallAffectedByMetadataRequest
    | FetchCallAffectedByMetadataSuccess
    | FetchCallAffectedByMetadataFailure;