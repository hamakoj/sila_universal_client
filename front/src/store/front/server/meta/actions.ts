import {
    FETCH_CALL_AFFECTED_BY_METADATA_FAILURE,
    FETCH_CALL_AFFECTED_BY_METADATA_REQUEST,
    FETCH_CALL_AFFECTED_BY_METADATA_SUCCESS
} from "./actionTypes";
import {
    FetchCallAffectedByMetadataFailure,
    FetchCallAffectedByMetadataFailurePayload,
    FetchCallAffectedByMetadataRequest,
    FetchCallAffectedByMetadataRequestPayload, FetchCallAffectedByMetadataSuccess,
    FetchCallAffectedByMetadataSuccessPayload
} from "./types";

export const fetchCallAffectedByMetadataRequest = (
    payload: FetchCallAffectedByMetadataRequestPayload
): FetchCallAffectedByMetadataRequest => ({
    type: FETCH_CALL_AFFECTED_BY_METADATA_REQUEST,
    payload,
});

export const fetchCallAffectedByMetadataSuccess = (
    payload: FetchCallAffectedByMetadataSuccessPayload
): FetchCallAffectedByMetadataSuccess => ({
    type: FETCH_CALL_AFFECTED_BY_METADATA_SUCCESS,
    payload,
});

export const fetchCallAffectedByMetadataFailure = (
    payload: FetchCallAffectedByMetadataFailurePayload
): FetchCallAffectedByMetadataFailure => ({
    type: FETCH_CALL_AFFECTED_BY_METADATA_FAILURE,
    payload,
});
