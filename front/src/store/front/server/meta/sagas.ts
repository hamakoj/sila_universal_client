import axios, {AxiosResponse} from "axios";
import {all, call, put, StrictEffect, takeEvery} from "redux-saga/effects";

import {getBackEndpoint} from "../../../../utils/endpoint";
import {FETCH_CALL_AFFECTED_BY_METADATA_REQUEST} from "./actionTypes";
import {FetchCallAffectedByMetadataRequest, ServerMetadataAffectingCalls} from "./types";
import {fetchCallAffectedByMetadataFailure, fetchCallAffectedByMetadataSuccess} from "./actions";


const getServerCallAffectedByMetadataSaga = ({ payload }: FetchCallAffectedByMetadataRequest):Promise<AxiosResponse<ServerMetadataAffectingCalls>> => {
    return axios.get<ServerMetadataAffectingCalls>(getBackEndpoint("api/servers/" + payload.serverUuid  + "/features/metadatas/fcpAffectedBy"));
}

export function* fetchServerCallAffectedByMetadataSaga(request: FetchCallAffectedByMetadataRequest): Generator<StrictEffect, undefined, AxiosResponse<ServerMetadataAffectingCalls>> {
    try {
        const response = yield call(getServerCallAffectedByMetadataSaga, request);
        yield put(
            fetchCallAffectedByMetadataSuccess({
                serverMetadataAffectingCalls: response.data,
                serverUuid: request.payload.serverUuid
            })
        );
    } catch (e: any) {
        yield put(
            fetchCallAffectedByMetadataFailure({
                error: e.message,
                serverUuid: request.payload.serverUuid
            })
        );
    }
    return undefined;
}

export default function* serverMetaSaga(): Generator<StrictEffect, undefined, AxiosResponse<ServerMetadataAffectingCalls>> {
    yield all([takeEvery(FETCH_CALL_AFFECTED_BY_METADATA_REQUEST, fetchServerCallAffectedByMetadataSaga)]);
    return undefined;
}