import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {ServerCallsAffectedByMetadata, ServerMeta, ServersMetaState} from "./types";
import {ProtoSubscribe_ParametersConstraints_Responses} from "../../../../generated/ParameterConstraintsProvider";
import {ProtoSubscribe_PausedCommands_Responses} from "../../../../generated/PauseController";
import {ProtoSubscribe_AuthorizationProvider_Responses} from "../../../../generated/AuthorizationConfigurationService";
import {ProtoSubscribe_ConfiguredSiLAClients_Responses} from "../../../../generated/ConnectionConfigurationService";
import {ProtoSubscribe_RecoverableErrors_Responses} from "../../../../generated/ErrorRecoveryService";
import {ProtoSubscribe_IsLocked_Responses} from "../../../../generated/LockController";

const getServerMeta = (state: AppState) => state.serverMeta;
const getServerMetaById = (state: AppState, uuid: string) => uuid;

export const getServerMetaSelector: Selector<AppState, ServersMetaState> = createSelector(getServerMeta, (serverMeta) => serverMeta);

export const getServerMetaByIdSelector: Selector<AppState, ServerMeta> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers[serverUuid]
);

export const getServerMetaConstraintByIdSelector: Selector<AppState, ProtoSubscribe_ParametersConstraints_Responses | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.runtimeConstraints
);

export const getServerMetaPausedCommandsByIdSelector: Selector<AppState, ProtoSubscribe_PausedCommands_Responses | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.pausedCommands
);

export const getServerMetaAuthorizationProvidersByIdSelector: Selector<AppState, ProtoSubscribe_AuthorizationProvider_Responses | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.authorizationProvider
);

export const getServerMetaConfiguredSiLAClientsByIdSelector: Selector<AppState, ProtoSubscribe_ConfiguredSiLAClients_Responses | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.configuredSiLAClients
);

export const getServerMetaRecoverableErrorsByIdSelector: Selector<AppState, ProtoSubscribe_RecoverableErrors_Responses | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.recoverableErrors
);

export const getServerMetaIsLockedByIdSelector: Selector<AppState, ProtoSubscribe_IsLocked_Responses | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.isLocked
);

export const getServerMetaAffectedCallsByIdSelector: Selector<AppState, ServerCallsAffectedByMetadata | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.callAffectedByMetadata
);