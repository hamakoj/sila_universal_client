import {ServerCallsAffectedByMetadata, ServerMetaActions, ServersMetaState,} from "./types";
import {WS_PROPERTY_UPDATE} from "../../../sila/websocket/actionTypes";
import {ProtoSubscribe_ParametersConstraints_Responses} from "../../../../generated/ParameterConstraintsProvider";
import {ProtoSubscribe_PausedCommands_Responses} from "../../../../generated/PauseController";
import {
    ProtoSubscribe_AuthorizationProvider_Responses
} from "../../../../generated/AuthorizationConfigurationService";
import {ProtoSubscribe_ConfiguredSiLAClients_Responses} from "../../../../generated/ConnectionConfigurationService";
import {ProtoSubscribe_RecoverableErrors_Responses} from "../../../../generated/ErrorRecoveryService";
import {ProtoSubscribe_IsLocked_Responses} from "../../../../generated/LockController";
import {
    FETCH_CALL_AFFECTED_BY_METADATA_FAILURE,
    FETCH_CALL_AFFECTED_BY_METADATA_SUCCESS
} from "./actionTypes";

export const initialServerOpenState: ServersMetaState = {
};

export default function Reducer(state: ServersMetaState = initialServerOpenState, action: ServerMetaActions): ServersMetaState {
    switch (action.type) {
        case WS_PROPERTY_UPDATE: {
            const call = action.payload.baseCall;
            const serverId = call.serverId;
            // todo check feature version
            if (call.fullyQualifiedFeatureId.startsWith("org.silastandard/core.commands/ParameterConstraintsProvider") && call.callId === "ParametersConstraints") {
                const constraints: ProtoSubscribe_ParametersConstraints_Responses = action.payload.value as ProtoSubscribe_ParametersConstraints_Responses;
                return {
                    ...state,
                    [serverId]: {
                        ...state[serverId],
                        runtimeConstraints: {
                            ...constraints
                        }
                    }
                }
            }
            if (call.fullyQualifiedFeatureId.startsWith("org.silastandard/core.commands/PauseController") && call.callId === "PausedCommands") {
                const pausedCommands: ProtoSubscribe_PausedCommands_Responses = action.payload.value as ProtoSubscribe_PausedCommands_Responses;
                return {
                    ...state,
                    [serverId]: {
                        ...state[serverId],
                        pausedCommands: {
                            ...pausedCommands
                        }
                    }
                }
            }
            if (call.fullyQualifiedFeatureId.startsWith("org.silastandard/core/AuthorizationConfigurationService") && call.callId === "AuthorizationProvider") {
                const authorizationProvider: ProtoSubscribe_AuthorizationProvider_Responses = action.payload.value as ProtoSubscribe_AuthorizationProvider_Responses;
                return {
                    ...state,
                    [serverId]: {
                        ...state[serverId],
                        authorizationProvider: {
                            ...authorizationProvider
                        }
                    }
                }
            }
            if (call.fullyQualifiedFeatureId.startsWith("org.silastandard/core/ConnectionConfigurationService") && call.callId === "ConfiguredSiLAClients") {
                const configuredSiLAClients: ProtoSubscribe_ConfiguredSiLAClients_Responses = action.payload.value as ProtoSubscribe_ConfiguredSiLAClients_Responses;
                return {
                    ...state,
                    [serverId]: {
                        ...state[serverId],
                        configuredSiLAClients: {
                            ...configuredSiLAClients
                        }
                    }
                }
            }
            if (call.fullyQualifiedFeatureId.startsWith("org.silastandard/core/ErrorRecoveryService") && call.callId === "RecoverableErrors") {
                const recoverableErrors: ProtoSubscribe_RecoverableErrors_Responses = action.payload.value as ProtoSubscribe_RecoverableErrors_Responses;
                return {
                    ...state,
                    [serverId]: {
                        ...state[serverId],
                        recoverableErrors: {
                            ...recoverableErrors
                        }
                    }
                }
            }
            if (call.fullyQualifiedFeatureId.startsWith("org.silastandard/core/LockController") && call.callId === "IsLocked") {
                const isLocked: ProtoSubscribe_IsLocked_Responses = action.payload.value as ProtoSubscribe_IsLocked_Responses;
                return {
                    ...state,
                    [serverId]: {
                        ...state[serverId],
                        isLocked: {
                            ...isLocked
                        }
                    }
                }
            }
            return state;
        }
        case FETCH_CALL_AFFECTED_BY_METADATA_SUCCESS: {
            const {serverMetadataAffectingCalls, serverUuid} = action.payload;
            const callsAffectedByMetadata = Object.entries(serverMetadataAffectingCalls).reduce((acc: ServerCallsAffectedByMetadata, entry) => {
                const metadata = entry[0];
                const calls = entry[1];
                return calls.reduce((currentCallsAcc: ServerCallsAffectedByMetadata, call) => {
                    return {
                        ...currentCallsAcc,
                        [call]: [...(currentCallsAcc[call] || []), metadata]
                    }
                }, acc);
            }, {});

            return {
                ...state,
                [serverUuid]: {
                    ...state[serverUuid],
                    callAffectedByMetadata: {
                        ...callsAffectedByMetadata
                    }
                }
            }
        }
        case FETCH_CALL_AFFECTED_BY_METADATA_FAILURE: {
            // todo handle
            return {
                ...state,
            }
        }
    }
    return state;
};