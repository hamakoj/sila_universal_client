import {
    CommandsFormState,
    FormInputType,
    CallInputAction, SiLATypeValueMap,
} from "./types";
import {
    INIT_CALL_INPUT,
    PUSH_CALL_INPUT_LIST_ITEM,
    REMOVE_CALL_INPUT_LIST_ITEM,
    UPDATE_CALL_INPUT_ITEM
} from "./actionTypes";
import {ListValueType} from "../../../../components/server/call/dataTypeForm/type/List";

// todo parameters and metadata should be saved per server and not under the
//  same object to prevent rerender on command / paramers not after by the updated

const initialState: CommandsFormState = {
    parameters: {},
    metadatas: {}
};

function getTargetPropertyForType(inputFormType: FormInputType): keyof CommandsFormState {
    return (inputFormType === "PARAMETER") ? ('parameters') : ('metadatas');
}

export default function Reducer(state: CommandsFormState = initialState, action: CallInputAction): CommandsFormState {
    switch (action.type) {
        case UPDATE_CALL_INPUT_ITEM: {
            const targetProperty = getTargetPropertyForType(action.payload.formType);
            const targetId = action.payload.fullyQualifiedCommandId
            return {
                ...state,
                [targetProperty]: {
                    ...state[targetProperty],
                    [targetId]: {
                        ...state[targetProperty][targetId],
                        [action.payload.stateId]: action.payload.updatedValue
                    }
                }
            };
        } case INIT_CALL_INPUT: {
            const targetProperty = getTargetPropertyForType(action.payload.formType);
            const targetId = action.payload.fullyQualifiedCommandId

            return {
                ...state,
                [targetProperty]: {
                    ...state[targetProperty],
                    [targetId]: {
                        ...action.payload.state
                    }
                }
            };
        }
        case PUSH_CALL_INPUT_LIST_ITEM: {
            const targetProperty = getTargetPropertyForType(action.payload.formType);
            const targetId = action.payload.fullyQualifiedCommandId

            return {
                ...state,
                [targetProperty]: {
                    ...state[targetProperty],
                    [targetId]: {
                        ...state[targetProperty][targetId],
                        ...action.payload.valueToPush,
                        [action.payload.stateId]: [
                            ...((state[targetProperty][action.payload.fullyQualifiedCommandId] as SiLATypeValueMap)[action.payload.stateId] as ListValueType),
                            action.payload.stateIdToPush
                        ],
                    }
                }
            };
        }
        case REMOVE_CALL_INPUT_LIST_ITEM: {
            const targetProperty = getTargetPropertyForType(action.payload.formType);
            const targetId = action.payload.fullyQualifiedCommandId;

            // todo do immutably

            (state[targetProperty][targetId] as SiLATypeValueMap)[action.payload.stateId] =
                ((state[targetProperty][targetId] as SiLATypeValueMap)[action.payload.stateId] as ListValueType)
                    .filter((stateId: string) => stateId !== action.payload.stateIdToRemove);
            state[targetProperty][targetId] = Object.fromEntries(
                Object.entries(state[targetProperty][targetId])
                    .filter(([key]) => !key.startsWith(action.payload.stateIdToRemove))
            );
            return {
                ...state,
                [targetProperty]: {
                    ...state[targetProperty],
                    [targetId]: {
                        ...state[targetProperty][targetId]
                    }
                }
            }
        }
    }
    return state;
};