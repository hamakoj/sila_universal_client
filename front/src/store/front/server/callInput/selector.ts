import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {CallsMetadataFormState, CallsParameterFormState, CommandsFormState, SiLATypeValueState} from "./types";

const getCallInput = (state: AppState) => state.callInput;
const getCallInputParameters = (state: AppState) => state.callInput.parameters;
const getCallInputParametersById = (state: AppState, id: string) => id;
const getCallInputMetadatas = (state: AppState) => state.callInput.metadatas;
const getCallInputMetadatasById = (state: AppState, id: string) => id;

export const getCallInputSelector: Selector<AppState, CommandsFormState> = createSelector(getCallInput, (getCallInput) => getCallInput);
export const getCallInputParametersSelector: Selector<AppState, CallsParameterFormState> = createSelector(getCallInputParameters, (getCallInput) => getCallInput);
export const getCallInputParametersByIdSelector: Selector<AppState, SiLATypeValueState> = createSelector(
    [getCallInputParameters, getCallInputParametersById],
    (inputParameters, fullyQualifiedId) => inputParameters[fullyQualifiedId]
);
export const getCallInputMetadatasByIdSelector: Selector<AppState, SiLATypeValueState | undefined> = createSelector(
    [getCallInputMetadatas, getCallInputMetadatasById],
    (metatadas, fullyQualifiedId) => metatadas[fullyQualifiedId]
);
export const getCallInputMetadatasSelector: Selector<AppState, CallsMetadataFormState> = createSelector(getCallInputMetadatas, (getCallInput) => getCallInput);