export const PUSH_CALL_INPUT_LIST_ITEM = "front/server/callInput/PUSH_CALL_INPUT_LIST_ITEM";
export const REMOVE_CALL_INPUT_LIST_ITEM = "front/server/callInput/REMOVE_CALL_INPUT_LIST_ITEM";
export const UPDATE_CALL_INPUT_ITEM = "front/server/callInput/UPDATE_CALL_INPUT_ITEM";
export const INIT_CALL_INPUT = "front/server/callInput/INIT_CALL_INPUT";