import {
    FETCH_SERVERS_BOOKMARK_FAILURE,
    FETCH_SERVERS_BOOKMARK_REQUEST, FETCH_SERVERS_BOOKMARK_SUCCESS, ADD_SERVER_BOOKMARK, DELETE_SERVER_BOOKMARK,
    SET_SERVER_BOOKMARK,
} from "./actionTypes";
import {UscServer} from "../../../../generated/silaModels";

export interface ServerBookmarkKeyValueState {
    [serverUuid: string]: boolean;
}

export interface ServerBookmarkSwitch {
    serverUuid: string;
    bookmarked: boolean;
}

export interface ServerBookmarkAdd {
    serverUuid: string;
}

export interface ServerBookmarkDelete {
    serverUuid: string;
}

export type ServerBookmark = {
    type: typeof SET_SERVER_BOOKMARK;
    payload: ServerBookmarkSwitch;
};

export interface FetchServersBookmarkSuccessPayload {
    servers: UscServer[];
}

export interface FetchServersBookmarkFailurePayload {
    error: string;
}

export interface FetchServersBookmarkRequest {
    type: typeof FETCH_SERVERS_BOOKMARK_REQUEST;
}

export type FetchServersBookmarkSuccess = {
    type: typeof FETCH_SERVERS_BOOKMARK_SUCCESS;
    payload: FetchServersBookmarkSuccessPayload;
};

export type FetchServersBookmarkFailure = {
    type: typeof FETCH_SERVERS_BOOKMARK_FAILURE;
    payload: FetchServersBookmarkFailurePayload;
};

export type ServersBookmarkAdd = {
    type: typeof ADD_SERVER_BOOKMARK;
    payload: ServerBookmarkAdd;
};

export type ServersBookmarkDelete = {
    type: typeof DELETE_SERVER_BOOKMARK;
    payload: ServerBookmarkDelete;
};


export type ServerBookmarkActions =
    | FetchServersBookmarkRequest
    | FetchServersBookmarkSuccess
    | FetchServersBookmarkFailure
    | ServersBookmarkAdd
    | ServersBookmarkDelete
    | ServerBookmark;