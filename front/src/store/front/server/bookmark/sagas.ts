import axios, {AxiosResponse} from "axios";
import {all, call, put, StrictEffect, takeLatest, takeEvery} from "redux-saga/effects";

import {UscServer} from "../../../../generated/silaModels";
import {
    FETCH_SERVERS_BOOKMARK_REQUEST,
    ADD_SERVER_BOOKMARK,
    DELETE_SERVER_BOOKMARK,
    SET_SERVER_BOOKMARK
} from "./actionTypes";
import {fetchServersBookmarkFailure, fetchServersBookmarkSuccess} from "./actions";
import { getBackEndpoint } from "../../../../utils/endpoint";

// todo implement client id
const getUserBookmarks = ():Promise<AxiosResponse<UscServer[]>> => {
    return axios.get<UscServer[]>(getBackEndpoint("/api/clients/59944a3d-de03-4965-bbcb-180559bf676b/servers"));
}

const addUserBookmarks = (e: {payload: {serverUuid: string}}):Promise<AxiosResponse<void>> => {
    return axios.post<void>(getBackEndpoint("/api/clients/59944a3d-de03-4965-bbcb-180559bf676b/servers/" + e.payload.serverUuid));
}

const deleteUserBookmarks = (e: {payload: {serverUuid: string}}):Promise<AxiosResponse<void>> => {
    return axios.delete<void>(getBackEndpoint("/api/clients/59944a3d-de03-4965-bbcb-180559bf676b/servers/" + e.payload.serverUuid));
}

export function* fetchUserBookmarksSaga(): Generator<StrictEffect, undefined, AxiosResponse<UscServer[]>> {
    try {
        const response = yield call(getUserBookmarks);
        yield put(
            fetchServersBookmarkSuccess({
                servers: Object.values<UscServer>(response.data),
            })
        );
    } catch (e: any) {
        yield put(
            fetchServersBookmarkFailure({
                error: e.message,
            })
        );
    }
    return undefined;
}

export function* serverBookmarkDelete(t: any): Generator<StrictEffect, undefined, AxiosResponse<void>> {
    yield call(deleteUserBookmarks, t);
    yield put({ type: SET_SERVER_BOOKMARK, payload: {serverUuid: t.payload.serverUuid, bookmarked: false}})
    return undefined;
}

export function* serverBookmarkAdd(t: any): Generator<StrictEffect, undefined, AxiosResponse<void>> {
    yield call(addUserBookmarks, t);
    yield put({ type: SET_SERVER_BOOKMARK, payload: {serverUuid: t.payload.serverUuid, bookmarked: true}})
    return undefined;
}

export default function* bookmarkSaga() {
    yield takeEvery(ADD_SERVER_BOOKMARK, serverBookmarkAdd)
    yield takeEvery(DELETE_SERVER_BOOKMARK, serverBookmarkDelete);
    yield all([takeLatest(FETCH_SERVERS_BOOKMARK_REQUEST, fetchUserBookmarksSaga)]);
}
