import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {ServerBookmarkKeyValueState} from "./types";

const getServerBookmarks = (state: AppState) => state.serverBookmark;

export const getServerBookmarksSelector: Selector<AppState, ServerBookmarkKeyValueState> = createSelector(getServerBookmarks, (serverBookmarks) => serverBookmarks);