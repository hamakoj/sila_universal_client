import {
    ServerBookmarkActions,
    ServerBookmarkKeyValueState,
} from "./types";
import {
    FETCH_SERVERS_BOOKMARK_FAILURE,
    FETCH_SERVERS_BOOKMARK_SUCCESS,
    SET_SERVER_BOOKMARK,
} from "./actionTypes";

const initialState: ServerBookmarkKeyValueState = {};

export default function Reducer(state: ServerBookmarkKeyValueState = initialState, action: ServerBookmarkActions): ServerBookmarkKeyValueState {
    switch (action.type) {
        case SET_SERVER_BOOKMARK:
            return {
                ...state,
                [action.payload.serverUuid]: action.payload.bookmarked
            };
        case FETCH_SERVERS_BOOKMARK_SUCCESS:
            return (
                action.payload.servers.reduce((acc,server) => ({
                    ...acc,
                    [server.serverUuid]: true
                }), state)
        )
        case FETCH_SERVERS_BOOKMARK_FAILURE:
            // todo handle
    }
    return state;
};