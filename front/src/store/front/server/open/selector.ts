import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {ServerOpenKeyValueState, ServersTabs} from "./types";

const getServerOpen = (state: AppState) => state.serverOpen;

const getCurrentServerOpen = (state: AppState): string | undefined => state.serverOpen.serverUuidOpen;
const getServerOpenTabs = (state: AppState): ServersTabs => state.serverOpen.serversTabs;

export const getServerOpenSelector: Selector<AppState, ServerOpenKeyValueState> = createSelector(getServerOpen, (serverOpen) => serverOpen);
export const getServerOpenTabsSelector: Selector<AppState, ServersTabs> = createSelector(getServerOpenTabs, (serverOpen) => serverOpen);
export const getCurrentServerOpenSelector: Selector<AppState, string | undefined> = createSelector(getCurrentServerOpen, (serverOpen) => serverOpen);