import {
    SET_ACTIVE_SERVER_TAB, CLOSE_SERVER_TAB,
} from "./actionTypes";

export type Tab = 'info' | 'command' | 'property';

export interface ServersTabs {
    [serverUuid: string]: Tab;
}

export interface ServerOpenKeyValueState {
    serverUuidOpen?: string;
    serversTabs: ServersTabs;
}

export interface ServerTab {
    serverUuid: string;
    tab?: Tab;
}

export interface ServerUUID {
    serverUuid: string;
}

export type ServerOpenRemoveTab = {
    type: typeof CLOSE_SERVER_TAB;
    payload: ServerUUID;
};

export type ServerCurrentOpenSet = {
    type: typeof SET_ACTIVE_SERVER_TAB;
    payload: ServerTab;
};

export type ServerOpenActions =
    | ServerOpenRemoveTab
    | ServerCurrentOpenSet;