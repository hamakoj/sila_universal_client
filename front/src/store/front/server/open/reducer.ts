import {ServerOpenActions, ServerOpenKeyValueState,} from "./types";
import {SET_ACTIVE_SERVER_TAB, CLOSE_SERVER_TAB} from "./actionTypes";

export const initialServerOpenState: ServerOpenKeyValueState = {
    serverUuidOpen: undefined,
    serversTabs: {}
};

export default function Reducer(state: ServerOpenKeyValueState = initialServerOpenState, action: ServerOpenActions): ServerOpenKeyValueState {
    switch (action.type) {
        case SET_ACTIVE_SERVER_TAB:
            if (action.payload.tab === undefined) {
                const { [action.payload.serverUuid]: remove, ...rest } = state.serversTabs
                return {
                    ...state,
                    serverUuidOpen: undefined,
                    serversTabs: {
                        ...rest
                    }
                }
            }
            return {
                ...state,
                serverUuidOpen: action.payload.serverUuid,
                serversTabs: {
                    ...state.serversTabs,
                    [action.payload.serverUuid]: action.payload.tab
                }
            }
        case CLOSE_SERVER_TAB:
            const { [action.payload.serverUuid]: remove, ...rest } = state.serversTabs
            return {
                ...state,
                serverUuidOpen: (state.serverUuidOpen === action.payload.serverUuid) ? undefined : state.serverUuidOpen,
                serversTabs: {
                    ...rest
                }
            }
    }
    return state;
};