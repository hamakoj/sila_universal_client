import {
    ServerCurrentOpenSet, ServerOpenRemoveTab,
    ServerTab, ServerUUID
} from "./types";
import {
    CLOSE_SERVER_TAB,
    SET_ACTIVE_SERVER_TAB
} from "./actionTypes";

export const serverCurrentOpenSet = (
    payload: ServerTab
): ServerCurrentOpenSet => ({
    type: SET_ACTIVE_SERVER_TAB,
    payload: payload
});

export const serverOpenRemoveTab = (
    payload: ServerUUID
): ServerOpenRemoveTab => ({
    type: CLOSE_SERVER_TAB,
    payload: payload
});
