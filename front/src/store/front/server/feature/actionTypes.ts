export const SERVER_FEATURE_SELECTION = "front/server/feature/SERVER_FEATURE_SELECTION";
export const SERVER_FEATURE_SELECTIONS = "front/server/feature/SERVER_FEATURE_SELECTIONS";
export const SELECT_ALL_SERVER_FEATURE = "front/server/feature/SELECT_ALL_SERVER_FEATURE";
export const UNSELECT_ALL_SERVER_FEATURE = "front/server/feature/UNSELECT_ALL_SERVER_FEATURE";
