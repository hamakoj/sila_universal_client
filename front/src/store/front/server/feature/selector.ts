import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {ServerFeatureKeyValueState} from "./types";

const getServerFeatures = (state: AppState) => state.serverFeature;

export const getServerFeaturesSelector: Selector<AppState, ServerFeatureKeyValueState> = createSelector(getServerFeatures, (serverFeatures) => serverFeatures);