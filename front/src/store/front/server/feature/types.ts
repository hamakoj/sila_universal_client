import {
    SELECT_ALL_SERVER_FEATURE,
    SERVER_FEATURE_SELECTION, SERVER_FEATURE_SELECTIONS, UNSELECT_ALL_SERVER_FEATURE
} from "./actionTypes";

export interface ServerFeatureKeyValueState {
    [serverUuid: string]: ServerFeatureSelectedKeyValue;
}

export interface ServerFeatureSelectedKeyValue {
    [featureIdentifier: string]: boolean;
}

export interface ServerFeatureSelectionUpdate {
    serverUuid: string;
    featureIdentifier: string;
    selected: boolean;
}

export interface ServerFeatureSelectionsUpdate {
    serverUuid: string;
    featuresSelection: ServerFeatureSelectedKeyValue;
}

export interface ServerIdentifier {
    serverUuid: string;
}

export type ServerFeatureSelection = {
    type: typeof SERVER_FEATURE_SELECTION;
    payload: ServerFeatureSelectionUpdate;
};

export type ServerFeatureSelections = {
    type: typeof SERVER_FEATURE_SELECTIONS;
    payload: ServerFeatureSelectionsUpdate;
};

export type SelectAllServerFeature = {
    type: typeof SELECT_ALL_SERVER_FEATURE;
    payload: ServerIdentifier;
};

export type UnselectAllServerFeature = {
    type: typeof UNSELECT_ALL_SERVER_FEATURE;
    payload: ServerIdentifier;
};

export type ServerFeatureActions =
    | ServerFeatureSelection
    | ServerFeatureSelections
    | SelectAllServerFeature
    | UnselectAllServerFeature;