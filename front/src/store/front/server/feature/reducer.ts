import {ServerFeatureActions, ServerFeatureKeyValueState} from "./types";
import {
    SELECT_ALL_SERVER_FEATURE,
    SERVER_FEATURE_SELECTION,
    SERVER_FEATURE_SELECTIONS,
    UNSELECT_ALL_SERVER_FEATURE
} from "./actionTypes";

export const initialServerFeatureState: ServerFeatureKeyValueState = {};

export default function Reducer(state: ServerFeatureKeyValueState = initialServerFeatureState, action: ServerFeatureActions): ServerFeatureKeyValueState {
    switch (action.type) {
        case SERVER_FEATURE_SELECTION:
            return {
                ...state,
                [action.payload.serverUuid]: {
                    ...state[action.payload.serverUuid],
                    [action.payload.featureIdentifier]: action.payload.selected
                }
            };
        case SERVER_FEATURE_SELECTIONS:
            return {
                ...state,
                [action.payload.serverUuid]: {
                    ...state[action.payload.serverUuid],
                    ...action.payload.featuresSelection
                }
            };
        case SELECT_ALL_SERVER_FEATURE:
            return {
                ...state,
                [action.payload.serverUuid]: Object.keys(state[action.payload.serverUuid] || {}).reduce((acc, featureId) => ({
                    ...acc,
                    [featureId]: true
                }), state[action.payload.serverUuid])
            };
        case UNSELECT_ALL_SERVER_FEATURE:
            return {
                ...state,
                [action.payload.serverUuid]: Object.keys(state[action.payload.serverUuid] || {}).reduce((acc, featureId) => ({
                    ...acc,
                    [featureId]: false
                }), state[action.payload.serverUuid])
            };
    }
    return state;
};