import axios, {AxiosResponse} from "axios";
import {all, call, put, StrictEffect, takeEvery} from "redux-saga/effects";

import {getBackEndpoint} from "../../../../utils/endpoint";
import {fetchResponseFromCacheFailure, fetchResponseFromCacheSuccess} from "./actions";
import {FETCH_RESPONSE_FROM_CACHE_REQUEST} from "./actionTypes";
import { FetchResponseFromCatchRequest} from "./types";
import {CachedCall} from "../../../../generated/silaModels";
import {
    wsCallExecInfo,
    wsCallInit,
    wsCallIntermediate,
    wsObservablePropertyUpdate
} from "../../../sila/websocket/actions";


const getCachedCall = ({ payload }: FetchResponseFromCatchRequest):Promise<AxiosResponse<CachedCall>> => {
    return axios.get<CachedCall>(getBackEndpoint("/api/calls/" + payload.callId + "/cache"));
}

export function* fetchResponseFromCacheSaga(request: FetchResponseFromCatchRequest): Generator<StrictEffect, undefined, AxiosResponse<CachedCall>> {
    try {
        const response = yield call(getCachedCall, request);
        if (response.data.siLACall) {
            yield put(
                fetchResponseFromCacheSuccess({
                    cachedResponse: response.data,
                    callId: request.payload.callId
                })
            );
            if (response.data.lastPropertyValue) {
                response.data.lastPropertyValue = JSON.parse(response.data.lastPropertyValue);
                yield put(
                    wsObservablePropertyUpdate({
                        baseCall: response.data.siLACall,
                        value: response.data.lastPropertyValue
                    })
                );
            }
            if (response.data.lastIntermediateResponse) {
                yield put(
                    wsCallIntermediate({
                        baseCall: response.data.siLACall,
                        responseType: response.data.lastIntermediateResponse
                    })
                );
            }
            if (response.data.lastCommandConfirmation) {
                yield put(
                    wsCallInit({
                        baseCall: response.data.siLACall,
                        command: response.data.lastCommandConfirmation
                    })
                );
            }
            if (response.data.lastCommandExecutionInfo) {
                yield put(
                    wsCallExecInfo({
                        baseCall: response.data.siLACall,
                        command: response.data.lastCommandExecutionInfo
                    })
                );
            }
        } else {
            yield put(
                fetchResponseFromCacheFailure({
                    error: 'No such task in cache',
                    callId: request.payload.callId
                })
            );
        }
    } catch (e: any) {
        yield put(
            fetchResponseFromCacheFailure({
                error: e.message,
                callId: request.payload.callId
            })
        );
    }
    return undefined;
}

export default function* callResponseSaga(): Generator<StrictEffect, undefined, AxiosResponse<CachedCall>> {
    yield all([takeEvery(FETCH_RESPONSE_FROM_CACHE_REQUEST, fetchResponseFromCacheSaga)]);
    return undefined;
}
