import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {CommandResponse, CommandResponseState} from "./types";

const getCommandResponse = (state: AppState) => state.commandResponse;
const getCommandResponseById = (state: AppState, fullId: string) => fullId;

export const getCommandResponseSelector: Selector<AppState, CommandResponseState> = createSelector(getCommandResponse, (commandResponse) => commandResponse);

export const getCommandResponseByIdSelector: Selector<AppState, CommandResponse[]> = createSelector(
    [getCommandResponse, getCommandResponseById],
    (responses, fullId) => Object.keys(responses.responseFullIdMap[fullId] || {})
        .map(uuid => responses.responseUuidMap[uuid])
);