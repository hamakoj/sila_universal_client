import {
    ADD_COMMAND_RESPONSE,
    ADD_COMMAND_RESPONSES,
    FETCH_RESPONSE_FROM_CACHE_FAILURE,
    FETCH_RESPONSE_FROM_CACHE_REQUEST,
    FETCH_RESPONSE_FROM_CACHE_SUCCESS,
    REMOVE_COMMAND_RESPONSE,
    REMOVE_COMMAND_RESPONSES,
} from "./actionTypes";
import {
    AddCommandResponse,
    AddCommandResponses,
    CommandIdentifier,
    CommandResponse,
    CommandResponseIdentifier,
    CommandResponses, FetchResponseFromCacheFailurePayload,
    FetchResponseFromCacheRequestPayload,
    FetchResponseFromCacheSuccessPayload, FetchResponseFromCatchFailure,
    FetchResponseFromCatchRequest, FetchResponseFromCatchSuccess,
    RemoveCommandResponse,
    RemoveCommandResponses
} from "./types";

export const addCommandResponse = (
    payload: CommandResponse
): AddCommandResponse => ({
    type: ADD_COMMAND_RESPONSE,
    payload: payload
});

export const addCommandResponses = (
    payload: CommandResponses
): AddCommandResponses => ({
    type: ADD_COMMAND_RESPONSES,
    payload: payload
});

export const removeCommandResponse = (
    payload: CommandResponseIdentifier
): RemoveCommandResponse => ({
    type: REMOVE_COMMAND_RESPONSE,
    payload: payload
});

export const removeCommandResponses = (
    payload: CommandIdentifier
): RemoveCommandResponses => ({
    type: REMOVE_COMMAND_RESPONSES,
    payload: payload
});

export const fetchResponseFromCacheRequest = (
    payload: FetchResponseFromCacheRequestPayload
): FetchResponseFromCatchRequest => ({
    type: FETCH_RESPONSE_FROM_CACHE_REQUEST,
    payload,
});

export const fetchResponseFromCacheSuccess = (
    payload: FetchResponseFromCacheSuccessPayload
): FetchResponseFromCatchSuccess => ({
    type: FETCH_RESPONSE_FROM_CACHE_SUCCESS,
    payload,
});

export const fetchResponseFromCacheFailure = (
    payload: FetchResponseFromCacheFailurePayload
): FetchResponseFromCatchFailure => ({
    type: FETCH_RESPONSE_FROM_CACHE_FAILURE,
    payload,
});
