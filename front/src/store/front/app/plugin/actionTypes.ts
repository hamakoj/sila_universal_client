
export const FETCH_PLUGINS_REQUEST = "front/app/plugin/FETCH_PLUGINS_REQUEST";
export const FETCH_PLUGINS_SUCCESS = "front/app/plugin/FETCH_PLUGINS_SUCCESS";
export const FETCH_PLUGINS_FAILURE = "front/app/plugin/FETCH_PLUGINS_FAILURE";
