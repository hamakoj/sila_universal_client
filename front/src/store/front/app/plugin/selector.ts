import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {AppPlugins} from "./types";

const getAppPlugins = (state: AppState) => state.appPlugins;

export const getAppPluginsSelector: Selector<AppState, AppPlugins> = createSelector(getAppPlugins, (appPlugins) => appPlugins);