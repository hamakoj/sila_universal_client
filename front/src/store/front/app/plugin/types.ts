import {
    FETCH_PLUGINS_FAILURE,
    FETCH_PLUGINS_REQUEST, FETCH_PLUGINS_SUCCESS,
} from "./actionTypes";
import {PluginModel} from "../../../../generated/silaModels";

export interface AppPlugins {
    plugins: PluginModel[]
}

export interface FetchPluginsSuccessPayload {
    plugins: PluginModel[];
}

export interface FetchPluginsFailurePayload {
    error: string;
}

export interface FetchPluginsRequest {
    type: typeof FETCH_PLUGINS_REQUEST;
}

export type FetchPluginsSuccess = {
    type: typeof FETCH_PLUGINS_SUCCESS;
    payload: FetchPluginsSuccessPayload;
};

export type FetchPluginsFailure = {
    type: typeof FETCH_PLUGINS_FAILURE;
    payload: FetchPluginsFailurePayload;
};

export type AppSettingActions =
    FetchPluginsRequest |
    FetchPluginsSuccess |
    FetchPluginsFailure;