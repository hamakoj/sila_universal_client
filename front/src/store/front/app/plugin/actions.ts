import {
    FetchPluginsFailure, FetchPluginsFailurePayload,
    FetchPluginsRequest, FetchPluginsSuccess, FetchPluginsSuccessPayload,
} from "./types";

import {
    FETCH_PLUGINS_FAILURE,
    FETCH_PLUGINS_REQUEST, FETCH_PLUGINS_SUCCESS,
} from "./actionTypes";

export const fetchPluginsRequest = (): FetchPluginsRequest => ({
    type: FETCH_PLUGINS_REQUEST,
});

export const fetchPluginsSuccess = (
    payload: FetchPluginsSuccessPayload
): FetchPluginsSuccess => ({
    type: FETCH_PLUGINS_SUCCESS,
    payload,
});

export const fetchPluginsFailure = (
    payload: FetchPluginsFailurePayload
): FetchPluginsFailure => ({
    type: FETCH_PLUGINS_FAILURE,
    payload,
});