import axios, {AxiosResponse} from "axios";
import { all, call, put, StrictEffect, takeLatest } from "redux-saga/effects";

import {PluginModel} from "../../../../generated/silaModels";
import {fetchPluginsFailure, fetchPluginsSuccess} from "./actions";
import {FETCH_PLUGINS_REQUEST} from "./actionTypes";
import {getBackEndpoint} from "../../../../utils/endpoint";

const getPlugins = ():Promise<AxiosResponse<PluginModel[]>> => {
    return axios.get<PluginModel[]>(getBackEndpoint("/api/plugin"));
}

export function* fetchPluginsSaga(): Generator<StrictEffect, undefined, AxiosResponse<PluginModel[]>> {
    try {
        const response = yield call(getPlugins);
        yield put(
            fetchPluginsSuccess({
                plugins: response.data,
            })
        );
    } catch (e: any) {
        yield put(
            fetchPluginsFailure({
                error: e.message,
            })
        );
    }
    return undefined;
}

export default function* serverSaga(): Generator<StrictEffect, undefined, AxiosResponse<PluginModel[]>> {
    yield all([takeLatest(FETCH_PLUGINS_REQUEST, fetchPluginsSaga)]);
    return undefined;
}
