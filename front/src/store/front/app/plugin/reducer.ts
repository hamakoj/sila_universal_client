import {AppSettingActions, AppPlugins} from "./types";
import {
    FETCH_PLUGINS_FAILURE,
    FETCH_PLUGINS_REQUEST,
    FETCH_PLUGINS_SUCCESS,
} from "./actionTypes";

const initialState: AppPlugins = {
    plugins: [],
};

export default function Reducer(state: AppPlugins = initialState, action: AppSettingActions): AppPlugins {
    switch (action.type) {
        case FETCH_PLUGINS_REQUEST:
            return {
                ...state,
            };
        case FETCH_PLUGINS_SUCCESS:
            return {
                ...state,
                plugins: action.payload.plugins
            };
        case FETCH_PLUGINS_FAILURE:
            // todo handle
            return {
                ...state,
            };
    }
    return state;
};