import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {DevState} from "./types";

const getDev = (state: AppState) => state.dev;

export const getDevSelector: Selector<AppState, DevState> = createSelector(getDev, (dev) => dev);