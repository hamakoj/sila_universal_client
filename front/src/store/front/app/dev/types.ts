import {
    ADD_DEV_PLUGIN,
    REMOVE_DEV_PLUGIN,
} from "./actionTypes";

export interface DevFrontPlugins {
    [devPluginUrl: string]: null
}

export interface DevState {
    frontPlugins: DevFrontPlugins;
}

export interface AddDevFrontPluginPayload {
    url: string;
}

export interface RemoveDevFrontPluginPayload {
    url: string;
}

export type AddDevFrontPlugin = {
    type: typeof ADD_DEV_PLUGIN;
    payload: AddDevFrontPluginPayload;
};

export type RemoveDevFrontPlugin = {
    type: typeof REMOVE_DEV_PLUGIN;
    payload: RemoveDevFrontPluginPayload;
};

export type DevActions =
    AddDevFrontPlugin |
    RemoveDevFrontPlugin;