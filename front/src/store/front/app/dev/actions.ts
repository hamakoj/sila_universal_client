import {ADD_DEV_PLUGIN, REMOVE_DEV_PLUGIN,} from "./actionTypes";
import {AddDevFrontPlugin, AddDevFrontPluginPayload, RemoveDevFrontPlugin, RemoveDevFrontPluginPayload} from "./types";

export const addDevFrontPlugin = (
    payload: AddDevFrontPluginPayload
): AddDevFrontPlugin => ({
    type: ADD_DEV_PLUGIN,
    payload: payload
});

export const removeDevFrontPlugin = (
    payload: RemoveDevFrontPluginPayload
): RemoveDevFrontPlugin => ({
    type: REMOVE_DEV_PLUGIN,
    payload: payload
});