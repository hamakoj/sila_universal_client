import {DevActions, DevState} from "./types";
import {ADD_DEV_PLUGIN, REMOVE_DEV_PLUGIN} from "./actionTypes";

export const initialDevState: DevState = {
    frontPlugins: {}
};

export default function Reducer(state: DevState = initialDevState, action: DevActions): DevState {
    switch (action.type) {
        case ADD_DEV_PLUGIN:
            return {
                ...state,
                frontPlugins: {
                    ...state.frontPlugins,
                    [action.payload.url]: null
                }
            };
        case REMOVE_DEV_PLUGIN:
            const {[action.payload.url]: removed, ...rest } = state.frontPlugins
            return {
                ...state,
                frontPlugins: {
                    ...rest
                }
            };
    }
    return state;
};