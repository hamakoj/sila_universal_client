
export const SET_USER_ACCESS_TOKEN = "front/app/user/SET_USER_ACCESS_TOKEN"
export const REMOVE_USER_ACCESS_TOKEN = "front/app/user/REMOVE_USER_ACCESS_TOKEN"
export const SET_EXPIRED_USER_ACCESS_TOKEN = "front/app/user/SET_EXPIRED_USER_ACCESS_TOKEN"
export const SET_USER_LOCK_TOKEN = "front/app/user/SET_USER_LOCK_TOKEN"
export const REMOVE_USER_LOCK_TOKEN = "front/app/user/REMOVE_USER_LOCK_TOKEN"
export const SET_EXPIRED_USER_LOCK_TOKEN = "front/app/user/SET_EXPIRED_USER_LOCK_TOKEN"
