import {AppSettingActions, AppUserState} from "./types";
import {
    REMOVE_USER_ACCESS_TOKEN, REMOVE_USER_LOCK_TOKEN,
    SET_EXPIRED_USER_ACCESS_TOKEN, SET_EXPIRED_USER_LOCK_TOKEN,
    SET_USER_ACCESS_TOKEN,
    SET_USER_LOCK_TOKEN
} from "./actionTypes";

export const initialAppUserState: AppUserState = {
    servers: {}
};

export default function Reducer(state: AppUserState = initialAppUserState, action: AppSettingActions): AppUserState {
    switch (action.type) {
        case SET_USER_ACCESS_TOKEN: {
            return {
                ...state,
                servers: {
                    ...state.servers,
                    [action.payload.serverUuid]: {
                        ...state.servers[action.payload.serverUuid],
                        accessToken: {
                            expirationUTC: action.payload.expirationUTC.toISO(),
                            token: action.payload.token,
                            authorizedByServerUuid: action.payload.authorizedByServerUuid
                        }
                    }
                }
            };
        }
        case SET_EXPIRED_USER_ACCESS_TOKEN: {
            const accessToken = state.servers[action.payload.serverUuid]?.accessToken
            if (!accessToken) {
                // trigger listener anyway but should not happen
                return {
                    ...state
                }
            }
            return {
                ...state,
                servers: {
                    ...state.servers,
                    [action.payload.serverUuid]: {
                        ...state.servers[action.payload.serverUuid],
                        accessToken: {
                            ...accessToken,
                            token: action.payload.token,
                            expirationUTC: undefined
                        }
                    }
                }
            };
        }
        case REMOVE_USER_ACCESS_TOKEN: {
            const {accessToken, ...rest} = state.servers[action.payload.serverUuid];
            return {
                ...state,
                servers: {
                    ...state.servers,
                    [action.payload.serverUuid]: {
                        ...rest
                    }
                }
            };
        }
        case SET_USER_LOCK_TOKEN: {
            return {
                ...state,
                servers: {
                    ...state.servers,
                    [action.payload.serverUuid]: {
                        ...state.servers[action.payload.serverUuid],
                        lockToken: {
                            expirationUTC: action.payload.expirationUTC.toISO(),
                            token: action.payload.token,
                        }
                    }
                }
            };
        }
        case SET_EXPIRED_USER_LOCK_TOKEN: {
            const lockToken = state.servers[action.payload.serverUuid]?.lockToken
            if (!lockToken) {
                // trigger listener anyway but should not happen
                return {
                    ...state
                }
            }
            return {
                ...state,
                servers: {
                    ...state.servers,
                    [action.payload.serverUuid]: {
                        ...state.servers[action.payload.serverUuid],
                        lockToken: {
                            ...lockToken,
                            token: action.payload.token,
                            expirationUTC: undefined
                        }
                    }
                }
            };
        }
        case REMOVE_USER_LOCK_TOKEN: {
            const {lockToken, ...rest} = state.servers[action.payload.serverUuid];
            return {
                ...state,
                servers: {
                    ...state.servers,
                    [action.payload.serverUuid]: {
                        ...rest
                    }
                }
            };
        }
    }
    // todo in case the server unlock, expire the user lock token if he has one?
    return state;
};