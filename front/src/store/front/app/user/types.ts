import {
    REMOVE_USER_ACCESS_TOKEN, REMOVE_USER_LOCK_TOKEN, SET_EXPIRED_USER_ACCESS_TOKEN, SET_EXPIRED_USER_LOCK_TOKEN,
    SET_USER_ACCESS_TOKEN, SET_USER_LOCK_TOKEN,
} from "./actionTypes";
import {DateTime} from "luxon";

export interface AppUserState {
    servers: UserServers;
}

export interface UserServerAccessToken {
    token: string;
    authorizedByServerUuid: string;
    expirationUTC?: string; // undefined = expired
}

export interface UserServerLockToken {
    token: string;
    expirationUTC?: string; // undefined = expired
}

export interface UserServer {
    accessToken?: UserServerAccessToken;
    lockToken?: UserServerLockToken;
}

export interface UserServers {
    [uuid:string]: UserServer;
}

export interface SetUserAccessTokenPayload {
    serverUuid: string;
    authorizedByServerUuid: string;
    token: string;
    expirationUTC: DateTime;
}

export type SetUserAccessToken = {
    type: typeof SET_USER_ACCESS_TOKEN;
    payload: SetUserAccessTokenPayload;
};

export interface SetExpiredUserAccessTokenPayload {
    serverUuid: string;
    token: string;
}

export type SetExpiredUserAccessToken = {
    type: typeof SET_EXPIRED_USER_ACCESS_TOKEN;
    payload: SetExpiredUserAccessTokenPayload;
};


export interface RemoveUserAccessTokenPayload {
    serverUuid: string;
}

export type RemoveUserAccessToken = {
    type: typeof REMOVE_USER_ACCESS_TOKEN;
    payload: RemoveUserAccessTokenPayload;
};

export interface SetUserLockTokenPayload {
    serverUuid: string;
    token: string;
    expirationUTC: DateTime;
}

export type SetUserLockToken = {
    type: typeof SET_USER_LOCK_TOKEN;
    payload: SetUserLockTokenPayload;
};

export interface SetExpiredUserLockTokenPayload {
    serverUuid: string;
    token: string;
}

export type SetExpiredUserLockToken = {
    type: typeof SET_EXPIRED_USER_LOCK_TOKEN;
    payload: SetExpiredUserLockTokenPayload;
};


export interface RemoveUserLockTokenPayload {
    serverUuid: string;
}

export type RemoveUserLockToken = {
    type: typeof REMOVE_USER_LOCK_TOKEN;
    payload: RemoveUserLockTokenPayload;
};

export type AppSettingActions =
    SetUserAccessToken |
    SetExpiredUserAccessToken |
    RemoveUserAccessToken |
    SetUserLockToken |
    SetExpiredUserLockToken |
    RemoveUserLockToken;
