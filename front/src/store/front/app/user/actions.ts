import {
    REMOVE_USER_ACCESS_TOKEN, REMOVE_USER_LOCK_TOKEN,
    SET_EXPIRED_USER_ACCESS_TOKEN, SET_EXPIRED_USER_LOCK_TOKEN,
    SET_USER_ACCESS_TOKEN,
    SET_USER_LOCK_TOKEN
} from "./actionTypes";
import {
    RemoveUserAccessToken,
    RemoveUserAccessTokenPayload, RemoveUserLockToken,
    RemoveUserLockTokenPayload,
    SetExpiredUserAccessToken,
    SetExpiredUserAccessTokenPayload, SetExpiredUserLockToken, SetExpiredUserLockTokenPayload,
    SetUserAccessToken,
    SetUserAccessTokenPayload,
    SetUserLockToken,
    SetUserLockTokenPayload
} from "./types";

export const setUserAccessToken = (
    payload: SetUserAccessTokenPayload
): SetUserAccessToken => ({
    type: SET_USER_ACCESS_TOKEN,
    payload: payload
});

export const setExpiredUserAccessToken = (
    payload: SetExpiredUserAccessTokenPayload
): SetExpiredUserAccessToken => ({
    type: SET_EXPIRED_USER_ACCESS_TOKEN,
    payload: payload
});


export const removeUserAccessToken = (
    payload: RemoveUserAccessTokenPayload
): RemoveUserAccessToken => ({
    type: REMOVE_USER_ACCESS_TOKEN,
    payload: payload
});

export const setUserLockToken = (
    payload: SetUserLockTokenPayload
): SetUserLockToken => ({
    type: SET_USER_LOCK_TOKEN,
    payload: payload
});

export const setExpiredUserLockToken = (
    payload: SetExpiredUserLockTokenPayload
): SetExpiredUserLockToken => ({
    type: SET_EXPIRED_USER_LOCK_TOKEN,
    payload: payload
});


export const removeUserLockToken = (
    payload: RemoveUserLockTokenPayload
): RemoveUserLockToken => ({
    type: REMOVE_USER_LOCK_TOKEN,
    payload: payload
});