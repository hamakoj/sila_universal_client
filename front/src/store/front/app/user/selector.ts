import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {AppUserState, UserServer, UserServerAccessToken, UserServerLockToken} from "./types";

const getAppUser = (state: AppState) => state.appUser;
const getAppUserForServerId = (state: AppState, uuid: string) => uuid;

export const getAppUserSelector: Selector<AppState, AppUserState> = createSelector(getAppUser, (appUser) => appUser);

export const getAppUserForServerIdSelector: Selector<AppState, UserServer | undefined> = createSelector(
    [getAppUser, getAppUserForServerId],
    (userApp, serverUuid) => userApp.servers[serverUuid]
);

export const getAppUserAccessTokenForServerIdSelector: Selector<AppState, UserServerAccessToken | undefined> = createSelector(
    [getAppUser, getAppUserForServerId],
    (userApp, serverUuid) => userApp.servers[serverUuid]?.accessToken
);

export const getAppUserLockTokenForServerIdSelector: Selector<AppState, UserServerLockToken | undefined> = createSelector(
    [getAppUser, getAppUserForServerId],
    (userApp, serverUuid) => userApp.servers[serverUuid]?.lockToken
);