import {
    SET_SERVER_TABS_STORE_IN_MEMORY,
    SET_SIDEBAR_DEFAULT_EXPANDED,
    SET_THEME_MODE, SET_TIMEZONE_MODE, SET_CUSTOM_TIMEZONE_NAME, SET_ADVANCED_USER,
} from "./actionTypes";

export interface AppTimezoneSettings {
    useSystemTimezone: boolean;
    customTimezoneName: string;
}

export interface AppSettings {
    darkMode: boolean;
    sidebarExpandedByDefault: boolean;
    serverTabsStoreInMemory: boolean;
    isAdvancedUser: boolean;
    timezoneSettings: AppTimezoneSettings;
}

export interface ThemeModePayload {
    darkMode: boolean;
}

export interface SidebarDefaultExpandedPayload {
    sidebarExpandedByDefault: boolean;
}

export interface ServerTabsStoreInMemoryPayload {
    serverTabsStoreInMemory: boolean;
}

export interface SetAdvancedUserPayload {
    isAdvancedUser: boolean;
}

export interface TimezoneModePayload {
    useSystemTimezone: boolean;
}

export interface TimezoneNamePayload {
    customTimezoneName: string;
}

export type SetThemeMode = {
    type: typeof SET_THEME_MODE;
    payload: ThemeModePayload;
};

export type SetSidebarDefaultExpanded = {
    type: typeof SET_SIDEBAR_DEFAULT_EXPANDED;
    payload: SidebarDefaultExpandedPayload;
};

export type SetServerTabsStoreInMemory = {
    type: typeof SET_SERVER_TABS_STORE_IN_MEMORY;
    payload: ServerTabsStoreInMemoryPayload;
};

export type SetTimezoneMode = {
    type: typeof SET_TIMEZONE_MODE;
    payload: TimezoneModePayload;
};

export type SetCustomTimezoneName = {
    type: typeof SET_CUSTOM_TIMEZONE_NAME;
    payload: TimezoneNamePayload;
};

export type SetAdvancedUser = {
    type: typeof SET_ADVANCED_USER;
    payload: SetAdvancedUserPayload;
};

export type AppSettingActions =
    SetThemeMode |
    SetSidebarDefaultExpanded |
    SetServerTabsStoreInMemory |
    SetTimezoneMode |
    SetCustomTimezoneName |
    SetAdvancedUser;