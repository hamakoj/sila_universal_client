
export const SET_THEME_MODE = "front/app/setting/SET_THEME_MODE"
export const SET_SIDEBAR_DEFAULT_EXPANDED = "front/app/setting/SET_SIDEBAR_DEFAULT_EXPANDED"
export const SET_SERVER_TABS_STORE_IN_MEMORY = "front/app/setting/SET_SERVER_TABS_STORE_IN_MEMORY"
export const SET_TIMEZONE_MODE = "front/app/setting/SET_TIMEZONE_MODE"
export const SET_CUSTOM_TIMEZONE_NAME = "front/app/setting/SET_CUSTOM_TIMEZONE_NAME"
export const SET_ADVANCED_USER = "front/app/setting/SET_ADVANCED_USER"

