import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {AppSettings, AppTimezoneSettings} from "./types";

const getAppSettings = (state: AppState) => state.appSettings;
const getAppTimezoneSettings = (state: AppState) => state.appSettings.timezoneSettings;
const getAppIsAdvancedUser = (state: AppState) => state.appSettings.isAdvancedUser;

export const getAppSettingsSelector: Selector<AppState, AppSettings> = createSelector(getAppSettings, (appSettings) => appSettings);
export const getAppTimezoneSettingsSelector: Selector<AppState, AppTimezoneSettings> = createSelector(getAppTimezoneSettings, (appSettings) => appSettings);
export const getAppIsAdvancedUserSettingsSelector: Selector<AppState, boolean> = createSelector(getAppIsAdvancedUser, (isAdvancedUser) => isAdvancedUser);