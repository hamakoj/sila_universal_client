import {AppSettingActions, AppSettings} from "./types";
import {
    SET_ADVANCED_USER,
    SET_CUSTOM_TIMEZONE_NAME,
    SET_SERVER_TABS_STORE_IN_MEMORY,
    SET_SIDEBAR_DEFAULT_EXPANDED,
    SET_THEME_MODE,
    SET_TIMEZONE_MODE
} from "./actionTypes";

export const initialAppSettingsState: AppSettings = {
    darkMode: true,
    sidebarExpandedByDefault: false,
    serverTabsStoreInMemory: true,
    isAdvancedUser: false,
    timezoneSettings: {
        useSystemTimezone: true,
        customTimezoneName: 'Europe/Zurich'
    }
};

export default function Reducer(state: AppSettings = initialAppSettingsState, action: AppSettingActions): AppSettings {
    switch (action.type) {
        case SET_THEME_MODE:
            return {
                ...state,
                darkMode: action.payload.darkMode
            };
        case SET_SIDEBAR_DEFAULT_EXPANDED:
            return {
                ...state,
                sidebarExpandedByDefault: action.payload.sidebarExpandedByDefault
            };
        case SET_SERVER_TABS_STORE_IN_MEMORY:
            return {
                ...state,
                serverTabsStoreInMemory: action.payload.serverTabsStoreInMemory
            };
        case SET_TIMEZONE_MODE:
            return {
                ...state,
                timezoneSettings: {
                    ...state.timezoneSettings,
                    useSystemTimezone: action.payload.useSystemTimezone
                }
            };
        case SET_CUSTOM_TIMEZONE_NAME:
            return {
                ...state,
                timezoneSettings: {
                    ...state.timezoneSettings,
                    customTimezoneName: action.payload.customTimezoneName
                }
            };
        case SET_ADVANCED_USER:
            return {
                ...state,
                isAdvancedUser: action.payload.isAdvancedUser
            };
    }
    return state;
};