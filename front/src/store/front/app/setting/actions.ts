import {
    ServerTabsStoreInMemoryPayload, SetAdvancedUser, SetAdvancedUserPayload, SetCustomTimezoneName,
    SetServerTabsStoreInMemory,
    SetSidebarDefaultExpanded,
    SetThemeMode,
    SetTimezoneMode,
    SidebarDefaultExpandedPayload,
    ThemeModePayload, TimezoneModePayload, TimezoneNamePayload,
} from "./types";

import {
    SET_ADVANCED_USER,
    SET_CUSTOM_TIMEZONE_NAME,
    SET_SERVER_TABS_STORE_IN_MEMORY,
    SET_SIDEBAR_DEFAULT_EXPANDED,
    SET_THEME_MODE,
    SET_TIMEZONE_MODE,
} from "./actionTypes";

export const setAppThemeMode = (
    payload: ThemeModePayload
): SetThemeMode => ({
    type: SET_THEME_MODE,
    payload: payload
});

export const setSidebarDefaultExpanded = (
    payload: SidebarDefaultExpandedPayload
): SetSidebarDefaultExpanded => ({
    type: SET_SIDEBAR_DEFAULT_EXPANDED,
    payload: payload
});

export const setServerTabsStoreInMemory = (
    payload: ServerTabsStoreInMemoryPayload
): SetServerTabsStoreInMemory => ({
    type: SET_SERVER_TABS_STORE_IN_MEMORY,
    payload: payload
});

export const setTimezoneMode = (
    payload: TimezoneModePayload
): SetTimezoneMode => ({
    type: SET_TIMEZONE_MODE,
    payload: payload
});

export const setCustomTimezoneName = (
    payload: TimezoneNamePayload
): SetCustomTimezoneName => ({
    type: SET_CUSTOM_TIMEZONE_NAME,
    payload: payload
});

export const setAdvancedUser = (
    payload: SetAdvancedUserPayload
): SetAdvancedUser => ({
    type: SET_ADVANCED_USER,
    payload: payload
});
