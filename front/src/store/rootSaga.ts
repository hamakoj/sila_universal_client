import { all, fork } from "redux-saga/effects";

import {fetchServersSaga} from "./sila/manager/server/sagas";
import {fetchPluginsSaga} from "./front/app/plugin/sagas";
import {
    callComplete, callError, callExecInfo, callInit, callIntermediate,
    callProgress, observablePropertyUpdate,
    serverAddition,
    serverDelete,
    serverUpdate,
    wsConnection,
    wsDisconnection
} from "./sila/websocket/sagas";
import bookmarkSaga, {fetchUserBookmarksSaga} from "./front/server/bookmark/sagas";
import {fetchRunningCallsSaga} from "./sila/runningCall/sagas";
import callResponseSaga from "./front/server/response/sagas";
import serverMetaSaga from "./front/server/meta/sagas";

export function* rootSaga() {
    yield all([
        fork(wsConnection),
        fork(wsDisconnection),

        fork(callProgress),
        fork(callComplete),
        fork(callError),
        fork(callInit),
        fork(callExecInfo),
        fork(callIntermediate),
        fork(observablePropertyUpdate),

        fork(serverAddition),
        fork(serverUpdate),
        fork(serverDelete),

        fork(serverMetaSaga),

        fork(callResponseSaga),

        fork(fetchPluginsSaga),

        fork(fetchServersSaga),
        fork(fetchRunningCallsSaga),

        fork(fetchUserBookmarksSaga),
        fork(bookmarkSaga)
    ]);
}