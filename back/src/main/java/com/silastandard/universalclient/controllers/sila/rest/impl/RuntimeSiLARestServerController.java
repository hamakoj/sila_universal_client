package com.silastandard.universalclient.controllers.sila.rest.impl;

import com.silastandard.universalclient.common.models.sila.ServerHostPortCertificate;
import com.silastandard.universalclient.controllers.sila.rest.SiLARestServerController;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.models.Server;
import sila_java.library.manager.server_management.ServerConnectionException;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/runtime/servers")
public class RuntimeSiLARestServerController implements SiLARestServerController {
    private final ServerManager serverManager = ServerManager.getInstance();

    public Map<UUID, Server> all() {
        return this.serverManager.getServers();
    }

    public Map<UUID, Server> allOnline() {
        return this.serverManager.getServers()
                .entrySet()
                .stream()
                .filter(e -> e.getValue().getStatus().equals(Server.Status.ONLINE))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public void scan() {
        this.serverManager.getDiscovery().scanNetwork();
    }

    public Server one(@PathVariable @NonNull final String uuid) {
        return this.serverManager.getServers().get(UUID.fromString(uuid));
    }

    @SneakyThrows
    public ResponseEntity<String> add(@RequestBody @NonNull final ServerHostPortCertificate serverHostPortCertificate) {
        try {
            this.serverManager.addServer(
                    serverHostPortCertificate.getHost(),
                    serverHostPortCertificate.getPort(),
                    serverHostPortCertificate.getCertificate()
            );
            return ResponseEntity.ok("");
        } catch (ServerConnectionException e) {
            log.info("Server connection failed: ", e);
            final StringBuilder stringBuilder = new StringBuilder();
            Throwable it = e;
            while (it != null) {
                if (e == it) {
                    stringBuilder.append(it.getMessage());
                } else {
                    stringBuilder.append("- " + it.getMessage());
                }
                stringBuilder.append(System.lineSeparator());
                it = it.getCause();
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(stringBuilder.toString());
        }
    }

    public void remove(@PathVariable @NonNull final String uuid) {
        ServerManager.getInstance().removeServer(UUID.fromString(uuid));
    }
}
