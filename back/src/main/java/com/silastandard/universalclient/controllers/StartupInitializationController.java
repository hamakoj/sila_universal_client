package com.silastandard.universalclient.controllers;

import com.silastandard.universalclient.common.models.db.Client;
import com.silastandard.universalclient.common.models.sila.ServerHostPortCertificate;
import com.silastandard.universalclient.controllers.sila.ws.WsServerController;
import com.silastandard.universalclient.services.ClientService;
import com.silastandard.universalclient.services.ServerService;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.server_management.ServerConnectionException;

import java.util.HashSet;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import static com.silastandard.universalclient.utils.Constants.UNIQUE_CLIENT_ID;


@Component
@Slf4j
public class StartupInitializationController implements ApplicationListener<ApplicationReadyEvent> {

    private final ServerService serverService;
    private final ClientService clientService;
    private final WsServerController wsServerController;

    @Nullable
    private final String insecureConnection;

    @Autowired
    public StartupInitializationController(
            @NonNull final ServerService serverService,
            @NonNull final ClientService clientService,
            @NonNull final WsServerController wsServerController,
            @Nullable @Value("${insecure-connection}") final String insecureConnection
    ) {
        this.serverService = serverService;
        this.clientService = clientService;
        // make sure controller is instantiated such as it listen to server addition updates from server manager
        this.wsServerController = wsServerController;
        this.insecureConnection = insecureConnection;
    }

    @SneakyThrows
    @Override
    @Transactional
    public void onApplicationEvent(@NonNull final ApplicationReadyEvent event) {
        this.serverService.setAllServersStatusToUnknown();
        this.initializeUniqueClient();
        this.initializeServerManager();
        this.initializeServersFromDBAsync();
    }

    private void initializeServerManager() {
        final ServerManager serverManager = ServerManager.getInstance();
        if (this.insecureConnection != null && this.insecureConnection.equals("true")) {
            serverManager.setAllowInsecureConnection(true);
        }
    }

    private void initializeServersFromDBAsync() {
        CompletableFuture.runAsync(() -> {
            log.info("Startup server initialization in progress...");
            try {
                this.serverService.getAllServers().stream().map(s -> {
                    try {
                        return new ServerHostPortCertificate(s.getServerHost(), s.getPort(), s.getCertificateAuthority());
                    } catch (Exception e) {
                        log.debug("Invalid ServerHostPortCertificate for server {} ", s.getServerUuid(), e);
                        return null;
                    }
                }).filter(Objects::nonNull).distinct().forEach(s -> {
                    try {
                        sila_java.library.manager.models.Server server =
                                ServerManager.getInstance().addServer(s.getHost(), s.getPort(), s.getCertificate());
                        log.info("Startup initialization added Server [{}] - [{}] at [{}:{}] to ServerManager.",
                                server.getConfiguration().getName(),
                                server.getConfiguration().getUuid(),
                                server.getHost(),
                                server.getPort()
                        );
                    } catch (final ServerConnectionException e) {
                        log.info(
                                "Startup initialization failed to add Server at [{}:{}] to ServerManager.",
                                s.getHost(),
                                s.getPort()
                        );
                        log.debug("Failed to add Server at [{}:{}] to Server Manager.",
                                s.getHost(),
                                s.getPort(),
                                e
                        );
                    }
                });
            } catch (Throwable e) {
                log.warn("An error occurred while initializing Database Servers!", e);
                throw e;
            }
            log.info("Startup server initialization finished.");
        });
    }

    private void initializeUniqueClient() {
        this.clientService.getClient(UNIQUE_CLIENT_ID).orElseGet(() -> {
            final Client uniqueClient = new Client();
            uniqueClient.setName("default");
            uniqueClient.setClientUuid(UNIQUE_CLIENT_ID);
            uniqueClient.setServers(new HashSet<>());
            uniqueClient.setCalls(new HashSet<>());
            return this.clientService.save(uniqueClient);
        });
    }
}
