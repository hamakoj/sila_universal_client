package com.silastandard.universalclient.controllers.rest;

import com.silastandard.universalclient.common.models.db.Feature;
import com.silastandard.universalclient.services.FeatureService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.Set;

@RestController
@Slf4j
public class FeatureController {

    private final FeatureService featureService;

    @Autowired
    public FeatureController(@NonNull final FeatureService featureService) {
        this.featureService = featureService;
    }

    @GetMapping("/servers/{serverUuid}/features")
    public Set<Feature> allFromServer(@PathVariable @NonNull final String serverUuid) {
        return this.featureService.getAllServerFeatures(serverUuid);
    }

    @GetMapping("/features/{id}")
    public Optional<Feature> one(@PathVariable @NonNull final Long id) {
        return this.featureService.getFeature(id);
    }
}
