package com.silastandard.universalclient.controllers.rest;

import com.google.common.collect.Multimap;
import com.silastandard.universalclient.common.models.db.Call;
import com.silastandard.universalclient.common.models.db.Client;
import com.silastandard.universalclient.common.models.sila.CachedCall;
import com.silastandard.universalclient.common.models.sila.ServerUUIDFQIFeature;
import com.silastandard.universalclient.common.models.sila.SiLACallModel;
import com.silastandard.universalclient.common.models.sila.SiLACallResult;
import com.silastandard.universalclient.controllers.sila.ws.WsCallController;
import com.silastandard.universalclient.services.CallService;
import com.silastandard.universalclient.services.impl.CallFactoryService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * Transform SiLACall / SiLACallModel / SiLACallResult to USC persistent Call
 */
@Slf4j
@RestController
public class CallController {

    private final CallService callService;
    private final CallFactoryService callFactoryService;
    private final WsCallController wsCallController;

    @Autowired
    public CallController(
            @NonNull final CallService callService,
            @NonNull final CallFactoryService callFactoryService,
            @NonNull final WsCallController wsCallController
    ) {
        this.callService = callService;
        this.callFactoryService = callFactoryService;
        this.wsCallController = wsCallController;
    }

    @GetMapping("/servers/{serverUuid}/features/{originator}/{category}/{featureIdentifier}/{featureMajorVersion}/commands/{relativeIdentifier}/calls")
    public Set<Call> allCallsFromCommand(
            @PathVariable @NonNull final UUID serverUuid,
            @PathVariable @NonNull final String originator,
            @PathVariable @NonNull final String category,
            @PathVariable @NonNull final String featureIdentifier,
            @PathVariable @NonNull final String featureMajorVersion,
            @PathVariable @NonNull final String relativeIdentifier
    ) {
        final ServerUUIDFQIFeature uuidfqiFeature = new ServerUUIDFQIFeature(
                serverUuid, originator, category, featureIdentifier, featureMajorVersion
        );
        return this.callService.getAllCallsByServerFeatureAndCommandRelativeId(
                serverUuid.toString(), uuidfqiFeature.getFullyQualifiedFeatureIdentifier(), relativeIdentifier
        );
    }

    @GetMapping("/servers/{serverUuid}/features/{originator}/{category}/{featureIdentifier}/{featureMajorVersion}/properties/{relativeIdentifier}/calls")
    public Set<Call> allCallsFromProperty(
            @PathVariable @NonNull final UUID serverUuid,
            @PathVariable @NonNull final String originator,
            @PathVariable @NonNull final String category,
            @PathVariable @NonNull final String featureIdentifier,
            @PathVariable @NonNull final String featureMajorVersion,
            @PathVariable @NonNull final String relativeIdentifier
    ) {
        final ServerUUIDFQIFeature uuidfqiFeature = new ServerUUIDFQIFeature(
                serverUuid, originator, category, featureIdentifier, featureMajorVersion
        );
        return this.callService.getAllCallsByServerFeatureAndPropertyRelativeId(
                serverUuid.toString(), uuidfqiFeature.getFullyQualifiedFeatureIdentifier(), relativeIdentifier
        );
    }

    @DeleteMapping("/calls/{id}")
    public Boolean deleteCall(@PathVariable @NonNull final UUID id) {
        try {
            return this.callService.deleteCall(id);
        } catch (final UnexpectedRollbackException e) {
            log.warn("Could not delete Call [{}] : {}", id, e.getMessage());
            // todo should be 204 No Content
            return false;
        }
    }

    @GetMapping("/calls/{id}/cache")
    public CachedCall getFromCache(@PathVariable @NonNull final UUID id) {
        return this.wsCallController.getCallsCache().get(id);
    }

    @GetMapping("/servers/{serverUuid}/features/{originator}/{category}/{featureIdentifier}/{featureMajorVersion}/calls/{relativeIdentifier}/running")
    public Set<UUID> getRunningCallType(
            @PathVariable @NonNull final UUID serverUuid,
            @PathVariable @NonNull final String originator,
            @PathVariable @NonNull final String category,
            @PathVariable @NonNull final String featureIdentifier,
            @PathVariable @NonNull final String featureMajorVersion,
            @PathVariable @NonNull final String relativeIdentifier
    ) {
        final Multimap<String, UUID> strings = this.wsCallController.getCallsTypeServerMap().get(serverUuid);
        if (strings == null) {
            return Collections.emptySet();
        }
        final ServerUUIDFQIFeature uuidfqiFeature = new ServerUUIDFQIFeature(
                serverUuid, originator, category, featureIdentifier, featureMajorVersion
        );
        final Collection<UUID> uuids = strings.get(
                WsCallController.getFullyQualifiedIdentifierFromIds(
                        serverUuid, uuidfqiFeature.getFullyQualifiedFeatureIdentifier(), relativeIdentifier
                )
        );
        if (uuids == null) {
            return Collections.emptySet();
        }
        return new HashSet<>(uuids);
    }

    @DeleteMapping("/servers/{serverUuid}/features/{originator}/{category}/{featureIdentifier}/{featureMajorVersion}/commands/{relativeIdentifier}/calls")
    public Boolean deleteAllServerFeatureCallsByCommand(
            @PathVariable @NonNull final UUID serverUuid,
            @PathVariable @NonNull final String originator,
            @PathVariable @NonNull final String category,
            @PathVariable @NonNull final String featureIdentifier,
            @PathVariable @NonNull final String featureMajorVersion,
            @PathVariable @NonNull final String relativeIdentifier
    ) {
        final ServerUUIDFQIFeature uuidfqiFeature = new ServerUUIDFQIFeature(
                serverUuid, originator, category, featureIdentifier, featureMajorVersion
        );
        return this.callService.deleteAllFromCommand(
                serverUuid.toString(), uuidfqiFeature.getFullyQualifiedFeatureIdentifier(), relativeIdentifier
        );
    }

    @DeleteMapping("/servers/{serverUuid}/features/{originator}/{category}/{featureIdentifier}/{featureMajorVersion}/properties/{relativeIdentifier}/calls")
    public Boolean deleteAllServerFeatureCallsByProperty(
            @PathVariable @NonNull final UUID serverUuid,
            @PathVariable @NonNull final String originator,
            @PathVariable @NonNull final String category,
            @PathVariable @NonNull final String featureIdentifier,
            @PathVariable @NonNull final String featureMajorVersion,
            @PathVariable @NonNull final String relativeIdentifier
    ) {
        final ServerUUIDFQIFeature uuidfqiFeature = new ServerUUIDFQIFeature(
                serverUuid, originator, category, featureIdentifier, featureMajorVersion
        );
        return this.callService.deleteAllFromProperty(
                serverUuid.toString(), uuidfqiFeature.getFullyQualifiedFeatureIdentifier(), relativeIdentifier
        );
    }

    public Call startCall(
            @NonNull final UUID callUuid,
            @NonNull final SiLACallModel siLACallModel,
            @NonNull final Client client
    ) {
        final Call clientCall = this.callFactoryService.createClientCall(callUuid, siLACallModel, client);
        return this.callService.save(clientCall);
    }

    public Call setCommandExecutionUUID(@NonNull final UUID callUuid, @NonNull final String commandExecutionUUID) {
        final Call call = this.callService.findByUuid(callUuid)
                .orElseThrow(() -> new RuntimeException("Call " + callUuid + " does not exist"));
        return this.callService.save(callFactoryService.setCommandExecutionUUID(call, commandExecutionUUID));
    }

    public Call finishCall(
            final SiLACallResult siLACallResult,
            @NonNull final UUID callUuid
    ) {
        // todo fixme when removing a server that has running call, cause error because call does not exist anymore
        try {
            final Call call = this.callService.findByUuid(callUuid)
                    .orElseThrow(() -> new RuntimeException("Call " + callUuid + " does not exist"));
            return this.callService.save(callFactoryService.finishCall(siLACallResult, call));
        } catch (org.springframework.dao.CannotAcquireLockException | org.springframework.orm.ObjectOptimisticLockingFailureException e) {
            log.warn("Failed to finish call {} because of a deadlock/removal problem", callUuid);
            log.debug("Failed to finish call {}", callUuid, e);
            return null;
        } catch (Throwable e) {
            log.warn("Failed to finish call {}", callUuid, e);
            throw e;
        }
    }
}
