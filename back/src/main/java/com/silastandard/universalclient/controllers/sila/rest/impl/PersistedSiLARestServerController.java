package com.silastandard.universalclient.controllers.sila.rest.impl;

import com.silastandard.universalclient.controllers.sila.rest.SiLARestServerController;
import com.silastandard.universalclient.controllers.sila.ws.WsServerController;
import com.silastandard.universalclient.mappers.ServerMapper;
import com.silastandard.universalclient.common.models.sila.ServerHostPortCertificate;
import com.silastandard.universalclient.services.ServerService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.models.Server;
import sila_java.library.manager.server_management.ServerConnectionException;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RestController
public class PersistedSiLARestServerController implements SiLARestServerController {

    private final ServerService serverService;
    private final WsServerController wsServerController;

    @Autowired
    public PersistedSiLARestServerController(
            @NonNull final ServerService serverService,
            @NonNull final WsServerController wsServerController
    ) {
        this.serverService = serverService;
        this.wsServerController = wsServerController;
    }

    public Map<UUID, Server> all() {
        return this.serverService.getAllServers()
                .stream()
                .collect(Collectors.toMap(
                        (server) -> UUID.fromString(server.getServerUuid()),
                        ServerMapper::toSiLAServer
                ));
    }

    public Map<UUID, Server> allOnline() {
        return this.serverService.getAllOnlineServers()
                .stream()
                .collect(Collectors.toMap(
                        (server) -> UUID.fromString(server.getServerUuid()),
                        ServerMapper::toSiLAServer
                ));
    }

    public void scan() {
        ServerManager.getInstance().getDiscovery().scanNetwork();
    }

    public Server one(@PathVariable @NonNull final String uuid) {
        // todo implement this properly by finding in db
        return ServerMapper.toSiLAServer(
                this.serverService.getServer(uuid)
                        .orElseThrow(() -> new RuntimeException(
                                String.format("Request to get Server [%s] failed: Server not found.", uuid))
                        )
        );
    }

    public ResponseEntity<String> add(
            /*@RequestHeader("CLIENT_ID") String clientId, */
            @RequestBody @NonNull final ServerHostPortCertificate serverHostPortCertificate
    ) {
        // todo: wait for front to send clientId
        try {
            ServerManager.getInstance().addServer(
                    serverHostPortCertificate.getHost(),
                    serverHostPortCertificate.getPort(),
                    serverHostPortCertificate.getCertificate()
            );
            return ResponseEntity.ok("");
        } catch (ServerConnectionException e) {
            log.info("Server connection failed: ", e);
            final StringBuilder stringBuilder = new StringBuilder();
            Throwable it = e;
            while (it != null) {
                if (e == it) {
                    stringBuilder.append(it.getMessage());
                } else {
                    stringBuilder.append("- " + it.getMessage());
                }
                stringBuilder.append(System.lineSeparator());
                it = it.getCause();
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(stringBuilder.toString());
        }
    }

    @Transactional
    public void remove(@PathVariable @NonNull final String uuid) {
        // todo: check if persistent Server removal have to be done here or in the WS controllers
        ServerManager.getInstance().removeServer(UUID.fromString(uuid));
        // ignore if server does not exist
        this.serverService.getServer(uuid).ifPresent(server -> {
            this.wsServerController.sendServerRemovedNotification(UUID.fromString(server.getServerUuid()));
            this.serverService.delete(server);
        });
    }
}
