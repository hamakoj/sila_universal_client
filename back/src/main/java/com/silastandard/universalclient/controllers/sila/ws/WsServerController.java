package com.silastandard.universalclient.controllers.sila.ws;

import com.silastandard.universalclient.common.models.sila.ServerHostPortCertificate;
import com.silastandard.universalclient.controllers.rest.ServerController;
import com.silastandard.universalclient.services.application.MetadataManagerService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import sila_java.library.manager.ServerListener;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.models.Server;

import java.util.*;

@Slf4j
@Controller
public class WsServerController implements ServerListener {
    private final WebSocketMessenger websocketMessenger;
    private final ServerController serverController;
    private final MetadataManagerService metadataManagerService;

    @Autowired
    public WsServerController(
            @NonNull final WebSocketMessenger websocketMessenger,
            @NonNull final ServerController serverController,
            @NonNull final MetadataManagerService metadataManagerService
    ) {
        this.websocketMessenger = websocketMessenger;
        this.serverController = serverController;
        this.metadataManagerService = metadataManagerService;
        ServerManager.getInstance().addServerListener(this);
    }

    @Override
    public void onServerChange(@NonNull final UUID serverUuid, @NonNull final Server server) {
        log.info("Server {} got updated by ServerManager.", serverUuid);
        com.silastandard.universalclient.common.models.db.Server uscServer = this.serverController.persistSiLAServer(server, serverUuid.toString());
        //this.metadataManagerService.onServerAddedCacheAffectedFPC(server); // todo remove/update call cache
        this.websocketMessenger.send("server/update", uscServer, null);
    }

    @Override
    public void onServerAdded(@NonNull final UUID serverUuid, @NonNull final Server server) {
        log.info("Server {} was added to ServerManager.", serverUuid);

        try {
            com.silastandard.universalclient.common.models.db.Server uscServer = this.serverController.persistSiLAServer(server, serverUuid.toString());
            // Add Feature Command Properties Affected by Metadata to Metadata Cache
            this.metadataManagerService.onServerAddedCacheAffectedFPC(server);
            this.websocketMessenger.send("server/add", uscServer, null);
        } catch (Exception e) {
            // todo handle, try again or remove it from server manager
            log.warn("Failed to persist server {}", serverUuid, e);
        }
    }

    @Override
    public void onServerRemoved(@NonNull final UUID serverUuid, @NonNull final Server server) {
        log.info("On server removed " + serverUuid);
        // todo if the server exist in the DB we should maybe set its status to unknown and notify to websocket that server status is now unknown
        sendServerRemovedNotification(serverUuid);
        // todo remove call cache
        this.metadataManagerService.onServerRemovedAffectedFPC(serverUuid);
    }

    public void sendServerRemovedNotification(@NonNull final UUID serverUuid) {
        this.websocketMessenger.send("server/remove", serverUuid.toString(), null);
    }

    @Override
    public void onServerAdditionFail(@NonNull final String host, final int port, @NonNull final String reason) {
        log.info(
                "Failed to add Server at [{}:{}] to ServerManager.",
                host,
                port
        );
        this.websocketMessenger.send("server/fail", new ServerHostPortCertificate(host, port), null);
    }
}
