package com.silastandard.universalclient.controllers;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class MetadataTypeMapping {
    // Map<fullyQualifiedFeatureId, Map<MetadataRelativeId, Map<ParameterPartiallyQualifiedId/ResponsePartiallyQualifiedId, JsonPath>>>
    private final Map<String, Map<String, Map<String, String>>> mapping = new ConcurrentHashMap<>();

    public MetadataTypeMapping() { }

    @PostConstruct
    private void init() {
        final Map<String, Map<String, String>> metadataMap = new ConcurrentHashMap<>();
        final Map<String, String> mappingValue = new ConcurrentHashMap<>();
        mappingValue.put("Command/LockServer/Parameter/LockIdentifier", "$.LockIdentifier.value");
        metadataMap.put("Metadata/LockIdentifier", mappingValue);
        mapping.put("org.silastandard/core/LockController/v1", metadataMap);
    }

    public Optional<Map<String, String>> getMetadataValueRelativeIdAndJsonPath(
            @NonNull final String featureFullyQualifiedId,
            @NonNull final String metadataPartiallyQualifiedId
    ) {
        if (Objects.isNull(mapping.get(featureFullyQualifiedId))) {
            return Optional.empty();
        }
        return Optional.ofNullable(mapping.get(featureFullyQualifiedId).get(metadataPartiallyQualifiedId));
    }
}
