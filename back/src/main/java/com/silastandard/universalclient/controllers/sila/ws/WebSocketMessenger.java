package com.silastandard.universalclient.controllers.sila.ws;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.protobuf.AbstractMessage;
import com.google.protobuf.util.JsonFormat;
import com.silastandard.universalclient.plugin.events.websocket.WebsocketMessageEvent;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Map;

import static com.silastandard.universalclient.config.WebSocketConfig.EVENT_PREFIX_PATH;

@Slf4j
@Component
public class WebSocketMessenger {
    private final SimpMessagingTemplate webSocketTemplate;
    private final ObjectMapper objectMapper;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public WebSocketMessenger(
            @NonNull final ApplicationEventPublisher applicationEventPublisher,
            @NonNull final SimpMessagingTemplate webSocketTemplate,
            @NonNull final ObjectMapper objectMapper
    ) {
        this.applicationEventPublisher = applicationEventPublisher;
        this.webSocketTemplate = webSocketTemplate;
        SimpleModule simpleModule = new SimpleModule("SimpleModule", new Version(1, 0, 0, null));
        simpleModule.addSerializer(AbstractMessage.class, new JsonSerializer<AbstractMessage>() {
            @Override
            public void serialize(AbstractMessage abstractMessage, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
                String print = JsonFormat.printer().includingDefaultValueFields().print(abstractMessage);
                jsonGenerator.writeRawValue(print);
            }
        });
        this.objectMapper = objectMapper;
        this.objectMapper.registerModule(simpleModule);
    }

    public void send(
            @NonNull final String destination,
            @NonNull final Object object,
            @Nullable final String clientId
    ) {
        send(destination, object, clientId, null);
    }

    public void send(
            @NonNull final String destination,
            @NonNull final Object object,
            @Nullable final String clientId,
            @Nullable final Map<String, Object> headers
    ) {
        // todo make sure it is not broadcasted when client id is specified
        final String endpoint = EVENT_PREFIX_PATH + ((clientId != null) ? (clientId + '/') : ("")) + destination;
        final WebsocketMessageEvent websocketMessageEvent = new WebsocketMessageEvent(destination, clientId, headers, endpoint, object);
        this.applicationEventPublisher.publishEvent(websocketMessageEvent);
        if (!websocketMessageEvent.isCancelled()) {
            this.webSocketTemplate.convertAndSend(endpoint, serialize(object), headers);
        } else {
            log.info("Websocket message to {} has been cancelled by a plugin", endpoint);
        }
    }

    private Object serialize(final Object object) {
        try {
            return this.objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
