package com.silastandard.universalclient.controllers.rest;

import com.silastandard.universalclient.common.models.db.Property;
import com.silastandard.universalclient.common.models.sila.ServerUUIDFQIFeature;
import com.silastandard.universalclient.services.PropertyService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.UUID;

@Slf4j
@RestController
public class PropertyController {
    private final PropertyService propertyService;

    @Autowired
    public PropertyController(@NonNull final PropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @GetMapping("/servers/{serverUuid}/features/{originator}/{category}/{featureIdentifier}/{featureMajorVersion}/properties")
    public Set<Property> allFromServer(
            @PathVariable @NonNull final UUID serverUuid,
            @PathVariable @NonNull final String originator,
            @PathVariable @NonNull final String category,
            @PathVariable @NonNull final String featureIdentifier,
            @PathVariable @NonNull final String featureMajorVersion
    ) {
        final ServerUUIDFQIFeature uuidfqiFeature = new ServerUUIDFQIFeature(
                serverUuid, originator, category, featureIdentifier, featureMajorVersion
        );
        return this.propertyService.getAllPropertiesByServerFeature(
                serverUuid.toString(), uuidfqiFeature.getFullyQualifiedFeatureIdentifier()
        );
    }
}
