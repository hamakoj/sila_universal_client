package com.silastandard.universalclient.controllers.rest;

import com.silastandard.universalclient.common.models.db.Command;
import com.silastandard.universalclient.common.models.sila.ServerUUIDFQIFeature;
import com.silastandard.universalclient.services.CommandService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.UUID;

@RestController
@Slf4j
public class CommandController {

    private final CommandService commandService;

    @Autowired
    public CommandController(@NonNull final CommandService commandService) {
        this.commandService = commandService;
    }

    @GetMapping("/servers/{serverUuid}/features/{originator}/{category}/{featureIdentifier}/{featureMajorVersion}/commands")
    public Set<Command> allFromServer(
            @PathVariable @NonNull final UUID serverUuid,
            @PathVariable @NonNull final String originator,
            @PathVariable @NonNull final String category,
            @PathVariable @NonNull final String featureIdentifier,
            @PathVariable @NonNull final String featureMajorVersion
    ) {
        final ServerUUIDFQIFeature uuidfqiFeature = new ServerUUIDFQIFeature(
                serverUuid, originator, category, featureIdentifier, featureMajorVersion
        );
        return this.commandService.getAllCommandsByServerFeature(
                serverUuid.toString(), uuidfqiFeature.getFullyQualifiedFeatureIdentifier()
        );
    }
}
