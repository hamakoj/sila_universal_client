package com.silastandard.universalclient.controllers.sila.rest;

import com.silastandard.universalclient.common.models.sila.SiLACallModel;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;
import sila_java.library.manager.ServerManager;

import java.util.UUID;

@RequestMapping("/calls")
public interface SiLARestCallController<T> {

    @PostMapping("/execute")
    T execute(@RequestBody @NonNull final SiLACallModel request);

    @GetMapping("/{callUuid}/cancel")
    default void cancelCall(@PathVariable("callUuid") String callUuid) {
        final UUID uuid = UUID.fromString(callUuid);
        ServerManager.getInstance().getServerCallManager().getCall(uuid).ifPresent((c) -> {
            if (!c.getFuture().isDone()) {
                c.getFuture().cancel(true);
            }
        });
    }
}
