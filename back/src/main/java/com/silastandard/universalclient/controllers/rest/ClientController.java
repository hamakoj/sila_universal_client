package com.silastandard.universalclient.controllers.rest;

import com.silastandard.universalclient.common.models.db.Client;
import com.silastandard.universalclient.common.models.db.Server;
import com.silastandard.universalclient.services.ClientService;
import com.silastandard.universalclient.services.ServerService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@Slf4j
public class ClientController {

    private final ClientService clientService;
    private final ServerService serverService;

    @Autowired
    public ClientController(
            @NonNull final ClientService clientService,
            @NonNull final ServerService serverService
    ) {
        this.clientService = clientService;
        this.serverService = serverService;
    }

    @GetMapping("/clients/{clientId}/servers")
    public Set<Server> getBookmarkedServers(@PathVariable @NonNull final String clientId) {
        return this.serverService.getBookmarkedServers((
            clientService.getClient(clientId)
                .orElseThrow(() -> new RuntimeException("")))
        );
    }

    @PostMapping("/clients/{clientId}/servers/{serverUuid}")
    public Server addBookmarkedServer(
            @PathVariable @NonNull final String clientId,
            @PathVariable @NonNull final String serverUuid
    ) {
        return this.clientService.addBookmarkedServers(
            clientService.getClient(clientId)
                .orElseThrow(() -> new RuntimeException(
                        String.format("Client [%s] can't be found.", clientId)
                )
            ),
            serverService.getServer(serverUuid)
                .orElseThrow(() -> new RuntimeException(
                    String.format("Server [%s] can't be found.", serverUuid)
                )
            )
        );
    }

    @DeleteMapping("/clients/{clientId}/servers/{serverUuid}")
    public Server deleteBookmarkedServer(
            @PathVariable @NonNull final String clientId,
            @PathVariable @NonNull final String serverUuid
    ) {
        return this.clientService.deleteBookmarkedServers(
                clientService.getClient(clientId)
                        .orElseThrow(() -> new RuntimeException(
                                        String.format("Client [%s] can't be found.", clientId)
                                )
                        ),
                serverService.getServer(serverUuid)
                        .orElseThrow(() -> new RuntimeException(
                                        String.format("Server [%s] can't be found.", serverUuid)
                                )
                        )
        );
    }

    public Client createOrRetrieveClient(@NonNull final String uuid) {
        return this.clientService.createOrRetrieveClient(uuid);
    }
}
