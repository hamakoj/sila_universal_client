package com.silastandard.universalclient.controllers.sila.rest;

import com.google.protobuf.util.JsonFormat;
import com.silastandard.universalclient.common.models.sila.BinaryDownload;
import com.silastandard.universalclient.common.models.sila.BinaryUpload;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.executor.BinaryDownloader;
import sila_java.library.manager.executor.BinaryUploader;
import sila_java.library.manager.executor.ExecutableServerCall;
import sila_java.library.manager.models.Server;
import sila_java.library.manager.models.SiLACall;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Slf4j
@RestController
public class RestBinaryCallController {
    private static final int MAX_CHUNK_SIZE = 2097152;

    // fixme
    //  2022-01-09 19:21:38 WARN  o.s.w.c.r.a.WebAsyncManager:363 -
    //  !!!
    //  An Executor is required to handle java.util.concurrent.Callable return values.
    //  Please, configure a TaskExecutor in the MVC config under "async support".
    //  The SimpleAsyncTaskExecutor currently in use is not suitable under load.
    //  -------------------------------
    //  Request URI: '/api/calls/binary/download'
    //  Request URI: '/api/calls/binary/upload'
    //  !!!

    @PostMapping("/calls/binary/upload")
    //todo spring rest controller first download the file and then perform the callback, we should use the apache controller directly
    //  see https://stackoverflow.com/questions/37870989/spring-how-to-stream-large-multipart-file-uploads-to-database-without-storing
    String upload(@ModelAttribute @NonNull final BinaryUpload request) throws IOException, ExecutionException, InterruptedException {
        final UUID serverId = UUID.fromString(request.getServerId());
        final Server server = ServerManager.getInstance().getServers().get(serverId);
        String[] identifiers = request.getParameterIdentifier().split("/");
        if (server == null || identifiers.length != 8) {
            return null;
            // todo error
        }
        final String fullyQualifiedFeatureIdentifier = String.join("/", identifiers[0], identifiers[1], identifiers[2], identifiers[3]);
        final String commandIdentifier = identifiers[5];
        final InputStream inputStream = request.getFile().getInputStream();
        final BinaryUploader binaryUploader = new BinaryUploader(inputStream, request.getFile().getSize(), request.getParameterIdentifier());
        final SiLACall siLACall = new SiLACall.Builder(
                serverId, fullyQualifiedFeatureIdentifier, commandIdentifier, SiLACall.Type.UPLOAD_BINARY
        ).withMetadata(request.getMetadatas()).build();
        final String createBinaryResponse = ServerManager.getInstance().getServerCallManager()
                .runAsync(ExecutableServerCall
                        .newBuilder(siLACall)
                        .withBinaryUploader(binaryUploader)
                        .build()
                )
                .get();
        SiLABinaryTransfer.CreateBinaryResponse.Builder builder = SiLABinaryTransfer.CreateBinaryResponse.newBuilder();
        JsonFormat.parser().merge(createBinaryResponse, builder);
        return builder.build().getBinaryTransferUUID();
    }

    @PostMapping("/calls/binary/download")
    ResponseEntity<StreamingResponseBody> download(@RequestBody @NonNull final BinaryDownload request, HttpServletResponse response) {
        final UUID serverId = UUID.fromString(request.getServerId());
        final Server server = ServerManager.getInstance().getServers().get(serverId);
        if (server == null) {
            return null;
            // todo error
        }

        final StreamingResponseBody responseBody = outputStream -> {
            try {
                // todo fixme see https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/issues/47
                final BinaryDownloader binaryDownloader = new BinaryDownloader(response.getOutputStream(), UUID.fromString(request.getBinaryTransferUuid()));
                final String binaryInfo = ServerManager.getInstance().getServerCallManager()
                        .runAsync(ExecutableServerCall
                                .newBuilder(SiLACall.fromBinaryDownload(serverId))
                                .withBinaryDownloader(binaryDownloader)
                                .build()
                        )
                        .get();
                binaryDownloader.isValidAndCompleteOrThrow();
                SiLABinaryTransfer.GetBinaryInfoResponse.Builder builder = SiLABinaryTransfer.GetBinaryInfoResponse.newBuilder();
                JsonFormat.parser().merge(binaryInfo, builder);
            } catch (InterruptedException | ExecutionException e) {
                log.warn("Binary download failed {}", request.getBinaryTransferUuid(), e);
                throw new RuntimeException(e);
            }
        };
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + request.getBinaryTransferUuid() + ".bin")
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(responseBody);
    }
}
