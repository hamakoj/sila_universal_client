package com.silastandard.universalclient.controllers.rest;

import com.silastandard.universalclient.common.models.db.Server;
import com.silastandard.universalclient.mappers.ServerMapper;
import com.silastandard.universalclient.mappers.ServerMerger;
import com.silastandard.universalclient.services.ServerService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.Set;

@RestController
@Slf4j
@RequestMapping("/back")
public class ServerController {

    private final ServerService serverService;

    @Autowired
    public ServerController(@NonNull final ServerService serverService) {
        this.serverService = serverService;
    }

    @GetMapping("/servers")
    public Set<Server> all() {
        return this.serverService.getAllServers();
    }

    @GetMapping("/servers/online")
    public Set<Server> allOnline() {
        return this.serverService.getAllOnlineServers();
    }

    @GetMapping("/servers/{uuid}")
    public Optional<Server> one(@NonNull final @PathVariable String uuid) {
        return this.serverService.getServer(uuid);
    }


    @Transactional
    public Server persistSiLAServer(
            @NonNull final sila_java.library.manager.models.Server server,
            @NonNull final String serverUuid
    ) {
        final Optional<Server> previousServer = this.serverService.getServer(serverUuid);
        com.silastandard.universalclient.common.models.db.Server persistentServer;
        if (!previousServer.isPresent()) {
            persistentServer = ServerMapper.toPersistentServer(server);
            log.info("Added Server {} to persistence context.", persistentServer);
        } else {
            log.info("Server addition: Server [serverUuid={}] already in persistence context.", serverUuid);
            persistentServer = previousServer.get();
            ServerMerger.merge(ServerMapper.toPersistentServer(server), persistentServer);
            log.info("Updated Server {} to persistence context.", persistentServer.getServerUuid());
        }
        return this.serverService.save(persistentServer);
    }
}
