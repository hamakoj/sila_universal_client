package com.silastandard.universalclient.controllers.rest;

import com.silastandard.universalclient.common.models.sila.ServerUUIDFQIFeature;
import com.silastandard.universalclient.services.application.MetadataManagerService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;


@RestController
@Slf4j
public class MetadataController {
    private final MetadataManagerService metadataManagerService;

    @Autowired
    public MetadataController(
            @NonNull final MetadataManagerService metadataManagerService
    ) {
        this.metadataManagerService = metadataManagerService;
    }

    @GetMapping("/servers/{serverUuid}/features/metadatas/fcpAffectedBy")
    public Map<String, Set<String>> getServerFCPAffectedByMetadataMap(@PathVariable @NonNull final String serverUuid) {
        try {
            final UUID serverUUID = UUID.fromString(serverUuid);
            return this.metadataManagerService
                    .getServerMetadata(serverUUID)
                    .stream()
                    .collect(
                            Collectors.toMap(
                                    fqiMetadata -> fqiMetadata,
                                    fqiMetadata -> this.metadataManagerService.getFcpAffectedByMetadataForMetadata(serverUUID, fqiMetadata)
                            )
                    );
        } catch (RuntimeException e) {
            log.debug("Exception occurred while retrieving FCP affected by metadata", e);
        }
        return Collections.emptyMap();
    }

    @GetMapping("/servers/{serverUuid}/features/{originator}/{category}/{featureIdentifier}/{featureMajorVersion}/metadatas/{metadataRelativeId}/fcpAffectedBy")
    public Set<String> getServerFCPAffectedByMetadataSet(
            @PathVariable @NonNull final UUID serverUuid,
            @PathVariable @NonNull final String originator,
            @PathVariable @NonNull final String category,
            @PathVariable @NonNull final String featureIdentifier,
            @PathVariable @NonNull final String featureMajorVersion,
            @PathVariable @NonNull final String metadataRelativeId
    ) {
        final ServerUUIDFQIFeature uuidfqiFeature = new ServerUUIDFQIFeature(
                serverUuid, originator, category, featureIdentifier, featureMajorVersion
        );
        final String fullyQualifiedMetadataIdentifier = String.join(
                "/", uuidfqiFeature.getFullyQualifiedFeatureIdentifier(), "Metadata", metadataRelativeId
        );
        final Set<String> fcpAffectedByMetadataSet = this.metadataManagerService
                .getFcpAffectedByMetadataForMetadata(serverUuid, fullyQualifiedMetadataIdentifier);
        log.info("Request to get the list of Features, Commands, Properties affected by [{}] Metadata " +
                        "returned [{}] qualifiers for Server [{}] Feature [{}].",
                metadataRelativeId,
                fcpAffectedByMetadataSet.size(),
                serverUuid,
                uuidfqiFeature.getFullyQualifiedFeatureIdentifier()
        );
        return fcpAffectedByMetadataSet;
    }
}
