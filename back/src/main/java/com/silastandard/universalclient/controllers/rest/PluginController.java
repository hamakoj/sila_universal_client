package com.silastandard.universalclient.controllers.rest;

import com.silastandard.universalclient.common.models.plugins.PluginModel;
import com.silastandard.universalclient.plugin.PluginLoader;
import com.silastandard.universalclient.services.application.ApplicationRestartService;
import com.silastandard.universalclient.common.models.plugins.PluginRemove;
import com.silastandard.universalclient.common.models.plugins.PluginUpload;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@RestController
public class PluginController {
    private final ApplicationRestartService applicationRestartService;

    @Autowired
    public PluginController(ApplicationRestartService applicationRestartService) {
        this.applicationRestartService = applicationRestartService;
    }

    @GetMapping("/plugin")
    Set<PluginModel> list() {
        try {
            return PluginLoader.getPluginsToLoadModels();
        } catch (RuntimeException e) {
            log.info("Error while retrieve plugins model", e);
        }
        return new HashSet<>();
    }

    @PostMapping("/plugin/add")
    ResponseEntity<?> add(@ModelAttribute PluginUpload pluginUpload, ModelMap modelMap) {
        modelMap.addAttribute("pluginUpload", pluginUpload);
        try {
            // todo check if plugin is compatible with current app version
            if (PluginLoader.addAndLoadPlugin(pluginUpload.getFile().getInputStream(), pluginUpload.getFile().getOriginalFilename())) {
                applicationRestartService.restart();
                // todo fixme since app restart, the status code is not sent
                return new ResponseEntity(HttpStatus.OK);
            }
        } catch (IOException e) {
            log.error("Failed to load plugin {}", pluginUpload.getFile().getOriginalFilename(), e);
        }
        return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
    }

    @SneakyThrows
    @PostMapping("/plugin/remove")
    ResponseEntity remove(@RequestBody PluginRemove pluginRemove) {
        final URL url = PluginLoader.unloadPluginByName(pluginRemove.getName());
        if (url != null) {
            applicationRestartService.restart();
            // todo fixme since app restart, the status code is not sent
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
