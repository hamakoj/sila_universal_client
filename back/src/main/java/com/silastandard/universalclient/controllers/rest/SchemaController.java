package com.silastandard.universalclient.controllers.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.silastandard.universalclient.common.models.xml.XmlValidationRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import sila_java.library.sila_base.EmptyClass;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@Slf4j
@RequestMapping("/schema")
public class SchemaController {

    private static final int MAX_SIZE = 5 * 1024 * 1024; // 5 MiB

    static class PayloadSizeException extends RuntimeException {
        public PayloadSizeException() {
            super("The provided XSD is larger than 5 MiB.");
        }

        public PayloadSizeException(String uri) {
            super("The provided XSD " + uri + " is larger than 5 MiB.");
        }
    }

    @GetMapping("/fetch")
    public ResponseEntity<String> fetchSchema(@RequestParam("url") String url) {
        try {
            final String data = fetchData(url);

            if (data.length() > MAX_SIZE) {
                throw new PayloadSizeException();
            }

            if (isValidXsd(data)) {
                HttpHeaders headers = new HttpHeaders();
                headers.set("Cache-Control", "public, max-age=3600"); // 1 hour
                return ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_XML)
                        .body(data);
            }

            if (isValidJson(data)) {
                HttpHeaders headers = new HttpHeaders();
                headers.set("Cache-Control", "public, max-age=3600"); // 1 hour
                return ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(data);
            }

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("The provided data is not a valid XSD or JSON schema.");
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("An error occurred while fetching the data.");
        } catch (PayloadSizeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("The provided XSD is larger than 5 MiB.");
        }
    }

    @PostMapping("/validate-xml")
    public boolean validateXml(@RequestBody @NonNull final XmlValidationRequest request) {
        final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        schemaFactory.setResourceResolver(new CustomLSResourceResolver());
        StreamSource schemaStreamSource = null;
        StreamSource xmlStreamSource = null;
        try {
            schemaStreamSource = new StreamSource(IOUtils.toInputStream(request.getSchema(), StandardCharsets.UTF_8));
            final Schema schema = schemaFactory.newSchema(schemaStreamSource);
            final Validator validator = schema.newValidator();
            xmlStreamSource = new StreamSource(IOUtils.toInputStream(request.getXml(), StandardCharsets.UTF_8));
            validator.validate(xmlStreamSource);
            return true;
        } catch (org.xml.sax.SAXException | IOException e) {
            log.debug("Failed to Validate/parse xml ", e);
        } finally {
            if (schemaStreamSource != null) {
                IOUtils.closeQuietly(schemaStreamSource.getInputStream());
                IOUtils.closeQuietly(schemaStreamSource.getReader());
            }
            if (xmlStreamSource != null) {
                IOUtils.closeQuietly(xmlStreamSource.getInputStream());
                IOUtils.closeQuietly(xmlStreamSource.getReader());
            }
        }
        return false;
    }

    private String fetchData(String url) throws IOException, PayloadSizeException {
        final CustomLSResourceResolver resolver = new CustomLSResourceResolver();
        final LSInput lsInput = resolver.resolveResource(null, null, null, url, null);

        if (lsInput == null) {
            throw new IOException("Failed to fetch data from the URL: " + url);
        }

        try (final InputStream inputStream = lsInput.getByteStream()) {
            final byte[] data = IOUtils.toByteArray(inputStream);

            if (data.length > MAX_SIZE) {
                throw new PayloadSizeException();
            }

            return new String(data, StandardCharsets.UTF_8);
        }
    }

    private boolean isValidJson(String data) {
        final ObjectMapper objectMapper = new ObjectMapper();
        try {
            final JsonNode jsonNode = objectMapper.readTree(new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8)));
            return jsonNode.isObject();
        } catch (IOException e) {
            return false;
        }
    }


    private boolean isValidXsd(String data) {
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setValidating(true);
        factory.setAttribute(
                "http://java.sun.com/xml/jaxp/properties/schemaLanguage",
                "http://www.w3.org/2001/XMLSchema"
        );

        try {
            final DocumentBuilder builder = factory.newDocumentBuilder();
            builder.setErrorHandler(new ErrorHandler() {
                @Override
                public void warning(SAXParseException exception) {
                    // ignore warning
                }

                @Override
                public void error(SAXParseException exception) {
                    // ignore non critical error
                }

                @Override
                public void fatalError(SAXParseException exception) throws SAXException {
                    throw exception;
                }
            });
            builder.parse(new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8)));
            return true;
        } catch (ParserConfigurationException | SAXException | IOException e) {
            return false;
        }
    }

    public static class CustomLSResourceResolver implements LSResourceResolver {
        private static final String BASE_URL = "https://gitlab.com/SiLA2/sila_base/raw/master/schema/";
        private static final String LOCAL_SCHEMA_FOLDER = "/sila_base/schema/";

        @Override
        public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {
            try {
                String finalSystemId = systemId;
                InputStream resourceAsStream = null;

                if (shouldUseLocalResource(systemId) || isLocalFile(systemId)) {
                    final String localSystemId = systemId.replace(BASE_URL, "");
                    final Path basePath = Paths.get(LOCAL_SCHEMA_FOLDER);
                    // check directory traversal attack
                    final Path fullPath = basePath.resolve(localSystemId).normalize();

                    if (!fullPath.startsWith(basePath)) {
                        throw new RuntimeException("Invalid path: " + systemId);
                    }

                    resourceAsStream = EmptyClass.class.getResourceAsStream(LOCAL_SCHEMA_FOLDER + localSystemId);
                    finalSystemId = LOCAL_SCHEMA_FOLDER + localSystemId;
                } else {
                    finalSystemId = (!isAbsoluteUrl(systemId)) ? (BASE_URL + systemId) : systemId; // todo add a way to prevent loading external url
                    checkRemoteResourceSize(finalSystemId);
                    resourceAsStream = new URL(finalSystemId).openStream();
                }

                final DOMImplementationLS lsImpl = (DOMImplementationLS) DocumentBuilderFactory
                        .newInstance()
                        .newDocumentBuilder()
                        .getDOMImplementation()
                        .getFeature("LS", "3.0");
                final LSInput lsInput = lsImpl.createLSInput();
                lsInput.setPublicId(publicId);
                lsInput.setSystemId(finalSystemId);
                lsInput.setByteStream(resourceAsStream);
                return lsInput;
            } catch (IOException | ParserConfigurationException e) {
                throw new RuntimeException("Failed to load schema with systemId: " + systemId, e);
            }
        }

        private void checkRemoteResourceSize(String url) throws IOException, PayloadSizeException {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("HEAD");
            long contentLength = connection.getContentLengthLong();
            connection.disconnect();

            if (contentLength > MAX_SIZE) {
                throw new PayloadSizeException(url);
            }
        }


        private boolean isAbsoluteUrl(String url) {
            try {
                URI uri = new URI(url);
                return uri.isAbsolute();
            } catch (URISyntaxException e) {
                return false;
            }
        }

        private boolean isLocalFile(String systemId) {
            try {
                Path path = Paths.get(systemId);
                return !path.isAbsolute() || FileSystems.getDefault().getPath(systemId).isAbsolute();
            } catch (Exception e) {
                return false;
            }
        }

        private boolean shouldUseLocalResource(String systemId) {
            return systemId.startsWith(BASE_URL);
        }
    }
}