package com.silastandard.universalclient.controllers.sila.rest.impl;

import com.silastandard.universalclient.common.models.sila.SiLACallModel;
import com.silastandard.universalclient.common.models.sila.SiLACallResult;
import com.silastandard.universalclient.controllers.sila.rest.SiLARestCallController;
import com.silastandard.universalclient.plugin.events.call.RuntimeCallExecutionRequestEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.executor.ExecutableServerCall;
import sila_java.library.manager.executor.ServerCallManager;
import sila_java.library.manager.models.*;

import java.util.concurrent.ExecutionException;

@Slf4j
@RestController
@RequestMapping("/runtime/calls")
public class RuntimeSiLARestCallController implements SiLARestCallController<SiLACallResult> {
    private final ServerManager serverManager = ServerManager.getInstance();
    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public RuntimeSiLARestCallController(
            @lombok.NonNull final ApplicationEventPublisher applicationEventPublisher
    ) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public SiLACallResult execute(@RequestBody @NonNull final SiLACallModel request) {
        final SiLACall siLACall = request.toSiLACall();
        final Server server = this.serverManager.getServers().get(siLACall.getServerId());
        RuntimeCallExecutionRequestEvent runtimeCallExecutionRequestEvent = new RuntimeCallExecutionRequestEvent(request, server);
        this.applicationEventPublisher.publishEvent(runtimeCallExecutionRequestEvent);
        if (runtimeCallExecutionRequestEvent.isCancelled()) {
            throw new RuntimeException("Execution cancelled by a plugin");
        }

        if (server == null) {
            throw new RuntimeException("No Server were found for specified Call model");
        }

        return this.executeGrpcSiLACall(siLACall, request);
    }

    private SiLACallResult executeGrpcSiLACall(@NonNull final SiLACall siLACall, @NonNull final SiLACallModel request) {
        final String result;
        SiLACallResult siLACallResult;
        if (request.isRunAsync()) {
            log.warn("RunAsync is ignored because they are no listeners to retrieve the async updates when running asynchronously");
        }
        try {
            final ServerCallManager callManager = ServerManager.getInstance().getServerCallManager();
            final ExecutableServerCall serverCall = ExecutableServerCall.newBuilder(siLACall).build();
            // todo add timeout
            result = callManager.runAsync(serverCall, false, null).get();
            siLACallResult = new SiLACallResult(request, true, result);
        } catch (ExecutionException | InterruptedException | RuntimeException e) {
            siLACallResult = new SiLACallResult(request, false, e.getMessage());
            log.warn("Call execute exception", e);
        }
        return siLACallResult;
    }
}
