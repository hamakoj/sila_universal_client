package com.silastandard.universalclient.controllers.rest;

import com.silastandard.universalclient.common.models.dto.Version;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersionController {
    @Value("${git.branch}")
    private String branch;

    @Value("${git.build.host}")
    private String buildHost;

    @Value("${git.build.version}")
    private String buildVersion;

    @Value("${git.closest.tag.name}")
    private String closestTag;

    @Value("${git.commit.id}")
    private String commitId;

    @Value("${git.commit.message.full}")
    private String commitMessage;

    @Value("${git.dirty}")
    private String dirty;

    @GetMapping("version")
    public Version getVersion() {
        return new Version(
                this.branch,
                this.buildHost,
                this.buildVersion,
                this.closestTag,
                this.commitId,
                this.commitMessage,
                this.dirty
        );
    }
}
