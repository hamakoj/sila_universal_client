package com.silastandard.universalclient.controllers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.silastandard.universalclient.common.models.db.Call;
import com.silastandard.universalclient.common.models.sila.SiLACallModel;
import com.silastandard.universalclient.common.models.sila.SiLACallResult;
import com.silastandard.universalclient.controllers.rest.CallController;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.errors.SiLAErrorException;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.executor.CallListener;
import sila_java.library.manager.executor.ExecutableServerCall;
import sila_java.library.manager.executor.ServerCallManager;
import sila_java.library.manager.models.CallCompleted;
import sila_java.library.manager.models.CallErrored;
import sila_java.library.manager.models.ExtendedSiLACall;
import sila_java.library.manager.models.SiLACall;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

@Controller
@Slf4j
public class CallExecutor {

    private final CallController callController;

    @Autowired
    public CallExecutor(@NonNull final CallController callController) {
        this.callController = callController;
    }

    private static String errorStringToJson(@NonNull final String error) {
        final JsonObject json = new JsonObject();
        json.addProperty("error", error);
        return new Gson().toJson(json);
    }

    public Optional<SiLACallResult> executeGrpcSiLACallThroughServerManager(
            @NonNull final SiLACall siLACall,
            @NonNull final UUID callUUID,
            final boolean runAsync
    ) {
        final SiLACallModel request = SiLACallModel.fromSiLACall(siLACall);
        // todo remove, not needed anymore
        final ExtendedSiLACall extendedCall = new ExtendedSiLACall(siLACall);

        log.debug("Add Metadata {} to Call [{}]",
                extendedCall.getMetadatas(),
                siLACall.getCallId()
        );

        final ServerCallManager callManager = ServerManager.getInstance().getServerCallManager();
        final ExecutableServerCall serverCall = ExecutableServerCall.newBuilder(siLACall).build();
        final AtomicBoolean handledError = new AtomicBoolean(false);
        try {
            final Future<String> futureResult = callManager.runAsync(serverCall, new CallListener() {
                @Override
                public void onObservableCommandInit(SiLACall baseCall, SiLAFramework.CommandConfirmation command) {
                    try {
                        callController.setCommandExecutionUUID(callUUID, command.getCommandExecutionUUID().getValue());
                    } catch (Exception e) {
                        log.warn("Failed to save command execution uuid for call " + baseCall.getIdentifier().toString(), e);
                    }
                }

                @Override
                public void onComplete(CallCompleted callCompleted) {
                    SiLACallResult siLACallResult = new SiLACallResult(request, true, callCompleted.getResult());
                    callController.finishCall(siLACallResult, callUUID);
                }

                @Override
                public void onError(CallErrored callErrored) {
                    if (handledError.get()) {
                        return;
                    }
                    handledError.set(true);
                    SiLACallResult siLACallResult = new SiLACallResult(request, false, callErrored.getError());
                    callController.finishCall(siLACallResult, callUUID);
                }
            });
            if (!runAsync) {
                futureResult.get();
            }
        } catch (Exception e) {
            SiLACallResult siLACallResult;
            if (e instanceof SiLAErrorException) {
                try {
                    final String json = JsonFormat.printer().includingDefaultValueFields().print(((SiLAErrorException) e).getSiLAError());
                    siLACallResult = new SiLACallResult(request, false, json);
                } catch (InvalidProtocolBufferException ex) {
                    siLACallResult = new SiLACallResult(
                            request,
                            false,
                            errorStringToJson(((SiLAErrorException) e).getSiLAError().toString())
                    );
                }
            } else {
                String errorMessage = e.getMessage();
                if (e instanceof InterruptedException || e instanceof CancellationException) {
                    errorMessage = "Call has been interrupted";
                }
                if (errorMessage == null) {
                    errorMessage = "Unknown error";
                }
                siLACallResult = new SiLACallResult(request, false, errorStringToJson(errorMessage));
            }
            if (!handledError.get()) {
                handledError.set(true);
                callController.finishCall(siLACallResult, callUUID);
                return Optional.of(siLACallResult);
            }
        }
        return Optional.empty();
    }
}
