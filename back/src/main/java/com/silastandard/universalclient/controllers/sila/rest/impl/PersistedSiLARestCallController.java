package com.silastandard.universalclient.controllers.sila.rest.impl;

import com.silastandard.universalclient.common.models.db.Call;
import com.silastandard.universalclient.common.models.db.Client;
import com.silastandard.universalclient.common.models.db.Server;
import com.silastandard.universalclient.common.models.sila.SiLACallModel;
import com.silastandard.universalclient.controllers.CallExecutor;
import com.silastandard.universalclient.controllers.rest.CallController;
import com.silastandard.universalclient.controllers.rest.ClientController;
import com.silastandard.universalclient.controllers.rest.ServerController;
import com.silastandard.universalclient.controllers.sila.rest.SiLARestCallController;
import com.silastandard.universalclient.controllers.sila.ws.WsCallController;
import com.silastandard.universalclient.plugin.events.call.PersistedExecutionRequestEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.models.CallErrored;
import sila_java.library.manager.models.SiLACall;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.silastandard.universalclient.utils.Constants.UNIQUE_CLIENT_ID;

@Slf4j
@RestController
public class PersistedSiLARestCallController implements SiLARestCallController<Call> {

    private final CallController callController;
    private final ClientController clientController;
    private final ServerController serverController;
    private final CallExecutor callExecutor;
    private final WsCallController wsCallController;
    private final ApplicationEventPublisher applicationEventPublisher;


    @Autowired
    public PersistedSiLARestCallController(
            @NonNull final CallController callController,
            @NonNull final ClientController clientController,
            @NonNull final ServerController serverController,
            @NonNull final CallExecutor callExecutor,
            @NonNull final WsCallController wsCallController,
            @NonNull final ApplicationEventPublisher applicationEventPublisher
    ) {
        this.callController = callController;
        this.clientController = clientController;
        this.serverController = serverController;
        this.callExecutor = callExecutor;
        this.wsCallController = wsCallController;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @GetMapping("/running")
    public List<SiLACall> runningCalls() {
        return ServerManager.getInstance().getServerCallManager()
                .getCalls()
                .stream()
                .map(c -> c.getExecutableServerCall().getBaseCall())
                .collect(Collectors.toList());
    }

    @Override
    public Call execute(@RequestBody @NonNull final SiLACallModel model) {
        final Optional<Server> server = this.serverController.one(model.getServerId().toString());

        PersistedExecutionRequestEvent callExecutionRequestEvent = new PersistedExecutionRequestEvent(model, server);
        this.applicationEventPublisher.publishEvent(callExecutionRequestEvent);
        if (callExecutionRequestEvent.isCancelled()) {
            throw new RuntimeException("Execution cancelled by a plugin");
        }

        // Check server presence
        if (!server.isPresent()) {
            // todo log and handle what should happen
            throw new RuntimeException("No Server were found for specified Call model");
        }

        // Create SiLACall with unique Identifier
        final SiLACall siLACall = new SiLACall.Builder(model.getServerId(), model.getFullyQualifiedFeatureId(), model.getCallId(), model.getType())
                .withMetadata(model.getMetadatas().orElse("{}"))
                .withParameters(model.getParameters().orElse("{}"))
                .build();

        // Initialize Call in the persistence context
        final Call call = this.setupCallPersistence(siLACall.getIdentifier(), model);
        final OffsetDateTime startDate = OffsetDateTime.now();

        this.callExecutor.executeGrpcSiLACallThroughServerManager(siLACall, call.getUuid(), model.isRunAsync()).ifPresent((result) -> {
            // optional is present only in case of error when running async
            if (!result.getSuccess()) {
                final CallErrored callErrored = new CallErrored(
                        startDate,
                        OffsetDateTime.now(),
                        result.getValue(),
                        result.getRequest().toSiLACall()
                );
                this.wsCallController.onError(callErrored);
            }
        });

        return call;
    }

    private Call setupCallPersistence(
            @NonNull final UUID callUuid,
            @NonNull final SiLACallModel siLACallModel
    ) {
        // todo Parse REST call headers to obtain client_id
        final Client client = this.clientController.createOrRetrieveClient(UNIQUE_CLIENT_ID);
        return this.callController.startCall(callUuid, siLACallModel, client);
    }
}
