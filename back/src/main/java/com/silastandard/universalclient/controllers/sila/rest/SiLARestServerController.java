package com.silastandard.universalclient.controllers.sila.rest;

import com.silastandard.universalclient.common.models.sila.ServerHostPortCertificate;
import lombok.NonNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sila_java.library.manager.models.Server;

import java.util.Map;
import java.util.UUID;

@RequestMapping("/servers")
public interface SiLARestServerController {

    @GetMapping("")
    Map<UUID, Server> all();

    @GetMapping("/online")
    Map<UUID, Server> allOnline();

    @GetMapping("/scan")
    void scan();

    @GetMapping("/{uuid}")
    Server one(@PathVariable @NonNull final String uuid);

    @PostMapping("")
    ResponseEntity<String> add(@RequestBody @NonNull final ServerHostPortCertificate serverHostPortCertificate);

    @DeleteMapping("/{uuid}")
    void remove(@PathVariable @NonNull final String uuid);
}
