package com.silastandard.universalclient.plugin.events.call;

import com.silastandard.universalclient.common.models.db.Server;
import com.silastandard.universalclient.common.models.sila.SiLACallModel;
import com.silastandard.universalclient.plugin.events.Cancellable;
import com.silastandard.universalclient.plugin.events.UscEvent;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.lang.Nullable;

import java.util.Optional;

@Getter
public class PersistedExecutionRequestEvent extends UscEvent implements Cancellable {
    private final SiLACallModel request;
    private final Optional<Server> server;
    private boolean cancelled = false;

    public PersistedExecutionRequestEvent(@NonNull final SiLACallModel request, @Nullable final Server server) {
        this.request = request;
        this.server = Optional.ofNullable(server);
    }

    public PersistedExecutionRequestEvent(@NonNull final SiLACallModel request, @NonNull final Optional<Server> server) {
        this.request = request;
        this.server = server;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }
}
