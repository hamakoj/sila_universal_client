package com.silastandard.universalclient.plugin.events;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Getter
public class UscEvent implements Serializable {
    private final long timestamp = System.currentTimeMillis();
}
