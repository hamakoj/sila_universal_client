package com.silastandard.universalclient.plugin.events.lifecycle;

import com.silastandard.universalclient.plugin.events.UscEvent;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PluginStartEvent extends UscEvent {
    // Made for future use
}
