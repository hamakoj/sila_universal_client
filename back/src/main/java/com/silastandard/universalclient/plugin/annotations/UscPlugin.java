package com.silastandard.universalclient.plugin.annotations;

import lombok.NonNull;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Component()
public @interface UscPlugin {
    @NonNull String name();
    @NonNull String version();
}
