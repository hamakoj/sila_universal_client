package com.silastandard.universalclient.plugin;

import com.silastandard.universalclient.plugin.events.lifecycle.PluginCloseEvent;
import com.silastandard.universalclient.plugin.events.lifecycle.PluginLoadEvent;
import com.silastandard.universalclient.plugin.events.lifecycle.PluginStartEvent;

public interface PluginService {
    void onLoad(PluginLoadEvent event);

    void onStart(PluginStartEvent event);

    void onStop(PluginCloseEvent event);
    // todo implement onStop in usc
}
