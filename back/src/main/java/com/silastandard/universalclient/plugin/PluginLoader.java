package com.silastandard.universalclient.plugin;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import com.silastandard.universalclient.common.models.plugins.PluginModel;
import com.silastandard.universalclient.config.PluginBeanNameGenerator;
import com.silastandard.universalclient.plugin.annotations.UscPlugin;
import com.silastandard.universalclient.plugin.events.lifecycle.PluginLoadEvent;
import com.silastandard.universalclient.plugin.events.lifecycle.PluginStartEvent;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.BeansException;
import org.springframework.boot.context.event.*;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import sila_java.library.manager.ServerManager;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidObjectException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.stream.Collectors;

// todo fixme curr impl is not stable when loading / unloading at runtime
// todo check if we should use ApplicationStartingEvent
//  see: https://reflectoring.io/spring-boot-application-events-explained/
@Slf4j
public class PluginLoader {
    // doc https://reflectoring.io/spring-boot-application-events-explained/#spring-boots-application-events
    // https://stackoverflow.com/questions/4540713/add-bean-programmatically-to-spring-web-app-context
    // https://stackoverflow.com/questions/55071299/is-there-any-way-to-scan-all-component-like-annotated-class-after-spring-applic
    // https://programmer.help/blogs/what-is-the-principle-of-spring-cloud-refreshscope.html
    @Getter
    @Setter
    private static Set<URL> urls = getPluginsURLs();
    @Getter
    @Setter
    private static Map<String, URL> loadedPlugins = new HashMap<>();
    @Getter
    private static JarLoader.MergedClassPath mergedClassPath = null;
    @Getter
    private static final Set<URL> pluginsToRemove = new HashSet<>(); // todo make thread safe
    private ClassLoader originalClassloader = null;
    @Getter
    private final ApplicationListener<ApplicationStartingEvent> applicationStartingEvent = this::onApplicationStartingEvent;
    @Getter
    private final ApplicationListener<ApplicationEnvironmentPreparedEvent> applicationEnvironmentPreparedEvent = this::onApplicationEnvironmentPreparedEvent;
    @Getter
    private final ApplicationListener<ApplicationContextInitializedEvent> applicationContextInitializedEvent = this::onApplicationContextInitializedEvent;
    @Getter
    private final ApplicationListener<ApplicationPreparedEvent> applicationPreparedEvent = this::onApplicationPreparedEvent;
    @Getter
    private final ApplicationListener<ContextRefreshedEvent> contextRefreshedEvent = this::onContextRefreshedEvent;
    @Getter
    private final ApplicationListener<ApplicationReadyEvent> applicationReadyEvent = this::onApplicationReadyEvent;

    @Getter
    private static final Set<PluginService> registeredPluginServices = new HashSet<>();
    private static final Set<URL> pluginsToLoad = new HashSet<>();
    private static final Map<URL, JarLoader.PluginContextClash> clashMap = new HashMap<>();

    public void onApplicationStartingEvent(@NonNull final ApplicationStartingEvent event) {
        //first event fired on start or restart
        // todo this might be done before restarting the app, or cancel all calls before restarting the app to save everything in db
        ServerManager.getInstance().close(); // clean state of singleton in case of restart
        if (PluginLoader.mergedClassPath != null) {
            try {
                // The plugins jar will be unlocked in the callback ApplicationEnvironmentPreparedEvent
                // the files are still locked in the callback and cannot be removed.
                // This is why they are removed in the next callback
                PluginLoader.mergedClassPath.close();
                PluginLoader.mergedClassPath = null;
            } catch (Exception e) {
                log.error("Failed to close Merged Class path, this can lead to memory leak!!", e);
            }
        }
    }

    public void onApplicationEnvironmentPreparedEvent(@NonNull final ApplicationEnvironmentPreparedEvent event) {
        final Set<URL> successfullyRemovedPlugins = new HashSet<>();
        PluginLoader.pluginsToRemove.forEach(url -> {
            try {
                File file = new File(url.toURI());
                try {
                    FileDeleteStrategy.FORCE.delete(file);
                } catch (Exception e) {
                    file.deleteOnExit();
                    throw e;
                }
                successfullyRemovedPlugins.add(url);
            } catch (URISyntaxException | IOException e) {
                log.warn("Failed to delete plugin, will attempt to delete it when the app is closed!", e);
            }
        });
        PluginLoader.pluginsToRemove.removeAll(successfullyRemovedPlugins);

        if (this.originalClassloader != null) {
            // todo check if safe/needed
            Thread.currentThread().setContextClassLoader(originalClassloader);
        } else {
            this.originalClassloader = Thread.currentThread().getContextClassLoader();
        }
        final Set<URL> urlsSet = getUrls();
        final URL[] urls = urlsSet.stream().toArray(URL[]::new);
        PluginLoader.mergedClassPath = JarLoader.mergeClasspathOnThreadWithClashManagement(urls, Thread.currentThread());
        final Map<URL, JarLoader.PluginContextClash> map = mergedClassPath.getPluginContextClashMap();
        clashMap.clear();
        clashMap.putAll(map);
        pluginsToLoad.clear();
        pluginsToLoad.addAll(Sets.difference(urlsSet, map.keySet()).immutableCopy());
        log.info("Plugins rejected {}", clashMap.size());
        log.info("Plugins to load {}", pluginsToLoad.size());
    }

    // todo check why log are swallowed silently
    public void onApplicationContextInitializedEvent(@NonNull final ApplicationContextInitializedEvent event) {
        if (event.getApplicationContext() instanceof AnnotationConfigServletWebServerApplicationContext) {
            AnnotationConfigServletWebServerApplicationContext ctx = (AnnotationConfigServletWebServerApplicationContext) event.getApplicationContext();
            ctx.setBeanNameGenerator(new PluginBeanNameGenerator());
            final List<URL> pluginsToLoad = getPluginsURLs()
                    .stream()
                    .filter((url) -> !clashMap.containsKey(url))
                    .collect(Collectors.toList());
            final ArrayList<Class<?>> pluginsClass = new ArrayList<>();
            for (URL url : pluginsToLoad) {
                try {
                    final PluginModel pluginModel = getPluginModel(url);
                    final Class<?> uscPluginClass = JarLoader.getUscPluginClass(url);
                    final UscPlugin uscPluginClassAnnotation = uscPluginClass.getAnnotation(UscPlugin.class);
                    if (!uscPluginClassAnnotation.name().equals(pluginModel.getName())) {
                        throw new InvalidObjectException("Name specified in plugin annotation and plugin.json must be identical.");
                    }
                    if (!uscPluginClassAnnotation.version().equals(pluginModel.getVersion())) {
                        throw new InvalidObjectException("Version specified in plugin annotation and plugin.json must be identical.");
                    }
                    pluginsClass.add(uscPluginClass);
                } catch (Throwable e) {
                    // todo keep the error to be then reported on the front using a Map<URL, Throwable> for example
                    log.error("Failed to load plugin at {} ", url, e);
                }
            }
            final Class<?>[] annotatedClasses = pluginsClass.toArray(new Class<?>[0]);
            if (annotatedClasses.length > 0) {
                ctx.register(annotatedClasses);
            } else {
                log.info("No plugin to load");
            }
        } else {
            // todo handle as error
            log.error("invalid cast");
        }
    }

    public void onApplicationPreparedEvent(@NonNull final ApplicationPreparedEvent event) {
    }

    public void onContextRefreshedEvent(@NonNull final ContextRefreshedEvent event) {
        loadPlugins(event.getApplicationContext());
    }

    public void onApplicationReadyEvent(@NonNull final ApplicationReadyEvent event) {

    }

    private static Set<URL> getPluginsURLs() {
        final File pluginFolder = new File("./plugins/");
        final File[] pluginsFile = pluginFolder.listFiles((dir, name) -> name.toLowerCase().endsWith(".jar"));
        Set<URL> urls = new HashSet<>();
        if (pluginsFile == null || pluginsFile.length == 0) {
            return urls;
        }
        for (File pluginFile : pluginsFile) {
            try {
                urls.add(pluginFile.toURI().toURL());
            } catch (MalformedURLException e) {
                log.warn("failed to add plugin", e);
            }
        }
        return urls
                .stream()
                .filter((url) -> PluginLoader.pluginsToRemove == null || !PluginLoader.pluginsToRemove.contains(url))
                .collect(Collectors.toSet());
    }

    public static Set<PluginModel> getPluginsToLoadModels() {
        final Set<PluginModel> set = new HashSet<>();
        for (URL url : pluginsToLoad) {
            PluginModel pluginModel = null;
            try {
                pluginModel = getPluginModel(url);
            } catch (IOException e) {
                // todo remove this utility function, we dont want to "silently" catch this error
                log.warn("Failed to retrieve plugin model of plugin {} because", url, e);
            }
            set.add(pluginModel);
        }
        return set;
    }

    public static boolean addAndLoadPlugin(@NonNull final InputStream inMemoryJarPlugin, @NonNull final String fileName) {
        try {
            final File targetFile = new File("./plugins/" + fileName);
            targetFile.getParentFile().mkdirs();
            FileUtils.copyInputStreamToFile(inMemoryJarPlugin, targetFile);
            final URL pluginJarUrl = targetFile.toURI().toURL();
            PluginLoader.urls.add(pluginJarUrl);
            // remove it from the plugins to remove in case a previous attempt to remove it failed
            PluginLoader.pluginsToRemove.remove(pluginJarUrl);
            return true;
        } catch (Exception e) {
            log.error("Failed to load plugin {}", fileName, e);
        }
        return false;
    }

    /**
     *
     * @param name
     * @return the plugin URL if the plugin has been unloaded or null otherwise
     */
    public static URL unloadPluginByName(@NonNull final String name) {
        final URL url = PluginLoader.loadedPlugins.get(name);
        if (url == null) {
            // todo handle
            log.warn("No plugin named {} to unload found", name);
            return null;
        } else {
            PluginLoader.getUrls().remove(url);
            PluginLoader.getLoadedPlugins().remove(name);
        }
        // todo call pluginInterface.onStop();
        PluginLoader.pluginsToRemove.add(url);
        return url;
    }

    public static void loadPlugins(final ApplicationContext applicationContext) {
        pluginsToLoad.forEach(pluginUrl -> {
            final PluginModel pluginModel;
            try {
                pluginModel = getPluginModel(pluginUrl);
            } catch (IOException e) {
                log.error("Failed to load plugin because failed model for plugin {}!", pluginUrl);
                return;
            }
            final String pluginBeanName = PluginBeanNameGenerator.getPluginBeanName(pluginModel.getName(), pluginModel.getVersion());
            try {
                final PluginService pluginService = applicationContext.getBean(pluginBeanName, PluginService.class);
                pluginService.onLoad(new PluginLoadEvent());
                //final Map<String, String> metadataValueSourceCopy = new HashMap<>();
                //pluginService.addMetadataSupport(metadataValueSourceCopy);
                // Merge new MetadataValueSource list with core one
                //metadataValueSourceCopy.forEach((key, value) -> {
                //    MetadataManagerService.getMetadataValuesSource().putIfAbsent(key, value);
                //});
                pluginService.onStart(new PluginStartEvent());
                PluginLoader.registeredPluginServices.add(pluginService);
                PluginLoader.loadedPlugins.put(pluginBeanName, pluginUrl);
                log.info("Successfully loaded plugin {} v{}", pluginModel.getName(), pluginModel.getVersion());
            } catch (BeansException e) {
                log.error("Plugin {} v{} has not been properly loaded into Spring!", pluginModel.getName(), pluginModel.getVersion());
            } catch (Throwable e) {
                log.error("Plugin {} v{} raised an error while loading/starting! {}", pluginModel.getName(), pluginModel.getVersion(), e);
            }
        });
        final Map<String, Object> loadedPluginsBean = applicationContext.getBeansWithAnnotation(UscPlugin.class);
        final Set<String> loadedPlugins = PluginLoader.getLoadedPlugins().keySet();
        Sets.difference(loadedPlugins, loadedPluginsBean.keySet()).forEach((pluginDiff) -> {
            log.error("Plugin with bean {} has been loaded by Spring but failed to be loaded by the Plugin system", pluginDiff);
        });
    }

    public static PluginModel getPluginModel(File pluginFile) throws IOException {
        return getPluginModel(pluginFile.toURI().toURL());
    }

    public static PluginModel getPluginModel(URL pluginFile) throws IOException {
        final ClassLoader classLoader = new URLClassLoader(new URL[]{pluginFile});
        final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(classLoader);
        final Resource[] resources = resolver.getResources("classpath*:**/plugin.json") ;
        if (resources.length == 0) {
            throw new InvalidObjectException("Plugin does not contain a plugin.json file");
        } else if (resources.length > 1) {
            throw new InvalidObjectException("Plugin contains multiple plugin.json file");
        }
        return new ObjectMapper().readValue(resources[0].getInputStream(), PluginModel.class);
    }
}