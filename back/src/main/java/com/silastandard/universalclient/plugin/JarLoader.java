package com.silastandard.universalclient.plugin;

import com.silastandard.universalclient.plugin.annotations.UscPlugin;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;

import java.io.File;
import java.io.InvalidObjectException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class JarLoader {

    @Getter
    @AllArgsConstructor
    public static class ClassLoaderContext {
        final Set<String> types;
        final Set<String> resources;

        public ClassLoaderContext intersect(ClassLoaderContext b) {
            // could be optimized by initializing the set with the context with the fewest element
            final Set<String> typesIntersection = new HashSet<>(this.types);
            final Set<String> resourcesIntersection = new HashSet<>(this.resources);
            typesIntersection.retainAll(b.types);
            resourcesIntersection.retainAll(b.resources);
            return new ClassLoaderContext(typesIntersection, resourcesIntersection);
        }

        public boolean isEmpty() {
            return this.types.isEmpty() && this.resources.isEmpty();
        }
    }

    @Getter
    @AllArgsConstructor
    public static class PluginContextClash {
        final ClassLoaderContext classLoaderContext;
        final Set<URL> clashWith;
        final Optional<Boolean> clashWithApp;

        public PluginContextClash(ClassLoaderContext classLoaderContext, URL clashWith) {
            this(classLoaderContext, new HashSet<>(Collections.singleton(clashWith)), Optional.empty());
        }

        public PluginContextClash(ClassLoaderContext classLoaderContext, boolean clashWithApp) {
            this(classLoaderContext, new HashSet<>(), Optional.of(clashWithApp));
        }
    }

    @Getter
    @AllArgsConstructor
    public static class MergedClassPath implements AutoCloseable {
        private final Map<URL, PluginContextClash> pluginContextClashMap;
        private final URLClassLoader urlClassLoader;

        @Override
        public void close() throws Exception {
            this.urlClassLoader.close();
        }
    }

    @SneakyThrows
    public static synchronized URLClassLoader mergeJarClasspathOnThread(@NonNull final File jar, @NonNull final Thread thread) {
        // inspired from https://kostenko.org/blog/2019/06/runtime-class-loading.html
        // todo check if works in java 8/11/16
        //URLClassLoader childClassLoader = new URLClassLoader(new URL[]{jar.toURI().toURL()}, ClassLoader.getSystemClassLoader()); // merged with thread classpath
        final URLClassLoader childClassLoader = new URLClassLoader(new URL[]{jar.toURI().toURL()}, thread.getContextClassLoader()); // merged with thread classpath
        thread.setContextClassLoader(childClassLoader);
        return childClassLoader;
    }

    public static Class<?> getUscPluginClass(@NonNull final URL url) throws InvalidObjectException {
        final Reflections reflections = new Reflections(Collections.singleton(url), new SubTypesScanner(), new TypeAnnotationsScanner());
        final Set<Class<?>> typesAnnotatedWith = reflections.getTypesAnnotatedWith(UscPlugin.class);
        if (typesAnnotatedWith.isEmpty()) {
            throw new InvalidObjectException("Plugin does not contains a class annotated with " + UscPlugin.class.getCanonicalName());
        }
        if (typesAnnotatedWith.size() > 1) {
            throw new InvalidObjectException("Plugin contain multiple class annotated with " + UscPlugin.class.getCanonicalName());
        }
        return typesAnnotatedWith.iterator().next();
    }

    public static String getPackageWithUscPluginClass(@NonNull final URL url) throws InvalidObjectException {
        final Class<?> uscPluginClass = getUscPluginClass(url);
        return uscPluginClass.getPackage().getName();
    }

    public static synchronized MergedClassPath mergeClasspathOnThreadWithClashManagement(
            @NonNull final URL[] urls, @NonNull final Thread thread
    ) {
        // todo check if works in java 8/11/16
        final List<URL> urlList = Arrays.asList(urls);
        // sort to consistently exclude the same jar in case of clash with another plugin
        urlList.sort(Comparator.comparing(URL::toExternalForm));
        final Reflections initialContextReflections = new Reflections(Thread.currentThread().getContextClassLoader(), new SubTypesScanner(false), new ResourcesScanner());
        final ClassLoaderContext initialContext = new ClassLoaderContext(
                initialContextReflections.getAllTypes(), initialContextReflections.getResources((p) -> true)
        );
        final Map<URL, ClassLoaderContext> pluginsContext = urlList.stream().map((url) -> {
            final Reflections reflections = new Reflections(Collections.singleton(url), new SubTypesScanner(false), new ResourcesScanner());
            final Set<String> jarTypes = reflections.getAllTypes();
            final Set<String> jarResources = reflections.getResources((p) -> true);
            return new AbstractMap.SimpleEntry<>(url, new ClassLoaderContext(jarTypes, jarResources));
        }).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
        final Map<URL, PluginContextClash> clashingJars = new HashMap<>();

        // check if plugins clash between themselves
        for (int i = 0; i < urlList.size(); i++) {
            final URL aURL = urlList.get(i);
            final ClassLoaderContext a = pluginsContext.get(aURL);
            // todo check if jar contains multiple annotation of @UscPlugin
            for (int j = i + 1; j < urlList.size(); j++) {
                final URL bURL = urlList.get(j);
                final ClassLoaderContext b = pluginsContext.get(bURL);
                final ClassLoaderContext intersectedContext = a.intersect(b);
                if (!intersectedContext.isEmpty()) {
                    final PluginContextClash pluginContextClashA = clashingJars.get(aURL);
                    final PluginContextClash pluginContextClashB = clashingJars.get(bURL);
                    // todo this is a bit hacky, we should have a proper exclusion matrix since a plugin can be
                    //  excluded because it clash with strictly one other,
                    //  but if the other is also excluded, the first plugin could have have been executed
                    if (pluginContextClashB != null) {
                        // b already excluded
                        pluginContextClashB.getClashWith().add(aURL);
                    } else if (pluginContextClashA == null) {
                        // b not excluded but a is
                        clashingJars.put(bURL, new PluginContextClash(intersectedContext, aURL));
                    } else {
                        // a & b are not excluded
                        clashingJars.put(aURL, new PluginContextClash(intersectedContext, bURL));
                    }
                }
            }
        }

        // check if plugins clash with app
        urlList.stream().filter(url -> !clashingJars.containsKey(url)).forEach((url) -> {
            final ClassLoaderContext b = pluginsContext.get(url);
            final ClassLoaderContext intersectedContext = b.intersect(initialContext);
            if (!intersectedContext.isEmpty()) {
                clashingJars.put(url, new PluginContextClash(intersectedContext, true));
            }
        });

        final URL[] filteredUrls = urlList.stream().filter(url -> !clashingJars.containsKey(url)).toArray(URL[]::new);
        final URLClassLoader urlClassLoader = new URLClassLoader(filteredUrls, thread.getContextClassLoader());
        thread.setContextClassLoader(urlClassLoader);
        return new MergedClassPath(clashingJars, urlClassLoader);
    }

    public static synchronized URLClassLoader mergeClasspathOnThread(@NonNull final URL[] urls, @NonNull final Thread thread) {
        // todo check if works in java 8/11/16
        URLClassLoader childClassLoader = new URLClassLoader(urls, thread.getContextClassLoader());
        thread.setContextClassLoader(childClassLoader);
        return childClassLoader;
    }
}
