package com.silastandard.universalclient.plugin.events.websocket;

import com.silastandard.universalclient.plugin.events.Cancellable;
import com.silastandard.universalclient.plugin.events.UscEvent;
import lombok.Getter;

import java.util.Map;

@Getter
public class WebsocketMessageEvent extends UscEvent implements Cancellable {
    private final String destination;
    private final String clientId;
    private final Map<String, Object> headers;
    private final String endpoint;
    private final Object message;
    private boolean cancelled = false;

    public WebsocketMessageEvent(String destination, String clientId, Map<String, Object> headers, String endpoint, Object message) {
        this.destination = destination;
        this.clientId = clientId;
        this.headers = headers;
        this.endpoint = endpoint;
        this.message = message;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }
}
