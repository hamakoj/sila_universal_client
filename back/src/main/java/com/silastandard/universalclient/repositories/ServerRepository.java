package com.silastandard.universalclient.repositories;

import com.silastandard.universalclient.common.models.db.Client;
import com.silastandard.universalclient.common.models.db.Server;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.Set;

public interface ServerRepository extends CrudRepository<Server, String> {
    Set<Server> findAllByStatus(String status);

    Set<Server> findAllByClientsIsContaining(Client client);

    Optional<Server> findByServerUuid(String uuid);

    Optional<Server> findFirstByServerHostAndPort(String host, int port);

    @Modifying
    @Query("UPDATE Server s SET s.status = 'UNKNOWN'")
    void setAllServersStatusToUnknown();
}
