package com.silastandard.universalclient.repositories;

import com.silastandard.universalclient.common.models.db.Parameter;
import org.springframework.data.repository.CrudRepository;

public interface ParameterRepository extends CrudRepository<Parameter, Long> {
}
