package com.silastandard.universalclient.repositories;

import com.silastandard.universalclient.common.models.db.Feature;
import com.silastandard.universalclient.common.models.db.Server;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;

import java.util.Set;

public interface FeatureRepository extends CrudRepository<Feature, Long> {
    Set<Feature> findAllByServer(Server server);
    Set<Feature> findAllByFullyQualifiedIdentifier(String fullyQualifiedFeatureId);
}
