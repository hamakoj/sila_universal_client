package com.silastandard.universalclient.repositories;

import com.silastandard.universalclient.common.models.db.Property;
import org.springframework.data.repository.CrudRepository;

public interface PropertyRepository extends CrudRepository<Property, Long> {
    Property findByFeature_Server_ServerUuidAndFeature_FullyQualifiedIdentifierAndRelativeIdentifier(
            String serverId,
            String featureFullyQualifiedIdentifier,
            String propertyRelativeIdentifier
    );
}
