package com.silastandard.universalclient.mappers;

import com.silastandard.universalclient.common.models.db.Call;
import com.silastandard.universalclient.common.models.db.Command;
import com.silastandard.universalclient.common.models.db.Property;
import com.silastandard.universalclient.common.models.dto.ErrorResult;
import com.silastandard.universalclient.common.models.sila.SiLACallModel;
import com.silastandard.universalclient.common.models.sila.SiLACallResult;
import com.silastandard.universalclient.utils.UscJsonUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.UUID;


@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class CallMapper {

    public static Call startCommandCallToPersistentCall(
            @NonNull final UUID callUuid,
            @NonNull final SiLACallModel siLACallModel,
            @NonNull final Command command
    ) {

        final Call persistentCall = new Call();
        persistentCall.setUuid(callUuid);
        persistentCall.setParameters(siLACallModel.getParameters().orElse("{}"));
        persistentCall.setCommand(command);
        persistentCall.setStartDate(LocalDateTime.now());
        persistentCall.setType(siLACallModel.getType().toString());
        persistentCall.setRelativeIdentifier(siLACallModel.getCallId());
        persistentCall.setMetadatas(siLACallModel.getMetadatas().orElse("{}"));

        // todo: to define Client Side => set timeout unsigned int and constraint it to SECONDS units
        persistentCall.setTimeout(1L);
        return persistentCall;
    }

    public static Call startPropertyCallToPersistentCall(
            @NonNull final UUID callUuid,
            @NonNull final SiLACallModel siLACallModel,
            @NonNull final Property property
    ) {
        final Call persistentCall = new Call();
        persistentCall.setUuid(callUuid);
        persistentCall.setParameters(siLACallModel.getParameters().orElse("{}"));
        persistentCall.setProperty(property);
        persistentCall.setStartDate(LocalDateTime.now());
        persistentCall.setType(siLACallModel.getType().toString());
        persistentCall.setRelativeIdentifier(siLACallModel.getCallId());
        persistentCall.setMetadatas(siLACallModel.getMetadatas().orElse("{}"));

        // todo: to define Client Side => set timeout unsigned int and constraint it to SECONDS units
        persistentCall.setTimeout(1L);
        return persistentCall;
    }

    public static Call setCommandExecutionUUIDCall(
            @NonNull final Call existingCall,
            @NonNull final String commandExecutionUUID
    ) {
        existingCall.setCommandExecutionUUID(commandExecutionUUID);
        return existingCall;
    }

    public static Call finishCommandCallToPersistentCall(
            @NonNull final SiLACallResult siLACallResult,
            @NonNull final Call existingCall
    ) {
        existingCall.setEndDate(LocalDateTime.now());
        existingCall.setError(!siLACallResult.getSuccess());
        String result = siLACallResult.getValue();

        // Undefined/Definition ExecutionError handling
        if (!siLACallResult.getSuccess()) {
            result = UscJsonUtils.isJSONValid(result) ? result : UscJsonUtils.toJson(new ErrorResult());
            log.debug("ExecutionError [{}] raised during [{}]", result, siLACallResult.getRequest());
        }
        existingCall.setResult(result);
        return existingCall;
    }
}
