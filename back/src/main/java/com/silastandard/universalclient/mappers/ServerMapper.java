package com.silastandard.universalclient.mappers;

import com.silastandard.universalclient.common.models.db.*;
import com.silastandard.universalclient.common.models.dto.DataTypeDefinition;
import com.silastandard.universalclient.common.models.dto.DefinedExecutionError;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.core.models.SiLAElement;
import sila_java.library.server_base.config.ServerConfiguration;
import sila_java.library.server_base.identification.ServerInformation;

import java.time.ZoneOffset;
import java.util.Date;
import java.util.UUID;

// todo: DataTypeDefinition, Temporal and shits
@Slf4j
@NoArgsConstructor(access = AccessLevel.NONE)
public abstract class ServerMapper {

    // todo: Response (Call)
    public static Server toPersistentServer(@NonNull final sila_java.library.manager.models.Server server) {
        return Server.fromSiLA(server);
    }

    public static sila_java.library.manager.models.Server toSiLAServer(@NonNull final Server server) {
        final sila_java.library.manager.models.Server siLAServer = new sila_java.library.manager.models.Server();
        // todo check this time stuff, might be a simpler and safer way to achieve this
        final ZoneOffset zoneOffset = ZoneOffset.systemDefault().getRules().getOffset(server.getJoined());
        siLAServer.setJoined(new Date(server.getJoined().toInstant(zoneOffset).toEpochMilli()));
        siLAServer.setStatus(sila_java.library.manager.models.Server.Status.valueOf(
                server.getStatus().equals(ServerStatus.ONLINE) ? "ONLINE" : "OFFLINE")
        );
        siLAServer.setPort(server.getPort());
        siLAServer.setCertificateAuthority(server.getCertificateAuthority());
        siLAServer.setNegotiationType(server.getNegotiationType());
        siLAServer.setConnectionType(server.getConnectionType());
        siLAServer.setConfiguration(new ServerConfiguration(
                server.getName(),
                UUID.fromString(server.getServerUuid())
        ));
        siLAServer.setHost(server.getServerHost());
        siLAServer.setInformation(new ServerInformation(
                server.getServerType(),
                server.getDescription(),
                server.getVendorUrl(),
                server.getVersion()
        ));
        server.getFeatures()
                .stream()
                .map(ServerMapper::toSiLAFeature)
                .forEach(siLAServer.getFeatures()::add);
        return siLAServer;
    }

    private static sila_java.library.core.models.Feature toSiLAFeature(@NonNull final Feature feature) {
        final sila_java.library.core.models.Feature siLAFeature = new sila_java.library.core.models.Feature();
        siLAFeature.setCategory(feature.getCategory());
        siLAFeature.setFeatureVersion(feature.getFeatureVersion());
        siLAFeature.setDescription(feature.getDescription());
        siLAFeature.setDisplayName(feature.getDisplayName());
        siLAFeature.setLocale(feature.getLocale());
        siLAFeature.setOriginator(feature.getOriginator());
        siLAFeature.setMaturityLevel(feature.getMaturityLevel());
        siLAFeature.setIdentifier(feature.getRelativeIdentifier());
        siLAFeature.setSiLA2Version(feature.getSila2Version());
        feature.getDefinedExecutionErrors()
                .stream()
                .map(ServerMapper::toSiLADefinedExecutionError)
                .forEach(siLAFeature.getDefinedExecutionError()::add);
        feature.getDataTypeDefinitions()
                .stream()
                .map(ServerMapper::toSiLADataType)
                .forEach(siLAFeature.getDataTypeDefinition()::add);
        feature.getCommands()
                .stream()
                .map(ServerMapper::toSiLACommand)
                .forEach(siLAFeature.getCommand()::add);
        feature.getProperties()
                .stream()
                .map(ServerMapper::toSiLAProperty)
                .forEach(siLAFeature.getProperty()::add);
        feature.getMetadatas()
                .stream()
                .map(ServerMapper::toSiLAMetadata)
                .forEach(siLAFeature.getMetadata()::add);
        return siLAFeature;
    }

    private static sila_java.library.core.models.Feature.DefinedExecutionError toSiLADefinedExecutionError(
            @NonNull final DefinedExecutionError definedExecutionError
    ) {
        final sila_java.library.core.models.Feature.DefinedExecutionError siLAExecutionError =
                new sila_java.library.core.models.Feature.DefinedExecutionError();
        siLAExecutionError.setDescription(definedExecutionError.getDescription());
        siLAExecutionError.setDisplayName(definedExecutionError.getDisplayName());
        siLAExecutionError.setIdentifier(definedExecutionError.getRelativeIdentifier());
        return siLAExecutionError;
    }

    private static SiLAElement toSiLADataType(@NonNull final DataTypeDefinition dataTypeDefinition) {
        final SiLAElement siLADataTypeDefinition = new SiLAElement();
        siLADataTypeDefinition.setDescription(dataTypeDefinition.getDescription());
        siLADataTypeDefinition.setDisplayName(dataTypeDefinition.getDisplayName());
        siLADataTypeDefinition.setIdentifier(dataTypeDefinition.getRelativeIdentifier());
        siLADataTypeDefinition.setDataType(dataTypeDefinition.getDataType());
        return siLADataTypeDefinition;
    }

    // todo: Same thing for Response
    private static sila_java.library.core.models.Feature.Command toSiLACommand(@NonNull final Command command) {
        final sila_java.library.core.models.Feature.Command siLACommand =
                new sila_java.library.core.models.Feature.Command();
        siLACommand.setDescription(command.getDescription());
        siLACommand.setDisplayName(command.getDisplayName());
        siLACommand.setIdentifier(command.getRelativeIdentifier());
        siLACommand.setObservable(command.getObservable() ? "Yes" : "No");
        siLACommand.setDefinedExecutionErrors(command.getExecutionErrors());
        command.getParameters()
                .stream()
                .map(ServerMapper::toSiLAParameter)
                .forEach(siLACommand.getParameter()::add);
        command.getResponses()
                .stream()
                .map(ServerMapper::toSiLAResponse)
                .forEach(siLACommand.getResponse()::add);
        command.getIntermediateResponses()
                .stream()
                .map(ServerMapper::toSiLAIntermediateResponse)
                .forEach(siLACommand.getIntermediateResponse()::add);
        return siLACommand;
    }

    private static sila_java.library.core.models.Feature.Property toSiLAProperty(@NonNull final Property property) {
        final sila_java.library.core.models.Feature.Property siLAProperty =
                new sila_java.library.core.models.Feature.Property();
        siLAProperty.setDescription(property.getDescription());
        siLAProperty.setDisplayName(property.getDisplayName());
        siLAProperty.setIdentifier(property.getRelativeIdentifier());
        siLAProperty.setDataType(property.getResponse().getDataType());
        siLAProperty.setObservable(property.getObservable() ? "Yes" : "No");
        siLAProperty.setDefinedExecutionErrors(property.getDefinedExecutionErrors());
        return siLAProperty;
    }

    private static SiLAElement toSiLAParameter(@NonNull final Parameter parameter) {
        final SiLAElement siLAParameter = new SiLAElement();
        siLAParameter.setDescription(parameter.getDescription());
        siLAParameter.setDisplayName(parameter.getDisplayName());
        siLAParameter.setIdentifier(parameter.getRelativeIdentifier());
        siLAParameter.setDataType(parameter.getDataType());
        return siLAParameter;
    }

    private static SiLAElement toSiLAResponse(@NonNull final Response response) {
        final SiLAElement siLAResponse = new SiLAElement();
        siLAResponse.setDescription(response.getDescription());
        siLAResponse.setDisplayName(response.getDisplayName());
        siLAResponse.setIdentifier(response.getRelativeIdentifier());
        siLAResponse.setDataType(response.getDataType());
        return siLAResponse;
    }

    private static SiLAElement toSiLAIntermediateResponse(@NonNull final IntermediateResponse intermediateResponse) {
        final SiLAElement siLAResponse = new SiLAElement();
        siLAResponse.setDescription(intermediateResponse.getDescription());
        siLAResponse.setDisplayName(intermediateResponse.getDisplayName());
        siLAResponse.setIdentifier(intermediateResponse.getRelativeIdentifier());
        siLAResponse.setDataType(intermediateResponse.getDataType());
        return siLAResponse;
    }

    private static sila_java.library.core.models.Feature.Metadata toSiLAMetadata(@NonNull final Metadata metadata) {
        final sila_java.library.core.models.Feature.Metadata siLAMetadata =
                new sila_java.library.core.models.Feature.Metadata();
        siLAMetadata.setDescription(metadata.getDescription());
        siLAMetadata.setDisplayName(metadata.getDisplayName());
        siLAMetadata.setIdentifier(metadata.getRelativeIdentifier());
        siLAMetadata.setDataType(metadata.getDataType());
        siLAMetadata.setDefinedExecutionErrors(metadata.getDefinedExecutionErrors());
        return siLAMetadata;
    }
}
