package com.silastandard.universalclient.db.migrations;

import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.util.List;

public class V2__Add_Fully_Qualified_Feature_Identifier extends BaseJavaMigration {
    private static final String TABLE_NAME = "FEATURE";
    private static final String COLUMN_NAME = "FULLY_QUALIFIED_IDENTIFIER";

    @Override
    public void migrate(Context context) throws Exception {
        final SingleConnectionDataSource connectionDataSource = new SingleConnectionDataSource(context.getConnection(), true);
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(connectionDataSource);

        final List<String> rows = jdbcTemplate.queryForList("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" + TABLE_NAME + "'", String.class);
        if (rows.isEmpty()) {
            // nothing to migrate table does not exist
            return;
        }

        // Add the column if missing
        jdbcTemplate.execute("ALTER TABLE IF EXISTS " + TABLE_NAME + " ADD COLUMN IF NOT EXISTS " + COLUMN_NAME + " VARCHAR(255)");
        // Drop not null
        jdbcTemplate.execute("ALTER TABLE IF EXISTS "  + TABLE_NAME + " ALTER COLUMN " + COLUMN_NAME + " DROP NOT NULL");
        // Drop default property
        jdbcTemplate.execute("ALTER TABLE IF EXISTS "  + TABLE_NAME + " ALTER COLUMN " + COLUMN_NAME + " DROP DEFAULT");
        // Mark empty values for update
        jdbcTemplate.execute("UPDATE " + TABLE_NAME + " SET " + COLUMN_NAME + " = NULL WHERE " + COLUMN_NAME + " = ''");
        final String majorVersion = "CONCAT(originator, '/', category, '/', RELATIVE_IDENTIFIER, '/v', COALESCE(NULLIF(SUBSTRING(FEATURE_VERSION, 1, LOCATE('.', FEATURE_VERSION) - 1), ''), FEATURE_VERSION))";
        // Update column values
        jdbcTemplate.execute("UPDATE " + TABLE_NAME + "\n" +
                "SET " + COLUMN_NAME + " = " + majorVersion + "\n" +
                    "WHERE " + COLUMN_NAME + " IS NULL");
        // Restore not null property
        jdbcTemplate.execute("ALTER TABLE IF EXISTS " + TABLE_NAME + " ALTER COLUMN " + COLUMN_NAME + " SET NOT NULL");
    }
}