package com.silastandard.universalclient.db;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.*;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertySource;

import static com.silastandard.universalclient.db.UpgradeDbFrom1_4_200.databaseRequireUpgrade;

@Slf4j
public class DbUpgradeChecker {

    @Getter
    private final ApplicationListener<ApplicationEnvironmentPreparedEvent> applicationEnvironmentPreparedEvent = this::onApplicationEnvironmentPreparedEvent;

    public void onApplicationEnvironmentPreparedEvent(@NonNull final ApplicationEnvironmentPreparedEvent event) {
        final MutablePropertySources propertySources = event.getEnvironment().getPropertySources();
        final PropertySource<?> propertySource = propertySources
                .stream()
                .filter(p -> p.containsProperty("spring.datasource.url"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Unable to find spring.datasource.url configuration entry!"));
        final String dbUrl = (String) propertySource.getProperty("spring.datasource.url");
        final String user = (String) propertySource.getProperty("spring.datasource.username");
        final String password = (String) propertySource.getProperty("spring.datasource.password");
        if (dbUrl == null || user == null || password == null) {
            throw new RuntimeException("spring.datasource configuration is invalid! The url, username and password properties must all be set!");
        }
        if (databaseRequireUpgrade(dbUrl, user, password)) {
            log.warn("Database must to be upgraded.");
            log.info("Attempting to upgrade database...");
            UpgradeDbFrom1_4_200.upgrade(dbUrl, user, password, false);
            log.info("Database upgrade performed without errors, proceeding.");
        } else {
            log.info("Database ok.");
        }
    }
}