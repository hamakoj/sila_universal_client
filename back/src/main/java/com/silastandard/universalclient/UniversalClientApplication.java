package com.silastandard.universalclient;

import com.silastandard.universalclient.config.PluginBeanNameGenerator;
import com.silastandard.universalclient.db.DbUpgradeChecker;
import com.silastandard.universalclient.plugin.PluginLoader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;

@Slf4j
@SpringBootApplication(nameGenerator = PluginBeanNameGenerator.class)
public class UniversalClientApplication extends SpringBootServletInitializer {

	public static void main(final String[] args) {
		final SpringApplication springApplication = new SpringApplication(UniversalClientApplication.class);

		final DbUpgradeChecker dbUpgradeChecker = new DbUpgradeChecker();
		springApplication.addListeners(dbUpgradeChecker.getApplicationEnvironmentPreparedEvent());

		final PluginLoader pluginLoader = new PluginLoader();
		springApplication.addListeners(pluginLoader.getApplicationStartingEvent());
		springApplication.addListeners(pluginLoader.getApplicationEnvironmentPreparedEvent());
		springApplication.addListeners(pluginLoader.getApplicationContextInitializedEvent());
		springApplication.addListeners(pluginLoader.getApplicationPreparedEvent());
		springApplication.addListeners(pluginLoader.getContextRefreshedEvent());
		springApplication.addListeners(pluginLoader.getApplicationReadyEvent());

		final ConfigurableApplicationContext applicationContext = springApplication.run(args);
	}

}
