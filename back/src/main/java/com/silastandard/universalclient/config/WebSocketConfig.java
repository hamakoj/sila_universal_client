package com.silastandard.universalclient.config;

import lombok.NonNull;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.lang.Nullable;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.Map;
import java.util.UUID;

@Configuration
@EnableScheduling
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    public static final String EVENT_PREFIX_PATH = "/event/";
    public static final String WEBSOCKET_ENDPOINT = "/websocketsila";

    @Override
    public void configureMessageBroker(@NonNull final MessageBrokerRegistry config) {
        config.enableSimpleBroker(EVENT_PREFIX_PATH);
    }

    @Override
    public void registerStompEndpoints(@NonNull final StompEndpointRegistry registry) {
        registry.addEndpoint(WEBSOCKET_ENDPOINT)
                .setAllowedOriginPatterns("*")
                .setHandshakeHandler(new AssignRandomUserUUIDPrincipalHandshakeHandler())
                .withSockJS();
    }

    @Override
    public void configureClientOutboundChannel(@NonNull final ChannelRegistration registration) {
        registration.taskExecutor()
                .corePoolSize(1)
                .maxPoolSize(2); // todo find best values
    }

    /**
     * Assign a random username UUID as principal for each websocket client. This is
     * needed to be able to communicate with a specific client.
     */
    private static class AssignRandomUserUUIDPrincipalHandshakeHandler extends DefaultHandshakeHandler {
        private static final String ATTR_PRINCIPAL = "__principal__";

        @Override
        protected Principal determineUser(
                @Nullable final ServerHttpRequest request,
                @Nullable final WebSocketHandler wsHandler,
                @NonNull final Map<String, Object> attributes
        ) {
            final String userId;
            if (!attributes.containsKey(ATTR_PRINCIPAL)) {
                userId = generateRandomUsername();
                attributes.put(ATTR_PRINCIPAL, userId);
            } else {
                userId = (String) attributes.get(ATTR_PRINCIPAL);
            }
            return () -> userId;
        }

        private String generateRandomUsername() {
            return UUID.randomUUID().toString();
        }
    }
}
