package com.silastandard.universalclient.config;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Slf4j
@Configuration
@AutoConfigureAfter(DispatcherServletAutoConfiguration.class)
public class PluginWebResourceConfiguration implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(@NonNull final ResourceHandlerRegistry registry) {
        // classpath:/... for a resource in classpath or "file:///C:/... for a file in windows for example
        registry.addResourceHandler("/plugins/**").addResourceLocations("classpath:/plugin-public/");
    }
}