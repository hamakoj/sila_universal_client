package com.silastandard.universalclient.services;

import com.silastandard.universalclient.common.models.db.Call;
import com.silastandard.universalclient.common.models.db.Client;
import com.silastandard.universalclient.common.models.db.Server;

import java.util.Optional;
import java.util.Set;

public interface ClientService {
    Client save(Client client);

    Optional<Client> getClient(String uuid);

    Set<Client> getAllClients();

    Set<Client> getAllServerClients(Server server);

    boolean deleteClientCall(Call call, Client client);

    boolean deleteClientServer(Server server, Client client);

    Server addBookmarkedServers(Client client, Server server);

    Server deleteBookmarkedServers(Client client, Server server);

    Client createOrRetrieveClient(String uuid);
}
