package com.silastandard.universalclient.services.impl;

import com.silastandard.universalclient.common.models.db.Command;
import com.silastandard.universalclient.common.models.db.Feature;
import com.silastandard.universalclient.common.models.db.Parameter;
import com.silastandard.universalclient.common.models.db.Server;
import com.silastandard.universalclient.repositories.CommandRepository;
import com.silastandard.universalclient.repositories.ServerRepository;
import com.silastandard.universalclient.services.CommandService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

// todo: Add debug logs with entire entities log.
// todo: Look for Custom Exception raise in case of unwanted behaviour (eg. CommandNotFound).
@Service
@Slf4j
public class CommandServiceImpl implements CommandService {

    private final CommandRepository commandRepository;
    private final ServerRepository serverRepository;

    @Autowired
    public CommandServiceImpl(
            @NonNull final CommandRepository commandRepository,
            @NonNull final ServerRepository serverRepository
    ) {
        this.commandRepository = commandRepository;
        this.serverRepository = serverRepository;
    }

    @Override
    public Optional<Command> getCommand(@NonNull final Long id) {
        final Optional<Command> command = this.commandRepository.findById(id);
        if (command.isPresent()) {
            log.info("Command persistence service found the Command {}.", command.get());
        } else {
            log.warn("Command with ID {} could not be found in the persistence context!", id);
        }
        return command;
    }

    @Override
    public Set<Command> getAllCommandsByServerFeature(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureId
    ) {
        Optional<Server> server = this.serverRepository.findByServerUuid(serverUuid);
        if (server.isPresent()) {
            log.info("Features from Server {} to be fetched.", serverUuid);
            Optional<Feature> feature = server.get().getFeatures()
                    .stream()
                    .filter(serverFeature -> serverFeature.getFullyQualifiedIdentifier().equals(fullyQualifiedFeatureId))
                    .findFirst();
            if (feature.isPresent()) {
                log.info("{} Commands found for Server {} in the Feature persistence Service.",
                        feature.get().getCommands().size(), serverUuid);
                return feature.get().getCommands();
            }
        } else {
            // todo throw?
        }
        log.warn("No Commands for Feature {} for Server {} could be find : Cannot find Server or cannot find Feature.",
                fullyQualifiedFeatureId, serverUuid);
        return Collections.emptySet();
    }

    @Override
    public Set<Parameter> getCommandParameters(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureIdentifier,
            @NonNull final String commandRelativeIdentifier
    ) {
        final Optional<Server> server = this.serverRepository.findByServerUuid(serverUuid);
        if (server.isPresent()) {
            log.info("Features from Server {} to be fetched.", serverUuid);
            final Optional<Feature> feature = server.get().getFeatures()
                    .stream()
                    .filter(serverFeature -> serverFeature.getFullyQualifiedIdentifier()
                            .equals(fullyQualifiedFeatureIdentifier))
                    .findFirst();
            if (feature.isPresent()) {
                log.info("{} Commands found for Server {} in the Feature persistence Service.",
                        feature.get().getCommands().size(), serverUuid);
                final Optional<Command> command = feature.get().getCommands()
                        .stream()
                        .filter(featureCommand ->
                                featureCommand.getRelativeIdentifier()
                                        .equals(commandRelativeIdentifier))
                        .findFirst();

                if (command.isPresent())
                    return command.get().getParameters();
                else {
                    // todo log
                    return new HashSet<>();
                }
            }
        } else {
            // todo throw error?
        }

        log.warn("No Command {} for Feature {} for Server {} could be find : " +
                        "Cannot find Server or cannot find Feature, or cannot find Command.",
                commandRelativeIdentifier, fullyQualifiedFeatureIdentifier, serverUuid
        );
        return Collections.emptySet();
    }

    // todo: add logs
    @Override
    public Optional<Command> findByRelativeIdentifier(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureIdentifier,
            @NonNull final String relativeIdentifier
    ) {
        final Optional<Server> optionalServer = this.serverRepository.findByServerUuid(serverUuid);
        if (optionalServer.isPresent()) {
            Server server = optionalServer.get();
            Optional<Feature> optionalFeature = server.getFeatures()
                    .stream()
                    .filter(feature -> feature.getFullyQualifiedIdentifier()
                            .equals(fullyQualifiedFeatureIdentifier))
                    .findFirst();
            if (optionalFeature.isPresent()) {
                Feature feature = optionalFeature.get();
                Optional<Command> optionalCommand = feature.getCommands()
                        .stream()
                        .filter(featureCommand -> featureCommand.getRelativeIdentifier()
                                .equals(relativeIdentifier))
                        .findFirst();
                if (optionalCommand.isPresent()) {
                    return optionalCommand;
                }
            }
        } else {
            // todo throw?
        }
        return Optional.empty();
    }

    @Override
    public Optional<Command> findFeaturesCommandByFullyQualifiedIdentifier(
            @NonNull final String serverId,
            @NonNull final String fullyQualifiedFeatureId,
            @NonNull final String commandRelativeId

    ) {
        // todo fixme : externalize and improve
        return Optional.ofNullable(
                commandRepository.findByFeature_Server_ServerUuidAndFeature_FullyQualifiedIdentifierAndRelativeIdentifier(
                        serverId,
                        fullyQualifiedFeatureId,
                        commandRelativeId
                )
        );
    }
}
