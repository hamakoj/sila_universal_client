package com.silastandard.universalclient.services.impl;

import com.silastandard.universalclient.common.models.db.*;
import com.silastandard.universalclient.repositories.CallRepository;
import com.silastandard.universalclient.services.CallService;
import com.silastandard.universalclient.services.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
@Transactional
@Slf4j
public class CallServiceImpl implements CallService {

    private final CallRepository callRepository;

    @Autowired
    public CallServiceImpl(@NonNull final CallRepository callRepository, @NonNull final ClientService clientService) {
        this.callRepository = callRepository;
    }

    @Override
    public Set<Call> getCallWithRelativeId(@NonNull final String uuid) {
        return this.callRepository.findByRelativeIdentifier(uuid);
    }

    public Optional<Call> findByUuid(@NonNull final UUID uuid) {
        return this.callRepository.findByUuid(uuid);
    }

    @Override
    public Call save(@NonNull final Call call) {
        return this.callRepository.save(call);
    }

    @Override
    public Set<Call> getAllCallsByServerFeatureAndCommandRelativeId(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureIdentifier,
            @NonNull final String commandRelativeIdentifier

    ) {
        return callRepository.findAllByCommand_Feature_Server_ServerUuidAndCommand_Feature_FullyQualifiedIdentifierAndCommand_RelativeIdentifier(
                serverUuid,
                fullyQualifiedFeatureIdentifier,
                commandRelativeIdentifier
        );
    }

    @Override
    public Set<Call> getAllCallsByServerFeatureAndPropertyRelativeId(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureIdentifier,
            @NonNull final String propertyRelativeIdentifier
    ) {
        return callRepository.findAllByProperty_Feature_Server_ServerUuidAndProperty_Feature_FullyQualifiedIdentifierAndProperty_RelativeIdentifier(
                serverUuid,
                fullyQualifiedFeatureIdentifier,
                propertyRelativeIdentifier
        );
    }

    @Override
    public Boolean deleteCall(@NonNull final UUID id) {
        try {
            callRepository.deleteById(id);
        } catch (final EmptyResultDataAccessException e) {
            log.warn("Could not delete Call [{}] : {}", id, e.getMessage());
            return false;
        }
        return !callRepository.existsById(id);
    }

    @Override
    public Boolean deleteAllFromCommand(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureIdentifier,
            @NonNull final String commandRelativeIdentifier
    ) {
        log.info("Delete all Calls of Server {}, Feature {}, Identifier {}",
                serverUuid,
                fullyQualifiedFeatureIdentifier,
                commandRelativeIdentifier
        );
        callRepository.deleteAllByCommand_Feature_Server_ServerUuidAndCommand_Feature_FullyQualifiedIdentifierAndCommand_RelativeIdentifier(
                serverUuid,
                fullyQualifiedFeatureIdentifier,
                commandRelativeIdentifier
        );
        return callRepository.findAllByCommand_Feature_Server_ServerUuidAndCommand_Feature_FullyQualifiedIdentifierAndCommand_RelativeIdentifier(
                serverUuid,
                fullyQualifiedFeatureIdentifier,
                commandRelativeIdentifier
        ).size() == 0;
    }

    @Override
    public Boolean deleteAllFromProperty(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureIdentifier,
            @NonNull final String propertyRelativeIdentifier
    ) {
        log.info("Delete all Calls of Server {}, Feature {}, Identifier {}",
                serverUuid,
                fullyQualifiedFeatureIdentifier,
                propertyRelativeIdentifier
        );
        callRepository.deleteAllByProperty_Feature_Server_ServerUuidAndProperty_Feature_FullyQualifiedIdentifierAndProperty_RelativeIdentifier(
                serverUuid,
                fullyQualifiedFeatureIdentifier,
                propertyRelativeIdentifier
        );
        return callRepository.findAllByProperty_Feature_Server_ServerUuidAndProperty_Feature_FullyQualifiedIdentifierAndProperty_RelativeIdentifier(
                serverUuid,
                fullyQualifiedFeatureIdentifier,
                propertyRelativeIdentifier
        ).size() == 0;
    }

    @Override
    public Set<Call> getPendingServerFeatureCommandCall(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureIdentifier,
            @NonNull final String commandRelativeIdentifier
    ) {
        return this.callRepository.findAllByCommand_Feature_Server_ServerUuidAndCommand_Feature_FullyQualifiedIdentifierAndCommand_RelativeIdentifierAndEndDateNull(
                serverUuid,
                fullyQualifiedFeatureIdentifier,
                commandRelativeIdentifier
        );
    }

    @Override
    public Set<Call> getPendingServerFeaturePropertyCall(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureIdentifier,
            @NonNull final String propertyRelativeIdentifier
    ) {
        return this.callRepository.findAllByProperty_Feature_Server_ServerUuidAndProperty_Feature_FullyQualifiedIdentifierAndProperty_RelativeIdentifierAndEndDateNull(
                serverUuid,
                fullyQualifiedFeatureIdentifier,
                propertyRelativeIdentifier
        );
    }

    @Override
    public Optional<Call> getClientCommandLastCall(
            @NonNull final String clientId,
            @NonNull final Command command
    ) {
        return Optional.ofNullable(
                callRepository.findFirstByCommand_Feature_Server_ServerUuidAndCommand_Feature_FullyQualifiedIdentifierAndCommand_RelativeIdentifierAndErrorFalseAndClientClientUuidOrderByEndDateDesc(
                        command.getFeature().getServer().getServerUuid(),
                        command.getFullyQualifiedIdentifier(),
                        command.getRelativeIdentifier(),
                        clientId
                )
        );
    }

    @Override
    public Optional<Call> getClientPropertyLastCall(
            @NonNull final String clientId,
            @NonNull final Property property
    ) {
        return Optional.ofNullable(
                callRepository.findFirstByProperty_Feature_Server_ServerUuidAndProperty_Feature_FullyQualifiedIdentifierAndProperty_RelativeIdentifierAndErrorFalseAndClientClientUuidOrderByEndDateDesc(
                        property.getFeature().getServer().getServerUuid(),
                        property.getFeature().getFullyQualifiedIdentifier(),
                        property.getRelativeIdentifier(),
                        clientId
                )
        );
    }

    @Override
    public boolean delete(@NonNull final Call persistentCall) {
        this.callRepository.delete(persistentCall);
        return !this.callRepository.findByUuid(persistentCall.getUuid()).isPresent();
    }

    private Call getNewOrExistingCall(@NonNull final Call call) {
        final Optional<Call> existingCall = this.callRepository.findByUuid(call.getUuid());
        // Find potential existing Call
        if (existingCall.isPresent()) {
            return existingCall.get();
        } else {
            log.debug("Creating call [{}]", call);
        }
        return call;
    }

    private Client getCallClientOrThrowError(@NonNull final Call call) {
        Optional<Client> optionalClient = Optional.ofNullable(call.getClient());
        if (!optionalClient.isPresent()) {
            throw new RuntimeException("No Client could be found for a Call update.");
        }
        return optionalClient.get();
    }

    private Call getNewOrExistingCallOrThrowError(@NonNull final Call call) {
        Optional<Call> optionalCall = Optional.ofNullable(save(getNewOrExistingCall(call)));
        if (!optionalCall.isPresent()) {
            throw new RuntimeException("No Call update could be saved.");
        }
        return call;
    }

}
