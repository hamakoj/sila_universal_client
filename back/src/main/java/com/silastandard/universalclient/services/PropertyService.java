package com.silastandard.universalclient.services;

import com.silastandard.universalclient.common.models.db.Property;
import lombok.NonNull;

import java.util.Optional;
import java.util.Set;

public interface PropertyService {
    Set<Property> getAllPropertiesByServerFeature(String serverUuid, String fullyQualifiedFeatureIdentifier);
    Optional<Property> findByRelativeIdentifier(
            String serverUuid, String fullyQualifiedFeatureIdentifier, String relativePropertyIdentifier
    );

    Optional<Property> findFeaturesPropertyByFullyQualifiedIdentifier(
            String serverId, String fullyQualifiedFeatureIdentifier, String relativePropertyIdentifier
    );
}
