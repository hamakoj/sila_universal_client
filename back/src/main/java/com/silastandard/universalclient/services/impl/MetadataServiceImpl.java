package com.silastandard.universalclient.services.impl;

import com.silastandard.universalclient.common.models.db.Feature;
import com.silastandard.universalclient.common.models.db.Metadata;
import com.silastandard.universalclient.common.models.db.Server;
import com.silastandard.universalclient.repositories.MetadataRepository;
import com.silastandard.universalclient.repositories.ServerRepository;
import com.silastandard.universalclient.services.MetadataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


@Service
@Slf4j
public class MetadataServiceImpl implements MetadataService {

    private final ServerRepository serverRepository;
    private final MetadataRepository metadataRepository;

    @Autowired
    public MetadataServiceImpl(
            @NonNull final ServerRepository serverRepository,
            @NonNull final MetadataRepository metadataRepository
    ) {
        this.serverRepository = serverRepository;
        this.metadataRepository = metadataRepository;
    }

    @Override
    public Metadata getServerFeatureMetadata(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureId,
            @NonNull final String metadataRelativeId
    ) {
        return Optional.ofNullable(this.metadataRepository.findByFeature_Server_ServerUuidAndFeature_FullyQualifiedIdentifierAndRelativeIdentifier(
                serverUuid,
                fullyQualifiedFeatureId,
                metadataRelativeId
        )).orElseThrow(() -> new RuntimeException(String.format("No Metadata [%s] found for Server [%s] Feature [%s]",
                        metadataRelativeId,
                        serverUuid,
                        fullyQualifiedFeatureId)
                )
        );
    }

    @Override
    public Metadata findById(Long id) {
        return this.metadataRepository.findById(id).orElseThrow(() -> new RuntimeException(
                String.format("No Metadata id [%d] in persistence context.", id)
        ));
    }

    // todo remove if unused
    @Override
    public Set<Metadata> getAllMetadataByServerFeature(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureId
    ) {
        final Optional<Server> server = this.serverRepository.findByServerUuid(serverUuid);
        if (server.isPresent()) {
            log.info("Metadata from Server {} to be fetched.", serverUuid);
            // todo fixme filter in sql
            final Optional<Feature> feature = server.get().getFeatures()
                    .stream()
                    .filter(serverFeature -> serverFeature.getFullyQualifiedIdentifier().equals(fullyQualifiedFeatureId))
                    .findFirst();
            if (feature.isPresent()) {
                log.info("{} Metadata found for Server {} in the Feature persistence Service.",
                        feature.get().getMetadatas().size(), serverUuid);
                return feature.get().getMetadatas();
            }
        } else {
            // todo throw error?
        }
        log.warn("No Commands for Feature {} for Server {} could be find : Cannot find Server or cannot find Feature.",
                fullyQualifiedFeatureId, serverUuid);
        return Collections.emptySet();
    }

    // todo remove if unused
    @Override
    public Set<Metadata> getAllMetadataByServer(@NonNull final Server server) {
        log.info("Metadata from Server [{}] to be fetched.", server.getServerUuid());
        final Set<Metadata> allMetadataByServer = new HashSet<>();
        // todo fixme filter in sql
        server.getFeatures().forEach(feature -> {
            final Set<Metadata> featureMetadata = feature.getMetadatas();
            if (allMetadataByServer.addAll(featureMetadata)) log.debug(
                    "[{}] Metadata set found for Server {} and Feature {}.",
                    featureMetadata,
                    server.getServerUuid(),
                    feature.getFullyQualifiedIdentifier()
            );
            else log.warn("[{}] feature's Metadata [{}] could not be added to Server [{}] Metadata.",
                    feature.getFullyQualifiedIdentifier(),
                    featureMetadata,
                    server.getServerUuid()
            );
        });
        return allMetadataByServer;
    }
}
