package com.silastandard.universalclient.services;

import com.silastandard.universalclient.common.models.db.Client;
import com.silastandard.universalclient.common.models.db.Server;

import java.util.Optional;
import java.util.Set;

public interface ServerService {
    Set<Server> getAllServers();

    Set<Server> getAllOnlineServers();

    Optional<Server> getServer(String uuid);

    Server save(Server server);
    boolean delete(Server persistentServer);

    Set<Server> getBookmarkedServers(Client client);

    void setAllServersStatusToUnknown();
}
