package com.silastandard.universalclient.services.impl;

import com.silastandard.universalclient.common.models.db.Feature;
import com.silastandard.universalclient.common.models.db.Property;
import com.silastandard.universalclient.common.models.db.Server;
import com.silastandard.universalclient.repositories.PropertyRepository;
import com.silastandard.universalclient.repositories.ServerRepository;
import com.silastandard.universalclient.services.PropertyService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
public class PropertyServiceImpl implements PropertyService {

    private final ServerRepository serverRepository;
    private final PropertyRepository propertyRepository;

    @Autowired
    public PropertyServiceImpl(
            @NonNull final ServerRepository serverRepository,
            @NonNull final PropertyRepository propertyRepository
    ) {
        this.serverRepository = serverRepository;
        this.propertyRepository = propertyRepository;
    }

    @Override
    public Set<Property> getAllPropertiesByServerFeature(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureId
    ) {
        final Optional<Server> server = this.serverRepository.findByServerUuid(serverUuid);
        if (server.isPresent()) {
            log.info("Properties from Server {} to be fetched.", serverUuid);
            // todo fixme filter in sql
            final Optional<Feature> feature = server.get().getFeatures()
                    .stream()
                    .filter(serverFeature -> serverFeature.getFullyQualifiedIdentifier().equals(fullyQualifiedFeatureId))
                    .findFirst();
            if (feature.isPresent()) {
                log.info("{} Properties found for Server {} in the Feature persistence Service.",
                        feature.get().getCommands().size(),
                        serverUuid
                );
                return feature.get().getProperties();
            }
        } else {
            // todo throw?
        }
        log.warn("No Properties for Feature {} for Server {} could be find : Cannot find Server or cannot find Feature.",
                fullyQualifiedFeatureId, serverUuid);
        return Collections.emptySet();
    }

    @Override
    public Optional<Property> findByRelativeIdentifier(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureId,
            @NonNull final String relativeIdentifier
    ) {
        final Optional<Server> optionalServer = this.serverRepository.findByServerUuid(serverUuid);
        if (optionalServer.isPresent()) {
            Server server = optionalServer.get();
            Optional<Feature> optionalFeature = server.getFeatures()
                    .stream()
                    .filter(feature -> feature.getFullyQualifiedIdentifier()
                            .equals(fullyQualifiedFeatureId))
                    .findFirst();
            if (optionalFeature.isPresent()) {
                Feature feature = optionalFeature.get();
                Optional<Property> optionalProperty = feature.getProperties()
                        .stream()
                        .filter(featureProperty -> featureProperty.getRelativeIdentifier()
                                .equals(relativeIdentifier))
                        .findFirst();
                if (optionalProperty.isPresent()) {
                    return optionalProperty;
                }
            }
        }
        // todo throw?
        return Optional.empty();
    }

    @Override
    public Optional<Property> findFeaturesPropertyByFullyQualifiedIdentifier(
            @NonNull final String serverId,
            @NonNull final String fullyQualifiedFeatureIdentifier,
            @NonNull final String relativePropertyIdentifier
    ) {
        // todo fixme : externalize and improve
        return Optional.ofNullable(
                this.propertyRepository.findByFeature_Server_ServerUuidAndFeature_FullyQualifiedIdentifierAndRelativeIdentifier(
                        serverId,
                        fullyQualifiedFeatureIdentifier,
                        relativePropertyIdentifier
                )
        );
    }
}
