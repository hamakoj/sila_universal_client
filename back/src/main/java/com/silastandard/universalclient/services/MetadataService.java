package com.silastandard.universalclient.services;

import com.silastandard.universalclient.common.models.db.Metadata;
import com.silastandard.universalclient.common.models.db.Server;

import java.util.Set;

public interface MetadataService {
    Set<Metadata> getAllMetadataByServerFeature(String serverUuid, String relativeIdentifier);

    Metadata findById(Long id);

    Set<Metadata> getAllMetadataByServer(Server server);

    Metadata getServerFeatureMetadata(String serverUuid, String fullyQualifiedFeatureId, String metadataRelativeId);
}
