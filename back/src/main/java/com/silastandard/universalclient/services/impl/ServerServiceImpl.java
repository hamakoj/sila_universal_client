package com.silastandard.universalclient.services.impl;

import com.google.common.collect.ImmutableSet;
import com.silastandard.universalclient.common.models.db.Client;
import com.silastandard.universalclient.common.models.db.Server;
import com.silastandard.universalclient.repositories.ServerRepository;
import com.silastandard.universalclient.services.ClientService;
import com.silastandard.universalclient.services.ServerService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Transactional
@Service
@Slf4j
// todo: Add debug logs with entire entities log.
// todo: Look for Custom Exception raise in case of unwanted behaviour (ie. ServerNotFound).
public class ServerServiceImpl implements ServerService {

    private final ServerRepository serverRepository;
    private final ClientService clientService;

    @Autowired
    public ServerServiceImpl(
            @NonNull final ServerRepository serverRepository,
            @NonNull final ClientService clientService
    ) {
        this.serverRepository = serverRepository;
        this.clientService = clientService;
    }

    @Override
    public Set<Server> getAllServers() {
        final Set<Server> servers = ImmutableSet.copyOf(this.serverRepository.findAll());
        log.debug("{} Servers found in the Server persistence Service", servers.size());
        return servers;
    }

    @Override
    public Set<Server> getAllOnlineServers() {
        // todo: make Status an enum
        final Set<Server> servers = ImmutableSet.copyOf(this.serverRepository.findAllByStatus("ONLINE"));
        log.debug("Server persistence Service found {} ONLINE Servers", servers.size());
        return servers;
    }

    @Override
    public Optional<Server> getServer(@NonNull final String uuid) {
        final Optional<Server> server = this.serverRepository.findByServerUuid(uuid);
        if (!server.isPresent()) {
            log.info("Server with ID {} could not be found in the persistence context.", uuid);
        } else {
            log.debug("Server persistence Service found the Server {}", server.get());
        }
        return server;
    }

    @Override
    public Server save(@NonNull final Server server) {
        // todo: Add Catch org.hibernate.exception.ConstraintViolationException
        return this.serverRepository.save(server);
    }

    @Override
    @Transactional
    public boolean delete(@NonNull final Server persistentServer) {
        for (Client client : persistentServer.getClients()) {
            client.getServers().remove(persistentServer);
            this.clientService.save(client);
        }
        persistentServer.getClients().clear();
        this.serverRepository.delete(persistentServer);
        return !this.serverRepository.existsById(persistentServer.getServerUuid());
    }

    @Override
    public Set<Server> getBookmarkedServers(@NonNull final Client client) {
        return this.serverRepository.findAllByClientsIsContaining(client);
    }

    @Override
    public void setAllServersStatusToUnknown() {
        this.serverRepository.setAllServersStatusToUnknown();
    }
}
