package com.silastandard.universalclient.services;

import com.silastandard.universalclient.common.models.db.Call;
import com.silastandard.universalclient.common.models.db.Command;
import com.silastandard.universalclient.common.models.db.Property;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface CallService {

    Set<Call> getCallWithRelativeId(String relativeId);

    Optional<Call> findByUuid(UUID uuid);

    Set<Call> getPendingServerFeatureCommandCall(
            String serverUuid, String fullyQualifiedFeatureIdentifier, String commandRelativeIdentifier
    );
    Set<Call> getPendingServerFeaturePropertyCall(
            String serverUuid, String fullyQualifiedFeatureIdentifier, String propertyRelativeIdentifier
    );

    boolean delete(Call persistentCall);

    Call save(Call call);

    Set<Call> getAllCallsByServerFeatureAndCommandRelativeId(
            String serverUuid, String fullyQualifiedFeatureIdentifier, String commandRelativeIdentifier
    );

    Set<Call> getAllCallsByServerFeatureAndPropertyRelativeId(
            String serverUuid, String fullyQualifiedFeatureIdentifier, String propertyRelativeIdentifier
    );

    Boolean deleteCall(UUID id);

    Boolean deleteAllFromCommand(
            String serverUuid, String fullyQualifiedFeatureIdentifier, String commandRelativeIdentifier
    );

    Boolean deleteAllFromProperty(
            String serverUuid, String fullyQualifiedFeatureIdentifier, String propertyRelativeIdentifier
    );

    Optional<Call> getClientCommandLastCall(String clientId, Command command);

    Optional<Call> getClientPropertyLastCall(String clientId, Property property);
}
