package com.silastandard.universalclient.services.application;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Slf4j
@Service
public class ApplicationListenerService {

    @EventListener
    public void handleContextRefreshed(@NonNull final ContextRefreshedEvent event) {
    }

    private void printActiveProperties(@NonNull final ConfigurableEnvironment env) {
        log.info("************************* ACTIVE APP PROPERTIES ******************************");
        env.getPropertySources()
                .stream()
                .filter((it) -> it instanceof MapPropertySource)
                .map(it -> (MapPropertySource)it)
                .map(propertySource -> propertySource.getSource().keySet())
                .flatMap(Collection::stream)
                .distinct()
                .sorted()
                .forEach(key -> {
                    try {
                        log.info(key + "=" + env.getProperty(key));
                    } catch (Exception e) {
                        log.warn("{} -> {}", key, e.getMessage());
                    }
                });
        log.info("******************************************************************************");
    }
}
