package com.silastandard.universalclient.services.impl;

import com.silastandard.universalclient.common.models.db.Call;
import com.silastandard.universalclient.common.models.db.Client;
import com.silastandard.universalclient.common.models.db.Command;
import com.silastandard.universalclient.common.models.db.Property;
import com.silastandard.universalclient.common.models.sila.SiLACallModel;
import com.silastandard.universalclient.common.models.sila.SiLACallResult;
import com.silastandard.universalclient.mappers.CallMapper;
import com.silastandard.universalclient.services.CommandService;
import com.silastandard.universalclient.services.PropertyService;
import com.sun.istack.NotNull;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class CallFactoryService {

    private final CommandService commandService;
    private final PropertyService propertyService;

    @Autowired
    public CallFactoryService(
            @NotNull final CommandService commandService,
            @NotNull final PropertyService propertyService
    ) {
        this.commandService = commandService;
        this.propertyService = propertyService;
    }

    public Call createClientCall(
            @NonNull final UUID callUuid,
            @NotNull final SiLACallModel siLACallModel,
            @NotNull final Client client
    ) {
        Optional<Call> newCallOpt = Optional.empty();

        switch (siLACallModel.getType()) {
            case OBSERVABLE_COMMAND:
            case UNOBSERVABLE_COMMAND:
                newCallOpt = this.createCallFromCommand(callUuid, siLACallModel);
                break;
            case OBSERVABLE_PROPERTY:
            case OBSERVABLE_PROPERTY_READ:
            case UNOBSERVABLE_PROPERTY:
                newCallOpt = this.createCallFromProperty(callUuid, siLACallModel);
                break;
            default:
                // todo support BINARY_UPLOAD BINARY_DOWNLOAD ect... ?

                // todo: throw Custom error ?
        }

        Call newCall;
        if (!newCallOpt.isPresent()) {
            // todo log and handle what should happen
            log.error("Call could not be created with CallModel [{}]", siLACallModel);
            throw new RuntimeException("Could not create call with input CallModel");
        } else {
            newCall = newCallOpt.get();
            newCall.setClient(client);
            newCall.setUuid(callUuid);
        }

        return newCall;
    }

    public Call finishCall(
            @NonNull final SiLACallResult siLACallResult,
            @NonNull final Call call
    ) {
        return CallMapper.finishCommandCallToPersistentCall(siLACallResult, call);
    }

    public Call setCommandExecutionUUID(
            @NonNull final Call call,
            @NonNull final String commandExecutionUUID
    ) {
        return CallMapper.setCommandExecutionUUIDCall(call, commandExecutionUUID);
    }

    private Optional<Call> createCallFromCommand(@NonNull final UUID uuid, @NonNull final SiLACallModel siLACallModel) {
        final Optional<Command> relatedCommand = this.commandService.findFeaturesCommandByFullyQualifiedIdentifier(
                siLACallModel.getServerId().toString(),
                siLACallModel.getFullyQualifiedFeatureId(),
                siLACallModel.getCallId()
        );
        return relatedCommand.map(command -> CallMapper.startCommandCallToPersistentCall(
                uuid,
                siLACallModel,
                command
        ));
    }

    private Optional<Call> createCallFromProperty(@NonNull final UUID uuid, @NonNull final SiLACallModel siLACallModel) {
        final Optional<Property> relatedProperty = this.propertyService.findFeaturesPropertyByFullyQualifiedIdentifier(
                siLACallModel.getServerId().toString(),
                siLACallModel.getFullyQualifiedFeatureId(),
                siLACallModel.getCallId()
        );
        return relatedProperty.map(property -> CallMapper.startPropertyCallToPersistentCall(
                uuid,
                siLACallModel,
                property
        ));
    }


}
