package com.silastandard.universalclient.services;

import com.silastandard.universalclient.common.models.db.Feature;

import java.util.Optional;
import java.util.Set;

public interface FeatureService {
    Set<Feature> getAllServerFeatures(String serverUuid);
    Optional<Feature> getFeature(Long id);
    Set<Feature> getFeatureByFullyQualifiedIdentifier(String relativeIdentifier);
    Optional<Feature> getServerFeatureByFullQualifiedIdentifier(String serverUuid, String relativeIdentifier);
}
