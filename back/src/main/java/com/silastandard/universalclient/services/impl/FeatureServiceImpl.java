package com.silastandard.universalclient.services.impl;

import com.silastandard.universalclient.common.models.db.Feature;
import com.silastandard.universalclient.common.models.db.Server;
import com.silastandard.universalclient.repositories.FeatureRepository;
import com.silastandard.universalclient.repositories.ServerRepository;
import com.silastandard.universalclient.services.FeatureService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
// todo: Add debug logs with entire entities log.
// todo: Look for Custom Exception raise in case of unwanted behaviour (eg. ServerNotFound).
public class FeatureServiceImpl implements FeatureService {

    private final FeatureRepository featureRepository;
    private final ServerRepository serverRepository;

    @Autowired
    public FeatureServiceImpl(
            @NonNull final  FeatureRepository featureRepository,
            @NonNull final  ServerRepository serverRepository
    ) {
        this.featureRepository = featureRepository;
        this.serverRepository = serverRepository;
    }

    @Override
    public Set<Feature> getAllServerFeatures(@NonNull final String serverUuid) {
        // todo fixme try to find a cleaner solution
        final Optional<Server> server = this.serverRepository.findByServerUuid(serverUuid);
        if (!server.isPresent()) {
            // todo throw error instead of returning an empty set ?
            log.warn("No Features for Server {} could be find : Cannot find Server.", serverUuid);
            return Collections.emptySet();
        }
        log.info("Features from Server {} to be fetched.", serverUuid);
        final Set<Feature> features = this.featureRepository.findAllByServer(server.get());
        log.info("{} Features found for Server {} in the Feature persistence Service.", features.size(), serverUuid);
        return features;
    }

    @Override
    public Optional<Feature> getFeature(@NonNull final Long id) {
        final Optional<Feature> feature = this.featureRepository.findById(id);
        if (feature.isPresent()) {
            log.info("Feature persistence service found the Feature {}.", feature.get());
        } else {
            log.warn("Feature with ID {} could not be found in the persistence context!", id);
        }
        return feature;
    }

    @Override
    public Set<Feature> getFeatureByFullyQualifiedIdentifier(@NonNull final String fullyQualifiedFeatureId) {
        return this.featureRepository.findAllByFullyQualifiedIdentifier(fullyQualifiedFeatureId);
    }

    @Override
    public Optional<Feature> getServerFeatureByFullQualifiedIdentifier(
            @NonNull final String serverUuid,
            @NonNull final String fullyQualifiedFeatureId
    ) {
        // todo fixme filter in sql
        final Optional<Server> server = this.serverRepository.findByServerUuid(serverUuid);
        if (server.isPresent()) {
            log.info("Feature {} from Server {} to be fetched.", fullyQualifiedFeatureId, serverUuid);
            final Optional<Feature> feature = server.get().getFeatures()
                    .stream()
                    .filter(serverFeature -> serverFeature.getFullyQualifiedIdentifier().equals(fullyQualifiedFeatureId))
                    .findFirst();
            if (feature.isPresent()) {
                return feature;
            }
        } else {
            // todo throw?
        }
        log.warn("No Feature {} for Server {} could be find : Cannot find Server or Feature.", fullyQualifiedFeatureId, serverUuid);
        return Optional.empty();
    }
}
