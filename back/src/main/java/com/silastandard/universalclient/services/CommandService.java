package com.silastandard.universalclient.services;

import com.silastandard.universalclient.common.models.db.Command;
import com.silastandard.universalclient.common.models.db.Parameter;
import lombok.NonNull;

import java.util.Optional;
import java.util.Set;

public interface CommandService {
    Optional<Command> getCommand(Long id);
    Set<Command> getAllCommandsByServerFeature(
            String serverUuid,
            String relativeIdentifier
    );
    Set<Parameter> getCommandParameters(
            String serverUuid,
            String fullyQualifiedFeatureIdentifier,
            String commandRelativeIdentifier
    );
    Optional<Command> findByRelativeIdentifier(
            String serverUuid,
            String fullyQualifiedFeatureIdentifier,
            String commandRelativeIdentifier
    );

    Optional<Command> findFeaturesCommandByFullyQualifiedIdentifier(
            String serverUuid,
            String fullyQualifiedFeatureIdentifier,
            String commandRelativeIdentifier
    );
}
