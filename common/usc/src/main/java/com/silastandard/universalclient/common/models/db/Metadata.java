package com.silastandard.universalclient.common.models.db;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.vladmihalcea.hibernate.type.json.JsonType;
import lombok.*;
import org.hibernate.annotations.*;
import sila_java.library.core.models.DataTypeType;
import sila_java.library.core.models.DefinedExecutionErrorList;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

// todo: look at @ToString
@Entity
@Table(name="METADATA")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@TypeDef(name = "json", typeClass = JsonType.class)
//@EqualsAndHashCode(onlyExplicitlyIncluded = true) // can't be implemented because ID is assigned by DB
public class Metadata {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ID", unique=true, nullable=false)
    @EqualsAndHashCode.Include
    private long id;

    @JsonBackReference
    @ManyToOne(fetch=FetchType.LAZY, cascade = {javax.persistence.CascadeType.PERSIST})
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="FEATURE_ID")
    private Feature feature;

    @Column(name="RELATIVE_IDENTIFIER", nullable=false)
    private String relativeIdentifier;

    @Column(name="DISPLAY_NAME", nullable=false)
    private String displayName;

    @Column(name="DESCRIPTION", nullable=false, columnDefinition="TEXT")
    private String description;

    @Column(name="DATA_TYPE", nullable=false, columnDefinition = "JSON")
    @Type(type = "json")
    private DataTypeType dataType;

    @Column(name="EXECUTION_ERRORS", columnDefinition = "JSON")
    @Type(type = "json")
    private DefinedExecutionErrorList definedExecutionErrors;

    public static Metadata fromSiLA(
            @NonNull final Feature feature,
            @NonNull final sila_java.library.core.models.Feature.Metadata silaMetadata
    ) {
        Metadata persistentMetadata = new Metadata();
        persistentMetadata.setDescription(silaMetadata.getDescription());
        persistentMetadata.setDisplayName(silaMetadata.getDisplayName());
        persistentMetadata.setRelativeIdentifier(silaMetadata.getIdentifier());
        persistentMetadata.setDataType(silaMetadata.getDataType());
        persistentMetadata.setDefinedExecutionErrors(silaMetadata.getDefinedExecutionErrors());
        persistentMetadata.setFeature(feature);
        return persistentMetadata;
    }

    public String getFullyQualifiedIdentifier() {
        return this.feature.getFullyQualifiedIdentifier() + "/Metadata/" + this.relativeIdentifier;
    }
}