package com.silastandard.universalclient.common.models.plugins;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PluginModel {
    private String name;
    private String version;
    private String description;
    private String author;
    private String website;
    private String jsEntryPoint;
    private String backApiVersion;
    private String frontApiVersion;
    private Set<String> hardDependencies;
    private Set<String> softDependencies;
    private Set<String> loadBefore;
}
