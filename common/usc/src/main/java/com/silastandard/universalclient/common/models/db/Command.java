package com.silastandard.universalclient.common.models.db;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vladmihalcea.hibernate.type.json.JsonType;
import lombok.*;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import sila_java.library.core.models.DefinedExecutionErrorList;

import java.util.*;
import java.util.stream.Collectors;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

// todo: look at @ToString
@Entity
@Table(name = "COMMAND")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@TypeDef(name = "json", typeClass = JsonType.class)
//@EqualsAndHashCode(onlyExplicitlyIncluded = true) // can't be implemented because ID is assigned by DB
public class Command {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ID", unique=true, nullable=false)
    @EqualsAndHashCode.Include
    private long id;

    @ManyToOne(fetch=FetchType.LAZY)
    @Fetch(FetchMode.JOIN)
    @JsonBackReference
    @JoinColumn(name = "FEATURE_ID")
    private Feature feature;

    @Column(name="RELATIVE_IDENTIFIER", nullable=false)
    private String relativeIdentifier;

    @Column(name="DISPLAY_NAME", nullable=false)
    private String displayName;

    @Column(name="DESCRIPTION", nullable=false, columnDefinition="TEXT")
    private String description;

    @Column(name="OBSERVABLE", nullable=false)
    private Boolean observable;

    @Column(name="EXECUTION_ERRORS", columnDefinition = "JSON")
    @Type(type = "json")
    private DefinedExecutionErrorList executionErrors;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="command", orphanRemoval = true)
    @Fetch(FetchMode.JOIN)
    @JsonManagedReference
    private Set<Call> calls = new HashSet<>(0);

    @OneToMany(fetch=FetchType.LAZY, mappedBy="command", orphanRemoval = true, cascade = {javax.persistence.CascadeType.ALL})
    @Cascade(CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    @JsonManagedReference
    private Set<Parameter> parameters = new HashSet<>(0);

    @OneToMany(fetch=FetchType.LAZY, mappedBy="command", orphanRemoval = true, cascade = {javax.persistence.CascadeType.ALL})
    @Cascade(CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    @JsonManagedReference
    private Set<Response> responses = new HashSet<>(0);

    @OneToMany(fetch=FetchType.LAZY, mappedBy="command", orphanRemoval = true, cascade = {javax.persistence.CascadeType.ALL})
    @Cascade(CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    @JsonManagedReference
    private Set<IntermediateResponse> intermediateResponses = new HashSet<>(0);

    public static Command fromSiLA(
            @NonNull final Feature feature,
            @NonNull final sila_java.library.core.models.Feature.Command silaCommand
    ) {
        final Command persistentCommand = new Command();
        persistentCommand.setObservable(
                silaCommand.getObservable() != null &&
                silaCommand.getObservable().toLowerCase(Locale.ROOT).equals("yes")
        );
        persistentCommand.setDescription(silaCommand.getDescription());
        persistentCommand.setDisplayName(silaCommand.getDisplayName());
        persistentCommand.setRelativeIdentifier(silaCommand.getIdentifier());
        persistentCommand.setExecutionErrors(silaCommand.getDefinedExecutionErrors());
        persistentCommand.setFeature(feature);
        persistentCommand.setParameters(
                silaCommand.getParameter()
                        .stream()
                        .map((parameter) -> Parameter.fromSiLA(persistentCommand, parameter))
                        .collect(Collectors.toSet())
        );
        persistentCommand.setResponses(
                silaCommand.getResponse()
                        .stream()
                        .map((response) -> Response.fromSiLA(persistentCommand, response))
                        .collect(Collectors.toSet())
        );
        persistentCommand.setIntermediateResponses(
                silaCommand.getIntermediateResponse()
                        .stream()
                        .map((intermediateResponse) -> IntermediateResponse.fromSiLA(persistentCommand, intermediateResponse))
                        .collect(Collectors.toSet())
        );
        return persistentCommand;
    }

    public String getFullyQualifiedIdentifier() {
        return this.feature.getFullyQualifiedIdentifier() + "/Command/" + this.relativeIdentifier;
    }
	
    public Command(long id, Feature feature, String relativeIdentifier, String displayName, String description, boolean observable, DefinedExecutionErrorList executionErrors) {
        this.id = id;
        this.feature = feature;
        this.relativeIdentifier = relativeIdentifier;
        this.displayName = displayName;
        this.description = description;
        this.observable = observable;
        this.executionErrors = executionErrors;
    }

    public Command(long id, Feature feature, String relativeIdentifier, String displayName, String description, boolean observable) {
        this.id = id;
        this.feature = feature;
        this.relativeIdentifier = relativeIdentifier;
        this.displayName = displayName;
        this.description = description;
        this.observable = observable;
    }
}