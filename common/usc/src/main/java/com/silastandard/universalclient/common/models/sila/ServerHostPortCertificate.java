package com.silastandard.universalclient.common.models.sila;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServerHostPortCertificate {
    @NonNull
    private String host;

    @NonNull
    private Integer port;

    @Nullable
    private String certificate;

    public ServerHostPortCertificate(@NonNull final String host, @NonNull final Integer port) {
        this.host = host;
        this.port = port;
        this.certificate = null;
    }
}