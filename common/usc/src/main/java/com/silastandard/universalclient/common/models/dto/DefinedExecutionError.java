package com.silastandard.universalclient.common.models.dto;

import lombok.*;


@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class DefinedExecutionError {
    private String relativeIdentifier;
    private String displayName;
    private String description;

    public static DefinedExecutionError fromSiLA(
            @NonNull final sila_java.library.core.models.Feature.DefinedExecutionError siLADefinedExecutionError
    ) {
        final DefinedExecutionError persistentDataTypeDefinition = new DefinedExecutionError();
        persistentDataTypeDefinition.setDescription(siLADefinedExecutionError.getDescription());
        persistentDataTypeDefinition.setRelativeIdentifier(siLADefinedExecutionError.getIdentifier());
        persistentDataTypeDefinition.setDisplayName(siLADefinedExecutionError.getDisplayName());
        return persistentDataTypeDefinition;
    }
}