package com.silastandard.universalclient.common.models.db;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.vladmihalcea.hibernate.type.json.JsonType;
import lombok.*;
import org.hibernate.annotations.*;
import sila_java.library.core.models.DataTypeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

// todo: look at @ToString
@Entity
@Table(name="INTERMEDIATE_RESPONSE")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@TypeDef(name = "json", typeClass = JsonType.class)
//@EqualsAndHashCode(onlyExplicitlyIncluded = true) // can't be implemented because ID is assigned by DB
public class IntermediateResponse {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="ID", unique=true, nullable=false)
    @EqualsAndHashCode.Include
    private long id;

    @JsonBackReference
    @ManyToOne(fetch=FetchType.LAZY, cascade = {javax.persistence.CascadeType.PERSIST})
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="COMMAND_ID")
    private Command command;

    @Column(name="RELATIVE_IDENTIFIER", nullable=false)
    private String relativeIdentifier;

    @Column(name="DISPLAY_NAME", nullable=false)
    private String displayName;

    @Column(name="DESCRIPTION", nullable=false, columnDefinition="TEXT")
    private String description;

    @Column(name="DATA_TYPE", nullable=false, columnDefinition = "JSON")
    @Type(type = "json")
    private DataTypeType dataType;

    public static IntermediateResponse fromSiLA(
            @NonNull final Command command,
            @NonNull final sila_java.library.core.models.SiLAElement siLAElement
    ) {
        final IntermediateResponse persistentResponse = new IntermediateResponse();
        persistentResponse.setDataType(siLAElement.getDataType());
        persistentResponse.setDisplayName(siLAElement.getDisplayName());
        persistentResponse.setDescription(siLAElement.getDescription());
        persistentResponse.setRelativeIdentifier(siLAElement.getIdentifier());
        persistentResponse.setCommand(command);
        return persistentResponse;
    }

    public String getFullyQualifiedIdentifier() {
        if (Objects.nonNull(this.command)) {
            return this.command.getFullyQualifiedIdentifier() + "/IntermediateResponse/" + this.relativeIdentifier;
        } else {
            // todo log, CustomError ?
            throw new RuntimeException("Intermediate Response is not linked to a Command or a Property: " +
                    "cannot build FullyQualifiedIdentifier");
        }
    }
}