package com.silastandard.universalclient.common.models.dto;

import lombok.*;
import sila_java.library.core.models.DataTypeType;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class SiLAElement {
    private String relativeIdentifier;
    private String displayName;
    private String description;
    private DataTypeType dataType;

    public static SiLAElement fromSiLA(
            @NonNull final sila_java.library.core.models.SiLAElement siLAElement
    ) {
        final SiLAElement persistentSiLAElement = new SiLAElement();
        persistentSiLAElement.setDataType(siLAElement.getDataType());
        persistentSiLAElement.setDescription(siLAElement.getDescription());
        persistentSiLAElement.setRelativeIdentifier(siLAElement.getIdentifier());
        persistentSiLAElement.setDisplayName(siLAElement.getDisplayName());
        return persistentSiLAElement;
    }
}
