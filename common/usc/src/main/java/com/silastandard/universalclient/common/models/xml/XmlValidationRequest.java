package com.silastandard.universalclient.common.models.xml;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class XmlValidationRequest {
    private String xml;
    private String schema;
}
