package com.silastandard.universalclient.common.models.sila;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sila_java.library.manager.models.SiLACall;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ObservablePropertyResponse {
    SiLACall baseCall;
    String value;
}
