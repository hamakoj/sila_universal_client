package com.silastandard.universalclient.common.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Version {
    private String branch;

    private String buildHost;

    private String buildVersion;

    private String closestTag;

    private String commitId;

    private String commitMessage;

    private String dirty;
}
