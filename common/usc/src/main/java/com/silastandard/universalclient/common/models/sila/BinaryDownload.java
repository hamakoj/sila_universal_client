package com.silastandard.universalclient.common.models.sila;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BinaryDownload {
    private String serverId;
    private String binaryTransferUuid;
}
