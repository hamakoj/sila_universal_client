package com.silastandard.universalclient.common.models.db;

public enum ServerStatus {
    ONLINE, OFFLINE, UNKNOWN
}
