package com.silastandard.universalclient.common.models.db;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

// todo: look at @ToString
@Entity
@Table(name = "CLIENT")
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Client {

    @Id
    @Column(name = "CLIENT_UUID", nullable = false, unique = true)
    @EqualsAndHashCode.Include
    private String clientUuid;

    @Column(name = "NAME", nullable = false)
    private String name;

    @ToString.Exclude
    @JsonBackReference
    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY, orphanRemoval = true)
    @Fetch(FetchMode.JOIN)
    private Set<Call> calls = new HashSet<>(0);

    @ToString.Exclude
    @JsonBackReference
    @ManyToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.JOIN)
    private Set<Server> servers = new HashSet<>(0);
}