package com.silastandard.universalclient.common.models.sila;

public class ServerAdditionException extends RuntimeException {
    public ServerAdditionException(ServerHostPortCertificate serverHostPortCertificate, String reason) {
        super(
                "Couldn't add server at " + serverHostPortCertificate.getHost() + ":" + serverHostPortCertificate.getPort() +
                " because " + reason
        );
    }

    public ServerAdditionException(ServerHostPortCertificate serverHostPortCertificate, Exception e) {
        super(
                "Couldn't add server at " + serverHostPortCertificate.getHost() + ":" + serverHostPortCertificate.getPort() +
                        " because " + e.getMessage()
        , e);
    }
}
