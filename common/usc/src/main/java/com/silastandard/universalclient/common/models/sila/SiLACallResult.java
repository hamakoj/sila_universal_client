package com.silastandard.universalclient.common.models.sila;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SiLACallResult {
    private SiLACallModel request;
    private Boolean success;
    private String value; // value of "commandExecutionUUID" when observable Property/Command
}
