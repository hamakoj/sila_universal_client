package com.silastandard.universalclient.common.models.sila;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.manager.models.SiLACall;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ObservableExecutionInfo {
    SiLACall baseCall;
    SiLAFramework.ExecutionInfo command;
}
