package com.silastandard.universalclient.common.models.db;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.vladmihalcea.hibernate.type.json.JsonType;
import lombok.*;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

import sila_java.library.core.models.DataTypeType;

// todo: look at @ToString
@Entity
@Table(name="PARAMETER")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@TypeDef(name = "json", typeClass = JsonType.class)
//@EqualsAndHashCode(onlyExplicitlyIncluded = true) // can't be implemented because ID is assigned by DB
public class Parameter {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ID", unique=true, nullable=false)
    @EqualsAndHashCode.Include
    private long id;

    @JsonBackReference
    @ManyToOne(fetch=FetchType.LAZY, cascade = {javax.persistence.CascadeType.PERSIST})
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="COMMAND_ID")
    private Command command;

    @Column(name="RELATIVE_IDENTIFIER", nullable=false)
    private String relativeIdentifier;

    @Column(name="DISPLAY_NAME", nullable=false)
    private String displayName;

    @Column(name="DESCRIPTION", nullable=false, columnDefinition="TEXT")
    private String description;

    @Column(name="DATA_TYPE", nullable=false, columnDefinition = "JSON")
    @Type(type = "json")
    private DataTypeType dataType;

    public static Parameter fromSiLA(
            @NonNull final Command command,
            @NonNull final sila_java.library.core.models.SiLAElement siLAElement
    ) {
        final Parameter persistentParameter = new Parameter();
        persistentParameter.setDataType(siLAElement.getDataType());
        persistentParameter.setDisplayName(siLAElement.getDisplayName());
        persistentParameter.setDescription(siLAElement.getDescription());
        persistentParameter.setRelativeIdentifier(siLAElement.getIdentifier());
        persistentParameter.setCommand(command);
        return persistentParameter;
    }

    public String getFullyQualifiedIdentifier() {
        return this.command.getFullyQualifiedIdentifier() + "/Parameter/" + this.relativeIdentifier;
    }
}