package com.silastandard.universalclient.common.models.db;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vladmihalcea.hibernate.type.json.JsonType;
import lombok.*;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

// todo: look at @ToString
@Entity
@Table(name = "CALL")
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@TypeDef(name = "json", typeClass = JsonType.class)
@ToString()
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Call {

    @Id
    @Column(name = "CALL_UUID", unique = true, updatable = false, nullable = false)
    @Type(type="uuid-char")
    @EqualsAndHashCode.Include
    private UUID uuid;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY, cascade = {javax.persistence.CascadeType.PERSIST})
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "CLIENT_UUID")
    private Client client;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, cascade = {javax.persistence.CascadeType.PERSIST})
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="COMMAND_ID")
    private Command command;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, cascade = {javax.persistence.CascadeType.PERSIST})
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="PROPERTY_ID")
    private Property property;

    @Column(name="COMMAND_EXECUTION_UUID")
    private String commandExecutionUUID;

    // Useless as this is already contained in Command or Property (relativeIdentifier)
    @Column(name = "RELATIVE_IDENTIFIER", nullable = false)
    private String relativeIdentifier;

    // Parameters is populate before the call ({parameter*.relativeIdentifier})
    @Column(name = "PARAMETERS", columnDefinition = "JSON")
    @Type(type = "json")
    private String parameters;

    @Column(name = "METADATAS", columnDefinition = "JSON")
    @Type(type = "json")
    private String metadatas;

    @Column(name = "START_DATE", columnDefinition = "TIMESTAMP", nullable = false)
    private LocalDateTime startDate;

    @Column(name = "END_DATE", columnDefinition = "TIMESTAMP")
    private LocalDateTime endDate;

    // If error in the result, add ExecutionError to the Command? to the Call result
    @Column(name = "RESULT", columnDefinition = "JSON")
    @Type(type = "json")
    private String result;

    @Column(name = "TIMEOUT")
    private Long timeout;

    // todo: Index ?
    // todo: Enum ?
    @Column(name = "TYPE", nullable = false, length = 32)
    private String type;

    @Column(name = "ERROR", columnDefinition = "BOOLEAN")
    private Boolean error;

    // todo: get Command or Property flag using Call.type
    // todo: redo constructor with relativeIdentifier
    public Call(UUID uuid, Client client, Command command, String parameters, LocalDateTime startDate, LocalDateTime endDate, String result, String type) {
        this.uuid = uuid;
        this.client = client;
        this.command = command;
        this.parameters = parameters;
        this.startDate = startDate;
        this.endDate = endDate;
        this.result = result;
        this.type = type;
    }

    public Call(UUID uuid, Client client, Command command, LocalDateTime startDate, LocalDateTime endDate, String result, String type) {
        this.uuid = uuid;
        this.client = client;
        this.command = command;
        this.startDate = startDate;
        this.endDate = endDate;
        this.result = result;
        this.type = type;
    }

    public Call(UUID uuid, Client client, Command command, String parameters, LocalDateTime startDate, String type) {
        this.uuid = uuid;
        this.client = client;
        this.command = command;
        this.parameters = parameters;
        this.startDate = startDate;
        this.type = type;
    }

    public Call(UUID uuid, Client client, Command command, LocalDateTime startDate, String type) {
        this.uuid = uuid;
        this.client = client;
        this.command = command;
        this.startDate = startDate;
        this.type = type;
    }

    public String getFullyQualifiedIdentifier() {
        if (Objects.nonNull(this.command)) {
            return this.command.getFullyQualifiedIdentifier();
        } else if (Objects.nonNull(this.property)) {
            return this.property.getFullyQualifiedIdentifier();
        } else {
            // todo log, CustomError ?
            throw new RuntimeException("Call is not linked to a Command or a Property: " +
                    "cannot build FullyQualifiedIdentifier");
        }
    }

    // todo: look for other constructor for other use cases
}