package com.silastandard.universalclient.common.models.sila;

import com.google.protobuf.DynamicMessage;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.manager.models.SiLACall;

@Getter
@Setter
@RequiredArgsConstructor
public class CachedCall {
    private final SiLACall siLACall;
    private String lastPropertyValue = null;
    private SiLAFramework.CommandConfirmation lastCommandConfirmation = null;
    private SiLAFramework.ExecutionInfo lastCommandExecutionInfo = null;
    private DynamicMessage lastIntermediateResponse = null;
}