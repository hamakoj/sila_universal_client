package com.silastandard.universalclient.common.models.sila;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import sila_java.library.manager.models.SiLACall;

import java.util.Optional;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SiLACallModel {
    // todo add support for timeout when running synchronously in /Calls/execute
    private UUID serverId;
    private String fullyQualifiedFeatureId;
    private String callId;
    private SiLACall.Type type;
    private Optional<String> parameters = Optional.empty();
    private Optional<String> metadatas = Optional.empty();
    private boolean runAsync = true;

    public SiLACallModel(UUID serverId, String featureId, String callId, SiLACall.Type type, Optional<String> parameters, Optional<String> metadatas) {
        this(serverId, featureId, callId, type, parameters, metadatas, true);
    }

    // todo: remove or use SiLACall.Builder
    public SiLACall toSiLACall() {
        return new SiLACall.Builder(
                this.serverId,
                this.fullyQualifiedFeatureId,
                this.callId,
                this.type
        )
                .withParameters(parameters.orElse("{}"))
                .withMetadata(metadatas.orElse("{}"))
                .build();
    }

    public static SiLACallModel fromSiLACall(@NonNull final SiLACall siLACall) {
        return new SiLACallModel(
                siLACall.getServerId(),
                siLACall.getFullyQualifiedFeatureId(),
                siLACall.getCallId(),
                siLACall.getType(),
                Optional.ofNullable(siLACall.getParameters()),
                Optional.ofNullable(siLACall.getMetadatas()),
                true
        );
    }

}
